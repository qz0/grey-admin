import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { GeneralComponent } from './components/general/general.component';
// import { usersRoutes } from './users/users-routing.module';
import { categoriesRoutes } from './categories/categories-routing.module';
import { itemsRoutes } from './items/items-routing.module';
// import { CurrenciesComponent } from './components/currencies/currencies.component';
import { brandsRoutes } from './brands/brands-routing.module';
import { LoginComponent } from './components/login/login.component';
import { optionsRoutes } from './options/options-routing.module';
import { imagesRoutes } from './images/images-routing.module';
import { assembliesRoutes } from './assemblies/assemblies-routing.module';
import {incomingRoutes} from './incomings/incomings-routing.module';
import {deliveriesRoutes} from './deliveries/deliveries-routing.module';
import {paymentsRoutes} from './payments/payments-routing.module';
import {purchasesRoutes} from './purchases/purchases-routing.module';
import {ordersRoutes} from './orders/orders-routing.module';
import {productsRoutes} from './products/products-routing.module';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {checkSendRoutes} from './check-send/check-send-routing.modules';
import {settingsRoutes} from './settings/settings-routing.module';
import { logRoutes } from './logs/logs-routing.module';

//  маршруты
const routes: Routes = [
  //  домашняя
  { path: '', component: GeneralComponent },
  //  общий гвард для всех
  //  login screen must be default if not logged in
  { path: 'login', component: LoginComponent },
  //  assembly
  {
    path: 'assemblies',
    children: assembliesRoutes,
  },
  //  бренды
  {
    path: 'brands',
    children: brandsRoutes,
  },
  //  категории
  {
    path: 'categories',
    children: categoriesRoutes,
  },
  //  работа с валютой
  // {
  //   path: 'currencies',
  //   component: CurrenciesComponent,
  //   // resolve: { users: MemberListResolver }
  // },
  //  работа с check/send
  {
    path: 'check_send',
    children: checkSendRoutes
  },
  //  работа с валютой
  {
    path: 'deliveries',
    children: deliveriesRoutes,
    // resolve: { users: MemberListResolver }
  },
  //  картинки
  {
    path: 'images',
    children: imagesRoutes,
  },
  //  incoming
  {
    path: 'incomings',
    children: incomingRoutes,
  },
  //  итемы
  {
    path: 'items',
    children: itemsRoutes,
  },
// логи
  {
    path: 'logs',
    children: logRoutes,
  },
  //  работа с опциями
  {
    path: 'orders',
    children: ordersRoutes,
    // resolve: { users: MemberListResolver }
  },
  //  работа с опциями
  {
    path: 'options',
    children: optionsRoutes,
    // resolve: { users: MemberListResolver }
  },
  //  payments
  {
    path: 'payments',
    children: paymentsRoutes,
  },
  //  продукты
  {
    path: 'products',
    children: productsRoutes,
  },
  //  purchases
  {
    path: 'purchases',
    children: purchasesRoutes,
  },
  //  settings
  {
    path: 'settings',
    children: settingsRoutes,
  },
  //  пользователи
  // {
  //   path: 'users',
  //   children: usersRoutes,
  //   // loadChildren: UsersModule
  // },
  //  not found
  { path: 'not_found', component: NotFoundComponent },
  //  путь по умолчанию
  { path: '**', redirectTo: '/note_found', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
