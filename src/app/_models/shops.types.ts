//  магазины
//  сам магазин клиент
export interface Shop {
  id?: number; // id
  name: string; //  название магазина
}
//  сам магазин сервер
export interface ShopServer {
  ID?: number; // id
  name: string; //  название магазина
}

//  shadow магазина
//  сам магазин клиент
export interface ShopShadow {
  id?: number; // id
  name: string; //  название магазина
}
//  сам магазин сервер
export interface ShopShadowServer {
  ID?: number; // id
  name: string; //  название магазина
}
