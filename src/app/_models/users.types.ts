//  типы данных для работы с пользователями
//  клиентский тип для пользователя
import {Image, ImageServer} from './image.types';

export interface User {
  id?: number; //  ID
  username: string;  //  имя
  login: string; //  логин
  password: string;  //  пароль
  email: string;  //  почта
  description: string; //  описание
  image: Image; //  картинка пользователя
  roles: Role[]; // список ролей пользователя
}

//  серверный тип для пользователя
export interface UserServer {
  ID?: number; //  ID
  username: string;  //  имя
  login: string; //  логин
  password: string;  //  пароль
  email: string;  //  почта
  description: string; //  описание
  image: ImageServer; //  картинка пользователя
  roles: RoleServer[]; // список ролей пользователя
}

//  клиентский тип для роли
export interface Role {
  id?: number; //  ID
  name: string; //  название
  title: string; //  заголовок
  description: string; //  описание
}
//  серверный тип для роли
export interface RoleServer {
  ID?: number; //  ID
  name: string; //  название
  title: string; //  заголовок
  description: string; //  описание
}
