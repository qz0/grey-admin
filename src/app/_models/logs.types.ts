//  LOG
//  client


export interface Log {
    id?: number; // id
    сreatedAt?: Date;
    type: string;  //  тип изменяемой сущности
    action: string;  //  действие
    objectId: number;  // обьект изменяемой сущности
    funcPath: string;  // обьект изменяемой сущности
    json?: string;  //  что было до изменения
    jsonRequest?: string; //  что стало после изменения
}

//  server
export interface LogServer {
    ID?: number; // id
    CreatedAt?: Date;
    type: string;  //  тип изменяемой сущности
    action: string;  //  действие
    objectId: number;  // обьект изменяемой сущности
    funcPath: string;  // обьект изменяемой сущности
    json?: string;  //  что было до изменения
    jsonRequest?: string; //  что стало после изменения
}
