//  IMCOMING
//  client
import {Item, ItemServer} from './items.types';
import {Image, ImageServer} from './image.types';
import {OptionValue, OptionValueServer} from './options.types';
import {Product, ProductServer} from './products.types';

export interface Incoming {
  id?: number; // id
  createdAt?: Date;
  invoice: string;  //  номер инвойса
  comment: string;  //  comment
  scan: Image;  //  скан документа
  scanID: number;  // ID скан документа
  incomingItems: IncomingItem[];  //  позиции накладной
  checked: boolean; //  статус првоерки списка
}

//  server
export interface IncomingServer {
  ID?: number; // id
  CreatedAt?: Date;
  invoice: string;  //  номер инвойса
  comment: string;  //  comment
  scan: ImageServer;  //  скан документа
  scanID: number;  // ID скан документа
  incomingItems: IncomingItemServer[];  //  позиции накладной
  checked: boolean; //  статус првоерки списка
}

export interface IncomingItem {
  id?: number; // id
  incomingID: number;  // ид накладной
  product: Product;  //  продукт
  productID: number;  // ид продукта
  optionValues: OptionValue[];  //  опции товара
  amount: number; //  количество товара
  checked?: boolean; // техническое поле для удобства проверки списка на сервер не идет
}

export interface IncomingItemServer {
  ID?: number; // id
  incomingID: number;  // ид накладной
  product: ProductServer;  //  продукт
  productID: number;  // ид продукта
  optionValues: OptionValueServer[];  //  опции товара
  amount: number; //  количество товара
}
