//  типы данных для работы с доставкой
import {LocalizedDescription} from './localization';
import {Country, CountryServer} from './address.types';
import {Image, ImageServer} from './image.types';

//  клиентский тип доставки
export interface Delivery {
  id?: number; //  ID
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  image: Image; // url на лого
  weightType: boolean; //  Regular or Volume
  color: string; //  color
  zones: Zone[];  //  гео зоны
  enabled: boolean; //  видимость
  priority: number; //  приоритеты
}
//  серверный тип доставки
export interface DeliveryServer {
  ID?: number; //  ID
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  image: ImageServer; // url на лого
  weightType: boolean; //  Regular or Volume
  color: string; //  color
  zones: ZoneServer[];  //  гео зоны
  enabled: boolean; //  видимость
  priority: number; //  приоритеты
}

//  гео-зоны
//  клиентские гео-зоны
export interface Zone {
  id?: number; //  ID
  countries: Country[]; //  страны гео-зоны, (проверка уникальности)
  zoneCosts: ZoneCost[]; //  отношения цен к весам
}
//  серверные гео-зоны
export interface ZoneServer {
  ID?: number; //  ID
  countries: CountryServer[]; //  страны гео-зоны, (проверка уникальности)
  zoneCosts: ZoneCostServer[]; //  отношения цен к весам
}

//  отношения цен к весам
//  клиентские отношения цен к весам
export interface ZoneCost {
  id?: number; //  ID
  weight: number; //  вес
  cost: number; //  цена
}
//  серверные отношения цен к весам
export interface ZoneCostServer {
  ID?: number; //  ID
  weight: number; //  вес
  cost: number; //  цена
}
