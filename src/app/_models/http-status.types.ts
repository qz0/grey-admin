export interface HttpStatusTypes {
  id: number; //  ID
  code: string; //  идентификационный код
  title: string;  //  описание
  multiplier: number; //  множитель
}
