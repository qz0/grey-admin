//  assembly
//  клиентский тип сборки
import {Order, OrderServer} from './orders.types';

export interface Assembly {
  id?: number; //  ID
  order: Order; // заказ
  deliveryColor: string; //  цвет доставки
  weight: number; //  вес
}

//  серверный тип сборки
export interface AssemblyServer {
  ID?: number; //  ID
  order: OrderServer; // заказ
  deliveryColor: string; //  цвет доставки
  weight: number; //  вес
}
