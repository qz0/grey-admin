//  типы данных для работы с продуктами
//  клиентский тип для продуктов
import {LocalizedDescription, LocalizedDescriptionServer, LocalizedText, LocalizedTextServer} from './localization';
import {Brand, BrandServer, Manufacturer, ManufacturerServer} from './brands.types';
import {Category, CategoryServer} from './categories.types';
import {Option, OptionServer, OptionsSet, OptionsSetServer, OptionValue, OptionValueServer} from './options.types';
import {Delivery, DeliveryServer} from './deliveries.types';
import {Sale, SaleServer} from './sales.types';
import {Image, ImageServer} from './image.types';
import {Price, PriceServer} from './prices.types';
import { Item, ItemServer } from './items.types';

//  products
//  клиентский тип для продуктов
export interface Product {
  id?: number;  //  product ID
  imageFront: Image;  //  картинка
  imageFrontID: number;  //  картинка
  imageBack: Image;  //  картинка
  imageBackID: number;  //  картинка
  productImages: ProductImage[];  //  картинки продукта
  items: Item[];  //  items
  localizedDescriptions: LocalizedDescription[]; //  описание
  title?: string; //  заголовок для выпадающих списков
  localizedFeatures: LocalizedText[]; //  локализованые детали
  localizedMaterial: LocalizedText[]; //  локализованые детали
  localizedSEOs: LocalizedDescription[]; //  сео расширенный
  seoLink: string;  //  сео линк
  brand: Brand; //  торговая марка
  brandID: number; //  ID торговая марка
  manufacturer: Manufacturer; //  производитель
  manufacturerID: number; //  ID производитель
  price: Price; //  цены
  categories: Category[]; //  категории
//  parameters
  restrictedDeliveries: Delivery[]; //  запрещенные доставки
  weight: number; // вес
  volumeWeightX: number; //  объемный вес
  volumeWeightY: number; //  объемный вес
  volumeWeightZ: number; //  объемный вес
  warehousePosition: string;  //  позиция на складе
  model?: string;  //  номер модели для таможни
  vendorCode?: string;  //  код производителя
  localizedProductTypes: LocalizedDescription[]; //  тип товара для таможни и не только
  article: string; //  артикул производителя
  fstek: string; // FSTEK
  priority: number; // приоритет при отображении
  productAvailability: ProductAvailability; //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
  productAvailabilityID: number; // ID - доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
  //  tslint:disable-next-line
  productVisibility: ProductVisibility; //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
  productVisibilityID: number; // ID - отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
  productOptions: ProductOption[]; //  список доступных опций для данного продукта
  optionsSets: OptionsSet[]; //  список массивов опций
  sale: Sale; //  скидка
  type: string; // тип
}

//  серверный тип для продуктов
export interface ProductServer {
  ID?: number;  //  product ID
  imageFront: ImageServer;  //  картинка
  imageFrontID: number;  //  картинка
  imageBack: ImageServer;  //  картинка
  imageBackID: number;  //  картинка
  productImages: ProductImageServer[];  //  картинки продукта
  items: ItemServer[];  //  items
  localizedDescriptions: LocalizedDescriptionServer[]; //  описание
  localizedMaterial: LocalizedTextServer[]; //  локализованые детали
  localizedFeatures: LocalizedTextServer[]; //  локализованые детали
  localizedSEOs: LocalizedDescriptionServer[]; //  сео расширенный
  seoLink: string;  //  сео линк
  brand: BrandServer; //  торговая марка
  brandID: number; //  ID торговая марка
  manufacturer: ManufacturerServer; //  производитель
  manufacturerID: number; //  ID торговая марка
  price: PriceServer; //  цены
  categories: CategoryServer[]; //  категории
//  parameters
  restrictedDeliveries: DeliveryServer[]; //  запрещенные доставки
  weight: number; // вес
  volumeWeightX: number; //  объемный вес
  volumeWeightY: number; //  объемный вес
  volumeWeightZ: number; //  объемный вес
  fstek: string; // FSTEK
  warehousePosition: string;  //  позиция на складе
  localizedProductTypes: LocalizedDescription[]; //  тип товара для таможни и не только
  article: string; //  артикул производителя
  priority: number; // приоритет при отображении
  productAvailability: ProductAvailabilityServer; //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
  productAvailabilityID: number; // ID -  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
  //  tslint:disable-next-line
  productVisibility: ProductVisibilityServer; //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
  productVisibilityID: number; // ID - отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
  productOptions: ProductOptionServer[]; //  список доступных опций для данного продукта
  optionsSets: OptionsSetServer[]; //  список массивов опций
  sale: SaleServer; //  скидка
  type: string; //  тип для брокера
}

//  ProductShadow
//  клиентский тип для продуктов
export interface ProductShadow {
  id?: number;  //  product ID
  image: Image;  //  картинка
  seoLink: string;  //  сео линк
  brand: Brand; //  торговая марка
  manufacturer: Manufacturer; //  производитель
  price: Price; //  цены
  categories: string[]; //  копии названий категорий
//  parameters
  weight: number; // вес
  volumeWeight: number; //  объемный вес
  article: string; //  артикул производителя
  options: Option[]; // список опций
  optionsSets: OptionsSet[]; //  список массивов опций
  sale: Sale; //  скидка
}
//  серверный тип для продуктов
export interface ProductShadowServer {
  ID?: number;  //  product ID
  image: Image;  //  картинка
  seoLink: string;  //  сео линк
  brand: BrandServer; //  торговая марка
  manufacturer: ManufacturerServer; //  производитель
  price: PriceServer; //  цены
  categories: string[]; //  копии названий категорий
//  parameters
  weight: number; // вес
  volumeWeight: number; //  объемный вес
  article: string; //  артикул производителя
  options: OptionServer[]; // список опций
  optionsSets: OptionsSetServer[]; //  список массивов опций
  sale: SaleServer; //  скидка
}
//  Product Visibility Status
//  клиентский тип ProductVisibilityStatus
export interface ProductVisibility {
  id?: number;  //  id
  name: string; //  name
}

//  серверный тип ProductVisibilityStatus
export interface ProductVisibilityServer {
  ID?: number;  //  id
  name: string; //  name
}
//  Product Availability Status
//  клиентский тип ProductAvailability
export interface ProductAvailability {
  id?: number;  //  id
  name: string; //  name
}
//  серверный тип ProductAvailability
export interface ProductAvailabilityServer {
  ID?: number;  //  id
  name: string; //  name
}

//  Product image
//  client
export interface ProductImage {
  id?: number;  //  id
  product?: Product; //  продукт опционнго блока
  productID: number;  //  ID продукта
  image: Image; // image
  imageID: number; //  image id
  optionValues: OptionValue[];   //  optionValues
}

//  server
export interface ProductImageServer {
  ID?: number;  //  id
  product: ProductServer; //  продукт опционнго блока
  productID: number;  //  ID продукта
  image: ImageServer; // image
  imageID: number; //  image id
  optionValues: OptionValueServer[];   //  optionValues
}

//  Product option
//  client
export interface ProductOption {
  id?: number;  //  id
  product: Product; //  продукт опционнго блока
  productID: number;  //  ID продукта
  option: Option; // option
  optionID: number; //  option id
  optionValues: OptionValue[];   //  optionValues
}

//  server
export interface ProductOptionServer {
  ID?: number;  //  id
  product: ProductServer; //  продукт опционнго блока
  productID: number;  //  ID продукта
  option: OptionServer; // option
  optionID: number; //  option id
  optionValues: OptionValueServer[];   //  optionValues
}
