//  типы данных для работы с меню
//  клиентский тип
export interface Menu {
  id: number; //  id
  name: string; //  название
  title: string;  //  тултип
  route: string;  //  маршрут
  icon?: string; //  иконка
  children: Menu[]; //  потомки, если есть
}
//  серверный тип
export interface MenuServer {
  ID: number; //  id
  name: string; //  название
  title: string;  //  тултип
  route: string;  //  маршрут
  icon?: string; //  иконка
  children: Menu[]; //  потомки, если есть
}
