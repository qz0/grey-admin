import {LocalizedDescription} from './localization';
import {Image, ImageServer} from './image.types';
//  PAYMENT
//  payment client
export interface Payment {
  id?: number; // ID
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  image: Image; //  картинка
  discounts: PaymentDiscount[];
  enabled: boolean; //  видимость
  priority: number; //  приоритеты
}
//  payment server
export interface PaymentServer {
  ID?: number; // ID
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  image: ImageServer; //  картинка
  discounts: PaymentDiscount[];
  enabled: boolean; //  видимость
  priority: number; //  приоритеты
}

//  PAYMENT DISCOUNT
//  payment discount client
export interface PaymentDiscount {
  id?: number; // ID
  shop: string; //  название магазина
  discount: number; //  размер скидки
  enabled: boolean; //  активно?
}
//  payment discount server
export interface PaymentDiscountServer {
  ID?: number; // ID
  shop: string; //  название магазина
  discount: number; //  размер скидки
  enabled: boolean; //  активно?
}
