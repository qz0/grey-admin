import {Order, OrderServer} from './orders.types';
import {Delivery, DeliveryServer} from './deliveries.types';
import {Payment, PaymentServer} from './payments.types';
import {Address, AddressServer} from './address.types';
import {Customer, CustomerServer} from './customers.types';
import {OptionValue, OptionValueServer} from './options.types';
import {Product, ProductServer} from './products.types';

// CART
//  client
export interface Cart {
  id?: number; //  ID
  cartItems: CartItem[];  //  отдельный товар
  order: Order; //  заказ
  orderID: number;
  delivery: Delivery; //  доставка
  deliveryID: number;
  payment: Payment; //  оплата
  paymentID: number;
  address: Address; //  адрес доставки
  addressID: number;
  customer: Customer;
  customerID: number;
  comment: string;
}

//  server
export interface CartServer {
  ID?: number; //  ID
  cartItems: CartItemServer[];  //  отдельный товар
  order: OrderServer; //  заказ
  orderID: number;
  delivery: DeliveryServer; //  доставка
  deliveryID: number;
  payment: PaymentServer; //  оплата
  paymentID: number;
  address: AddressServer; //  адрес доставки
  addressID: number;
  customer: CustomerServer;
  customerID: number;
  comment: string;
}

//  CART ITEM
//  client
export interface CartItem {
  id?: number; //  ID
  product: Product; //   товар
  productID: number;  //  ID - товара
  optionValues: OptionValue[];  //  опции
  orderCount: number; //  необходимо единиц товара
  availableCount: number; //  доступно единиц товара
  cartID: number;
}
//  server
export interface CartItemServer {
  ID?: number; //  ID
  product: ProductServer; //   товар
  productID: number;  //  ID - товара
  optionValues: OptionValueServer[];  //  опции
  orderCount: number; //  необходимо единиц товара
  availableCount: number; //  доступно единиц товара
  cartID: number;
}
