//  Типы данных для работы с валютой
//  клиентский тип
import {LocalizedDescription} from './localization';
import {Image, ImageServer} from './image.types';

export interface Category {
  id?: number; //  ID
  parentID: number; //  ID родительской категории
  localizedDescriptions: LocalizedDescription[];  //  описания
  localizedSEOs: LocalizedDescription[]; //  SEO
  imageBackground: Image; //  фон
  imageIcon: Image; //  иконка
  // firstImage: Image; // первая картинка
  // secondImage: Image; // вторая картинка
  images: Image[];  //  картинки
  priority: number; //  приоритет в списке
  categories: Category[];  //  дети
  url: string;  //  url
  seoLink: string;  //  seoLink
  isEnabled: boolean; // видимость
}
//  server
export interface CategoryServer {
  ID?: number; //  ID
  parentID: number; //  ID родительской категории
  localizedDescriptions: LocalizedDescription[];  //  описания
  localizedSEOs: LocalizedDescription[]; //  SEO
  imageBackground: ImageServer; //  фон
  imageIcon: ImageServer; //  иконка
  // firstImage: ImageServer; // первая картинка
  // secondImage: ImageServer; // вторая картинка
  images: ImageServer[];  //  картинки
  priority: number; //  приоритет в списке
  categories: CategoryServer[];  //  дети
  url: string;  //  url
  seoLink: string;  //  seoLink
  isEnabled: boolean; // видимость
  type: string; //  тип для брокера
}
