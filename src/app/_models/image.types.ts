//  Типы данных для работы с картинками
//  клиентский тип
export interface Image {
  id?: number; //  ID
  name: string;  //  описание
  url: string; //  url
  priority: number; //  url
  type: ImageType; //  тип картинки
  typeID: number;  //  ID типа
  options: ImageOptions;  //  опции картинки
  optionsID: number;  //  ID опций
}
//  серверный тип
export interface ImageServer {
  ID?: number; //  ID
  name: string; //  ID
  url: string; //  ID
  priority: number; //  ID
  type: ImageTypeServer; //  тип картинки
  typeID: number;  //  ID типа
  options: ImageOptionsServer;  //  опции картинки
  optionsID: number;  //  ID опций
}

//  Image Options
//  клиентский тип
export interface ImageOptions {
  id?: number; //  ID
  name: string; //  наименование
}
//  серверный тип
export interface ImageOptionsServer {
  ID?: number; //  ID
  name: string; //  наименование
}

//  Image Type
//  клиентский тип
export interface ImageType {
  id?: number; //  ID
  name: string; //  наименование
  path: string; //  путь
}
//  серверный тип
export interface ImageTypeServer {
  ID?: number; //  ID
  name: string; //  наименование
  path: string; //  путь
}
