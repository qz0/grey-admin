import {LocalizedDescription, LocalizedDescriptionServer} from './localization';
import {Image, ImageServer} from './image.types';
//  типы данных для работы с валютой
//  клиентский тип бренда
export interface Brand {
  id?: number; //  ID
  title?: string; //  заголовок для списков
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  image: Image; // url на лого
  priority: number; //  приоритет
  manufacturer?: Manufacturer; // производитель
  seoLinks: string; //  seo url
  ownerID: number;  //  ownerID
  ownerType: string;  //  ownerType
}
//  серверный тип брэнда
export interface BrandServer {
  ID?: number; //  ID
  localizedDescriptions: LocalizedDescriptionServer[]; //  локализованное описание
  image: ImageServer; // url на лого
  priority: number; //  приоритет
  manufacturer?: ManufacturerServer; // производитель
  seoLinks: string; //  seo url
  ownerID: number;  //  ownerID
  ownerType: string;  //  ownerType
}
//  клиентский тип производителя
export interface Manufacturer {
  id?: number; //  ID
  name: string; //  название
  cent: number; //  процент надбавки
  color: string;  //  цвет
  contacts: string; //  контактное лицо
  site: string; //  сайт производителя
  email: string;  //  почта
  vk: string; //  линк на ВК
  phone: string;  //  телефон
  bank: string; //  банковские реквизиты
  contract: string; //  данные контракта
  discount: string; //  скидка
  address: string;  //  адрес
  delivery: string; //  доставка
  comment: string;  //  комментарий
  brands?: Brand[]; // список брэндов
}
//  серверный тип производителя
export interface ManufacturerServer {
  ID?: number; //  ID
  name: string; //  название
  cent: number; //  процент надбавки
  color: string;  //  цвет
  contacts: string; //  контактное лицо
  site: string; //  сайт производителя
  email: string;  //  почта
  vk: string; //  линк на ВК
  phone: string;  //  телефон
  bank: string; //  банковские реквизиты
  contract: string; //  данные контракта
  discount: string; //  скидка
  address: string;  //  адрес
  delivery: string; //  доставка
  comment: string;  //  комментарий
  brands: BrandServer[]; // список брэндов
}
