//  типы данных для работы с локалью
//  клиентский тип для локализованного описания
export interface LocalizedDescription {
  id?: number; //   ID в бд
  name?: string;  //  название
  title?: string;  //  заголовок
  description?: string; // описание
  descriptionTwo?: string; // описание2
  descriptionThree?: string; // описание3
  keywords?: string; //  ключевые слова
  language: string; // кодовое обозначение языка
}

//  серверный тип для локализованного описания
export interface LocalizedDescriptionServer {
  ID?: number; //   ID в бд
  name?: string;  //  название
  title?: string;  //  заголовок
  description?: string; // описание
  descriptionTwo?: string; // описание2
  descriptionThree?: string; // описание3
  keywords?: string; //  ключевые слова
  language: string; // кодовое обозначение языка
}

//  localized text
//  клиентский тип для локализованного описания
export interface LocalizedText {
  id?: number; //  ID
  text?: string;  //  текст
  language: string; // кодовое обозначение языка
}

//  серверный тип для локализованного описания
export interface LocalizedTextServer {
  ID?: number; //  ID
  text?: string;  //  текст
  language: string; // кодовое обозначение языка
}

//  язык
//  клиентский тип языка
export interface Language {
  id: number; //   ID в бд
  language: string; //  язык
  title: string; //  название
}

//  серверный тип языка
export interface LanguageServer {
  ID: number; //   ID в бд
  language: string; //  язык
  title: string; //  название
}


//  клиентский тип кейворда
export interface LocalizedKeyword {
  ID: number; //   ID в бд
  keyword: string; //кейворд для замещения слова по языку
  ru: string; //русская версия для замены по кейворду
  en: string; //английская версия для замены по кейворду
  description: string; //описание. где используется кейворд и что меняет.
}

//  серверный тип кейворда
export interface LocalizedKeywordServer {
  ID: number; //  ID в бд
  keyword: string;  //кейворд для замещения слова по языку
  ru: string; //русская версия для замены по кейворду
  en: string; //английская версия для замены по кейворду
  description: string; //описание. где используется кейворд и что меняет.
}
