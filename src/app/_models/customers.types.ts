//  типы данных для работы с покупателями
//  клиентский тип для пользователя
import {Role, RoleServer} from './users.types';
import {Address, AddressServer, Country, CountryServer, Region, RegionServer} from './address.types';
import {LocalizedDescription, LocalizedDescriptionServer} from './localization';

export interface Customer {
  id?: number; //  ID
  addresses: Address[]; //  адресс
  username: string;  //  логин
  title: string; //  заголовок
  description: string; //  описание
  password: string;  //  пароль
  rank: CustomerRank; // список ролей пользователя
}

//  серверный тип для пользователя
export interface CustomerServer {
  ID?: number; //  ID
  addresses: AddressServer[]; //  адресс
  username: string;  //  логин
  title: string; //  заголовок
  description: string; //  описание
  password: string;  //  пароль
  rank: CustomerRankServer; // список ролей пользователя
}

//  Customer Rank
//  клиентский тип для рангов
export interface CustomerRank {
  id?: number; //  ID
  localizedDescriptions: LocalizedDescription[]; //  описание
}

//  серверный тип для рангов
export interface CustomerRankServer {
  ID?: number; //  ID
  localizedDescriptions: LocalizedDescriptionServer[]; //  описание
}

