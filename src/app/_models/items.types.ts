//
import {Product, ProductServer, ProductShadow, ProductShadowServer } from './products.types';
import {Option, OptionServer, OptionsSet, OptionsSetServer, OptionValue, OptionValueServer} from './options.types';
import {Order, OrderServer} from './orders.types';
import {Currency, CurrencyServer, CurrencyShadow, CurrencyShadowServer} from './currencies.types';
import {Price, PriceServer} from './prices.types';
import {Sale, SaleServer} from './sales.types';

//  ITEMS
//  клиентский тип Item
export interface Item {
  id?: number; //  идентификатор айтема
  itemID?: number; // идентификатор для куаркода и склада
  product: Product; //  продукт
  productID: number; //  продукт ИД
  deliveryPackageNumber: number;  //  в каком из пакетов заказа этот айтем
  optionValues: OptionValue[]; // значения опций для айтема
  // optionsSets: OptionsSet[]; //  optionsSet
  //  TODO: продумать статусы для разных складов
  status: ItemStatus; //  status item
  placement: string;
  warehouseBoxNumber: number; //  номер коробки на складе
  order: Order; //  заказ
  comments: string; //  комментс
  price: Price;  //  цена продажи
  currency: Currency; //  валюта на момент покупки
  assembled?: boolean;
  checked?: boolean;
  sale: Sale; //  скидка
}
//  серверный тип Item
export interface ItemServer {
  ID?: number; //  идентификатор айтема
  itemID?: number; // идентификатор для куаркода и склада
  product: ProductServer; //  продукт
  productID: number; //  продукт ИД
  deliveryPackageNumber: number;  //  в каком из пакетов заказа этот айтем
  optionValues: OptionValueServer[]; // значения опций для айтема
  // optionsSets: OptionsSetServer[]; //  optionsSet
  //  TODO: продумать статусы для разных складов
  status: ItemStatusServer; //  status item
  placement: string;
  warehouseBoxNumber: number; //  номер коробки на складе
  order: OrderServer; //  заказ
  comments: string; //  комментс
  price: PriceServer;  //  цена продажи
  currency: CurrencyServer; //  валюта на момент покупки
  sale: SaleServer; //  скидка
}

//  logItem
//  клиентский тип ItemShadow
export interface ItemShadow {
  id?: number; //  идентификатор айтема
  itemID?: number; // идентификатор для куаркода и склада
  product: ProductShadow; //  продукт
  oldProductName: string; //  формируется при покупке копией из продукта
  deliveryPackageNumber: number;  //  в каком из пакетов заказа этот айтем
  options?: Option[];  //  options
  optionsSets: OptionsSet[]; //  optionsSet
  //  TODO: продумать статусы для разных складов
  status: ItemStatus; //  status item
  placement: string;
  warehouseBoxNumber: number; //  номер коробки на складе
  order: Order; //  заказ
  comments: string; //  комментс
  // priceSell: number;  //  цена продажи
  currency: CurrencyShadow; //  валюта на момент покупки
  sale: Sale; //  скидка
}
//  серверный тип ItemShadow
export interface ItemShadowServer {
  ID?: number; //  идентификатор айтема
  itemID?: number; // идентификатор для куаркода и склада
  product: ProductShadowServer; //  продукт
  oldProductName: string; //  формируется при покупке копией из продукта
  deliveryPackageNumber: number;  //  в каком из пакетов заказа этот айтем
  options?: OptionServer[];  //  options
  optionsSets: OptionsSetServer[]; //  optionsSet
  //  TODO: продумать статусы для разных складов
  status: ItemStatusServer; //  status item
  placement: string;
  warehouseBoxNumber: number; //  номер коробки на складе
  order: OrderServer; //  заказ
  comments: string; //  комментс
  // priceSell: number;  //  цена продажи
  currency: CurrencyShadowServer; //  валюта на момент покупки
  sale: SaleServer; //  скидка
}

//  ITEM STATUS
//  клиентский тип ItemStatus
export interface ItemStatus {
  id?: number;  //  id
  name: string; //  name
}

//  серверный тип айтема
export interface ItemStatusServer {
  ID?: number;  //  id
  name: string; //  name
}
