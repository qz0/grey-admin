import {Component, OnDestroy, OnInit} from '@angular/core';
import {AssembliesService} from '../assemblies.service';
import {Subscription} from 'rxjs';
import {Assembly} from '../../_models/assemblies.types';
import {DataService} from '../../_services/data.service';
import {OrdersService} from '../../orders/orders.service';
import {Order} from '../../_models/orders.types';


@Component({
  selector: 'app-assemblies',
  templateUrl: './assemblies.component.html',
  styleUrls: ['./assemblies.component.scss']
})

//  компонент отдельной сборки
export class AssembliesComponent implements OnInit, OnDestroy {

  // public
  public orders: Order[] = null;

  //  private
  private AssembliesOrdersSub: Subscription;


  //  конструктор
  constructor(private ordersService: OrdersService, private dataService: DataService) {
   //  подписка orders
   this.AssembliesOrdersSub = this.dataService.castOrders.subscribe((orders: Order[]) => this.orders = orders);

  }

  //  Destroy
  public ngOnDestroy() {
    //  отписка assemblies
    this.AssembliesOrdersSub.unsubscribe();
  }

  //  инитор
  public ngOnInit() {
    this.ordersService.getOrdersForAssembly();
  }

  //  выбор случайного  assembly
  /*   private chooseRandomAssembly(assemblies: Assembly[]) {
    const randomIndex = Math.floor(Math.random() * assemblies.length);
    if (randomIndex) {
      return this.randomAssembly = this.assemblies[randomIndex];
    } else {
      return this.randomAssembly = this.assemblies[0];
    }
  } */
}
