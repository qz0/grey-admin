import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssembliesRoutingModule } from './assemblies-routing.module';
import { AssembliesService } from './assemblies.service';
import { AssemblyComponent } from './assembly/assembly.component';
import { AssembliesComponent } from './assemblies/assemblies.component';
import { FormsModule } from '@angular/forms';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AssemblyComponent,
    AssembliesComponent
  ],
  imports: [
    CommonModule,
    AssembliesRoutingModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [
    AssembliesService
  ]
})
export class AssembliesModule {
}
