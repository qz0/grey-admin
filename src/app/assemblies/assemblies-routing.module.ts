import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AssemblyComponent} from './assembly/assembly.component';
import {AssembliesComponent} from './assemblies/assemblies.component';

export const assembliesRoutes: Routes = [
  //  все сборки
  {path: '', component: AssembliesComponent},
  //  отдельная сборка
  {path: 'assembly', component: AssemblyComponent},
  {path: 'assembly/:id', component: AssemblyComponent}
];

@NgModule({
  imports: [RouterModule.forChild(assembliesRoutes)],
  exports: [RouterModule]
})
export class AssembliesRoutingModule {
}
