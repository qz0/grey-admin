import { TestBed } from '@angular/core/testing';

import { AssembliesService } from './assemblies.service';

describe('AssembliesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssembliesService = TestBed.get(AssembliesService);
    expect(service).toBeTruthy();
  });
});
