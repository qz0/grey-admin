import {Component, OnDestroy, OnInit} from '@angular/core';
import {AssembliesService} from '../assemblies.service';
import {Assembly} from '../../_models/assemblies.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import { NgForm } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Item} from '../../_models/items.types';
import {OrdersService} from '../../orders/orders.service';
import {Order} from '../../_models/orders.types';
import {imgProto, imgURL} from '../../_models/urls';

@Component({
  selector: 'app-assembly',
  templateUrl: './assembly.component.html',
  styleUrls: ['./assembly.component.scss']
})

//  компонент отдельной сборки
export class AssemblyComponent implements OnInit, OnDestroy {
  //  public
  public boxNumber = 1; //  номер коробки
  public order: Order = null;
  public imagePath: string = imgProto + imgURL;

  //  private
  private AssembliesOrdersSub: Subscription;

  //  конструктор
  constructor(private ordersService: OrdersService,
              private dataService: DataService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
                this.AssembliesOrdersSub = this.dataService.castOrder.subscribe((order: Order) => {
                  this.order = order;
                  console.log(this.order);
                });
  }

  //  Destroy
  ngOnDestroy() {
    this.AssembliesOrdersSub.unsubscribe();
  }

  //  инитор
  ngOnInit() {
    //  получаем assembly
    const assemblyID = this.activatedRoute.snapshot.paramMap.get('id');

    if (assemblyID) {
      this.ordersService.getOrderByID(assemblyID);
    }
  }

  //  отправка  формы
  public submit(form: NgForm) {
    const value = form.value;
    console.log(value);
    form.reset();
    this.ordersService.orderHasAssemblied(this.order.id, String(this.boxNumber));
    this.router.navigate(['assemblies']);
  }

  public checkerHasChanged(item: Item) {
    console.log(item);
  }
}
