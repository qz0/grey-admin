import {Injectable} from '@angular/core';
import {DataService} from '../_services/data.service';
import {Subscription} from 'rxjs';
import {Assembly} from '../_models/assemblies.types';

@Injectable()
//  сервис для сборки
export class AssembliesService {
  // public
  public assembly: Assembly = null;
  public assemblies: Assembly[] = null;

  //  private
  private assemblySub: Subscription;
  private assembliesSub: Subscription;

  constructor(private dataService: DataService) {

    //  подписка assemblies
    this.assemblySub = this.dataService.castAssembly.subscribe((assembly: Assembly) => this.assembly = assembly);
    this.assembliesSub = this.dataService.castAssemblies.subscribe((assemblies: Assembly[]) => this.assemblies = assemblies);
  }

  public getAssemblies() {
    this.dataService.getAssemblies();
  }

  public getAssemblyByID(assemblyID: string) {
    if (this.assemblies) {
      this.dataService.putAssembly(this.assemblies.find(assembly => assembly.id === Number(assemblyID)));
    }
  }
}
