// animations.ts
import { trigger, style, transition, animate } from '@angular/animations';

export const Animations = {
  // универсальная анимация раскрытия блоков
  fadeInOut: trigger('fadeInOut', [
    transition(':enter', [
      style({ opacity: 0, height: 0 }),
      animate('.2s ease', style({ opacity: 1, height: '*' })),
    ]),
    transition(':leave', [
      animate('.2s ease', style({ opacity: 0, height: 0 }))
    ])
  ])
};
