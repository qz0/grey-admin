import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemsModule } from './items/items.module';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
import { MenuComponent } from './components/menu/menu.component';
import { TestComponent } from './components/test/test.component';
import { GeneralComponent } from './components/general/general.component';
import { BrandsModule } from './brands/brands.module';
import { OptionsModule } from './options/options.module';
import { CategoriesModule } from './categories/categories.module';
import { LoginComponent } from './components/login/login.component';
import { ImagesModule } from './images/images.module';
import { AssembliesModule } from './assemblies/assemblies.module';
import { IncomingsModule } from './incomings/incomings.module';
import { DeliveriesModule } from './deliveries/deliveries.module';
import { PaymentsModule } from './payments/payments.module';
import { PurchasesModule } from './purchases/purchases.module';
import { OrdersModule } from './orders/orders.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CheckSendModule } from './check-send/check-send.module';
import { SettingsModule } from './settings/settings.module';
import {NgSelectModule} from '@ng-select/ng-select';
import { LogsComponent } from './logs/logs.component';
import { LogListComponent } from './logs/log-list/log-list.component';
import { LogCardComponent } from './logs/log-card/log-card.component';
import { NgxTextDiffModule } from 'ngx-text-diff';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    TestComponent,
    GeneralComponent,
    LoginComponent,
    NotFoundComponent,
    LogsComponent,
    LogListComponent,
    LogCardComponent,
  ],
  imports: [
    AssembliesModule,
    AppRoutingModule,
    BrandsModule,
    BrowserAnimationsModule,
    BrowserModule,
    CategoriesModule,
    DeliveriesModule,
    HttpClientModule,
    ImagesModule,
    IncomingsModule,
    ItemsModule,
    OrdersModule,
    OptionsModule,
    PaymentsModule,
    ProductsModule,
    PurchasesModule,
    UsersModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    NgxBootstrapSliderModule,
    CheckSendModule,
    SettingsModule,
    NgSelectModule,
    NgxTextDiffModule
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
