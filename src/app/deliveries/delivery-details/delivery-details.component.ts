import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DeliveriesService} from '../deliveries.service';
import {DataService} from '../../_services/data.service';
import {Delivery} from '../../_models/deliveries.types';
import {Subscription} from 'rxjs';
import {GenerateService} from '../../_services/generate.service';
import {imgProto, imgURL} from '../../_models/urls';
import {Image} from '../../_models/image.types';
import { ColorOutput } from 'ng-color/color-output';

@Component({
  selector: 'app-delivery-details',
  templateUrl: './delivery-details.component.html',
  styleUrls: ['./delivery-details.component.scss']
})
export class DeliveryDetailsComponent implements OnInit, OnDestroy {

  //  public
  public delivery: Delivery = null;
  public currentLangTab = 'EN';
  public imagePath: string = imgProto + imgURL + '/deliveries/';
  public color: ColorOutput = null; // colors
  //  private
  private deliverySub: Subscription = null;
  //  constructor
  constructor(private activatedRoute: ActivatedRoute, //  активированный роут
              private deliveriesService: DeliveriesService, //  сервис для работы с доставкой
              private dataService: DataService, //  сервис для работы с данными
  ) {
    //  subscribe to delivery
    this.deliverySub = this.dataService.castDelivery.subscribe((delivery: Delivery) => {
      //  если прилетел
      if (delivery) {
        this.delivery = delivery;
        //  получаем список стран для доставки
        console.log('run getCountryList with new delivery: ', delivery);
        // this.getCountryList(delivery);
        this.deliveriesService.getCountryList(delivery);
      }
    });
  }

  //  destroy
  ngOnDestroy() {
    //  unsubscribes
    this.deliverySub.unsubscribe();
  }

  //  initor
  ngOnInit() {
    //  получаем brand
    const deliveryID = this.activatedRoute.snapshot.paramMap.get('id');

    //  если передали id
    if (deliveryID) {
      //  получаем существующую доставку
      this.deliveriesService.getDeliveryByID(deliveryID);
    } else {
      //  видимо генерируем новую
      this.deliveriesService.getDeliveryByID(null);
    }
  }

  //  изменениетипа веса
  /*  
  public changeWeightType(weightType: boolean) {
    this.delivery.weightType = weightType;
    this.getCurrentDelivery();
  }
  */
  public changeImage(eventImage: Image) {
    this.delivery.image = eventImage;
  }

  //  добавляем зону в доставку
  public createZone() {
    //  добавляем зону
    this.deliveriesService.createZone(this.delivery);

    const zone = GenerateService.createZone();

    //  пушим пустую зону в доставку
    this.delivery.zones.push(zone);

    //  публикуем доставку
    this.dataService.putDelivery(this.delivery);
  }

  //  remove image
  public deleteImage(delivery: Delivery) {

  }

  //  включить/выключить доставку
  public enableDelivery(isDeliveryEnabled: boolean) {
    //  если существует передаем новое значение
    if (this.delivery) {
      this.delivery.enabled = isDeliveryEnabled;
    }
  }

  //  переключение табов языка
  public selectLangTab(lang: string) {
    this.currentLangTab = lang;
  }

  public updateDelivery(delivery: Delivery) {
    this.deliveriesService.updateDelivery(delivery);
  }

  public getCurrentDelivery() {
    console.log(this.delivery);
  }

  public colorChange(color: ColorOutput): void {
    this.color = color;
    this.delivery.color = color.hexString;
  }
}
