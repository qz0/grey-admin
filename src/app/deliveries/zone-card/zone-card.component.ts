import {Component, Input, OnInit} from '@angular/core';
import {Delivery, Zone, ZoneCost} from '../../_models/deliveries.types';
import {DeliveriesService} from '../deliveries.service';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';
import {Country, CountryShort} from '../../_models/address.types';

@Component({
  selector: 'app-zone-card',
  templateUrl: './zone-card.component.html',
  styleUrls: ['./zone-card.component.scss']
})
export class ZoneCardComponent implements OnInit {
  //  public
  @Input() zoneIndex: number = 0; //  номер зоны от начала списка
  @Input() zone: Zone = null; //  экземпляр зоны
  public delivery: Delivery = null; //  доставка
  public country: CountryShort = null; //  страна
  public countryShort: CountryShort = null; //  страна в кратком формате
  public countryList: CountryShort[] = [];  //  список стран в сокращенном виде
  // public isShownCountrySelect: boolean = false; //  бит отображения списка выбора
  // public isDisabledCountryAddButton: boolean = false; //  бит отображения списка выбора

  //  private
  private deliverySub: Subscription = null;
  private countryListSub: Subscription = null;

  constructor(
    private deliveriesService: DeliveriesService,
    private dataService: DataService) {

    //  подписка на список стран для зон
    this.countryListSub = this.dataService.castCountryList.subscribe((countryList: CountryShort[]) => {
      this.countryList = countryList;
    });
    //  subscribe to delivery
    this.deliverySub = this.dataService.castDelivery.subscribe((delivery: Delivery) => {
      //  если не пустая присваиваем доставку
      if (delivery) {
        this.delivery = delivery;
      }
    });
  }

  //  destroy
  ngOnDestroy() {
    //  unsubscribes
    this.deliverySub.unsubscribe();
    this.countryListSub.unsubscribe();
  }

  ngOnInit() {
    //  получаем список стран вдоставке
    console.log('zone: ', this.zone.countries);
  }

  //  добавить цены
  public addCostToZone() {
      const newCost: ZoneCost = {
        weight: 0,
        cost: 0,
      };
      //  дергаем сервис
      this.deliveriesService.addCostToZone(newCost, this.zoneIndex);
  }

  //  добавить страну
  public addCountry(newCountry: CountryShort) {
      //  дергаем сервис
      this.deliveriesService.addCountryToZone(newCountry, this.zoneIndex).subscribe((temp: boolean) => {
        if (temp) {
          //  обнулить выбор
          this.countryShort = null;
        }
      });
    //  деактивировать кнопку
    // this.isDisabledCountryAddButton = true;
  }

  //  активировать кнопку доабвления страны
  /* 
  public changeCountrySelect() {
    //  активировать кнопку
    this.isDisabledCountryAddButton = false;
  }
  */

  //  удалить страну
  public delCountry(delCountry: Country) {
    //  если не пустое дергаем сервис удаления страны
    if (delCountry) {
      this.deliveriesService.delCountryFromZone(delCountry, this.zone);
    }
  }

  //  удалить страну
  public delZoneCost(delZoneCost: ZoneCost) {
    //  если не пустое дергаем сервис удаления страны
    if (delZoneCost) {
      this.deliveriesService.delCostFromZone(delZoneCost, this.zone);
    }
  }

  //  удалить страну
  public delZone() {
    //  если есть доставка
    if (this.delivery) {
      const zoneForDeleteIndex = this.delivery.zones.findIndex((currentZone: Zone) => this.zone === currentZone);
      //  если нашел - удаляем эту запись
      if (zoneForDeleteIndex !== -1) {
        this.delivery.zones.splice(zoneForDeleteIndex, 1);
        //  публикуем деливери
        this.dataService.putDelivery(this.delivery);
      }
    }
  }

}
