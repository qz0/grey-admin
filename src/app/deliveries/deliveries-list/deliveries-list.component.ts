import {Component, OnDestroy, OnInit} from '@angular/core';
import {Delivery} from '../../_models/deliveries.types';
import {DeliveriesService} from '../deliveries.service';
import {Brand} from '../../_models/brands.types';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';
import {DndDropEvent} from 'ngx-drag-drop';
import {GenerateService} from '../../_services/generate.service';

@Component({
  selector: 'app-deliveries-list',
  templateUrl: './deliveries-list.component.html',
  styleUrls: ['./deliveries-list.component.scss']
})
export class DeliveriesListComponent implements OnInit, OnDestroy {
  //  public
  public deliveries: Delivery[] = null; //  доставки

  //  private
  private deliveriesSub: Subscription = null;

  constructor(private deliveriesService: DeliveriesService,
              private dataService: DataService) {
    //  подписываемся на доставки
    this.deliveriesSub = this.dataService.castDeliveries.subscribe((deliveries: Delivery[]) => this.deliveries = deliveries);
  }

  //  destroy
  public ngOnDestroy() {
    // отписываемся от доставок
    this.deliveriesSub.unsubscribe();
  }

  //  init
  public ngOnInit() {
    this.deliveriesService.getDeliveries();
    console.log('Delivery: ', this.deliveries);
  }

  public onDraggableMoved(event) {
    console.log(event);
  }

  public onDragStart(event) {
    console.log('Дроп начали', event);
  }

  public onDragEnd(event) {
    console.log('Дроп закончили', event);
  }

  public onDragCanceled(event) {
    console.log('Дроп отменили', event);
  }

  //  part Drop
  public onDragover(event: DragEvent) {

    console.log('dragover', JSON.stringify(event, null, 2));
  }

  //  Сброшен
  public onDrop(event: DndDropEvent) {
    //  проверка на пустоту
    if (event && event.data && event.data.id && event.index != null) {
    //  перемещаем доставку на новую позицию
      const delivery: Delivery = event.data;
      if (event.index + 1 !== delivery.priority && event.index !== delivery.priority) {
        //  если перемещаем индекс надо уменьшать на единицу
        if (delivery.priority < event.index) {
          event.index = event.index - 1;
        }
        //  отлавливаем самый нижний индекс
        if (event.index === this.deliveries.length) {
          delivery.priority = this.deliveries.length;
        } else {
          delivery.priority = event.index + 1;
        }
        //  дергаем функцию
        this.deliveriesService.moveDelivery(delivery);
      }
    }
  }

  //  создание новой доставки
  public createDelivery() {
    if (this.deliveries) {
      //
      this.deliveries.push(GenerateService.createDelivery(this.deliveries.length + 1));
    }
  }
}
