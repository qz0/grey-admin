import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DeliveriesListComponent} from './deliveries-list/deliveries-list.component';
import {DeliveryDetailsComponent} from './delivery-details/delivery-details.component';

export const deliveriesRoutes: Routes = [
  {
    //  все deliveries
    path: '', component: DeliveriesListComponent
  },
  {
    //  отдельная delivery
    path: 'delivery/:id', component: DeliveryDetailsComponent
  },
  {
    //  отдельная delivery
    path: 'delivery', component: DeliveryDetailsComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(deliveriesRoutes)],
  exports: [RouterModule]
})
export class DeliveriesRoutingModule { }
