import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveriesRoutingModule } from './deliveries-routing.module';
import { DeliveryDetailsComponent } from './delivery-details/delivery-details.component';
import { DeliveriesListComponent } from './deliveries-list/deliveries-list.component';
import { DeliveriesService} from './deliveries.service';
import { DeliveryCardComponent } from './delivery-card/delivery-card.component';
import { PipesModule } from '../pipes/pipes.module';
import { DndModule } from 'ngx-drag-drop';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ZoneCardComponent } from './zone-card/zone-card.component';
import {FormsModule} from '@angular/forms';
import {ImagesModule} from '../images/images.module';
import {NgSelectModule} from '@ng-select/ng-select';
import { NgColorModule } from 'ng-color';

@NgModule({
  declarations: [
    DeliveryDetailsComponent,
    DeliveriesListComponent,
    DeliveryCardComponent,
    ZoneCardComponent
  ],
  imports: [
    CommonModule,
    DeliveriesRoutingModule,
    PipesModule,
    DndModule,
    FontAwesomeModule,
    FormsModule,
    ImagesModule,
    NgSelectModule,
    NgColorModule
  ],
  providers: [
    DeliveriesService,
  ]
})
export class DeliveriesModule {
}
