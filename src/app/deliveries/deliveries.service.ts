import {Injectable, OnInit} from '@angular/core';
import {DataService} from '../_services/data.service';
import {GenerateService} from '../_services/generate.service';
import {Delivery, Zone, ZoneCost} from '../_models/deliveries.types';
import {Observable, of, Subscription} from 'rxjs';
import {AlertifyService} from '../_services/alertify.service';
import {Country, CountryShort} from '../_models/address.types';
import {catchError, map, tap} from 'rxjs/operators';
import {ImageTypeServer} from '../_models/image.types';

@Injectable({
  providedIn: 'root'
})
export class DeliveriesService implements OnInit {
  //  public
  public countryList: CountryShort[] = [];
  public countriesShort: CountryShort[] = [];
  public delivery: Delivery = null;

  //  конструктор
  constructor(private dataService: DataService,
              private alertifyService: AlertifyService) {
    //  подписываемся на доставку
    this.dataService.castDelivery.subscribe((delivery: Delivery) => {
      if (delivery) {
        this.delivery = delivery;
      }
    });
    //  подписываемя на список всех стран
    this.dataService.castCountriesShort.subscribe((countriesShort: CountryShort[]) => {
      //  получаем список всех стран
      if (countriesShort) {
        this.countriesShort = countriesShort;
      }
      //  пробуем дернуть список стран
      // if (this.delivery) {
      //   this.getCountryList(this.delivery);
      // }
    });
    //  получаем список свободных стран
    this.dataService.castCountryList.subscribe((countryList: CountryShort[]) => {
      if (countryList) {
        this.countryList = countryList;
      }
    });
  }
  //  инитор
  public ngOnInit() {
  }

  //  добавляем страну
  public addCostToZone(addedCost: ZoneCost, zoneIndex: number) {
    console.log(this.delivery.zones);
    if (addedCost) {
      this.delivery.zones[zoneIndex].zoneCosts.push(addedCost);
      //  публикуем доставку
      this.dataService.putDelivery(this.delivery);
    }
  }

  //  добавляем страну
  public addCountryToZone(addedCountry: CountryShort, zoneIndex: number): Observable<boolean> {

    //  добавить страну в список
    return this.dataService.getCountryByID(String(addedCountry.id))
      .pipe(
        // tap(_ => console.log('Into POST')),
        tap((country: Country) => {
          this.delivery.zones[zoneIndex].countries.push(country);
          //  публикуем доставку
          this.dataService.putDelivery(this.delivery);
          return true;
        }),
        catchError(err => {
          console.log(err, 'ERROR in getImageTypeByID service');
          return of(null);
        })
      );
  }

  //  создание зоны для доставки
  public createZone(delivery: Delivery) {
  //  если не пустое
    if (delivery && delivery.id) {
      console.log(delivery);
      this.alertifyService.message('Try to create zone', );
    } else {
      this.alertifyService.error('This Delivery doesn\'t saved. Save it for continues...');
    }
  }

  //  получаем все доставки
  public delCountryFromZone(deletedCountry: Country, currentZone: Zone) {
    console.log('!!!!!', this.delivery.zones, deletedCountry);
    //  если не пустые
    if (deletedCountry && currentZone) {
      //  ищем индекс зоны
      const zoneIndex = this.delivery.zones.findIndex((zone: Zone) => zone.id === currentZone.id);
      //  если нашли ищем индекс страны
      if (zoneIndex !== -1) {
        const countryIndex = this.delivery.zones[zoneIndex].countries.findIndex((country: Country) => country.id === deletedCountry.id);
        console.log('Try to remove country: ', deletedCountry);
        console.log('Try to remove countries: ', this.delivery.zones[zoneIndex].countries);
        console.log('Try to remove index: ', countryIndex);
        //  если нашли - удаляем
        if (countryIndex !== -1) {
          console.log('Try:: ', deletedCountry.id, this.delivery.zones[zoneIndex].countries, countryIndex);
          this.delivery.zones[zoneIndex].countries.splice(countryIndex, 1);
          //  публикуем новую доставку
          this.dataService.putDelivery(this.delivery);
        }
      }
    }
    // пробуем получить индекс страны в списке
    const index = this.countryList.findIndex((country: CountryShort) => country.id === deletedCountry.id);
    if (index !== -1) {
      //  удаляем страну из списка
      this.countryList.splice(index, 1);
    }
    //  публикуем изменения
    this.dataService.putCountryList(this.countryList);
  }

  //  получаем все доставки
  public delCostFromZone(deletedCost: ZoneCost, currentZone: Zone) {
    console.log('!!!!!', this.delivery.zones, deletedCost);
    //  если не пустые
    if (deletedCost && currentZone) {
      //  ищем индекс зоны
      const zoneIndex = this.delivery.zones.findIndex((zone: Zone) => zone.id === currentZone.id);
      //  если нашли ищем индекс страны
      if (zoneIndex !== -1) {
        const costIndex = this.delivery.zones[zoneIndex].zoneCosts.findIndex((costZone: ZoneCost) => costZone.id === deletedCost.id);
        console.log('Try to remove deletedCost: ', deletedCost);
        console.log('Try to remove countries: ', this.delivery.zones[zoneIndex].zoneCosts);
        console.log('Try to remove index: ', costIndex);
        //  если нашли - удаляем
        if (costIndex !== -1) {
          console.log('Try:: ', deletedCost.id, this.delivery.zones[zoneIndex].countries, costIndex);
          this.delivery.zones[zoneIndex].zoneCosts.splice(costIndex, 1);
          //  публикуем новую доставку
          this.dataService.putDelivery(this.delivery);
        }
      }
    }
  }

  //  получаем все доставки
  public getCountryList(delivery: Delivery) {

    // console.log('getCountryList');
    //  создаем переменную под список стран в доставке
    if (delivery) {
      const countryList: CountryShort[] = [...this.countriesShort];
      // console.log('getCountryList - begin: ', countryList, delivery.zones);
      //  перебираем все зоны
      if (delivery.zones && delivery.zones.length) {
        delivery.zones.forEach((zone: Zone) => {
          //  перебираем страны
          if (zone.countries && zone.countries.length) {
            // console.log('getCountryList - countries: ', zone.countries.length);
            zone.countries.forEach((country: Country) => {
              //  если есть совпадение убиваем страну в списке
              // console.log('Country, countryList: ', country, countryList);
              const index = countryList.findIndex((countryInList: CountryShort) => countryInList.id === country.id);
              // console.log('current index: ', index);
              if (index !== -1) {
                //  удаляем страну из списка
                countryList.splice(index, 1);
              }
            });
          }
        });
      }
      // console.log('getCountryList - countryList: ', countryList);
      //  публикуем изменения
      this.dataService.putCountryList(countryList);
      // this.dataService.putDelivery(delivery);
    }
  }

  //  получаем доставку по ИД
  public getDeliveryByID(deliveryID: string) {
    if (deliveryID) {
      this.dataService.getDeliveryByID(deliveryID);
    }
  }

  //  получаем все доставки
  public getDeliveries() {
    //  дергаем сервис
    this.dataService.getDeliveries();
    //  генерируем муляж
    // const newDeliveries: Delivery[] = [];
    // for (let i = 0; i < 3; i++) {
    //   newDeliveries.push(GenerateService.generateDelivery(i + 1));
    // }
    // this.dataService.putDeliveries(newDeliveries);
  }

  //  изменение отображения доставки
  //  delivery - экземпляр доставки
  public changeEnabledDelivery(delivery: Delivery) {
    if (delivery) {
      delivery.enabled = !delivery.enabled;
      this.updateDelivery(delivery);
    }
  }

  //  перемещение клетки доставки
  //  delivery - экземпляр доставки
  public moveDelivery(delivery: Delivery) {
    if (delivery) {
      this.dataService.changePriorityOfDelivery(delivery);
    }
  }

  //  изменение доставки
  //  delivery - экземпляр доставки
  public updateDelivery(delivery: Delivery) {
    if (delivery) {
      this.dataService.updateDelivery(delivery);
    }
  }
}
