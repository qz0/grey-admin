import {Component, Input, OnInit} from '@angular/core';
import {Delivery} from '../../_models/deliveries.types';
import {Router} from '@angular/router';
import { imgProto, imgURL} from '../../_models/urls';

@Component({
  selector: 'app-delivery-card',
  templateUrl: './delivery-card.component.html',
  styleUrls: ['./delivery-card.component.scss']
})
export class DeliveryCardComponent implements OnInit {
  // public
  public imagePath: string = imgProto + imgURL;
  @Input() public delivery: Delivery = null; // экземпляр доставки
  @Input() public deliveryIndex: number = null;

  //  constructor
  constructor(private router: Router) { }

  //  init
  ngOnInit() {
    console.log('Delivery: ', this.delivery);
  }

  //  change Delivery
  public changeDelivery(delivery: Delivery) {
    console.log(delivery);
    //  если доставка не пустая
    if (delivery) {
      if (delivery.id) {
        this.router.navigate(['deliveries/delivery', delivery.id]);
      } else {
        this.router.navigate(['deliveries/delivery']);
      }
    } else {
      this.router.navigate(['deliveries/delivery']);
    }
  }

  public toString(digit: number): string {
    return String(digit);
  }
}
