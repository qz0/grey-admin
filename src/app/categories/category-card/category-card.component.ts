import {Component, Input, OnInit} from '@angular/core';
import {Category} from '../../_models/categories.types';
import {DataService} from '../../_services/data.service';
import {CategoriesService} from '../categories.service';
import {DndDropEvent} from 'ngx-drag-drop';
import {Router} from '@angular/router';

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.scss']
})
export class CategoryCardComponent implements OnInit {
  //  public
  @Input() public category: Category;
  @Input() public parentCategories?: Category[];
  @Input() public currentIndex: number;
  @Input() public levelIndex?: number = 0; //  уровень списка

  //  private
  private containerId: number;  //  служебная переменная для хранения текущего ид контейнера
  private tempCategory: Category = null;  //  временное хранилище категорий
  private tempCategories: Category[] = null;  //  временное хранилище категорий

  constructor(private dataService: DataService, private categoriesService: CategoriesService, private router: Router) { }

  ngOnInit() {
  }

  onDragStart(event: DragEvent, category: Category) {

    if (this.currentIndex && this.parentCategories) {
      this.tempCategory = Object.assign({}, category);
      this.tempCategories = this.parentCategories;
    }
    const categoryIndex = this.parentCategories.indexOf(category);
    console.log('DRAG START:\ntempCategory: ', this.tempCategory, category, this.parentCategories[this.currentIndex - 1]);
    console.log('Current Indexes: ', this.currentIndex, categoryIndex);
    console.log('Temp Categories: ', this.tempCategories);
  }

  //  конец переноса
  onDragEnd(event: DragEvent) {
    //  удаляем категорию из контейнера
    this.parentCategories.splice(this.currentIndex, 1);
  }

  onDragCanceled(event: DragEvent, i: number) {

    console.log('drag cancelled', JSON.stringify(event, null, 2));
  }

  //  DROP

  public onDragover(event) {
    // console.log('onDragover: ', event);
  }

  //  Сброшен
  onDropCategory(event: DndDropEvent, currentIndex: number, parentCategoriesArray: Category[]) {

    if (event && event.data && parentCategoriesArray && parentCategoriesArray.length >= event.index) {
      //  добавляем локально
      parentCategoriesArray.splice(event.index, 0, event.data);
    }
  }

  //  создание категории
  // public createCategory(parentCategoryArray: Category[]) {
  //   this.categoriesService.createCategory(parentCategoryArray);
  // }

  //  создание категории
  // public deleteCategory(category: Category, parentCategory?: Category[]) {
  //   this.categoriesService.deleteCategory(category, parentCategory);
  // }

  //  изменение категории
  //  category - экземпляр категории
  public updateCategory(category: Category) {
    console.log('category: ', category);
    if (category.id) {
      this.router.navigate(['/categories/category', {id: category.id}]);
    } else {
      this.router.navigate(['/categories/category']);
    }
  }
}
