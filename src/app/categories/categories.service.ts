import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Category} from '../_models/categories.types';
import {GenerateService} from '../_services/generate.service';

@Injectable({
  providedIn: 'root'
})
//  **** CATEGORIES ***********
export class CategoriesService {

  constructor(private dataService: DataService) { }

  //  создание нового значения категории
  //  category - экземпляр категории
  public createCategory(category: Category) {
    //  проверка на пустоту
    if (category) {
      // categoriesArray.push(GenerateService.generateCategory());
      this.dataService.createCategory(category);
    }
  }

  //  получение значения конкретной категории
  //  categoryID - идентификатор категории
  public getCategoryByID(categoryID: string) {
    this.dataService.getCategoryByID(categoryID);
  }

  //  получение значения всех категорий
  //  count - количество получаемых записей
  //  offset - с какой записи получать
  public getCategories(count?: number, offset?: number) {
    //  получаем список категорий
    this.dataService.getCategories();
  }


  //  удаление нового значения категории
  //  categoryID - идентификатор категории
  public deleteCategory(categoryID: string, parentCategory?: Category[]) {
    // console.log('DELETE: ', category, parentCategory);
    this.dataService.deleteCategory(categoryID);
    // if (categoryID && parentCategory) {
      // console.log('DELETE: ', category, parentCategory);
      // parentCategory.splice(parentCategory.indexOf(category), 1);
    // }
  }

  //  перемещение категории
  //  category - идентификатор категории
  //  oldParentID - идентификатор категории
  //  newParentID - идентификатор категории
  public moveCategory(category: Category, oldParentID: number, newParentID: number) {
    this.dataService.moveCategory(category, oldParentID, newParentID);
  }

  //  изменение категории
  //  categoryID - идентификатор категории
  public updateCategory(category: Category) {
    //  проверка на пустоту
    if (category) {
      // categoriesArray.push(GenerateService.generateCategory());
      this.dataService.updateCategory(category);
    }
  }

  //  времянка для генерации категорий
  //  categories - категории для добавления
  public initCategories() {
    const categories: Category[] = [];
    for (let i = 0; i < 4; i++) {
      categories.push(GenerateService.generateCategory());
    }
    for (let i = 0; i < 2; i++) {
      categories[0].categories.push(GenerateService.generateCategory());
    }
    for (let i = 0; i < 2; i++) {
      categories[1].categories.push(GenerateService.generateCategory());
    }
    for (let i = 0; i < 2; i++) {
      categories[1].categories[0].categories.push(GenerateService.generateCategory());
    }
    console.log(categories);
    this.dataService.putCategories(categories);
  }
}
