import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListCategoriesComponent } from './list-categories/list-categories.component';
import { CategoryDetailsComponent } from './category-details/category-details.component';

//  экспортируем маршруты
export const categoriesRoutes: Routes = [
  //  общая
  { path: '', component: ListCategoriesComponent },
  // //  блок бренды
  // { path: '/', component: ListUsersComponent },
  //  блок отдельный бренд
  { path: 'category', component: CategoryDetailsComponent },
  //  блок отдельный бренд
  { path: 'category/:id', component: CategoryDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(categoriesRoutes)],
  exports: [RouterModule]
})

export class CategoriesRoutingModule {

}
