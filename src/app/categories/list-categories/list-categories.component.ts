import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Category} from '../../_models/categories.types';
import {DndDropEvent} from 'ngx-drag-drop';
import {imgProto, imgURL} from '../../_models/urls';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import {CategoriesService} from '../categories.service';
import {GenerateService} from '../../_services/generate.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.scss']
})
export class ListCategoriesComponent implements OnInit, OnDestroy {
  //  public
  public categories: Category[] = null; //  список категорий
  public hiddenCategories: Category[] = null; //  список категорий
  @Input() public parentCategories?: Category[];
  @Input() public currentIndex: number;

  public imagePath: string = imgProto + imgURL;
  private tempCategory: Category = null;
  private tempCategories: Category[] = [];

  //  private
  private categoriesSub: Subscription;

  //  конструктор
  constructor(
    private dataService: DataService,
    private categoriesService: CategoriesService,
    private router: Router
  ) {
    this.categoriesSub = this.dataService.castCategoriesAll.subscribe((categories: Category[]) => this.categories = categories);
  }

  //  destroy
  public ngOnDestroy() {
    this.categoriesSub.unsubscribe();
  }

  //  инитор
  public ngOnInit() {
    //  дергаем категории
    // this.categoriesService.initCategories();

    this.categoriesService.getCategories();

    console.log('this.categories: ', this.categories);

    this.hiddenCategories = [];
    this.hiddenCategories.push(GenerateService.generateCategory());
    this.hiddenCategories.push(GenerateService.generateCategory());
  }

  onDragStart(event: DragEvent, category: Category) {

    if (this.currentIndex && this.categories) {
      this.tempCategory = Object.assign({}, category);
      this.tempCategories = this.parentCategories;
    }
    const categoryIndex = this.parentCategories.indexOf(category);
    console.log('DRAG START:\ntempCategory: ', this.tempCategory, category, this.parentCategories[this.currentIndex - 1]);
    console.log('Current Indexes: ', this.currentIndex, categoryIndex);
    console.log('Temp Categories: ', this.tempCategories);
  }
  //  конец переноса
  onDragEnd(event: DragEvent) {
    //  удаляем категорию из контейнера
    this.parentCategories.splice(this.currentIndex, 1);
  }

  onDragCanceled(event: DragEvent, i: number) {

    console.log('drag cancelled', JSON.stringify(event, null, 2));
  }

  onDragover(event: DragEvent) {

    console.log('dragover', JSON.stringify(event, null, 2));
  }

  //  Сброшен
  onDropCategory(event: DndDropEvent, i: number, parentCategoriesArray: Category[]) {

    if (event && event.data && parentCategoriesArray && parentCategoriesArray.length >= event.index) {
      //  добавляем локально
      parentCategoriesArray.splice(event.index, 0, event.data);
    }
  }

  //  создание категории
  public createCategory(category?: Category) {
    //  проверяем, что передали
    if (!category) {
      //  создаем нового производителя
      this.router.navigate(['/categories/category']);
    } else {
      //  правим существующего производителя
      this.router.navigate(['/categories/category', {id: category.id}]);
    }
  }

  // public newItem(): Category {
  //   let category: Category;
  //   category = {
  //     id: 0,
  //     name: 'Новая категория',
  //     title: 'Новая категория',
  //     items: [],
  //     parentId: 0,
  //   };
  //   return category;
  // }
  //
  // public removeItem(item: any, list: any[]): void {
  //   list.splice(list.indexOf(item), 1);
  // }

  //  создание категории
  // public deleteCategory(category: Category, parentCategoriesArray: Category[]) {
  //   this.categoriesService.deleteCategory(category, parentCategoriesArray);
  // }

  //  изменение категории
  //  category - экземпляр категории
  public updateCategory(category: Category) {
    console.log('category: ', category);
    if (category.id) {
      this.router.navigate(['/categories/category', {id: category.id}]);
    } else {
      this.router.navigate(['/categories/category']);
    }
  }
}
