import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

import {Category} from '../../_models/categories.types';
import {DataService} from '../../_services/data.service';
import {CategoriesService} from '../categories.service';
import {Image} from '../../_models/image.types';
import {imgProto, imgURL} from '../../_models/urls';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.scss']
})
export class CategoryDetailsComponent implements OnInit, OnDestroy {

  @ViewChild('f', {static: true}) categoryForm: NgForm;
  //  public
  public category: Category = null; // текущая категория
  public currentLangTab: string = 'RU'; // текущий язык
  public categoryID: string; // ID Категории
  public imagePath: string = imgProto + imgURL + '/categories/';

  public creatingCategory: Category = {
    localizedDescriptions: [
      {
        description: '',
        keywords: null,
        language: 'EN',
        id: null,
        name: '',
        title: ''
      },
      {
        description: '',
        keywords: null,
        language: 'RU',
        id: null,
        name: '',
        title: ''
      }
    ],
    imageBackground: null,
    categories: [],
    // firstImage: null,
    imageIcon: null,
    id: null,
    images: [],
    isEnabled: false,
    localizedSEOs: [],
    parentID: null,
    priority: 0,
    // secondImage: null,
    url: '',
    seoLink: '',
  };

  //  private
  private categorySub: Subscription;

  //  constructor
  constructor(private dataService: DataService,
              private categoriesService: CategoriesService,
              private activatedRoute: ActivatedRoute,
              private router: Router
  ) {
    this.categorySub = this.dataService.castCategory.subscribe((category: Category) => {
      console.log('category: ', category);
      this.category = category;
    });
  }

  // destroy
  ngOnDestroy() {
    this.categorySub.unsubscribe();
  }

  //  init
  ngOnInit() {
    //  получаем category
    this.categoryID = this.activatedRoute.snapshot.paramMap.get('id');

    //  если передали id
    if (this.categoryID) {
      this.categoriesService.getCategoryByID(this.categoryID);
      console.log('current category: ', this.category);
    } else {
      this.categoriesService.getCategoryByID(null);
    }
  }

  //  Изменение картинок
  //  изменение фона
  //  eventImage - новая картинка
  public changeBackgroundImage(eventImage: Image) {
    this.category.imageBackground = eventImage;
  }

  //  изменение иконки
  //  eventImage - новая картинка
  public changeIconImage(eventImage: Image) {
    this.category.imageIcon = eventImage;
  }

  public deleteCategory() {
    this.categoriesService.deleteCategory(this.categoryID);
    this.router.navigate(['/categories']);
  }

  //  включить/выключить  способ оплаты
  public enableCategory(isCategoryEnabled: boolean) {
    console.log('click');
    //  если существует передаем новое значение
    if (this.category && this.categoryID) {
      this.category.isEnabled = isCategoryEnabled;
    } else {
      this.creatingCategory.isEnabled = isCategoryEnabled;
    }
  }

  //  изменение / сохранение категории
  public updateCategory(form: NgForm) {
    if (this.categoryID) {
      // console.log('update', this.category);
      this.categoriesService.updateCategory(this.category);
      this.router.navigate(['/categories']);
    } else {
      // console.log('save', this.creatingCategory);
      this.categoriesService.createCategory(this.category);
      this.router.navigate(['/categories']);
    }
  }

  //  переключение табов языка
  public selectLangTab(lang: string) {
    this.currentLangTab = lang;
  }

}
