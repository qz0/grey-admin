import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { ListCategoriesComponent } from './list-categories/list-categories.component';
import { DndModule } from 'ngx-drag-drop';
import { CategoriesService } from './categories.service';
import { CategoryCardComponent } from './category-card/category-card.component';
import { PipesModule } from '../pipes/pipes.module';
import { CategoryDetailsComponent } from './category-details/category-details.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';
import {ImagesModule} from '../images/images.module';

@NgModule({
  declarations: [ListCategoriesComponent, CategoryCardComponent, CategoryDetailsComponent],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    DndModule,
    PipesModule,
    FontAwesomeModule,
    FormsModule,
    ImagesModule,
  ],
  providers: [
    CategoriesService,
  ]
})
export class CategoriesModule {
}
