import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasePreordersComponent } from './purchase-preorders.component';

describe('PurchasePreordersComponent', () => {
  let component: PurchasePreordersComponent;
  let fixture: ComponentFixture<PurchasePreordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasePreordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasePreordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
