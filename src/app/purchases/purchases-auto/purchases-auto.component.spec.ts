import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesAutoComponent } from './purchases-auto.component';

describe('PurchasesAutoComponent', () => {
  let component: PurchasesAutoComponent;
  let fixture: ComponentFixture<PurchasesAutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesAutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
