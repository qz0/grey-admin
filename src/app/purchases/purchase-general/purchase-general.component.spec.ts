import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseGeneralComponent } from './purchase-general.component';

describe('PurchaseGeneralComponent', () => {
  let component: PurchaseGeneralComponent;
  let fixture: ComponentFixture<PurchaseGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
