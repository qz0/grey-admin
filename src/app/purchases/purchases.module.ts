import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchasesRoutingModule } from './purchases-routing.module';
import { PurchasesManualComponent } from './purchases-manual/purchases-manual.component';
import { PurchasesAutoComponent } from './purchases-auto/purchases-auto.component';
import { PurchaseAutoComponent } from './purchase-auto/purchase-auto.component';
import { PurchaseSpecificComponent } from './purchase-specific/purchase-specific.component';
import { PurchasePreordersComponent } from './purchase-preorders/purchase-preorders.component';
import { PurchasesService } from './purchases.service';
import { PurchaseGeneralComponent } from './purchase-general/purchase-general.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    PurchasesManualComponent,
    PurchasesAutoComponent,
    PurchaseAutoComponent,
    PurchaseSpecificComponent,
    PurchasePreordersComponent,
    PurchaseGeneralComponent
  ],
  imports: [
    CommonModule,
    PurchasesRoutingModule,
    FontAwesomeModule
  ],
  providers: [
    PurchasesService
  ]
})
export class PurchasesModule {
}
