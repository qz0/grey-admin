import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseSpecificComponent } from './purchase-specific.component';

describe('PurchaseSpecificComponent', () => {
  let component: PurchaseSpecificComponent;
  let fixture: ComponentFixture<PurchaseSpecificComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseSpecificComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseSpecificComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
