import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesManualComponent } from './purchases-manual.component';

describe('PurchasesManualComponent', () => {
  let component: PurchasesManualComponent;
  let fixture: ComponentFixture<PurchasesManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
