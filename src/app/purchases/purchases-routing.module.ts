import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseGeneralComponent } from './purchase-general/purchase-general.component';

export const purchasesRoutes: Routes = [
  {
    //  все purchases
    path: '', component: PurchaseGeneralComponent
  },
  {
    //  отдельная purchases
    path: 'purchase/:id', component: PurchaseGeneralComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(purchasesRoutes)],
  exports: [RouterModule]
})
export class PurchasesRoutingModule { }
