import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckSendItemComponent } from './check-send-item.component';

describe('CheckSendItemComponent', () => {
  let component: CheckSendItemComponent;
  let fixture: ComponentFixture<CheckSendItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckSendItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckSendItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
