import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CheckSendListComponent} from './check-send-list/check-send-list.component';
import {CheckSendItemComponent} from './check-send-item/check-send-item.component';
import {CheckSendRoutingModule} from './check-send-routing.modules';
import {CheckSendService} from './check-send.service';
import { CheckSendSubmitComponent } from './check-send-submit/check-send-submit.component';

@NgModule({
  declarations: [CheckSendListComponent, CheckSendItemComponent, CheckSendSubmitComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    CheckSendRoutingModule,
    FormsModule
  ],
  providers: [
    CheckSendService
  ]
})
export class CheckSendModule { }
