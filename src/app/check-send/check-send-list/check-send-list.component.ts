import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Assembly} from '../../_models/assemblies.types';
import {DataService} from '../../_services/data.service';
import {OrdersService} from '../../orders/orders.service';
import {Order} from '../../_models/orders.types';

@Component({
  selector: 'app-check-send-list',
  templateUrl: './check-send-list.component.html',
  styleUrls: ['./check-send-list.component.scss']
})
export class CheckSendListComponent implements OnInit, OnDestroy {
  // public
  public orders: Order[] = null;

  //  private
  private checkSendOrdersSub: Subscription;

  //  конструктор
  constructor(private ordersService: OrdersService, private dataService: DataService) {
    //  подписка orders
    this.checkSendOrdersSub = this.dataService.castOrders.subscribe((orders: Order[]) => this.orders = orders);
  }

  //  destroy
  ngOnDestroy() {
    this.checkSendOrdersSub.unsubscribe();
  }

  //  init
  ngOnInit() {
    this.ordersService.getOrdersForCheckSend();
  }
}
