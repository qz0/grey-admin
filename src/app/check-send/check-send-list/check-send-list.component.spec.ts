import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckSendListComponent } from './check-send-list.component';

describe('CheckSendListComponent', () => {
  let component: CheckSendListComponent;
  let fixture: ComponentFixture<CheckSendListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckSendListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckSendListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
