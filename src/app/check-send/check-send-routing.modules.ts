import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckSendListComponent} from './check-send-list/check-send-list.component';
import {CheckSendItemComponent} from './check-send-item/check-send-item.component';
import {CheckSendSubmitComponent} from './check-send-submit/check-send-submit.component';


export const checkSendRoutes: Routes = [
  {path: '', component: CheckSendListComponent},
  {path: 'check_send_item', component: CheckSendItemComponent},
  {path: 'check_send_item/:id', component: CheckSendItemComponent},
  {path: 'submit', component: CheckSendSubmitComponent},
  {path: 'submit/:id', component: CheckSendSubmitComponent },
];

@NgModule({
  imports: [RouterModule.forChild(checkSendRoutes)],
  exports: [RouterModule]
})
export class CheckSendRoutingModule {
}
