import {Injectable} from '@angular/core';
import {DataService} from '../_services/data.service';
import {Assembly} from '../_models/assemblies.types';
import {Subscription} from 'rxjs';

@Injectable()
//  сервис для сборки
export class CheckSendService {
  // public
  public checkSendItem: Assembly = null;
  public checkSendList: Assembly[] = null;

  //  private
  private checkSendItemSub: Subscription;
  private checkSendListSub: Subscription;

  constructor(private dataService: DataService) {
    //  подписка checkSendItemSub
    this.checkSendItemSub = this.dataService.castAssembly.subscribe((checkSendItem: Assembly) => this.checkSendItem = checkSendItem);
    //  подписка checkSendListSub
    this.checkSendListSub = this.dataService.castAssemblies.subscribe((checkSendList: Assembly[]) => this.checkSendList = checkSendList);
  }

  public getCheckSendByID(checkSendID: string) {
    if (this.checkSendList) {
      this.dataService.putAssembly(this.checkSendList.find(checkSendItem => checkSendItem.id === Number(checkSendID)));
    }
  }

  public getCheckSendList() {
    this.dataService.getAssemblies();
  }
}
