import { Component, OnInit, OnDestroy } from '@angular/core';
import {CheckSendService} from '../check-send.service';
import {DataService} from '../../_services/data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Assembly} from '../../_models/assemblies.types';
import {Subscription} from 'rxjs';
import {Item} from '../../_models/items.types';
import {NgForm} from '@angular/forms';
import {OrdersService} from '../../orders/orders.service';
import {Order} from '../../_models/orders.types';
import {imgProto, imgURL} from '../../_models/urls';

@Component({
  selector: 'app-check-send-submit',
  templateUrl: './check-send-submit.component.html',
  styleUrls: ['./check-send-submit.component.scss']
})
export class CheckSendSubmitComponent implements OnInit {
  //  public
  public order: Order = null;
  public imagePath: string = imgProto + imgURL;
  

  //  private
  private OrdersItemSub: Subscription;

  constructor(private ordersService: OrdersService,
              private dataService: DataService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.OrdersItemSub = this.dataService.castOrder.subscribe((order: Order) => {
      this.order = order;
      console.log(this.order)
    });
  }

  //  destroy
  ngOnDestroy() {
    this.OrdersItemSub.unsubscribe();
  }

  //  init
  ngOnInit() {
    const checkSendID = this.activatedRoute.snapshot.paramMap.get('id');

    //  если  передали  id
    if (checkSendID) {
      this.ordersService.getOrderByID(checkSendID);
    } 
  }

}
