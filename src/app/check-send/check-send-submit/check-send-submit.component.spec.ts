import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckSendSubmitComponent } from './check-send-submit.component';

describe('CheckSendSubmitComponent', () => {
  let component: CheckSendSubmitComponent;
  let fixture: ComponentFixture<CheckSendSubmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckSendSubmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckSendSubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
