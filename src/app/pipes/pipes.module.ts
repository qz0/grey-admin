import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LocalizationPipe} from './localization.pipe';
import {LocalizationArrayPipe} from './localization-array.pipe';

@NgModule({
  declarations: [
    LocalizationPipe,
    LocalizationArrayPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LocalizationPipe,
    LocalizationArrayPipe
  ]
})
export class PipesModule { }
