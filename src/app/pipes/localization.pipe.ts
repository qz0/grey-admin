import { Pipe, PipeTransform } from '@angular/core';

import { LocalizedDescription } from '../_models/localization';

@Pipe({
  name: 'localization'
})
//  конвертация ы конкретный язык
export class LocalizationPipe implements PipeTransform {
  //  встроенный интерфейс для конвертации
  public transform(multilanguageItem: LocalizedDescription[]): LocalizedDescription {
    if (multilanguageItem) {
      const singleLanguageItem: LocalizedDescription = multilanguageItem.find(item => item.language === 'RU');
      return singleLanguageItem;
    } else {
      return {
        language: 'RU',
        title: 'заоголовк отсутствует',
        name: 'имя отсутствует',
        description: 'описание отсутствует',
        keywords: ''
      };
    }
  }
}
