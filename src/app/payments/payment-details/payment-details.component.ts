import {Component, OnDestroy, OnInit} from '@angular/core';
import {Payment} from '../../_models/payments.types';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {PaymentsService} from '../payments.service';
import {DataService} from '../../_services/data.service';
import { imgProto, imgURL} from '../../_models/urls';
import {Image} from '../../_models/image.types';

@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.scss']
})
export class PaymentDetailsComponent implements OnInit, OnDestroy {
  //  public
  public payment: Payment = null;
  public currentLangTab = 'EN';
  public imagePath: string = imgProto + imgURL + '/';

  //  private
  private paymentSub: Subscription;

  //  constructor
  constructor(private activatedRoute: ActivatedRoute,
              private paymentsService: PaymentsService,
              private dataService: DataService) {
    //  subscribe to  payment
    this.paymentSub = this.dataService.castPayment.subscribe((payment: Payment) => this.payment = payment);
  }

  //  destroy
  ngOnDestroy() {
    //  unsubscribe
    this.paymentSub.unsubscribe();
  }

  //  init
  ngOnInit() {
    const paymentID = this.activatedRoute.snapshot.paramMap.get('id');

    //  если  передали  id
    if (paymentID) {
      //  получаем существующую доставку
      this.paymentsService.getPaymentByID(paymentID);
    } else {
      //  видимо генерируем новую
      this.paymentsService.getPaymentByID(null);
    }
  }

  public changeImage(eventImage: Image) {
    console.log('EVENT', event);
    console.log('BEFORE BRAND', this.payment);
    this.payment.image = eventImage;
    console.log('AFTER BRAND', this.payment);
  }

  //  включить/выключить  способ оплаты
  public enablePayment(isPaymentEnabled: boolean) {
    console.log('click');
    //  если существует передаем новое значение
    if (this.payment) {
      this.payment.enabled = isPaymentEnabled;
    }
  }

  //  переключение табов языка
  public selectLangTab(lang: string) {
    this.currentLangTab = lang;
  }

  public toString(digit: number): string {
    return String(digit);
  }

  //  изменение payment
  public updatePayment(payment: Payment) {
    console.log(payment);
    this.paymentsService.updatePayment(payment);
  }

}
