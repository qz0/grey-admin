import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Payment} from '../_models/payments.types';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {
  // public
  public payment: Payment = null;
  public payments: Payment[] = null;

  //  private
  private paymentSub: Subscription;
  private paymentsSub: Subscription;

  constructor(private dataService: DataService) {
    //  подписка paymentSub
    this.paymentSub = this.dataService.castPayment.subscribe((payment: Payment) => this.payment = payment);
    //  подписка checkSendListSub
    this.paymentsSub = this.dataService.castPayments.subscribe((payments: Payment[]) => this.payments = payments);
  }

  //  получаем способ оплаты по ID
  public getPaymentByID(paymentID: string) {
    // if (paymentID) {
    //   this.dataService.getPaymentByID(paymentID).subscribe();
    // }
    if (paymentID && this.payments) {
      this.dataService.putPayment(this.payments.find(payment => payment.id === Number(paymentID)));
    }
  }

  //  получаем все способы  оплаты
  public getPayments() {
    //  дергаем сервис
    this.dataService.getPayments();
  }

  //  перемещение клетки способа  оплаты
  //  payment - экземпляр способа оплаты
  public movePayment(payment: Payment) {
    if (payment) {
      this.dataService.changePriorityOfPayment(payment);
    }
  }

  //  изменение способа оплаты
  //  payment - экземпляр способа оплаты
  public updatePayment(payment: Payment) {
    if (payment) {
      this.dataService.updatePayment(payment);
    }
  }
}
