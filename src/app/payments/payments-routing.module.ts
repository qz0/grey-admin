import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PaymentsListComponent} from './payments-list/payments-list.component';
import {PaymentDetailsComponent} from './payment-details/payment-details.component';

export const paymentsRoutes: Routes = [
  {
    //  все purchases
    path: '', component: PaymentsListComponent
  },
  {
    //  отдельная purchases
    path: 'payment/:id', component: PaymentDetailsComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(paymentsRoutes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
