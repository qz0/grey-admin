import { Component, OnInit, Input } from '@angular/core';
import {Payment} from '../../_models/payments.types';
import { imgProto, imgURL} from '../../_models/urls';
import {Router} from '@angular/router';

@Component({
  selector: 'app-payment-card',
  templateUrl: './payment-card.component.html',
  styleUrls: ['./payment-card.component.scss']
})
export class PaymentCardComponent implements OnInit {
  //  public
  public imagePath: string = imgProto + imgURL;
  @Input() public payment: Payment = null;
  @Input() public paymentIndex: number = null;

  //  constructor
  constructor(private router: Router) { }

  //  init
  ngOnInit() {
    console.log('Card Payment: ', this.payment);
  }

  //  change Payment
  public changePayment(payment: Payment) {
    console.log(payment);
    //  если доставка не пустая
    if (payment) {
      if (payment.id) {
        this.router.navigate(['payments/payment', payment.id]);
      } else {
        this.router.navigate(['payments/payment']);
      }
    } else {
      this.router.navigate(['payments/payment']);
    }
  }

  public toString(digit: number): string {
    return String(digit);
  }
}
