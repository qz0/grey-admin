import {Component, OnDestroy, OnInit} from '@angular/core';
import {Payment} from '../../_models/payments.types';
import {Subscription} from 'rxjs';
import {PaymentsService} from '../payments.service';
import {DataService} from '../../_services/data.service';
import {DndDropEvent} from 'ngx-drag-drop';

@Component({
  selector: 'app-payments-list',
  templateUrl: './payments-list.component.html',
  styleUrls: ['./payments-list.component.scss']
})
export class PaymentsListComponent implements OnInit, OnDestroy {
  //  public
  public payments: Payment[] = null; // способы оплаты

  //  private
  private paymentsSub: Subscription = null;

  //  конструктор
  constructor(private paymentsService: PaymentsService, private dataService: DataService) {
    //  подписываемся на  доставки
    this.paymentsSub = this.dataService.castPayments.subscribe((payments: Payment[]) => this.payments = payments);
  }

  //  Destroy
  ngOnDestroy() {
    //  отписываемся  от  доставок
    this.paymentsSub.unsubscribe();
  }

  //  инитор
  ngOnInit() {
    this.paymentsService.getPayments();
    console.log('Payment', this.payments);
  }

  public onDraggableMoved(event) {
    console.log(event);
  }

  public onDragStart(event) {
    console.log('Дроп начали', event);
  }

  public onDragEnd(event) {
    console.log('Дроп закончили', event);
  }

  public onDragCanceled(event) {
    console.log('Дроп отменили', event);
  }

  //  part Drop
  public onDragover(event: DragEvent) {

    console.log('dragover', JSON.stringify(event, null, 2));
  }

  //  Сброшен
  public onDrop(event: DndDropEvent) {
    //  проверка на пустоту
    if (event && event.data && event.data.id && event.index != null) {
      //  перемещаем доставку на новую позицию
      const payment: Payment = event.data;
      if (event.index + 1 !== payment.priority && event.index !== payment.priority) {
        //  если перемещаем индекс надо уменьшать на единицу
        if (payment.priority < event.index) {
          event.index = event.index - 1;
        }
        //  отлавливаем самый нижний индекс
        if (event.index === this.payments.length) {
          payment.priority = this.payments.length;
        } else {
          payment.priority = event.index + 1;
        }
        //  дергаем функцию
        this.paymentsService.movePayment(payment);
      }
    }
  }

  //  создание новой доставки для теста
  public createPayment() {
    if (this.payments) {
      this.payments.push({
        localizedDescriptions: [
          {
            description: 'Описание какой-то оплаты 1',
            id: 1,
            keywords: '',
            language: 'RU',
            name: 'Оплата 1',
            title: 'Какая-то оплата 1'
          },
          {
            description: 'Description of some payment 1',
            id: 2,
            keywords: '',
            language: 'EN',
            name: 'Payment 1',
            title: 'ome payment 1'
          }
        ],
        image: {
          id: 1,
          name: 'image of payment 1',
          url: 'url',
          priority: 0,
          options: null,  //  options
          optionsID: null,
          type: null, //  type
          typeID: null,
        },
        discounts: [],
        enabled: false,
        priority: null,
      });
    }
  }

}
