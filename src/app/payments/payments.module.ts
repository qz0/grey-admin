import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentsRoutingModule } from './payments-routing.module';
import { PaymentsListComponent } from './payments-list/payments-list.component';
import { PaymentDetailsComponent } from './payment-details/payment-details.component';
import { PaymentsService } from './payments.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PaymentCardComponent } from './payment-card/payment-card.component';
import { PipesModule } from '../pipes/pipes.module';
import { DndModule } from 'ngx-drag-drop';
import {FormsModule} from '@angular/forms';
import {ImagesModule} from '../images/images.module';

@NgModule({
  declarations: [
    PaymentsListComponent,
    PaymentDetailsComponent,
    PaymentCardComponent
  ],
  imports: [
    CommonModule,
    PaymentsRoutingModule,
    FontAwesomeModule,
    PipesModule,
    DndModule,
    FormsModule,
    ImagesModule
  ],
  providers: [
    PaymentsService
  ]
})
export class PaymentsModule {
}
