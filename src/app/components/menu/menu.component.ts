import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertifyService} from '../../_services/alertify.service';
import {DataService} from '../../_services/data.service';
import {Menu} from '../../_models/menu.types';
import {Currency} from '../../_models/currencies.types';
import {Subscription} from 'rxjs';
import {AuthService} from '../../_services/auth.service';
import {imgURL, imgProto} from '../../_models/urls';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

//  компонент меню
export class MenuComponent implements OnInit, OnDestroy {
  //  public
  //  меню
  public logoPath: string = imgProto + imgURL + '/logo.svg';
  // public logoPath: string = imagesURL + 'logo.svg';
  public menu: Menu[] = null;
  //  фактор логина
  public isLogon: boolean = null;

  //  private
  //  subscriptions
  private menuSubscription: Subscription;
  //  subscriptions
  private isLoginSubscription: Subscription;

  constructor(private alertify: AlertifyService, private dataService: DataService, private authService: AuthService) { }

  //  дестроер
  ngOnDestroy() {
    //  отписки
    this.menuSubscription.unsubscribe();
    this.isLoginSubscription.unsubscribe();
  }

  //  инитор
  ngOnInit() {
    // console.log('menu: ', this.menu);
    //  подписка menu
    this.menuSubscription = this.dataService.castMenu.subscribe((menu: Menu[]) => {
      // console.log(menu);
      this.menu = menu;
    });
    //  подписка на логин
    this.isLoginSubscription = this.authService.castIsLogon.subscribe((isLogon: boolean) => {
      this.isLogon = isLogon;
    });
    //  дергаем обновление меню
    this.dataService.getMenu();
  }

  //  тестовая функция
  public testClick() {
    // console.log('test');
    this.alertify.warning('test');
  }

}
