import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  //  объявление формы
  public loginForm: FormGroup;

  constructor(private authService: AuthService, //  сервис авторизации
              private formBuilder: FormBuilder, //  форм билдер
              private router: Router,  //  Роутер
  ) {
  }

  //  инитор
  ngOnInit() {
    //  принудительный разлогон
    this.logout();
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      userPassword: ['', [Validators.required]],
    });
  }

  //  сброс формы и переход на главную
  private onSuccess(): void {
    this.loginForm.reset();
    this.router.navigate(['/']);
  }

  //  регистрация нового пользователя
  //  login - логин
  //  password  - пароль
  public login(username, password) {
    //  если не пустые - дергаем сервис логина
    if (username && password) {
      this.authService.login(username, password);
      //  TODO: если нет ошибок -выполнить onSuccess
      this.onSuccess();
    }
  }

  //  разлогон
  public logout() {
    //  дергаем сервис
    this.authService.logout(null);
  }

}
