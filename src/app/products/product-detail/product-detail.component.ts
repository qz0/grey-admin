import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ProductsService} from '../products.service';
import {DataService} from '../../_services/data.service';
import {Product} from '../../_models/products.types';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  //  public
  public menuLink: string = 'description';
  public product: Product = null; //  продукт
  public showDeleteModal: boolean = false; // видимость модального окна подтверждения удаления

  //  private
  private productSub: Subscription;
  // private productsSub: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private productsService: ProductsService, private dataService: DataService, private router: Router) {
    //  подписываемся на асинхроны
    this.productSub = this.dataService.castProduct.subscribe((product: Product) => this.product = product);
  }
  //  дестройер
  public ngOnDestroy() {
    //  отписываемся от подписок
    this.productSub.unsubscribe();
    // this.productsSub.unsubscribe();
    // this. optionTypesSubscription.unsubscribe();
  }
  //  инитор
  public ngOnInit() {
    //  получаем brand
    const productID = this.activatedRoute.snapshot.paramMap.get('id');
    //  получаем типы опций
    // this.produsService.getAllOptionTypes();
    console.log(productID);
    //  если передали id
    if (productID) {
      this.productsService.getProductByID(productID);
    } else {
      this.productsService.getProductByID(null);
    }
  }

  //  работа с верхним меню
  public selectMenu(menu: string) {
    this.menuLink = menu;
  }

  //  сохранение изменений в продукте
  public updateProduct() {
    console.log('updateProduct: 1', this.product);
    this.productsService.updateProduct(this.product);
  }

  // показать модальное окно удаления продукта
  public showProductModal() {
    this.showDeleteModal = true;
  }

  // скрыть модальное окно удаления продукта
  public hideProductModal() {
    this.showDeleteModal = false;
  }

  // удалить продукт
  public deleteProductModal(productId: string) {
    this.showDeleteModal = false;
    this.dataService.deleteProduct(productId);
    this.router.navigate(['/products']);
  }

}
