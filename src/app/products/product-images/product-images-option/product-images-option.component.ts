import {Component, Input, OnInit} from '@angular/core';
import {Product, ProductImage} from '../../../_models/products.types';
import {ProductsService} from '../../products.service';
import {DataService} from '../../../_services/data.service';
import {Subscription} from 'rxjs';
import {Image} from '../../../_models/image.types';
import {imgProto, imgURL} from '../../../_models/urls';
import {Option, OptionValue} from '../../../_models/options.types';

@Component({
  selector: 'app-product-images-option',
  templateUrl: './product-images-option.component.html',
  styleUrls: ['./product-images-option.component.scss']
})
export class ProductImagesOptionComponent implements OnInit {

  // public
  @Input() productImage: ProductImage = null;
  @Input() productImageIndex: number = null;
  public product: Product = null; //  продукт
  public imagePath: string = imgProto + imgURL + 'products/';
  public optionsImagePath: string = imgProto + imgURL;
  public selectedColorId: number = null; // id выбранной опция цвета
  public selectedSizeId: number; // id выбранной опция размера

  //  private
  private productSub: Subscription;

  constructor(private productsService: ProductsService, private dataService: DataService) {
    //  подписываемся на продукты
    this.productSub = this.dataService.castProduct.subscribe((product: Product) => this.product = product);
  }

  ngOnInit() {
  }

  //  дестройер
  public ngOnDestroy() {
    //  отписываемся от подписок
    this.productSub.unsubscribe();
  }

  //  изменение иконки
  //  eventImage - новая картинка
  public changeImage(eventImage: Image) {
    this.product.productImages[this.productImageIndex].image = eventImage;
  }


  //  изменить опции картинки
  public changeImageOptions(option: Option, optionValue: OptionValue) {
    //  если все прилетело
    if (optionValue && this.product && this.product.productImages && this.productImageIndex < this.product.productImages.length) {
      //  ищем категорию в продукте
      if (this.product.productImages[this.productImageIndex].optionValues &&
        this.product.productImages[this.productImageIndex].optionValues.length) {
        //  если есть что перебирать
        option.optionValues.forEach((ov: OptionValue) => {
          //  ищем индекс значения опции в списке значений базовой опции
          const ovIndex: number = this.product.productImages[this.productImageIndex].optionValues
            .findIndex((optV: OptionValue) => optV.id === ov.id);
          //  если найдено - убиваем
          if (ovIndex !== -1) {
            this.product.productImages[this.productImageIndex].optionValues.splice(ovIndex, 1);
          }
        });
      }
      //  добавляем новую опцию в список
      this.product.productImages[this.productImageIndex].optionValues.push(optionValue);
      //  публикуем продукт
      this.productsService.putProduct(this.product);
      }
  }

  //  подсвечивать опцию или нет
  public isOptionSelected(optionValue: OptionValue): boolean {
    //  если существует
    if (optionValue && this.product && this.product.productImages && this.productImageIndex < this.product.productImages.length
      && this.product.productImages[this.productImageIndex].optionValues
      && this.product.productImages[this.productImageIndex].optionValues.length) {
      //  ищем в списке
      if (this.product.productImages[this.productImageIndex].optionValues
        .findIndex((ov: OptionValue) => ov.id === optionValue.id) !== -1) {
        return true;
      }
    }
    return false;
  }

  // delete image from product images
  public deleteImage() {
    this.product.productImages.splice(this.productImageIndex, 1);
  }

  // select front images
  public selectFrontImage() {
    this.product.imageFront = this.product.productImages[this.productImageIndex].image;
    this.product.imageFrontID = this.product.productImages[this.productImageIndex].image.id;
  }

  // select back images
  public selectBackImage() {
    this.product.imageBack = this.product.productImages[this.productImageIndex].image;
    this.product.imageBackID = this.product.productImages[this.productImageIndex].image.id;
  }

}
