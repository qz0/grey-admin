import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductImagesOptionComponent } from './product-images-option.component';

describe('ProductImagesOptionComponent', () => {
  let component: ProductImagesOptionComponent;
  let fixture: ComponentFixture<ProductImagesOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductImagesOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductImagesOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
