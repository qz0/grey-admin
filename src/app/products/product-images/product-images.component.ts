import {Component, Input, OnInit} from '@angular/core';
import {Product, ProductImage} from '../../_models/products.types';
import {ProductsService} from '../products.service';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';
import {Image} from '../../_models/image.types';

@Component({
  selector: 'app-product-images',
  templateUrl: './product-images.component.html',
  styleUrls: ['./product-images.component.scss']
})
export class ProductImagesComponent implements OnInit {
  // public
  public product: Product = null; //  продукт
  public imageOptions: boolean = false;

  //  private
  private productSub: Subscription;

  constructor(private productsService: ProductsService, private dataService: DataService) {
    //  подписываемся на продукты
    this.productSub = this.dataService.castProduct.subscribe((product: Product) => this.product = product);
  }

  //  дестройер
  public ngOnDestroy() {
    //  отписываемся от подписок
    this.productSub.unsubscribe();
  }

  ngOnInit() {
    console.log('Product: ', this.product);
  }

  public addProductImage() {
    // если продукт есть и есть куда добавлять
    if (this.product && this.product.productImages) {
      // формируем новый массив
      const newProductImage: ProductImage = {
        productID: this.product.id,
        image: null,
        imageID: 0,
        optionValues: [],
      };
      // добавляем в массив
      this.product.productImages.push(newProductImage);
    }
    // публикуем новый продукт
    this.dataService.putNewProduct(this.product);
  }

  //  Изменение картинок
  //  изменение фона
  //  eventImage - новая картинка
  public changeImageFront(eventImage: Image) {
    this.product.imageFront = eventImage;
  }

  //  изменение иконки
  //  eventImage - новая картинка
  public changeImageBack(eventImage: Image) {
    this.product.imageBack = eventImage;
  }

  public showImageOptions() {
    this.imageOptions = !this.imageOptions;
  }

}
