import { Component, Input, OnInit } from '@angular/core';
import { Product, ProductAvailability, ProductVisibility } from '../../_models/products.types';
import { Subscription } from 'rxjs';
import { ProductsService } from '../products.service';
import { DataService } from '../../_services/data.service';
import { Brand, Manufacturer } from '../../_models/brands.types';
import { Category } from '../../_models/categories.types';
import { LocalizedText } from '../../_models/localization';
import { LocalizedComponent } from 'src/app/settings/localized/localized.component';

@Component({
  selector: 'app-product-description',
  templateUrl: './product-description.component.html',
  styleUrls: ['./product-description.component.scss']
})
export class ProductDescriptionComponent implements OnInit {
  // public
  public brands: Brand[] = null; //  список брэндов
  public categoriesAll: Category[] = null; //  список всех категорий
  public currentLangTab = 'EN';
  public manufacturers: Manufacturer[] = null; //  список производителей
  public product: Product = null; //  продукт
  public productAvailabilityStatuses: ProductAvailability[] = null; //  варианты заказа в магазинах
  public productVisibilityStatuses: ProductVisibility[] = null; //  варианты отображения в магазинах

  //  private
  private brandsSub: Subscription;
  private categoriesALLSub: Subscription;
  private manufacturersSub: Subscription;
  private productSub: Subscription;
  private productAvailabilitiesStatusesSub: Subscription;
  private productVisibilitiesStatusesSub: Subscription;


  constructor(private productsService: ProductsService, private dataService: DataService) {
    //  подписываемся асинхрон
    //  brand
    this.brandsSub = this.dataService.castBrands.subscribe((brands: Brand[]) => {
      this.brands = brands;
      // console.log('Product brand: ', brands);
    });
    //  categories
    this.categoriesALLSub = this.dataService.castCategoriesAll
      .subscribe((categoriesAll: Category[]) => {
        this.categoriesAll = categoriesAll;
        console.log('CategoriesALL: ', categoriesAll);
      });
    //  manufacturers
    this.manufacturersSub = this.dataService.castManufacturers
      .subscribe((manufacturers: Manufacturer[]) => this.manufacturers = manufacturers);
    //  product
    this.productSub = this.dataService.castProduct.subscribe((product: Product) => {
      this.product = product;
      console.log(product);
    });
    this.productAvailabilitiesStatusesSub = this.dataService.castProductAvailabilities
      .subscribe((productAvailabilityStates: ProductAvailability[]) => this.productAvailabilityStatuses = productAvailabilityStates);
    this.productVisibilitiesStatusesSub = this.dataService.castProductVisibilities
      .subscribe((productVisibilityStates: ProductVisibility[]) => this.productVisibilityStatuses = productVisibilityStates);
  }

  //  дестройер
  public ngOnDestroy() {
    //  отписываемся от подписок
    this.brandsSub.unsubscribe();
    this.categoriesALLSub.unsubscribe();
    this.manufacturersSub.unsubscribe();
    this.productSub.unsubscribe();
    this.productAvailabilitiesStatusesSub.unsubscribe();
    this.productVisibilitiesStatusesSub.unsubscribe();
  }

  ngOnInit() {
    this.dataService.getBrands();
    this.dataService.getManufacturers();
    this.dataService.getProductAvailabilities();
    this.dataService.getProductVisibilities();
    this.dataService.getCategories();
  }

  //  поменяли статус доступности продукта
  public changeAvailability(availability: ProductAvailability) {
    this.product.productAvailability.id = availability.id;
  }
  //  поменяли брэнд
  public changeBrand(brandID: number) {
    this.product.brand = this.brands.find(brand => brand.id === brandID);
    this.product.brandID = this.product.brand.id;
  }
  //  поменяли категорию
  public changeCategory(category: Category) {
    if (category) {
      //  ищем категорию в продукте
      const categoryIndex = this.product.categories.findIndex((cat: Category) => cat.id === category.id);
      if (categoryIndex === -1) {
        //  не нашли
        //  добавляем категорию
        this.product.categories.push(category);
      } else {
        // нашли
        //  удаляем категорию
        this.product.categories.splice(categoryIndex, 1);
      }
      //  публикуем
      // this.productsService.putProduct(this.product);
    }
  }
  //  поменяли тип
  public changeManufacturer(manufacturerID: number) {
    this.product.manufacturer = this.manufacturers.find(manufacturer => manufacturer.id === manufacturerID);
    this.product.manufacturerID = this.product.manufacturer.id;
  }
  //  меняем приоритет у опции
  public changePriority(priority: number) {
    //  если передали корректный параметр - меняем значение
    if (priority && priority > 0 && priority <= 10) {
      this.product.priority = Number(priority);
    }
  }
  //  поменяли тип
  public changeVisibility(visibility: ProductVisibility) {
    //  записываем новый ид
    this.product.productVisibilityID = visibility.id;
  }

  //  проверка категории на принадлежность
  public isProductInCategory(category: Category): boolean {
    if (this.product.categories.findIndex((prodCategory: Category) => prodCategory.id === category.id) !== -1) {
      return true;
    } else {
      return false;
    }
  }

  //  переключение табов языка
  public selectLangTab(lang: string) {
    this.currentLangTab = lang;
  }

  // для кнопочки добавления Material
  addMaterial() {
    // const materialRu: LocalizedText = { language: 'RU' }
    // const materialEN: LocalizedText = { language: 'EN' }
    // this.product.localizedMaterial.push(materialRu)
    // this.product.localizedMaterial.push(materialEN)
    // this.product.localizedMaterial.sort(this.dynamicSort('language'));
  }

  // для кнопочки удаления Material
  deleteMaterial(index: number) {
 //    var fl= this.product.localizedMaterial.length/2
 //    if (index>=fl){
 //      this.product.localizedMaterial.splice(index,1)
 //      this.product.localizedMaterial.splice(index-fl,1)
 //    }else{
 //      this.product.localizedMaterial.splice(index+fl,1)
 //      this.product.localizedMaterial.splice(index,1)
 //    }
 //    this.product.localizedMaterial.sort(this.dynamicSort("language"));
 }

  // для кнопочки добавления feature
  addFeature() {
    // const featureRu: LocalizedText = { language: "RU" }
    // const featureEN: LocalizedText = { language: "EN" }
    // this.product.localizedFeatures.push(featureRu)
    // this.product.localizedFeatures.push(featureEN)
    // this.product.localizedFeatures.sort(this.dynamicSort('language'));
  }

  // для кнопочки удаления feature
  deleteFeature(index: number) {
    // let fl= this.product.localizedFeatures.length/2
    // if (index>=fl){
    //   this.product.localizedFeatures.splice(index,1)
    //   this.product.localizedFeatures.splice(index-fl,1)
    // }else{
    //   this.product.localizedFeatures.splice(index+fl,1)
    //   this.product.localizedFeatures.splice(index,1)
    // }
    // this.product.localizedFeatures.sort(this.dynamicSort("language"));
  }

  //  вспомогательная функция динамической сортировки
   private dynamicSort(property) {
    // let sortOrder = 1;
    // if(property[0] === "-") {
    //     sortOrder = -1;
    //     property = property.substr(1);
    // }
    // return function (a,b) {
    //     var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
    //     return result * sortOrder;
    // }
  }

}
