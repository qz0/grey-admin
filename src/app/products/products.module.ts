import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProductsService } from './products.service';
import { FormsModule } from '@angular/forms';
import { ProductCardComponent } from './product-card/product-card.component';
import {PipesModule} from '../pipes/pipes.module';
import { ProductDescriptionComponent } from './product-description/product-description.component';
import { ProductOptionsComponent } from './product-options/product-options.component';
import { ProductImagesComponent } from './product-images/product-images.component';
import {ProductOptionsAddCardComponent} from './product-options/product-options-add-card/product-options-add-card.component';
import {ProductOptionsOptionCardComponent} from './product-options/product-options-option-card/product-options-option-card.component';
import {ImagesModule} from '../images/images.module';
import {NgSelectModule} from '@ng-select/ng-select';
import { ItemCardComponent } from './product-card/item-card/item-card.component';
import {ProductImagesOptionComponent} from './product-images/product-images-option/product-images-option.component';
import {DirectivesModule} from '../__directives/directives.module';

@NgModule({
  declarations: [
    ProductsListComponent,
    ProductDetailComponent,
    ProductCardComponent,
    ProductDescriptionComponent,
    ProductOptionsComponent,
    ProductOptionsAddCardComponent,
    ProductOptionsOptionCardComponent,
    ProductImagesComponent,
    ProductImagesOptionComponent,
    ItemCardComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FontAwesomeModule,
    FormsModule,
    PipesModule,
    ImagesModule,
    NgSelectModule,
    DirectivesModule
  ],
  providers: [
    ProductsService,
  ]
})
export class ProductsModule {
}
