import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Product} from '../_models/products.types';
import {Manufacturer} from '../_models/brands.types';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private dataService: DataService) { }

  //  получаем продукт по ИД
  public getProductByID(productID?: string) {
    // дергаем сервис данных
    this.dataService.getProductByID(productID);
  }

  //  получаем продукты
  public getProducts() {
    // дергаем сервис данных
    this.dataService.getProducts();
  }

  //  получаем статусы доступности продуктов
  public getProductAvailabilities() {
    // дергаем сервис данных
    this.dataService.getProductAvailabilities();
  }

  //  получаем статусы видимости продуктов
  public getProductVisibilities() {
    // дергаем сервис данных
    this.dataService.getProductVisibilities();
  }

  //  получаем продукты с конкретным производителем
  //  manufacturer - экземпляр производителя
  public getProductsByManufacturer(manufacturer: Manufacturer) {
    // дергаем сервис данных
    this.dataService.getProducts();
  }

  //  публикация изменений в продукте
  public putProduct(product: Product) {
    if (product) {
      this.dataService.putNewProduct(product);
    }
  }

  // инитим продукты
  public updateProduct(product: Product) {
    // дергаем сервис данных (проверяем новый ли продукт)
    if (product && product.id) {
      this.dataService.updateProduct(product);
    } else {
      this.dataService.createProduct(product);
    }
  }
}
