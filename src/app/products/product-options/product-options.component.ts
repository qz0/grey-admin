import {Component, OnDestroy, OnInit} from '@angular/core';
import {Option} from '../../_models/options.types';
import {Product, ProductOption} from '../../_models/products.types';
import {ProductsService} from '../products.service';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-product-options',
  templateUrl: './product-options.component.html',
  styleUrls: ['./product-options.component.scss']
})
export class ProductOptionsComponent implements OnInit, OnDestroy {
  // public
  public product: Product = null; //  продукт
  public options: Option[] = []; //  продукт
  public productOptions: ProductOption[] = []; //   технический массив для опций продукта
  public currentOptionSelect;
  public optionSelectVisible: boolean = false;


  //  private
  private productSub: Subscription;
  private optionsSub: Subscription;

  constructor(private productsService: ProductsService, private dataService: DataService) {
    //  подписываемся на продукты
    this.productSub = this.dataService.castProduct.subscribe((product: Product) => {
      this.product = product;
    });
    //  подписываемся на опции
    this.optionsSub = this.dataService.castOptions.subscribe((options: Option[]) => {
      this.options = options;
    });
  }

  //  дестройер
  ngOnDestroy() {
    //  отписываемся от подписок
    this.productSub.unsubscribe();
    this.optionsSub.unsubscribe();
  }

  //  initor
  ngOnInit() {
  }

  //  добавить сет опций
  public addOptionsSet() {
  }

}
