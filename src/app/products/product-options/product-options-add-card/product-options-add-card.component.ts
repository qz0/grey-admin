import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Product, ProductOption} from '../../../_models/products.types';
import {Subscription} from 'rxjs';
import {ProductsService} from '../../products.service';
import {DataService} from '../../../_services/data.service';
import {Option, OptionValue} from '../../../_models/options.types';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'product-options-add-card',
  templateUrl: './product-options-add-card.component.html',
  styleUrls: ['./product-options-add-card.component.scss']
})

//  прототип для карточки добавления
export class ProductOptionsAddCardComponent {
  @Input() orderOfcard: number = null;  //  порядок карточки в списке
  // public
  public product: Product = null; //  продукт
  public options: Option[] = null; //  варианты опций
  public option: Option = null; //  варианты опций
  public optionValues: OptionValue[] = null; //  варианты опций
  public isShownList: boolean = false; //  показывать список опций
  public isShownOptionValues: boolean = false; //  показывать список опций
  public optionList: Array<any> = null;

  //  private
  private optionsSub: Subscription; //  подписка на опции
  private productSub: Subscription; //  подписка на продукт

  constructor(private productsService: ProductsService, private dataService: DataService) {
    //  оформляем подписки
    //  подписываемся на опции
    this.optionsSub = this.dataService.castOptions.subscribe((options: Option[]) => {
      this.options = options;
      //  подписываемся на продукты
      this.productSub = this.dataService.castProduct.subscribe((product: Product) => {
        this.product = product;
        console.log('before getOption');
        this.getOptionList();
        console.log('after getOption');
        // console.log('product', product);
      });
    });
  }

  //  дестройер
  ngOnDestroy() {
    //  отписываемся от существующих подписок
    this.optionsSub.unsubscribe();
    this.productSub.unsubscribe();
  }

  //  добавить продуктовую опцию к продукту
  public addProductOption(option: Option) {
    //  делаем копию продукта и обнуляем у нее продукт опшнс чтобы не было циклической конвертации
    const newProduct: Product = Object.assign({}, this.product);
    // newProduct.productOptions = null;
    //  создали новый набор опций
    const productOption: ProductOption = {
      product: this.product,
      productID: this.product.id,
      option,
      optionID: option.id,
      optionValues: []
    };

    //  добавили новый набор опций
    this.product.productOptions.push(productOption);
    // this.product.productOptions = [...this.product.productOptions, productOption];

    //  опубликовали новую версию продукта
    this.dataService.putNewProduct(this.product);

    //  убрали опцию из списка
    this.getOptionList(option);

    this.option = null;

    this.isShownList = false;
  }

  //  сделать видимым список выбора опции
  public showOptionsList() {
    this.isShownList = true;
  }

  //  показать список занчений опций
  public showOptionValues() {
    this.isShownOptionValues = true;
  }

  private getOptionList(option?: Option) {
    //  если передали опцию - просто удаляем ее из глоабльного списка
    if (option) {
      const index = this.optionList.indexOf(option);
      if (index !== -1) {
        this.optionList.splice(index, 1);
      }
    } else {
      this.optionList = [...this.options];
    //   если нет - это первый запуск и надо выбрать лишние опции
      if (this.product && this.product.productOptions && this.product.productOptions.length) {
        //  перебираем все элементы существующего массива опций в продукте
        this.product.productOptions.forEach((productOptions: ProductOption) => {
          //  если опция в массиве опшн лист найдена ее убирают
          const index = this.optionList.findIndex((tempOption: Option) => tempOption.id === productOptions.optionID);
          // const index = this.optionList.indexOf(productOptions.option);
          if (index !== -1) {
            this.optionList.splice(index, 1);
          }
        });
      } else {
      }
    }
  }
}
