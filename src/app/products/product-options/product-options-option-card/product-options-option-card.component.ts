import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Product, ProductOption} from '../../../_models/products.types';
import {Subscription} from 'rxjs';
import {ProductsService} from '../../products.service';
import {DataService} from '../../../_services/data.service';
import {Option, OptionValue} from '../../../_models/options.types';
import {Category} from '../../../_models/categories.types';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'product-options-option-card',
  templateUrl: './product-options-option-card.component.html',
  styleUrls: ['./product-options-option-card.component.scss']
})

//  прототип для карточки добавления
export class ProductOptionsOptionCardComponent {
  // public
  public options: Option[] = null; //  варианты опций
  public product: Product = null; //  продукт
  @Input() public productOption: ProductOption = null; //  продукт опшн
  @Input() public productOptionIndex: number = null; //  номер опции

  //  private
  private optionsSub: Subscription; //  подписка на опции
  private productSub: Subscription;

  constructor(private productsService: ProductsService, private dataService: DataService) {
    //  оформляем подписки
    //  подписываемся на опции
    this.optionsSub = this.dataService.castOptions.subscribe((options: Option[]) => {
      this.options = options;
      console.log('options:', this.options);
      //  подписываемся на продукты
      this.productSub = this.dataService.castProduct.subscribe((product: Product) => {
        this.product = product;
        console.log(this.product);
      });
    });
  }

  ngOnInit() {
    //
    console.log('productOption: ', this.productOption);
  }

  //  поменяли категорию
  public changeOptionValue(optionValue: OptionValue) {
    if (optionValue && this.product && this.product.productOptions) {
      //  ищем категорию в продукте
      const optionValueIndex = this.product.productOptions[this.productOptionIndex].optionValues
        .findIndex((ov: OptionValue) => ov.id === optionValue.id);
      if (optionValueIndex === -1) {
        //  не нашли
        //  добавляем категорию
        this.product.productOptions[this.productOptionIndex].optionValues.push(optionValue);
      } else {
        // нашли
        //  удаляем категорию
        this.product.productOptions[this.productOptionIndex].optionValues.splice(optionValueIndex, 1);
      }
      //  публикуем
      // this.productsService.putProduct(this.product);
    }
  }

  public isOptionSelected(testedOption: OptionValue): boolean {
    //  если опция в массиве опшн лист найдена ее убирают
    const index = this.productOption.optionValues.findIndex((optionValue: OptionValue) => optionValue.id === testedOption.id);
    // const index = this.optionList.indexOf(productOptions.option);
    if (index !== -1) {
      return true;
    }
    return false;
  }

  //  удаление опции
  public removeProductOption() {
    console.log('Try');
    //  если не пустая
    if (this.product && this.product.productOptions && this.productOptionIndex < this.product.productOptions.length) {
      //  удаляем опцию по индексу
      this.product.productOptions.splice(this.productOptionIndex, 1);
      //  публикуем
      this.productsService.putProduct(this.product);
      console.log('Success', this.product);
    }
  }

}
