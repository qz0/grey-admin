import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

//  экспортируем маршруты
export const productsRoutes: Routes = [
  {
    //  все products
    path: '', component: ProductsListComponent
  },
  {
    //  отдельный product
    path: 'product', component: ProductDetailComponent
  },
  {
    //  отдельный product
    path: 'product/:id', component: ProductDetailComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(productsRoutes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
