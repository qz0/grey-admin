import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../../../_models/items.types';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {
  //  public
  @Input() item: Item = null; //  item для отображения

  constructor() { }

  ngOnInit() {
  }

}
