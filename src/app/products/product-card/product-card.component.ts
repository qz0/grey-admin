import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Product} from '../../_models/products.types';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductsService} from '../products.service';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';
import {Brand} from '../../_models/brands.types';
import {imgProto, imgURL} from '../../_models/urls';
import {Animations} from '../../Animations';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
  animations: [Animations.fadeInOut]
})
export class ProductCardComponent implements OnInit {
  //  public
  public imagePath: string = imgProto + imgURL + 'products/';
  public productItems: boolean = false; // product options visible
  @Input() public product: Product = null;

  //  private
  private productSub: Subscription = null;

  constructor(private activatedRoute: ActivatedRoute,
              private productsService: ProductsService,
              private dataService: DataService,
              private router: Router) {
    //  подписываемся на продукты
    // this.productSub = this.dataService.castProduct.subscribe((product: Product) => {this.product = product});
  }

  //  init
  ngOnInit() {
    console.log('Product: ', this.product);
  }

  //  create / update product
  public updateProduct(product?: Product) {
    console.log('updateProduct: ', product);
    //  проверяем, что передали
    if (!product) {
      //  создаем нового производителя
      this.router.navigate(['/products/product']);
    } else {
      //  правим существующего производителя
      //  TODO: router.navigate(['brand', {id: brandId}])
      this.router.navigate(['/products/product', product.id]);
    }
  }

    // open product details block
    public openProductItems() {
      this.productItems = !this.productItems;
    }  

}
