import { Component, OnInit } from '@angular/core';
import { Product } from '../../_models/products.types';
import { ProductsService } from '../products.service';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import {Delivery} from '../../_models/deliveries.types';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  //  public
  public  products: Product[] = null; //  продукты
  public  textForFiltering: string = ''; //  фильтр

  //  private
  private productsSub: Subscription = null;

  constructor(private productsService: ProductsService, private dataService: DataService) {
    //  подписываемся на продукты
    this.productsSub = this.dataService.castProducts.subscribe((products: Product[]) => {
      this.products = products;
      console.log('products: ', products);
    });
  }

  //  destroy
  public ngOnDestroy() {
    //  отписываемся от продуктов
    this.productsSub.unsubscribe();
  }

  //  init
  public ngOnInit() {
  //  дергаем сервис получение продуктов
    this.productsService.getProducts();
  }

  // use fiter
  public filterProducts(textForFiltering?: string) {
    //  проверяем что передали
    if (textForFiltering && textForFiltering.trim().length) {
    //  фильтр активирован
      this.textForFiltering = textForFiltering.trim();
      console.log('Ищем:', textForFiltering);
    } else {
      this.textForFiltering = '';
      console.log('Ничего не ищем');
    }

  }

  //  переходим к карточке продукта
  //  productID - идентификатор продукта
  public goToProduct(productID: number) {
    if (productID) {
      console.log(productID);
    }
  }

}
