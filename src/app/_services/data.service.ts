import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Currency, CurrencyServer } from '../_models/currencies.types';
import {
  apiProto,
  baseUrl,
  urlBrand, urlCategory, urlCountry,
  urlCurrency,
  urlDelivery,
  urlImage, urlIncoming,
  urlLanguage,
  urlLocalizedKeyword,
  urlManufacturer,
  urlOption,
  urlOptionType,
  urlOrder,
  urlOrderPriorities,
  urlOrderStatuses,
  urlPayment,
  urlProduct, urlProductAvailability, urlProductVisibility, urlRegion,
  urlRole,
  urlLog
} from '../_models/urls';
import { ConvertService } from './convert.service';
import { Menu } from '../_models/menu.types';
import { Brand, BrandServer, Manufacturer, ManufacturerServer } from '../_models/brands.types';
import { Language, LanguageServer, LocalizedKeyword, LocalizedKeywordServer } from '../_models/localization';
import { Role, RoleServer } from '../_models/users.types';
import { Option, OptionServer, OptionsSet, OptionType, OptionTypeServer, OptionValue } from '../_models/options.types';
import { Image, ImageServer, ImageType, ImageTypeServer } from '../_models/image.types';
import { GenerateService } from './generate.service';
import { Assembly } from '../_models/assemblies.types';
import { Delivery, DeliveryServer } from '../_models/deliveries.types';
import { Category, CategoryServer } from '../_models/categories.types';
import { Customer, CustomerRank } from '../_models/customers.types';
import {
  Address,
  AddressServer,
  Country,
  CountryServer,
  CountryShort,
  CountryShortServer,
  Region,
  RegionServer
} from '../_models/address.types';
import { Item, ItemShadow } from '../_models/items.types';
import {
  Order,
  OrderPriority,
  OrderPriorityServer,
  OrderServer,
  OrderStatus,
  OrderStatusServer
} from '../_models/orders.types';
import { Payment, PaymentServer } from '../_models/payments.types';
import {
  Product,
  ProductAvailability,
  ProductAvailabilityServer,
  ProductServer,
  ProductShadow,
  ProductVisibility, ProductVisibilityServer
} from '../_models/products.types';
import { Purchase } from '../_models/purchases.types';
import { Incoming, IncomingServer } from '../_models/incomings.types';
import { Log, LogServer } from '../_models/logs.types';

@Injectable({
  providedIn: 'root'
})
//  основной сервис взаимодействия с api
export class DataService {
  //  public
  //  casts
  //  cast assembly
  public castAssembly: Observable<Assembly>;
  //  cast assemblies
  public castAssemblies: Observable<Assembly[]>;
  //  cast address
  public castAddress: Observable<Address>;
  //  cast addresses
  public castAddresses: Observable<Address[]>;
  //  cast brand
  public castBrand: Observable<Brand>;
  //  cast brands
  public castBrands: Observable<Brand[]>;
  //  cast category
  public castCategory: Observable<Category>;
  //  cast categoriesAll
  public castCategoriesAll: Observable<Category[]>;
  //  cast categoriesTop
  public castCategoriesTop: Observable<Category[]>;
  //  cast countries
  public castCountry: Observable<Country>;
  //  cast countries
  public castCountries: Observable<Country[]>;
  //  cast countries
  public castCountriesShort: Observable<CountryShort[]>;
  //  cast countries
  public castCountryList: Observable<CountryShort[]>;
  //  cast customer
  public castCustomer: Observable<Customer>;
  //  cast customers
  public castCustomers: Observable<Customer[]>;
  //  cast customer rank
  public castCustomerRank: Observable<CustomerRank>;
  //  cast customer ranks
  public castCustomerRanks: Observable<CustomerRank[]>;
  //  cast currency
  public castCurrency: Observable<Currency>;
  //  cast currencies
  public castCurrencies: Observable<Currency[]>;
  //  cast delivery
  public castDelivery: Observable<Delivery>;
  //  cast deliveries
  public castDeliveries: Observable<Delivery[]>;
  //  cast incoming
  public castIncoming: Observable<Incoming>;
  //  cast incomings
  public castIncomings: Observable<Incoming[]>;
  //  cast image
  public castImage: Observable<Image>;
  //  cast images
  public castImages: Observable<Image[]>;
  //  cast imageTypes
  public castImageTypes: Observable<ImageType[]>;
  //  cast item
  public castItem: Observable<Item>;
  //  cast items
  public castItems: Observable<Item[]>;
  //  cast item shadow
  public castItemShadow: Observable<ItemShadow>;
  //  cast item shadows
  public castItemShadows: Observable<ItemShadow[]>;
  //  cast language
  public castLanguage: Observable<Language>;
  //  cast languages
  public castLanguages: Observable<Language[]>;
  //  cast logs
  public castLogs: Observable<Log[]>;
  //  cast log
  public castLog: Observable<Log>;
  //  cast languages
  public castLocalizedKeywords: Observable<LocalizedKeyword[]>;
  //  cast manufacturer
  public castManufacturer: Observable<Manufacturer>;
  //  cast manufacturers
  public castManufacturers: Observable<Manufacturer[]>;
  //  cast admin-menu
  public castMenu: Observable<Menu[]>;
  //  cast option
  public castOption: Observable<Option>;
  //  cast options
  public castOptions: Observable<Option[]>;
  //  cast options set
  public castOptionsSet: Observable<OptionsSet>;
  //  cast options sets
  public castOptionsSets: Observable<OptionsSet[]>;
  //  cast option types
  public castOptionTypes: Observable<OptionType[]>;
  //  cast option value
  public castOptionValue: Observable<OptionValue>;
  //  cast option values
  public castOptionValues: Observable<OptionValue[]>;
  //  cast order
  public castOrder: Observable<Order>;
  //  cast orders
  public castOrders: Observable<Order[]>;
  //  cast order priorities
  public castOrderPriorities: Observable<OrderPriority[]>;
  //  cast order statuses
  public castOrderStatuses: Observable<OrderStatus[]>;
  //  cast payment
  public castPayment: Observable<Payment>;
  //  cast payments
  public castPayments: Observable<Payment[]>;
  //  cast product availabilities
  public castProductAvailabilities: Observable<ProductAvailability[]>;
  //  cast product visibilities
  public castProductVisibilities: Observable<ProductVisibility[]>;
  //  cast product
  public castProduct: Observable<Product>;
  //  cast products
  public castProducts: Observable<Product[]>;
  //  cast product shadow
  public castProductShadow: Observable<ProductShadow>;
  //  cast product shadows
  public castProductShadows: Observable<ProductShadow[]>;
  //  cast purchase
  public castPurchase: Observable<Purchase>;
  //  cast purchases
  public castPurchases: Observable<Purchase[]>;
  //  cast region
  public castRegion: Observable<Region>;
  //  cast regions
  public castRegions: Observable<Region[]>;

  //  private
  //  bs
  //  bs for addresses
  private address: BehaviorSubject<Address> = new BehaviorSubject<Address>(null);
  //  bs for addresses
  private addresses: BehaviorSubject<Address[]> = new BehaviorSubject<Address[]>(null);
  //  bs for assembly
  private assembly: BehaviorSubject<Assembly> = new BehaviorSubject<Assembly>(null);
  //  bs for assemblies
  private assemblies: BehaviorSubject<Assembly[]> = new BehaviorSubject<Assembly[]>(null);
  //  bs for brand
  private brand: BehaviorSubject<Brand> = new BehaviorSubject<Brand>(null);
  //  bs for brands
  private brands: BehaviorSubject<Brand[]> = new BehaviorSubject<Brand[]>(null);
  //  bs for category
  private category: BehaviorSubject<Category> = new BehaviorSubject<Category>(null);
  //  bs for categoriesAll
  private categoriesAll: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>(null);
  //  bs for categoriesTop
  private categoriesTop: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>(null);
  //  bs for countries
  private country: BehaviorSubject<Country> = new BehaviorSubject<Country>(null);
  //  bs for countries
  private countries: BehaviorSubject<Country[]> = new BehaviorSubject<Country[]>([]);
  //  bs for countries
  private countriesShort: BehaviorSubject<CountryShort[]> = new BehaviorSubject<CountryShort[]>([]);
  //  bs for countries
  private countryList: BehaviorSubject<CountryShort[]> = new BehaviorSubject<CountryShort[]>([]);
  //  bs for currency
  private currency: BehaviorSubject<Currency> = new BehaviorSubject<Currency>(null);
  //  bs for currencies
  private currencies: BehaviorSubject<Currency[]> = new BehaviorSubject<Currency[]>(null);
  //  bs for customer
  private customer: BehaviorSubject<Customer> = new BehaviorSubject<Customer>(null);
  //  bs for customers
  private customers: BehaviorSubject<Customer[]> = new BehaviorSubject<Customer[]>(null);
  //  bs for customer rank
  private customerRank: BehaviorSubject<CustomerRank> = new BehaviorSubject<CustomerRank>(null);
  //  bs for customer ranks
  private customerRanks: BehaviorSubject<CustomerRank[]> = new BehaviorSubject<CustomerRank[]>(null);
  //  bs for delivery
  private delivery: BehaviorSubject<Delivery> = new BehaviorSubject<Delivery>(null);
  //  bs for deliveries
  private deliveries: BehaviorSubject<Delivery[]> = new BehaviorSubject<Delivery[]>(null);
  //  bs for incoming
  private incoming: BehaviorSubject<Incoming> = new BehaviorSubject<Incoming>(null);
  //  bs for incomings
  private incomings: BehaviorSubject<Incoming[]> = new BehaviorSubject<Incoming[]>(null);
  //  bs for image
  private image: BehaviorSubject<Image> = new BehaviorSubject<Image>(null);
  //  bs for images
  private images: BehaviorSubject<Image[]> = new BehaviorSubject<Image[]>(null);
  //  bs for imageTypes
  private imageTypes: BehaviorSubject<ImageType[]> = new BehaviorSubject<ImageType[]>(null);
  //  bs for item
  private item: BehaviorSubject<Item> = new BehaviorSubject<Item>(null);
  //  bs for items
  private items: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>(null);
  //  bs for item shadow
  private itemShadow: BehaviorSubject<ItemShadow> = new BehaviorSubject<ItemShadow>(null);
  //  bs for item shadows
  private itemShadows: BehaviorSubject<ItemShadow[]> = new BehaviorSubject<ItemShadow[]>(null);
  //  bs for language
  private language: BehaviorSubject<Language> = new BehaviorSubject<Language>(null);
  //  bs for languages
  private languages: BehaviorSubject<Language[]> = new BehaviorSubject<Language[]>(null);
  //  bs for Log
  private log: BehaviorSubject<Log> = new BehaviorSubject<Log>(null);
  //  bs for Log
  private logs: BehaviorSubject<Log[]> = new BehaviorSubject<Log[]>(null);
  //  bs for localizedKeyword
  private localizedKeywords: BehaviorSubject<LocalizedKeyword[]> = new BehaviorSubject<LocalizedKeyword[]>(null);
  //  bs for manufacturer
  private manufacturer: BehaviorSubject<Manufacturer> = new BehaviorSubject<Manufacturer>(null);
  //  bs for manufacturers
  private manufacturers: BehaviorSubject<Manufacturer[]> = new BehaviorSubject<Manufacturer[]>(null);
  //  bs for admin-menu
  private menu: BehaviorSubject<Menu[]> = new BehaviorSubject<Menu[]>(null);
  //  bs for option
  private option: BehaviorSubject<Option> = new BehaviorSubject<Option>(null);
  //  bs for options
  private options: BehaviorSubject<Option[]> = new BehaviorSubject<Option[]>(null);
  //  bs for option types
  private optionTypes: BehaviorSubject<OptionType[]> = new BehaviorSubject<OptionType[]>(null);
  //  bs for option value
  private optionValue: BehaviorSubject<OptionValue> = new BehaviorSubject<OptionValue>(null);
  //  bs for option values
  private optionValues: BehaviorSubject<OptionValue[]> = new BehaviorSubject<OptionValue[]>(null);
  //  bs for options set
  private optionsSet: BehaviorSubject<OptionsSet> = new BehaviorSubject<OptionsSet>(null);
  //  bs for options sets
  private optionsSets: BehaviorSubject<OptionsSet[]> = new BehaviorSubject<OptionsSet[]>(null);
  //  bs for order
  private order: BehaviorSubject<Order> = new BehaviorSubject<Order>(null);
  //  bs for orders
  private orders: BehaviorSubject<Order[]> = new BehaviorSubject<Order[]>(null);
  //  bs for order priorities
  private orderPriorities: BehaviorSubject<OrderPriority[]> = new BehaviorSubject<OrderPriority[]>(null);
  //  bs for order statuses
  private orderStatuses: BehaviorSubject<OrderStatus[]> = new BehaviorSubject<OrderStatus[]>(null);
  //  bs for payment
  private payment: BehaviorSubject<Payment> = new BehaviorSubject<Payment>(null);
  //  bs for payments
  private payments: BehaviorSubject<Payment[]> = new BehaviorSubject<Payment[]>(null);
  //  bs for product
  private product: BehaviorSubject<Product> = new BehaviorSubject<Product>(null);
  //  bs for product availability
  private productAvailability: BehaviorSubject<ProductAvailability[]> = new BehaviorSubject<ProductAvailability[]>([]);
  //  bs for product visibility
  private productVisibility: BehaviorSubject<ProductVisibility[]> = new BehaviorSubject<ProductVisibility[]>([]);
  //  bs for products
  private products: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>(null);
  //  bs for product shadow
  private productShadow: BehaviorSubject<ProductShadow> = new BehaviorSubject<ProductShadow>(null);
  //  bs for product shadows
  private productShadows: BehaviorSubject<ProductShadow[]> = new BehaviorSubject<ProductShadow[]>(null);
  //  bs for purchase
  private purchase: BehaviorSubject<Purchase> = new BehaviorSubject<Purchase>(null);
  //  bs for purchases
  private purchases: BehaviorSubject<Purchase[]> = new BehaviorSubject<Purchase[]>(null);
  //  bs for region
  private region: BehaviorSubject<Region> = new BehaviorSubject<Region>(null);
  //  bs for regions
  private regions: BehaviorSubject<Region[]> = new BehaviorSubject<Region[]>(null);
  //  bs for roles
  private roles: BehaviorSubject<Role[]> = new BehaviorSubject<Role[]>(null);

  //  constructor
  constructor(private http: HttpClient, private convertService: ConvertService) {
    //  link casts & bs for
    //  link casts & bs for address
    this.castAddress = this.address.asObservable();
    //  link casts & bs for addresses
    this.castAddresses = this.addresses.asObservable();
    //  link casts & bs for assembly
    this.castAssembly = this.assembly.asObservable();
    //  link casts & bs for assemblies
    this.castAssemblies = this.assemblies.asObservable();
    //  link casts & bs for brand
    this.castBrand = this.brand.asObservable();
    //  link casts & bs for brands
    this.castBrands = this.brands.asObservable();
    //  link casts & bs for category
    this.castCategory = this.category.asObservable();
    //  link casts & bs for categories
    this.castCategoriesAll = this.categoriesAll.asObservable();
    //  link casts & bs for categories
    this.castCategoriesTop = this.categoriesTop.asObservable();
    //  link casts & bs for countries
    this.castCountry = this.country.asObservable();
    //  link casts & bs for countries
    this.castCountries = this.countries.asObservable();
    //  link casts & bs for country list
    this.castCountryList = this.countryList.asObservable();
    //  link casts & bs for countries short
    this.castCountriesShort = this.countriesShort.asObservable();
    //  link casts & bs for customer
    this.castCustomer = this.customer.asObservable();
    //  link casts & bs for customers
    this.castCustomers = this.customers.asObservable();
    //  link casts & bs for customer rank
    this.castCustomerRank = this.customerRank.asObservable();
    //  link casts & bs for customers rank
    this.castCustomerRanks = this.customerRanks.asObservable();
    //  link casts & bs for currency
    this.castCurrency = this.currency.asObservable();
    //  link casts & bs for currencies
    this.castCurrencies = this.currencies.asObservable();
    //  link casts & bs for delivery
    this.castDelivery = this.delivery.asObservable();
    //  link casts & bs for deliveries
    this.castDeliveries = this.deliveries.asObservable();
    //  link casts & bs for incoming
    this.castIncoming = this.incoming.asObservable();
    //  link casts & bs for incomings
    this.castIncomings = this.incomings.asObservable();
    //  link casts & bs for image
    this.castImage = this.image.asObservable();
    //  link casts & bs for images
    this.castImages = this.images.asObservable();
    //  link casts & bs for imageTypes
    this.castImageTypes = this.imageTypes.asObservable();
    //  link casts & bs for item
    this.castItem = this.item.asObservable();
    //  link casts & bs for items
    this.castItems = this.items.asObservable();
    //  link casts & bs for item shadow
    this.castItemShadow = this.itemShadow.asObservable();
    //  link casts & bs for items shadow
    this.castItemShadows = this.itemShadows.asObservable();
    //  link casts & bs for language
    this.castLanguage = this.language.asObservable();
    //  link casts & bs for languages
    this.castLanguages = this.languages.asObservable();
    //  link casts & bs for language
    this.castLog = this.log.asObservable();
    //  link casts & bs for languages
    this.castLogs = this.logs.asObservable();
    //  link casts & bs for localizedKeyword
    this.castLocalizedKeywords = this.localizedKeywords.asObservable();
    //  link casts & bs for manufacturer
    this.castManufacturer = this.manufacturer.asObservable();
    //  link casts & bs for manufacturers
    this.castManufacturers = this.manufacturers.asObservable();
    //  link casts & bs for admin-menu
    this.castMenu = this.menu.asObservable();
    //  link casts & bs for option
    this.castOption = this.option.asObservable();
    //  link casts & bs for options
    this.castOptions = this.options.asObservable();
    //  link casts & bs for option type
    this.castOptionTypes = this.optionTypes.asObservable();
    //  link casts & bs for option value
    this.castOptionValue = this.optionValue.asObservable();
    //  link casts & bs for option values
    this.castOptionValues = this.optionValues.asObservable();
    //  link casts & bs for options set
    this.castOptionsSet = this.optionsSet.asObservable();
    //  link casts & bs for options sets
    this.castOptionsSets = this.optionsSets.asObservable();
    //  link casts & bs for order
    this.castOrder = this.order.asObservable();
    //  link casts & bs for orders
    this.castOrders = this.orders.asObservable();
    //  link casts & bs for order priorities
    this.castOrderPriorities = this.orderPriorities.asObservable();
    //  link casts & bs for order statuses
    this.castOrderStatuses = this.orderStatuses.asObservable();
    //  link casts & bs for payment
    this.castPayment = this.payment.asObservable();
    //  link casts & bs for payments
    this.castPayments = this.payments.asObservable();
    //  link casts & bs for product
    this.castProduct = this.product.asObservable();
    //  link casts & bs for product availabilities
    this.castProductAvailabilities = this.productAvailability.asObservable();
    //  link casts & bs for product visibilities
    this.castProductVisibilities = this.productVisibility.asObservable();
    //  link casts & bs for products
    this.castProducts = this.products.asObservable();
    //  link casts & bs for product shadow
    this.castProductShadow = this.productShadow.asObservable();
    //  link casts & bs for product shadows
    this.castProductShadows = this.productShadows.asObservable();
    //  link casts & bs for purchase
    this.castPurchase = this.purchase.asObservable();
    //  link casts & bs for purchases
    this.castPurchases = this.purchases.asObservable();
    //  link casts & bs for region
    this.castRegion = this.region.asObservable();
    //  link casts & bs for regions
    this.castRegions = this.regions.asObservable();
    //  привязываем каст и БС для бита изменяемости ролей
    // this.castRoles = this.roles.asObservable();

    //  первичная инициализация
    this.initial();
  }

  //  **** ASSEMBLIES ****
  //  получение значений сборок
  public getAssemblies() {

    const newAssemblies: Assembly[] = [];
    // console.log('Assembly: ');
    for (let i = 0; i < 30; i++) {
      newAssemblies.push(GenerateService.generateAssembly(i + 1));
    }
    // console.log(newAssemblies);
    this.assemblies.next(newAssemblies);
  }

  //  получение значения конкретной сборки
  //  assemblyID - идентификатор сборки
  public getAssemblyByID(assemblyID: number, assembly?: Assembly) {
    if (assembly) {
      this.assembly.next(assembly);
    } else {
      //  todo get from server
    }
  }

  //  публикация новой сборки
  public putAssembly(assembly: Assembly) {
    //  публикуем новую сборку
    this.assembly.next(assembly);
  }

  //  BRANDS
  //  создание нового значения опции
  //  brand - экземпляр торговой марки
  public createBrand(brand: Brand) {
    const serverBrand: BrandServer = this.convertService.brandClientToServer(brand);
    //  проверка, что передали непустоту
    if (brand != null) {
      //  дергаем сервис
      this.http.put<BrandServer[]>(apiProto + baseUrl + urlBrand.create, serverBrand)
        .pipe(
          //  конвертация массива
          map(brandsServer =>
            brandsServer.map(brandServer => this.convertService.brandServerToClient(brandServer))),
          tap((brands: Brand[]) => {
            //  публикация  массива
            this.publishBrands(brands);
          }),
          catchError(err => {
            console.log(err, 'ERROR in deleteBrand service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление значения brand
  //  brandID - идентификатор торговой марки
  public deleteBrand(brandID: string) {
    //  проверка, что передали непустоту
    if (brandID != null) {
      //  дергаем сервис
      this.http.delete<BrandServer[]>(apiProto + baseUrl + urlBrand.delete, {
        params: new HttpParams().set(`id`, brandID)
      })
        .pipe(
          //  конвертация массива
          map(brandsServer =>
            brandsServer.map(brandServer => this.convertService.brandServerToClient(brandServer))),
          //  публикация  массива
          tap(brands => this.publishBrands(brands)),
          catchError(err => {
            console.log(err, 'ERROR in deleteBrand service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значений брэнда
  //  brandID - идентификатор торговой марки
  public getBrandByID(id: string) {
    //  если что-то передали
    if (id) {
      //  дергаем соответствующий сервис
      this.http.get<BrandServer>(apiProto + baseUrl + urlBrand.readCurrent, {
        params: new HttpParams().set(`id`, id)
      })
        .pipe(
          //  конвертация массива
          map(brandServer => this.convertService.brandServerToClient(brandServer)),
          //  публикация  массива
          tap(brand => {
            if (id) {
              //  публикация  brand
              this.brand.next(brand);
            } else {
              this.brand.next(GenerateService.createBrand());
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getBrandByID service');
            return of(null);
          })
        ).subscribe();
    } else {
      this.brand.next(GenerateService.createBrand());
    }
  }

  //  получение значений брэндов
  public getBrands() {
    //  дергаем соответствующий сервис
    this.http.get<BrandServer[]>(apiProto + baseUrl + urlBrand.readAll)
      .pipe(
        //  конвертация массива
        map(brandsServer =>
          brandsServer.map(brandServer => this.convertService.brandServerToClient(brandServer))),
        //  публикация  массива
        tap(brands => this.publishBrands(brands)),
        catchError(err => {
          console.log(err, 'ERROR in getBrands service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка брэндов - вспомогательная функция
  private publishBrands(brands: Brand[]) {
    //  проверка, что передали непустоту
    if (brands != null) {
      // console.log('brands:', brands);
      //  публикуем новое значение
      this.brands.next(brands);
    } else {
      //  публикуем null
      this.brands.next(null);
    }
  }

  //  изменение значения brand
  //  brand - экземпляр торговой марки
  public updateBrand(brand: Brand) {
    //  конвертим в серверный тип
    const serverBrand: BrandServer = this.convertService.brandClientToServer(brand);
    //  дергаем соответствующий сервис
    if (serverBrand != null) {
      //  дергаем сервис
      this.http.patch<BrandServer[]>(apiProto + baseUrl + urlBrand.update, serverBrand)
        .pipe(
          //  конвертация массива
          map(brandsServer =>
            brandsServer.map(brandServer => this.convertService.brandServerToClient(brandServer))),
          //  публикация  массива
          tap(brands => this.publishBrands(brands)),
          catchError(err => {
            console.log(err, 'ERROR in updateBrand service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  **** CATEGORIES ***********
  //  создание нового значения категории
  //  category - экземпляр категории
  public createCategory(category: Category) {
    const serverCategory: CategoryServer = this.convertService.categoryClientToServer(category);
    //  проверка, что передали непустоту
    if (category != null) {
      //  дергаем сервис
      this.http.put<CategoryServer[]>(apiProto + baseUrl + urlCategory.create, serverCategory)
        .pipe(
          //  конвертация массива
          map(categoriesServer =>
            categoriesServer.map(categoryServer => this.convertService.categoryServerToClient(categoryServer))),
          tap((categories: Category[]) => {
            //  публикация  массива
            this.publishCategories(categories);
          }),
          catchError(err => {
            console.log(err, 'ERROR in createCategory service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значения конкретной категории
  //  categoryID - идентификатор категории
  public getCategoryByID(categoryID: string) {
    //  если что-то передали
    if (categoryID) {
      //  дергаем соответствующий сервис
      this.http.get<CategoryServer>(apiProto + baseUrl + urlCategory.readCurrent, {
        params: new HttpParams().set(`id`, categoryID)
      })
        .pipe(
          //  конвертация массива
          map(categoryServer => this.convertService.categoryServerToClient(categoryServer)),
          //  публикация  массива
          tap(category => {
            if (categoryID) {
              //  публикация  category
              this.category.next(category);
            } else {
              this.category.next(null);
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getCategoryByID service');
            return of(null);
          })
        ).subscribe();
    } else {
      //  возвращаем сгенерированную категорию
      this.category.next(GenerateService.createCategory());
    }
  }

  //  получение значения всех категорий
  //  count - количество получаемых записей
  //  offset - с какой записи получать
  public getCategories(count?: number, offset?: number) {
    //  дергаем соответствующий сервис
    this.http.get<CategoryServer[]>(apiProto + baseUrl + urlCategory.readAll)
      .pipe(
        //  конвертация массива
        map(categoriesServer =>
          categoriesServer.map(categoryServer => this.convertService.categoryServerToClient(categoryServer))),
        //  публикация  массива
        tap(categories => this.publishCategories(categories)),
        catchError(err => {
          console.log(err, 'ERROR in getCategories service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка категорий - вспомогательная функция
  private publishCategories(categories: Category[]) {
    //  проверка, что передали непустоту
    if (categories != null) {
      //  публикуем новое значение
      this.categoriesAll.next(categories);
      // console.log('Categories: ', categories);
    } else {
      //  публикуем null
      this.categoriesAll.next(null);
    }
  }

  //  получение значения категорий верхнего уровня (для меню)
  //  count - количество получаемых записей
  //  offset - с какой записи получать
  public getCategoriesTop(count?: number, offset?: number) {

  }


  //  удаление нового значения категории
  //  categoryID - идентификатор категории
  public deleteCategory(categoryID: string) {
    //  проверка, что передали непустоту
    if (categoryID != null) {
      //  дергаем сервис
      this.http.delete<CategoryServer[]>(apiProto + baseUrl + urlCategory.delete, {
        params: new HttpParams().set(`id`, categoryID)
      })
        .pipe(
          //  конвертация массива
          map(categoriesServer =>
            categoriesServer.map(categoryServer => this.convertService.categoryServerToClient(categoryServer))),
          //  публикация  массива
          tap(categories => this.publishCategories(categories)),
          catchError(err => {
            console.log(err, 'ERROR in deleteCategory service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  перемещение категории
  //  category - идентификатор категории
  //  oldParentID - идентификатор категории
  //  newParentID - идентификатор категории
  public moveCategory(category: Category, oldParentID: number, newParentID: number) {

  }

  //  изменение категории
  //  categoryID - идентификатор категории
  public updateCategory(category: Category) {
    //  конвертим в серверный тип
    const serverCategory: CategoryServer = this.convertService.categoryClientToServer(category);
    //  дергаем соответствующий сервис
    if (serverCategory != null) {
      //  дергаем сервис
      this.http.patch<CategoryServer[]>(apiProto + baseUrl + urlCategory.update, serverCategory)
        .pipe(
          //  конвертация массива
          map(categoriesServer =>
            categoriesServer.map(categoryServer => this.convertService.categoryServerToClient(categoryServer))),
          //  публикация  массива
          tap(categories => this.publishCategories(categories)),
          catchError(err => {
            console.log(err, 'ERROR in updateCategory service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  времянка для генерации категорий
  //  categories - категории для добавления
  public putCategories(categories: Category[]) {
    this.categoriesTop.next(categories);
    this.categoriesAll.next(categories);
  }

  //  ****  COUNTRY  ********************
  //  создание новой страны
  public createCountry(country: Country) {
    const serverCountry: CountryServer = this.convertService.countryClientToServer(country);
    //  проверка, что передали непустоту
    if (country != null) {
      //  дергаем сервис
      this.http.put<CountryServer[]>(apiProto + baseUrl + urlCountry.create, serverCountry)
        .pipe(
          //  конвертация массива
          map(countriesServer =>
            countriesServer.map(countryServer => this.convertService.countryServerToClient(countryServer))),
          tap((countries: Country[]) => {
            //  публикация  массива
            this.publishCountries(countries);
          }),
          catchError(err => {
            console.log(err, 'ERROR in createCountry service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление страны
  public deleteCountry(country: Country) {
    const serverCountryId: string = String(this.convertService.countryClientToServer(country).ID);
    //  проверка, что передали непустоту
    if (country != null) {
      //  дергаем соответствующий сервис
      this.http.delete<CountryServer[]>(apiProto + baseUrl + urlCountry.delete, {
        params: new HttpParams().set('id', serverCountryId)
      }).pipe(
        //  конвертация массива
        map(countriesServer =>
          countriesServer.map(countryServer => this.convertService.countryServerToClient(countryServer))),
        //  публикуем список
        tap(countries => this.publishCountries(countries)),
        catchError(err => {
          console.log(err, 'ERROR in deleteCountry service');
          return of(null);
        })
      ).subscribe();
    }
  }

  //  получение значения конкретной страны
  //  countryID - идентификатор страны
  public getCountryByID(countryID: string): Observable<Country> {
    //  если что-то передали
    if (countryID) {
      //  дергаем соответствующий сервис
      return this.http.get<CountryServer>(apiProto + baseUrl + urlCountry.readCurrent, {
        params: new HttpParams().set(`id`, countryID)
      })
        .pipe(
          //  конвертация массива
          map(countryServer => this.convertService.countryServerToClient(countryServer)),
          //  публикация  массива
          tap(country => {
            if (countryID) {
              //  публикация  country
              this.country.next(country);
            } else {
              this.country.next(null);
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getCountryByID service');
            return of(null);
          })
        );
    } else {
      //  возвращаем сгенерированную категорию
      this.category.next(GenerateService.createCategory());
    }
  }

  public getCountries() {
    //  дергаем соответствующий сервис
    this.http.get<CountryServer[]>(apiProto + baseUrl + urlCountry.readAll)
      .pipe(
        //  конвертация массива
        map(countriesServer =>
          countriesServer.map(countryServer => this.convertService.countryServerToClient(countryServer))),
        //  публикуем список
        tap(countries => this.publishCountries(countries)),
        catchError(err => {
          console.log(err, 'ERROR in getCountries service');
          return of(null);
        })
      ).subscribe();
  }


  public getCountriesShort() {
    //  дергаем соответствующий сервис
    this.http.get<CountryShortServer[]>(apiProto + baseUrl + urlCountry.readAllShort)
      .pipe(
        //  конвертация массива
        map(countriesServer =>
          countriesServer.map(countryShortServer => this.convertService.countryShortServerToClient(countryShortServer))),
        //  публикуем список
        tap(countriesShort => this.publishCountriesShort(countriesShort)),
        catchError(err => {
          console.log(err, 'ERROR in getCountriesShort service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка  стран - вспомогательная функция
  private publishCountries(countries: Country[]) {
    //  проверка, что передали непустоту
    if (countries != null) {
      //  публикуем новое значение
      this.countries.next(countries);
    } else {
      //  публикуем null
      this.countries.next(null);
    }
  }

  //  публикация прилетевшего списка  стран в кратком формате - вспомогательная функция
  private publishCountriesShort(countriesShort: CountryShort[]) {
    //  проверка, что передали непустоту
    if (countriesShort != null) {
      //  публикуем новое значение
      this.countriesShort.next(countriesShort);
    } else {
      //  публикуем null
      this.countriesShort.next(null);
    }
  }

  //  публикация новый список стран для зон
  public putCountryList(countryList: CountryShort[]) {
    //  публикуем новый список стран для зон
    this.countryList.next(countryList);
  }

  //  изменение значения country
  public updateCountry(country: Country) {
    //  конвертим в серверный тип
    const serverCountry: CountryServer = this.convertService.countryClientToServer(country);
    //  дергаем соответствующий сервис
    if (serverCountry != null) {
      //  дергаем сервис
      this.http.patch<CountryServer[]>(apiProto + baseUrl + urlCountry.update, serverCountry)
        .pipe(
          //  конвертация массива
          map(countriesServer =>
            countriesServer.map(countryServer => this.convertService.countryServerToClient(countryServer))),
          //  публикация  массива
          tap(countries => this.publishCountries(countries)),
          catchError(err => {
            console.log(err, 'ERROR in updateCountry service');
            return of(null);
          })
        ).subscribe();
    }
  }


  //  INCOMINGS
  //  проставление бита проверки товарной накладной
  //  incomingID - ID экземпляра торговой марки
  public createItemsFromIncoming(incomingID: string) {
    //  дергаем соответствующий сервис
    if (incomingID != null) {
      //  дергаем сервис
      this.http.get<IncomingServer[]>(apiProto + baseUrl + urlIncoming.check, {
        params: new HttpParams().set('id', incomingID)
      })
        .pipe(
          //  конвертация массива
          map(incomingsServer =>
            incomingsServer.map(incomingServer => this.convertService.incomingServerToClient(incomingServer))),
          //  публикация  массива
          tap(incomings => this.publishIncomings(incomings)),
          catchError(err => {
            console.log(err, 'ERROR in checkIncoming service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  создание нового значения опции
  //  incoming - экземпляр товарной накладной
  public createIncoming(incoming: Incoming) {
    const serverIncoming: IncomingServer = this.convertService.incomingClientToServer(incoming);
    //  проверка, что передали непустоту
    if (incoming != null) {
      //  дергаем сервис
      this.http.put<IncomingServer[]>(apiProto + baseUrl + urlIncoming.create, serverIncoming)
        .pipe(
          //  конвертация массива
          map(incomingsServer =>
            incomingsServer.map(incomingServer => this.convertService.incomingServerToClient(incomingServer))),
          tap((incomings: Incoming[]) => {
            //  публикация  массива
            this.publishIncomings(incomings);
          }),
          catchError(err => {
            console.log(err, 'ERROR in createIncoming service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление значения brand
  //  incomingID - идентификатор товарной накладной
  public deleteIncoming(incomingID: string) {
    //  проверка, что передали непустоту
    if (incomingID != null) {
      //  дергаем сервис
      this.http.delete<IncomingServer[]>(apiProto + baseUrl + urlIncoming.delete, {
        params: new HttpParams().set(`id`, incomingID)
      })
        .pipe(
          //  конвертация массива
          map(incomingsServer =>
            incomingsServer.map(incomingServer => this.convertService.incomingServerToClient(incomingServer))),
          //  публикация  массива
          tap(incomings => this.publishIncomings(incomings)),
          catchError(err => {
            console.log(err, 'ERROR in deleteIncoming service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значений товарной накладной
  //  incomingID - идентификатор товарной накладной
  public getIncomingByID(incomingID: string) {
    //  если что-то передали
    if (incomingID) {
      //  дергаем соответствующий сервис
      this.http.get<IncomingServer>(apiProto + baseUrl + urlIncoming.readCurrent, {
        params: new HttpParams().set(`id`, incomingID)
      })
        .pipe(
          //  конвертация массива
          map(incomingServer => this.convertService.incomingServerToClient(incomingServer)),
          //  публикация  массива
          tap(incoming => {
            if (incomingID) {
              //  публикация  brand
              this.incoming.next(incoming);
            } else {
              this.incoming.next(GenerateService.createIncoming());
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getIncomingByID service');
            return of(null);
          })
        ).subscribe();
    } else {
      this.incoming.next(GenerateService.createIncoming());
    }
  }

  //  получение товарных накладных
  public getIncomings() {
    //  дергаем соответствующий сервис
    this.http.get<IncomingServer[]>(apiProto + baseUrl + urlIncoming.readAll)
      .pipe(
        //  конвертация массива
        map(incomingsServer =>
          incomingsServer.map(incomingServer => this.convertService.incomingServerToClient(incomingServer))),
        //  публикация  массива
        tap(incomings => this.publishIncomings(incomings)),
        catchError(err => {
          console.log(err, 'ERROR in getIncomings service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка товарной накладных - вспомогательная функция
  private publishIncomings(incomings: Incoming[]) {
    //  проверка, что передали непустоту
    if (incomings != null) {
      // console.log('brands:', brands);
      //  публикуем новое значение
      this.incomings.next(incomings);
    } else {
      //  публикуем null
      this.incomings.next(null);
    }
  }

  //  изменение значения incoming
  //  incoming - экземпляр товарной накладной
  public updateIncoming(incoming: Incoming) {
    //  конвертим в серверный тип
    const serverIncoming: IncomingServer = this.convertService.incomingClientToServer(incoming);
    //  дергаем соответствующий сервис
    if (serverIncoming != null) {
      //  дергаем сервис
      this.http.patch<IncomingServer[]>(apiProto + baseUrl + urlIncoming.update, serverIncoming)
        .pipe(
          //  конвертация массива
          map(incomingsServer =>
            incomingsServer.map(incomingServer => this.convertService.incomingServerToClient(incomingServer))),
          //  публикация  массива
          tap(incomings => this.publishIncomings(incomings)),
          catchError(err => {
            console.log(err, 'ERROR in updateIncoming service');
            return of(null);
          })
        ).subscribe();
    }
  }

  // ******* LANGUAGE **********************

  public createLanguage(newLanguage: Language) {
    const serverLanguage: LanguageServer = this.convertService.languageClientToServer(newLanguage);
    //  проверка, что передали непустоту
    if (newLanguage != null) {
      //  дергаем соответствующий сервис
      this.http.put<LanguageServer[]>(apiProto + baseUrl + urlLanguage.create, serverLanguage)
        .pipe(
          //  конвертация массива
          map(languageServers =>
            languageServers.map(languageServer => this.convertService.languageServerToClient(languageServer))),
          //  публикуем список
          tap(languages => this.publishLanguages(languages)),
          catchError(err => {
            console.log(err, 'ERROR in createLanguage service');
            return of(null);
          })
        ).subscribe();
    }
  }

  private publishLanguages(languages: Language[]) {
    //  проверка, что передали непустоту
    if (languages != null) {
      //  публикуем новое значение
      this.languages.next(languages);
    } else {
      //  публикуем null
      this.languages.next(null);
    }
  }

  public updateLanguage(changedLanguage: Language) {
    const serverLanguage: LanguageServer = this.convertService.languageClientToServer(changedLanguage);
    //  проверка, что передали непустоту
    if (changedLanguage != null) {
      //  дергаем сервис
      return this.http.patch<LanguageServer[]>(apiProto + baseUrl + urlLanguage.update, serverLanguage)
        .pipe(
          //  конвертация массива
          map(languagesServer =>
            languagesServer.map(languageServer => this.convertService.languageServerToClient(languageServer))),
          //  публикуем список
          tap(languages => this.publishLanguages(languages)),
          catchError(err => {
            console.log(err, 'ERROR in getlanguages service');
            return of(null);
          })
        ).subscribe();
    }
  }


  //  удаление валюты curren
  //  killedLanguage - создаваемой валюты
  public deleteLanguage(killedLanguage: Language) {
    const serverLanguageId: string = String(this.convertService.languageClientToServer(killedLanguage).ID);
    //  проверка, что передали непустоту
    if (killedLanguage != null) {
      //  дергаем соответствующий сервис
      this.http.delete<LanguageServer[]>(apiProto + baseUrl + urlLanguage.delete, {
        params: new HttpParams().set('id', serverLanguageId)
      }).pipe(
        //  конвертация массива
        map(languagesServer =>
          languagesServer.map(languageServer => this.convertService.languageServerToClient(languageServer))),
        //  публикуем список
        tap(languages => this.publishLanguages(languages)),
        catchError(err => {
          console.log(err, 'ERROR in deleteLanguage service');
          return of(null);
        })
      ).subscribe();
    }
  }


  // ******* Localized Keywords **********************

  public createLocalizedKeyword(newLocalizedKeyword: LocalizedKeyword) {
    const serverLocalizedKeyword: LocalizedKeywordServer = this.convertService.localizedKeywordClientToServer(newLocalizedKeyword);
    //  проверка, что передали непустоту
    if (newLocalizedKeyword != null) {
      //  дергаем соответствующий сервис
      this.http.put<LocalizedKeywordServer[]>(apiProto + baseUrl + urlLocalizedKeyword.create, serverLocalizedKeyword)
        .pipe(
          //  конвертация массива
          map(localKeywordServers =>
            localKeywordServers.map(localizedKeywordServer => this.convertService.LocalizedKeywordServerToClient(localizedKeywordServer))),
          //  публикуем список
          tap(localizedKeywords => this.publishLocalizedKeywords(localizedKeywords)),
          catchError(err => {
            console.log(err, 'ERROR in createLocalizedKeyword service');
            return of(null);
          })
        ).subscribe();
    }
  }

  private publishLocalizedKeywords(localizedKeywords: LocalizedKeyword[]) {
    //  проверка, что передали непустоту
    if (localizedKeywords != null) {
      //  публикуем новое значение
      this.localizedKeywords.next(localizedKeywords);
    } else {
      //  публикуем null
      this.localizedKeywords.next(null);
    }
  }

  public updateLocalizedKeyword(changedLocalizedKeyword: LocalizedKeyword) {
    const serverLocalizedKeyword: LocalizedKeywordServer = this.convertService.localizedKeywordClientToServer(changedLocalizedKeyword);
    //  проверка, что передали непустоту
    if (changedLocalizedKeyword != null) {
      //  дергаем сервис
      return this.http.patch<LocalizedKeywordServer[]>(apiProto + baseUrl + urlLocalizedKeyword.update, serverLocalizedKeyword)
        .pipe(
          //  конвертация массива
          map(localizedKeywordsServer => localizedKeywordsServer.map(localizedKeywordServer =>
            this.convertService.LocalizedKeywordServerToClient(localizedKeywordServer))),
          //  публикуем список
          tap(localizedKeywords => this.publishLocalizedKeywords(localizedKeywords)),
          catchError(err => {
            console.log(err, 'ERROR in getlocalizedKeywords service');
            return of(null);
          })
        ).subscribe();
    }
  }


  //  удаление валюты curren
  //  killedLocalizedKeyword - создаваемой валюты
  public deleteLocalizedKeyword(killedLocalizedKeyword: LocalizedKeyword) {
    const serverLocalizedKeywordId: string = String(this.convertService.localizedKeywordClientToServer(killedLocalizedKeyword).ID);
    //  проверка, что передали непустоту
    if (killedLocalizedKeyword != null) {
      //  дергаем соответствующий сервис
      this.http.delete<LocalizedKeywordServer[]>(apiProto + baseUrl + urlLocalizedKeyword.delete, {
        params: new HttpParams().set('id', serverLocalizedKeywordId)
      }).pipe(
        //  конвертация массива
        map(localizedKeywordsServer => localizedKeywordsServer.map(localizedKeywordServer =>
          this.convertService.LocalizedKeywordServerToClient(localizedKeywordServer))),
        //  публикуем список
        tap(localizedKeywords => this.publishLocalizedKeywords(localizedKeywords)),
        catchError(err => {
          console.log(err, 'ERROR in deleteLocalizedKeyword service');
          return of(null);
        })
      ).subscribe();
    }
  }


  //  ****  CURRENCY  ********************
  //  создание валюты
  //  newCurrency - создаваемая валюта
  public createCurrency(newCurrency: Currency) {
    const serverCurrency: CurrencyServer = this.convertService.currencyClientToServer(newCurrency);
    //  проверка, что передали непустоту
    if (newCurrency != null) {
      //  дергаем соответствующий сервис
      this.http.put<CurrencyServer[]>(apiProto + baseUrl + urlCurrency.create, serverCurrency)
        .pipe(
          //  конвертация массива
          map(currenciesServer =>
            currenciesServer.map(currencyServer => this.convertService.currencyServerToClient(currencyServer))),
          //  публикуем список
          tap(currencies => this.publishCurrencies(currencies)),
          catchError(err => {
            console.log(err, 'ERROR in createCurrency service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление валюты
  //  killedCurrency - создаваемой валюты
  public deleteCurrency(killedCurrency: Currency) {
    const serverCurrencyId: string = String(this.convertService.currencyClientToServer(killedCurrency).ID);
    //  проверка, что передали непустоту
    if (killedCurrency != null) {
      //  дергаем соответствующий сервис
      this.http.delete<CurrencyServer[]>(apiProto + baseUrl + urlCurrency.delete, {
        params: new HttpParams().set('id', serverCurrencyId)
      }).pipe(
        //  конвертация массива
        map(currenciesServer =>
          currenciesServer.map(currencyServer => this.convertService.currencyServerToClient(currencyServer))),
        //  публикуем список
        tap(currencies => this.publishCurrencies(currencies)),
        catchError(err => {
          console.log(err, 'ERROR in deleteCurrency service');
          return of(null);
        })
      ).subscribe();
    }
  }

  //  получение значений валюты
  public getCurrencies() {
    //  дергаем соответствующий сервис
    this.http.get<CurrencyServer[]>(apiProto + baseUrl + urlCurrency.readAll)
      .pipe(
        //  конвертация массива
        map(currenciesServer =>
          currenciesServer.map(currencyServer => this.convertService.currencyServerToClient(currencyServer))),
        //  публикуем список
        tap(currencies => this.publishCurrencies(currencies)),
        catchError(err => {
          console.log(err, 'ERROR in getCurrencies service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка  валют - вспомогательная функция
  private publishCurrencies(currencies: Currency[]) {
    //  проверка, что передали непустоту
    if (currencies != null) {
      //  публикуем новое значение
      this.currencies.next(currencies);
    } else {
      //  публикуем null
      this.currencies.next(null);
    }
  }

  //  изменение валюты
  //  updateCurrency - изменяемая валюта
  public updateCurrency(changedCurrency: Currency) {
    const serverCurrency: CurrencyServer = this.convertService.currencyClientToServer(changedCurrency);
    //  проверка, что передали непустоту
    if (changedCurrency != null) {
      //  дергаем сервис
      return this.http.patch<CurrencyServer[]>(apiProto + baseUrl + urlCurrency.update, serverCurrency)
        .pipe(
          //  конвертация массива
          map(currenciesServer =>
            currenciesServer.map(currencyServer => this.convertService.currencyServerToClient(currencyServer))),
          //  публикуем список
          tap(currencies => this.publishCurrencies(currencies)),
          catchError(err => {
            console.log(err, 'ERROR in getCurrencies service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  **** DELIVERIES ***********
  //  создание нового значения delivery
  //  delivery - экземпляр доставки
  public createDelivery(delivery: Delivery) {
    const serverDelivery: DeliveryServer = this.convertService.deliveryClientToServer(delivery);
    //  проверка, что передали непустоту
    if (delivery != null) {
      //  дергаем сервис
      this.http.put<DeliveryServer[]>(apiProto + baseUrl + urlDelivery.create, serverDelivery)
        .pipe(
          //  конвертация массива
          map(deliveriesServer =>
            deliveriesServer.map(deliveryServer => this.convertService.deliveryServerToClient(deliveryServer))),
          //  публикация  массива
          tap(deliveries => this.publishDeliveries(deliveries)),
          catchError(err => {
            console.log(err, 'ERROR in createDelivery service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление значения delivery
  //  deliveryID - идентификатор торговой марки
  public deleteDelivery(deliveryID: string) {
    //  проверка, что передали непустоту
    if (deliveryID != null) {
      //  дергаем сервис
      this.http.delete<DeliveryServer[]>(apiProto + baseUrl + urlDelivery.update, {
        params: new HttpParams().set(`id`, deliveryID)
      })
        .pipe(
          //  конвертация массива
          map(deliveriesServer =>
            deliveriesServer.map(deliveryServer => this.convertService.deliveryServerToClient(deliveryServer))),
          //  публикация  массива
          tap(deliveries => this.publishDeliveries(deliveries)),
          catchError(err => {
            console.log(err, 'ERROR in deleteDelivery service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение delivery
  //  deliveryID - идентификатор торговой марки
  public getDeliveryByID(deliveryID: string) {
    //  проверка, что передали непустоту
    if (deliveryID) {
      //  дергаем соответствующий сервис
      this.http.get<DeliveryServer>(apiProto + baseUrl + urlDelivery.readCurrent, {
        params: new HttpParams().set(`id`, deliveryID)
      })
        .pipe(
          map(deliveryServer => {
            const newDeliveryServer = this.convertService.deliveryServerToClient(deliveryServer);
            //  возвращаем  сконвертированный элемент
            return newDeliveryServer;
          }),
          //  публикация
          tap(delivery => {
            //  проверка, что передали непустоту
            if (deliveryID) {
              //  публикация  delivery
              this.delivery.next(delivery);
            } else {
              this.delivery.next(GenerateService.createDelivery(1));
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getDeliveryByID service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значений deliveries
  public getDeliveries() {
    //  дергаем соответствующий сервис
    this.http.get<DeliveryServer[]>(apiProto + baseUrl + urlDelivery.readAll)
      .pipe(
        //  конвертация массива
        map(deliveriesServer =>
          deliveriesServer.map(deliveryServer => this.convertService.deliveryServerToClient(deliveryServer))),
        //  публикация  массива
        tap(deliveries => this.publishDeliveries(deliveries)),
        catchError(err => {
          console.log(err, 'ERROR in getDeliveries service');
          return of(null);
        })
      ).subscribe();
  }

  //  перемещение delivery
  //  delivery - экземпляр доставки
  public changePriorityOfDelivery(delivery: Delivery) {
    const serverDelivery: DeliveryServer = this.convertService.deliveryClientToServer(delivery);
    //  проверка, что передали непустоту
    if (serverDelivery != null) {
      //  дергаем сервис
      this.http.patch<DeliveryServer[]>(apiProto + baseUrl + urlDelivery.changePriority, serverDelivery)
        .pipe(
          //  конвертация массива
          map(deliveriesServer =>
            deliveriesServer.map(deliveryServer => this.convertService.deliveryServerToClient(deliveryServer))),
          //  публикация  массива
          tap(deliveries => this.publishDeliveries(deliveries)),
          catchError(err => {
            console.log(err, 'ERROR in changePriorityOfDelivery service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  публикация прилетевшего списка доставок - вспомогательная функция
  //  deliveriesServer - массив продуктов серверного типа
  private publishDeliveries(deliveries: Delivery[]) {
    //  проверка, что передали непустоту
    if (deliveries != null) {
      console.log('DELIVERY:', deliveries);
      //  публикуем новое значение
      this.deliveries.next(deliveries);
    } else {
      //  публикуем null
      this.deliveries.next(null);
    }
  }

  //  времянка для добавления payment
  public putDelivery(delivery: Delivery) {
    console.log('Try to renew delivery with zones: ', delivery.zones);
    this.delivery.next(delivery);
  }

  //  изменение значения delivery
  //  delivery - экземпляр доставки
  public updateDelivery(delivery: Delivery) {
    console.log('try to jupdate delivery');
    const serverDelivery: DeliveryServer = this.convertService.deliveryClientToServer(delivery);
    //  дергаем соответствующий сервис
    if (serverDelivery != null) {
      //  дергаем сервис
      this.http.patch<DeliveryServer[]>(apiProto + baseUrl + urlDelivery.update, serverDelivery)
        .pipe(
          //  конвертация массива
          map(deliveriesServer =>
            deliveriesServer.map(deliveryServer => this.convertService.deliveryServerToClient(deliveryServer))),
          //  публикация  массива
          tap(deliveries => this.publishDeliveries(deliveries)),
          catchError(err => {
            console.log(err, 'ERROR in updateDelivery service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  ****  IMAGES  *************
  //  создание новой картинки
  public createImage(image: Image) {

    // console.log('dataService.createImage');
    this.http.put(apiProto + baseUrl + urlImage.create, image)
      .subscribe((res: Response) => {
        // console.log('OK');
      }
        , error => {
          console.log(error, 'ERROR in createImage service');
        });
  }

  //  получение значений картинок
  public getImages(count?: number, offset?: number) {
    //  дергаем соответствующий сервис
    this.http.get<ImageServer[]>(apiProto + baseUrl + urlImage.readAll)
      .subscribe((imagesServer: ImageServer[]) => {
        // const serverCurrencies: CurrencyServer = JSON.parse(resp);
        const newImages: Image[] = [];
        imagesServer.forEach((imageServer: ImageServer) => {
          newImages.push(this.convertService.imageServerToClient(imageServer));
        });
        console.log(newImages);
        //  публикуем новое значение
        this.images.next(newImages);
      }, error => {
        console.log(error, 'ERROR in getImages service');
      });
  }

  //  загрузка файла картинки
  public uploadImage(files: FileList, imageTypeID: number): Observable<Image> {
    // console.log('imageTypeID: ', imageTypeID);
    //  экземпляр переменной formData
    const formData: FormData = new FormData();        //  перебираем, идентификатор формы - uploadfile
    Array.from(files).forEach((file: File) => {
      formData.append('myFile', file, file.name);
    });
    formData.append('type', String(imageTypeID));

    return this.http.post<ImageServer>(apiProto + baseUrl + urlImage.upload, formData)
      .pipe(
        // tap(_ => console.log('Into POST')),
        map((image: ImageServer) => this.convertService.imageServerToClient(image))
      );
  }

  //  IMAGE TYPES
  //  получение значений current image type
  //  imageTypeID - ID
  public getImageTypeByID(imageTypeID: number): Observable<ImageType> {
    //  дергаем соответствующий сервис
    return this.http.get<ImageTypeServer>(apiProto + baseUrl + urlImage.readCurrentType)
      .pipe(
        // tap(_ => console.log('Into POST')),
        map((imageTypeServer: ImageTypeServer) => this.convertService.imageTypeServerToClient(imageTypeServer)),
        catchError(err => {
          console.log(err, 'ERROR in getImageTypeByID service');
          return of(null);
        })
      );
  }

  //  получение значений image types
  public getImageTypes() {
    //  дергаем соответствующий сервис
    this.http.get<ImageTypeServer[]>(apiProto + baseUrl + urlImage.readAllTypes)
      .pipe(
        //  конвертация массива
        map(imageTypesServer =>
          imageTypesServer.map(imageTypeServer => this.convertService.imageTypeServerToClient(imageTypeServer))),
        //  публикация  массива
        tap(imageTypes => this.publishImageTypes(imageTypes)),
        catchError(err => {
          console.log(err, 'ERROR in getBrands service');
          return of(null);
        })
      ).subscribe();
  }

  //  publish image types
  public publishImageTypes(imageTypes: ImageType[]) {
    //  проверка, что передали непустоту
    if (imageTypes != null) {
      //  публикуем новое значение
      // console.log('image types: ', imageTypes);
      this.imageTypes.next(imageTypes);
    } else {
      //  публикуем null
      this.imageTypes.next(null);
    }
  }

  //  LANGUAGE
  //  получение значений языков
  public getLanguages() {
    //  дергаем соответствующий сервис
    this.http.get<LanguageServer[]>(apiProto + baseUrl + urlLanguage.readAll)
      .subscribe((languagesServer: LanguageServer[]) => {
        const newLanguages: Language[] = [];
        languagesServer.forEach((languageServer: LanguageServer) => {
          newLanguages.push(this.convertService.languageServerToClient(languageServer));
        });
        // console.log('getLanguages', newLanguages);
        //  публикуем новое значение
        this.languages.next(newLanguages);
      }, error => {
        console.log(error, 'ERROR in getLanguages service');
      });
  }


  public getLocalizedKeywords() {
    //  дергаем соответствующий сервис
    this.http.get<LocalizedKeywordServer[]>(apiProto + baseUrl + urlLocalizedKeyword.readAll)
      .subscribe((localizedKeywordsServer: LocalizedKeywordServer[]) => {
        //  создаем временную переменную
        const newLocalizedKeywords: LocalizedKeyword[] = [];
        //  convert
        localizedKeywordsServer.forEach((localizedKeywordServer: LocalizedKeywordServer) => {
          newLocalizedKeywords.push(this.convertService.LocalizedKeywordServerToClient(localizedKeywordServer));
        });
        // console.log('getLocalizedKeywords', newLocalizedKeywords);
        //  публикуем новое значение
        this.localizedKeywords.next(newLocalizedKeywords);
      }, error => {
        console.log(error, 'ERROR in getLocalizedKeywords service');
      });
  }

  //  LOG
  public getLogs() {
    //  дергаем соответствующий сервис
    this.http.get<LogServer[]>(apiProto + baseUrl + urlLog.readAll)
      .subscribe((logsServer: LogServer[]) => {
        const newLogs: Log[] = [];
        logsServer.forEach((logServer: LogServer) => {
          newLogs.push(this.convertService.logServerToClient(logServer));
        });
        // console.log('getLogs', newLogs);
        //  публикуем новое значение
        this.logs.next(newLogs);
      }, error => {
        console.log(error, 'ERROR in getLogs service');
      });
  }

  //  задать текущий язык
  public setLanguage(language: Language) {
    //  задаем новое значение языка
    if (language) {
      this.language.next(language);
      // this.changeDetectorRef.detectChanges();
    }
  }

  //  MANUFACTURERS
  //  создание нового значения опции
  //  manufacturer - экземпляр производителя
  public createManufacturer(manufacturer: Manufacturer) {
    const serverManufacturer: ManufacturerServer = this.convertService.manufacturerClientToServer(manufacturer);
    //  проверка, что передали непустоту
    if (manufacturer != null) {
      //  дергаем соответствующий сервис
      this.http.put<ManufacturerServer[]>(apiProto + baseUrl + urlManufacturer.create, serverManufacturer)
        .pipe(
          //  конвертация массива
          map(manufacturersServer =>
            manufacturersServer.map(manufacturerServer => this.convertService.manufacturerServerToClient(manufacturerServer))),
          //  публикуем список
          tap(manufacturers => this.publishManufacturers(manufacturers)),
          catchError(err => {
            console.log(err, 'ERROR in createManufacturer service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление значения manufacturer
  //  manufacturerID - идентификатор производителя
  public deleteManufacturer(manufacturerID: string) {
    //  проверка, что передали непустоту
    if (manufacturerID != null) {
      //  дергаем сервис
      this.http.delete<ManufacturerServer[]>(apiProto + baseUrl + urlManufacturer.delete, {
        params: new HttpParams().set(`id`, manufacturerID)
      })
        .pipe(
          //  конвертация массива
          map(manufacturersServer =>
            manufacturersServer.map(manufacturerServer => this.convertService.manufacturerServerToClient(manufacturerServer))),
          //  публикуем список
          tap(manufacturers => this.publishManufacturers(manufacturers)),
          catchError(err => {
            console.log(err, 'ERROR in deleteManufacturer service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значений производителей
  public getManufacturers() {
    //  дергаем соответствующий сервис
    this.http.get<ManufacturerServer[]>(apiProto + baseUrl + urlManufacturer.readAll)
      .pipe(
        //  конвертация массива
        map(manufacturersServer =>
          manufacturersServer.map(manufacturerServer => this.convertService.manufacturerServerToClient(manufacturerServer))),
        //  публикуем список
        tap(manufacturers => this.publishManufacturers(manufacturers)),
        catchError(err => {
          console.log(err, 'ERROR in getManufacturers service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение значения производителя
  //  manufacturerID - идентификатор производителя
  public getManufacturerByID(manufacturerID: string) {
    //  дергаем соответствующий сервис
    this.http.get<ManufacturerServer>(apiProto + baseUrl + urlManufacturer.readCurrent, {
      params: new HttpParams().set(`id`, manufacturerID)
    })
      .pipe(
        //  возвращаем  новый массив  со  сконвертированными элементами
        map(manufacturerServer => this.convertService.manufacturerServerToClient(manufacturerServer)),
        tap(manufacturer => {
          //  проверка, что передали непустоту
          if (manufacturerID) {
            //  публикуем новый manufacturer
            this.manufacturer.next(manufacturer);
          } else {
            this.manufacturer.next(GenerateService.createManufacturer());
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getManufacturerByID service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка  manufacturers - вспомогательная функция
  private publishManufacturers(manufacturers: Manufacturer[]) {
    //  проверка, что передали непустоту
    if (manufacturers != null) {
      // console.log('manufacturers:', manufacturers);
      //  публикуем список
      this.manufacturers.next(manufacturers);
    } else {
      //  публикуем null
      this.manufacturers.next(null);
    }
  }

  //  изменение значения manufacturer
  //  manufacturer - экземпляр производителя
  public updateManufacturer(manufacturer: Manufacturer) {
    const serverManufacturer: ManufacturerServer = this.convertService.manufacturerClientToServer(manufacturer);
    // console.log('serverManufacturer', serverManufacturer);
    //  проверка, что передали непустоту
    if (serverManufacturer != null) {
      //  дергаем сервис
      this.http.patch<ManufacturerServer[]>(apiProto + baseUrl + urlManufacturer.update, serverManufacturer)
        .pipe(
          //  конвертация массива
          map(manufacturersServer =>
            manufacturersServer.map(manufacturerServer => this.convertService.manufacturerServerToClient(manufacturerServer))),
          //  публикуем список
          tap(manufacturersServer => this.publishManufacturers(manufacturersServer)),
          catchError(err => {
            console.log(err, 'ERROR in updateManufacturer service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  ****  OPTION  *********
  //  создание нового значения опции
  public createOption(option: Option) {
    const serverOption: OptionServer = this.convertService.optionClientToServer(option);
    //  дергаем соответствующий сервис
    if (option != null) {
      //  дергаем сервис
      this.http.put<OptionServer[]>(apiProto + baseUrl + urlOption.create, serverOption)
        .pipe(
          //  конвертация массива
          map(optionsServer =>
            optionsServer.map(optionServer => this.convertService.optionServerToClient(optionServer))),
          //  публикуем список
          tap(options => this.publishOptions(options)),
          catchError(err => {
            console.log(err, 'ERROR in createOption service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление значения опции
  public deleteOption(option: Option) {
    const serverOptionId: string = String(this.convertService.optionClientToServer(option).ID);
    //  дергаем соответствующий сервис
    this.http.delete<OptionServer[]>(apiProto + baseUrl + urlOption.delete, {
      params: new HttpParams().set('id', serverOptionId)
    })
      .pipe(
        //  конвертация массива
        map(optionsServer =>
          optionsServer.map(optionServer => this.convertService.optionServerToClient(optionServer))),
        //  публикуем список
        tap(options => this.publishOptions(options)),
        catchError(err => {
          console.log(err, 'ERROR in deleteManufacturer service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение конкретной опции
  //  id - идентификатор опции
  public getOptionByID(optionID: string) {
    //  дергаем соответствующий сервис
    if (optionID) {
      //  если передали ид - дергаем сервис
      this.http.get<OptionServer>(apiProto + baseUrl + urlOption.readCurrent, {
        params: new HttpParams().set('id', optionID)
      })
        .pipe(
          //  возвращаем  новый массив  со  сконвертированными элементами
          map(optionServer => this.convertService.optionServerToClient(optionServer)),
          tap(option => {
            //  проверка, что передали непустоту
            if (optionID) {
              //  публикуем новый manufacturer
              this.option.next(option);
            } else {
              this.option.next(GenerateService.createOption());
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getOptionByID service');
            return of(null);
          })
        ).subscribe();
    } else {
      //  передали пустой ИД - генериим новую опцию
      this.option.next(GenerateService.createOption());
    }
  }

  //  получение значений опций
  public getOptions() {
    //  дергаем соответствующий сервис
    this.http.get<OptionServer[]>(apiProto + baseUrl + urlOption.readAll)
      .pipe(
        //  конвертация массива
        map(optionsServer =>
          optionsServer.map(optionServer => this.convertService.optionServerToClient(optionServer))),
        //  публикуем список
        tap(options => {
          this.publishOptions(options);
        }),
        catchError(err => {
          console.log(err, 'ERROR in getOptions service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка опций - вспомогательная функция
  //  optionsServer - массив опций серверного типа
  private publishOptions(options: Option[]) {
    //  проверка, что передали непустоту
    if (options != null) {
      // console.log('options:', options);
      //  публикуем список
      this.options.next(options);
    } else {
      //  публикуем null
      this.options.next(null);
    }
  }

  //  изменение значения опции
  public updateOption(option: Option) {
    console.log('option: ', option);
    const serverOption: OptionServer = this.convertService.optionClientToServer(option);
    //  дергаем соответствующий сервис
    if (option != null) {
      //  дергаем сервис
      this.http.patch<OptionServer[]>(apiProto + baseUrl + urlOption.update, serverOption)
        .pipe(
          //  конвертация массива
          map(optionsServer =>
            optionsServer.map(optionServer => this.convertService.optionServerToClient(optionServer))),
          //  публикуем список
          tap(options => this.publishOptions(options)),
          catchError(err => {
            console.log(err, 'ERROR in updateOption service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  ****  OPTION TYPES  *******
  //  получение списка существующих типов опций
  public getOptionTypes() {
    //  дергаем соответствующий сервис
    this.http.get<OptionTypeServer[]>(apiProto + baseUrl + urlOptionType.readAll)
      .subscribe((optionTypesServer: OptionTypeServer[]) => {
        //  задаем начальное значение
        const newOptionTypes: OptionType[] = [];
        //  если передали не пустоту - перебираем
        if (optionTypesServer) {
          optionTypesServer.forEach((optionTypeServer: OptionTypeServer) => {
            newOptionTypes.push(this.convertService.optionTypeServerToClient(optionTypeServer));
          });
          //  публикуем новое значение
          this.optionTypes.next(newOptionTypes);
        } else {
          //  публикуем пустое значение
          this.optionTypes.next(null);
        }
      }, error => {
        console.log(error, 'ERROR in getOptionTypes service');
      });
  }

  //  ****  ORDERS  **********
  //  получение конкретного ордера
  //  id - идентификатор ордера
  public getOrderByID(orderID: string) {
    //  дергаем соответствующий сервис
    if (orderID) {
      //  если передали ид - дергаем сервис
      this.http.get<OrderServer>(apiProto + baseUrl + urlOrder.readCurrent, {
        params: new HttpParams().set('id', orderID)
      })
        .pipe(
          //  возвращаем  новый массив  со  сконвертированными элементами
          map(orderServer => this.convertService.orderServerToClient(orderServer)),
          tap(order => {
            //  проверка, что передали непустоту
            if (orderID) {
              //  публикуем новый manufacturer
              this.order.next(order);
            } else {
              this.order.next(null);
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getOptionByID service');
            return of(null);
          })
        ).subscribe();
    } else {
      //  передали пустой ИД - генериим новую опцию
      this.order.next(null);
    }
  }

  //  получение списка существующих orders
  public getOrders() {
    //  дергаем соответствующий сервис
    this.http.get<OrderServer[]>(apiProto + baseUrl + urlOrder.readAll)
      .pipe(
        //  конвертация массива
        map(ordersServer =>
          ordersServer.map(orderServer => this.convertService.orderServerToClient(orderServer))),
        //  публикуем список
        tap(orders => {
          //  проверка, что передали непустоту
          if (orders != null) {
            //  публикуем новое значение
            this.orders.next(orders);
          } else {
            //  публикуем null
            this.orders.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getOrders service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение orders по статусу
  //  orderStatus - статус
  //  offset - смещение в массиве
  //  count - количество orders
  public getOrdersByStatus(orderStatus: OrderStatus, offset: number, limit: number): Observable<Order[]> {
    // console.log('dataService - getOrdersByStatus', orderStatus);
    const realOffset = offset ? offset.toString() : '0';
    const realLimit = limit ? limit.toString() : '50';
    //  если что-то передали
    if (orderStatus && orderStatus.name && orderStatus.name.length) {
      //  дергаем соответствующий сервис
      return this.http.get<OrderServer[]>(apiProto + baseUrl + urlOrder.readByStatus, {
        params: new HttpParams()
          .set(`statusID`, orderStatus.id.toString())
          .append(`offset`, realOffset)
          .append(`limit`, realLimit)
      })
        .pipe(
          //  конвертация массива
          map(ordersServer => ordersServer.map((orderServer: OrderServer) =>
            this.convertService.orderServerToClient(orderServer))),
          // map(newOrders => newOrders.filter(newOrder => newOrder.statusID !== orderStatus.id)),
          //  публикация  массива
          tap(orders => {
              //   console.log('status: ', orderStatus.id);
              //   //  TODO: to map
              //   let newOrders: Order[] = [];
              //   //  убрали старые значения этого статуса
              //   console.log('newOrders from: ', newOrders, this.orders.value);
              //   //  добавили новые значения этого статуса
              //   orders.forEach(order => newOrders.push(order));
              // console.log('newOrders to: ', orders);
              //   //  public
              //   this.orders.next(newOrders);
              return orders;
            }
          ),
          catchError(err => {
            console.log(err, 'ERROR in getOrdersByStatus service');
            return of(null);
          })
        );
    }
  }

  //  изменение статуса ордера orders по статусу
  //  orderID - ид ордера
  //  statusID - ид нового статуса
  public changeOrderStatus(orderID: number, statusID: number, boxNumber: string) {
    //  дергаем соответствующий сервис
    this.http.get<OrderServer[]>(apiProto + baseUrl + urlOrder.changeStatus, {
      params: new HttpParams()
        .set(`orderID`, orderID.toString())
        .set(`statusID`, statusID.toString())
        .set(`boxNumber`, boxNumber)
    })
      .pipe(
        //  конвертация массива
        map(orderServers => orderServers.map((orderServer: OrderServer) => this.convertService.orderServerToClient(orderServer))),
        //  публикация  массива
        tap(orders => {
            this.publishOrders(orders);
          }
        ),
        catchError(err => {
          console.log(err, 'ERROR in changeOrderStatus service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка ров
  public publishOrders(orders: Order[]) {
    //  проверка, что передали непустоту
    if (orders != null) {
      //  публикация  списка
      this.orders.next(orders);
    } else {
      //  публикация  null
      this.orders.next(null);
    }
  }

  //  ****  ORDER PRIORITIES  *******
  //  получение списка существующих приоритетов заказа
  public getOrderPriorities() {
    // console.log('getOrderPriorities');
    //  дергаем соответствующий сервис
    this.http.get<OrderPriorityServer[]>(apiProto + baseUrl + urlOrderPriorities.readAll).pipe(
      //  конвертация массива
      map(orderPrioritiesServer => orderPrioritiesServer.map((orderPriorityServer: OrderPriorityServer) =>
        this.convertService.orderPriorityServerToClient(orderPriorityServer))),
      //  публикация  массива
      tap((orderPriorities: OrderPriority[]) => {
        this.orderPriorities.next(orderPriorities);
        // console.log('orderPriorities: ', orderPriorities);
      }),
      catchError(err => {
        console.log(err, 'ERROR in getOrderPriorities service');
        return of(null);
      })
    ).subscribe();
  }

  //  ****  ORDER STATUSES  ********
  //  получение списка существующих статусов заказа
  public getOrderStatuses() {
    //  дергаем соответствующий сервис
    return this.http.get<OrderStatusServer[]>(apiProto + baseUrl + urlOrderStatuses.readAll).pipe(
      //  конвертация массива
      map(orderStatusesServer => orderStatusesServer.map((orderStatusServer: OrderStatusServer) =>
        this.convertService.orderStatusServerToClient(orderStatusServer))),
      //  публикация  массива
      tap((orderStatuses: OrderStatus[]) => this.orderStatuses.next(orderStatuses)),
      catchError(err => {
        console.log(err, 'ERROR in getBrands service');
        return of(null);
      })
    ).subscribe();
  }

  //  ****  PAYMENTS  **********
  //  создание нового payment
  //  payment - экземпляр способа оплаты
  public createPayment(payment: Payment) {
    const serverPayment: PaymentServer = this.convertService.paymentClientToServer(payment);
    //  проверка, что передали непустоту
    if (payment != null) {
      //  дергаем сервис
      this.http.put<PaymentServer[]>(apiProto + baseUrl + urlPayment.create, serverPayment)
        .pipe(
          //  конвертация массива
          map(paymentsServer =>
            paymentsServer.map(paymentServer => this.convertService.paymentServerToClient(paymentServer))),
          //  публикуем список
          tap(payments => this.publishPayments(payments)),
          catchError(err => {
            console.log(err, 'ERROR in createPayment service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление значения payment
  //  paymentID - идентификатор способа оплаты
  public deletePayment(paymentID: string) {
    //  проверка, что передали непустоту
    if (paymentID != null) {
      //  дергаем сервис
      this.http.delete<PaymentServer[]>(apiProto + baseUrl + urlPayment.update, {
        params: new HttpParams().set(`id`, paymentID)
      }).pipe(
        //  конвертация массива
        map(paymentsServer =>
          paymentsServer.map(paymentServer => this.convertService.paymentServerToClient(paymentServer))),
        //  публикуем список
        tap(payments => this.publishPayments(payments)),
        catchError(err => {
          console.log(err, 'ERROR in deletePayment service');
          return of(null);
        })
      ).subscribe();
    }
  }

  //  перемещение payment
  //  payment - экземпляр способа оплаты
  public changePriorityOfPayment(payment: Payment) {
    const serverPayment: PaymentServer = this.convertService.paymentClientToServer(payment);
    //  проверка, что передали непустоту
    if (serverPayment != null) {
      //  дергаем сервис
      this.http.patch<PaymentServer[]>(apiProto + baseUrl + urlPayment.changePriority, serverPayment)
        .pipe(
          //  конвертация массива
          map(paymentsServer =>
            paymentsServer.map(paymentServer => this.convertService.paymentServerToClient(paymentServer))),
          //  публикуем список
          tap(payments => this.publishPayments(payments)),
          catchError(err => {
            console.log(err, 'ERROR in changePriorityOfPayment service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значений payments
  public getPayments() {
    //  дергаем соответствующий сервис
    this.http.get<PaymentServer[]>(apiProto + baseUrl + urlPayment.readAll)
      .pipe(
        //  конвертация массива
        map(paymentsServer =>
          paymentsServer.map(paymentServer => this.convertService.paymentServerToClient(paymentServer))),
        //  публикуем список
        tap(payments => this.publishPayments(payments)),
        catchError(err => {
          console.log(err, 'ERROR in getPayments service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение payment
  //  paymentID - идентификатор способа оплаты
  public getPaymentByID(paymentID: string) {
    //  дергаем соответствующий сервис
    this.http.get<PaymentServer>(apiProto + baseUrl + urlPayment.readCurrent, {
      params: new HttpParams().set(`id`, paymentID)
    })
      .pipe(
        //  конвертация массива
        map(paymentsServer => {
          const newPaymentsServer = this.convertService.paymentServerToClient(paymentsServer);
          //  возвращаем  новый сконвертированный элемент
          return newPaymentsServer;
        }),
        //  публикация  массива
        tap(payment => {
          //  проверка, что передали непустоту
          if (paymentID) {
            //  публикация  payment
            this.payment.next(payment);
          } else {
            //  публикация  null
            this.payment.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getPaymentByID service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка способов  оплаты - вспомогательная функция
  private publishPayments(payments: Payment[]) {
    //  проверка, что передали непустоту
    if (payments != null) {
      //  публикация  списка
      this.payments.next(payments);
    } else {
      //  публикация  null
      this.payments.next(null);
    }
  }

  //  времянка для добавления payment
  public putPayment(payment: Payment) {
    this.payment.next(payment);
  }

  //  изменение значения payment
  //  payment - экземпляр способа оплаты
  public updatePayment(payment: Payment) {
    const serverPayment: PaymentServer = this.convertService.paymentClientToServer(payment);
    //  проверка, что передали непустоту
    if (serverPayment != null) {
      //  дергаем сервис
      this.http.patch<PaymentServer[]>(apiProto + baseUrl + urlPayment.update, serverPayment)
        .pipe(
          //  публикация  списка
          map(paymentsServer =>
            paymentsServer.map(paymentServer => this.convertService.paymentServerToClient(paymentServer))),
          //  публикуем список
          tap(payments => this.publishPayments(payments)),
          catchError(err => {
            console.log(err, 'ERROR in updatePayment service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  ****  PRODUCTS  **********
  //  создание нового значения опции
  //  product - экземпляр торговой марки
  public createProduct(product: Product) {
    // console.log('createProduct');
    //  конвертируем продукт в серверный тип
    const serverProduct: ProductServer = this.convertService.productClientToServer(product);
    //  дергаем соответствующий сервис
    if (product != null) {
      //  дергаем сервис
      this.http.put<ProductServer[]>(apiProto + baseUrl + urlProduct.create, serverProduct)
        .subscribe((productsServer: ProductServer[]) => {
          //  дергаем функцию публикации
          this.publishProducts(productsServer);
        }, error => {
          console.log(error, 'ERROR in createProduct service');
          //   });
        });
    }
  }

  //  удаление значения product
  //  productID - идентификатор торговой марки
  public deleteProduct(productID: string) {
    //  дергаем соответствующий сервис
    if (productID != null) {
      //  дергаем сервис
      this.http.delete<ProductServer[]>(apiProto + baseUrl + urlProduct.delete, {
        params: new HttpParams().set(`id`, productID)
      })
        .subscribe((productsServer: ProductServer[]) => {
          //  дергаем функцию публикации
          this.publishProducts(productsServer);
        }, error => {
          console.log(error, 'ERROR in getCurrencies service');
          //   });
        });
    }
  }

  //  получение значений продуктов в категории
  //  category - идентификатор торговой марки
  public getProductByCategory(category: Category) {
    //  если что-то передали
    if (category && category.id) {
      //  дергаем соответствующий сервис
      this.http.post<ProductServer[]>(apiProto + baseUrl + urlProduct.readAllByCategory,
        this.convertService.categoryClientToServer(category))
        .subscribe((productsServer: ProductServer[]) => {
          //  дергаем функцию публикации
          this.publishProducts(productsServer);
        }, error => {
          console.log(error, 'ERROR in getProducts service');
        });
    }
  }

  //  получение значений брэнда
  //  productID - идентификатор торговой марки
  public getProductByID(productID: string) {
    //  если что-то передали
    if (productID) {
      //  дергаем соответствующий сервис
      this.http.get<ProductServer>(apiProto + baseUrl + urlProduct.readCurrent, {
        params: new HttpParams().set(`id`, productID)
      })
        .subscribe((productServer: ProductServer) => {
          const newProduct: Product = this.convertService.productServerToClient(productServer);
          //  публикуем новое значение
          this.product.next(newProduct);
        }, error => {
          console.log(error, 'ERROR in getProductByID service');
        });
    } else {
      //  ничего не передавали, делаем текущий брэнд пустым
      const newProd: Product = GenerateService.createProduct();
      // console.log('Новый продукт: ', newProd);
      this.product.next(newProd);
    }
  }

  //  получение значений продуктов
  public getProducts() {
    //  дергаем соответствующий сервис
    this.http.get<ProductServer[]>(apiProto + baseUrl + urlProduct.readAll)
      .subscribe((productsServer: ProductServer[]) => {
        //  дергаем функцию публикации
        this.publishProducts(productsServer);
      }, error => {
        console.log(error, 'ERROR in getProducts service');
      });
  }

  //  получение значений статусов доступности продуктов
  public getProductAvailabilities() {
    //  дергаем соответствующий сервис
    this.http.get<ProductAvailabilityServer[]>(apiProto + baseUrl + urlProductAvailability.readAll)
      .pipe(
        //  конвертация массива
        map(productAvailabilitiesServer => productAvailabilitiesServer
          .map(productAvailabilityServer => this.convertService.productAvailabilityServerToClient(productAvailabilityServer))
        ),
        //  публикация  массива
        tap(productAvailability => {
          //  проверка, что передали непустоту
          if (productAvailability) {
            //  публикация  payment
            this.productAvailability.next(productAvailability);
          } else {
            //  публикация  null
            this.productAvailability.next([]);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getProductAvailabilities service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение значений статусов видимости продуктов
  public getProductVisibilities() {
    //  дергаем соответствующий сервис
    this.http.get<ProductVisibilityServer[]>(apiProto + baseUrl + urlProductVisibility.readAll)
      .pipe(
        //  конвертация массива
        map(productVisibilitiesServer => productVisibilitiesServer
          .map(productVisibilityServer => this.convertService.productVisibilityServerToClient(productVisibilityServer))
        ),
        //  публикация  массива
        tap(productVisibility => {
          //  проверка, что передали непустоту
          if (productVisibility) {
            //  публикация  payment
            this.productVisibility.next(productVisibility);
          } else {
            //  публикация  null
            this.productVisibility.next([]);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getProductAvailabilities service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка продуктов - вспомогательная функция
  //  productsServer - массив продуктов серверного типа
  private publishProducts(productsServer: ProductServer[]) {
    // console.log('publishProducts', productsServer);
    const newProducts: Product[] = [];
    if (productsServer) {
      productsServer.forEach((productServer: ProductServer) => {
        newProducts.push(this.convertService.productServerToClient(productServer));
      });
      //  публикуем новое значение
      this.products.next(newProducts);
    } else {
      //  публикуем null
      this.products.next(null);
    }
  }

  //  публикация нового экземпляра продукта
  //  newProduct - новый экземпляр продукта
  public putNewProduct(newProduct: Product) {
    console.log('new product');
    //  если не пустой - публикуем
    if (newProduct) {
      //  публикуем новое значение
      this.product.next(newProduct);
    }
  }

  //  изменение значения product
  //  product - экземпляр торговой марки
  public updateProduct(product: Product) {
    const serverProduct: ProductServer = this.convertService.productClientToServer(product);
    //  дергаем соответствующий сервис
    if (serverProduct != null) {
      //  дергаем сервис
      this.http.patch<ProductServer[]>(apiProto + baseUrl + urlProduct.update, serverProduct)
        .subscribe((productsServer: ProductServer[]) => {
          //  дергаем функцию публикации
          this.publishProducts(productsServer);
        }, error => {
          console.log(error, 'ERROR in updateProduct service');
        });
    }
  }

  //  ****  REGION  ********************
  //  создание нового региона
  public createRegion(region: Region) {
    const serverRegion: RegionServer = this.convertService.regionClientToServer(region);
    //  проверка, что передали непустоту
    if (region != null) {
      //  дергаем сервис
      this.http.put<RegionServer[]>(apiProto + baseUrl + urlRegion.create, serverRegion)
        .pipe(
          //  конвертация массива
          map(regionsServer =>
            regionsServer.map(regionServer => this.convertService.regionServerToClient(regionServer))),
          tap((regions: Region[]) => {
            //  публикация  массива
            this.publishRegions(regions);
          }),
          catchError(err => {
            console.log(err, 'ERROR in createRegion service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  удаление региона
  public deleteRegion(region: Region) {
    const serverRegionId: string = String(this.convertService.regionClientToServer(region).ID);
    //  проверка, что передали непустоту
    if (region != null) {
      //  дергаем соответствующий сервис
      this.http.delete<RegionServer[]>(apiProto + baseUrl + urlRegion.delete, {
        params: new HttpParams().set('id', serverRegionId)
      }).pipe(
        //  конвертация массива
        map(regionsServer =>
          regionsServer.map(regionServer => this.convertService.regionServerToClient(regionServer))),
        //  публикуем список
        tap(regions => this.publishRegions(regions)),
        catchError(err => {
          console.log(err, 'ERROR in deleteRegion service');
          return of(null);
        })
      ).subscribe();
    }
  }

  public getRegions() {
    //  дергаем соответствующий сервис
    this.http.get<RegionServer[]>(apiProto + baseUrl + urlRegion.readAll)
      .pipe(
        //  конвертация массива
        map(regionsServer =>
          regionsServer.map(regionServer => this.convertService.regionServerToClient(regionServer))),
        //  публикуем список
        tap(regions => this.publishRegions(regions)),
        catchError(err => {
          console.log(err, 'ERROR in getRegions service');
          return of(null);
        })
      ).subscribe();
  }

  //  публикация прилетевшего списка  стран - вспомогательная функция
  private publishRegions(regions: Region[]) {
    //  проверка, что передали непустоту
    if (regions != null) {
      //  публикуем новое значение
      this.regions.next(regions);
    } else {
      //  публикуем null
      this.regions.next(null);
    }
  }

  //  изменение значения region
  public updateRegion(region: Region) {
    //  конвертим в серверный тип
    const serverRegion: RegionServer = this.convertService.regionClientToServer(region);
    //  дергаем соответствующий сервис
    if (serverRegion != null) {
      //  дергаем сервис
      this.http.patch<RegionServer[]>(apiProto + baseUrl + urlRegion.update, serverRegion)
        .pipe(
          //  конвертация массива
          map(regionsServer =>
            regionsServer.map(regionServer => this.convertService.regionServerToClient(regionServer))),
          //  публикация  массива
          tap(regions => this.publishRegions(regions)),
          catchError(err => {
            console.log(err, 'ERROR in updateRegion service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  ****  ROLES ************
  //  получение значений ролей
  public getRoles() {
    //  дергаем соответствующий сервис
    this.http.get<RoleServer[]>(apiProto + baseUrl + urlRole.readAll)
      .subscribe((rolesServer: RoleServer[]) => {
        //  публикуем
        this.publishRoles(rolesServer);
      }, error => {
        console.log(error, 'ERROR in getRoles service');
      });
  }

  //  публикация прилетевшего списка продуктов - вспомогательная функция
  //  rolesServer - массив ролей серверного типа
  private publishRoles(rolesServer: RoleServer[]) {
    //  задаем начальный массив
    const newRoles: Role[] = [];
    //  проверяем на пустоту
    if (rolesServer) {
      rolesServer.forEach((roleServer: RoleServer) => {
        newRoles.push(this.convertService.roleServerToClient(roleServer));
      });
      //  публикуем новое значение
      this.roles.next(newRoles);
    } else {
      //  публикуем null
      this.roles.next(null);
    }
  }

  //  **********************************
  //  получение значений меню
  public getMenu() {
    //  создаем переменную под меню
    const newMenu: Menu[] = GenerateService.generateMenu();
    //  публикуем новое значение
    this.menu.next(newMenu);
  }

  //  функция инициализации
  private initial() {
    this.getCurrencies();
    this.getCategories();
    this.getLanguages();
    this.getLocalizedKeywords();
    this.getOrderPriorities();
    this.getOrderStatuses();
    this.getImageTypes();
    this.getOptions();
    this.getCountries();
    this.getCountriesShort();
  }
}
