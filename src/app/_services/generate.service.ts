import {Injectable} from '@angular/core';
import {LocalizedDescription, LocalizedDescriptionServer, LocalizedText} from '../_models/localization';
import {Brand, Manufacturer} from '../_models/brands.types';
import {Assembly} from '../_models/assemblies.types';
import {Menu} from '../_models/menu.types';
import {Delivery, Zone} from '../_models/deliveries.types';
import {Image} from '../_models/image.types';
import {Category} from '../_models/categories.types';
import {User} from '../_models/users.types';
import {Option, OptionType, OptionValue } from '../_models/options.types';
import {Product } from '../_models/products.types';
import {Order } from '../_models/orders.types';
import {Customer} from '../_models/customers.types';
import {Item } from '../_models/items.types';
import {Currency} from '../_models/currencies.types';
import {Shop} from '../_models/shops.types';
import {Address, Country, Region} from '../_models/address.types';
import {Incoming} from '../_models/incomings.types';
import {CartItem} from '../_models/cart.types';

@Injectable({
  providedIn: 'root'
})
export class GenerateService {

  constructor() {
  }

  //  создать пустую локаль
  public static createLocale(): LocalizedDescription[] {
    const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return [{
      language: 'RU',
      title: 'Заголовок ' + randomString,
      name: 'Название ' + randomString,
      description: 'Название ' + randomString,
    },
      {
        language: 'EN',
        title: 'Title ' + randomString,
        name: 'Name ' + randomString,
        description: 'Description ' + randomString,
      }
    ];
  }

  //  создать пустую локаль
  public static createLocaleText(): LocalizedText[] {
    const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return [{
      language: 'RU',
      text: 'Текст ' + randomString,
    },
      {
        language: 'EN',
        text: 'Text ' + randomString,
      }
    ];
  }

  //  создать пустую торговую марку
  public static createBrand(): Brand {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      priority: 50,
      image: GenerateService.createImage(),
      localizedDescriptions: GenerateService.createLocale(),
      seoLinks: '',
      ownerID: null,
      ownerType: null
    };
  }

  //  создать пустую категорию
  public static createCategory(categories?: Category[]): Category {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      parentID: null, //  ID родительской категории
      localizedDescriptions: GenerateService.createLocale(), //  локализованное описание
      localizedSEOs: GenerateService.createLocale(), //  локализованное описание
      imageBackground: GenerateService.createImage(), // фон
      imageIcon: GenerateService.createImage(), // иконка
      // firstImage: null, //  первая картинка
      // secondImage: null,  //  вторая картинка
      images: [], // картинки
      categories: [],  //  дети
      priority: categories ? categories.length + 1 : 1,
      url: '',
      seoLink: '',
      isEnabled: false
    };
  }

  //  создать пустую delivery
  public static createDelivery(priority: number): Delivery {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      localizedDescriptions: GenerateService.createLocale(),
      image: null,
      weightType: null,
      color: '#ffffff',
      zones: [],
      enabled: true,
      priority  //  приоритет
    };
  }

  //  создать пустую картинку
  public static createImage(type?: number): Image {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      priority: 5,
      name: 'default',
      url: 'none',
      options: null,
      optionsID: null,
      type: {
        id: 1,
        name: 'default',
        path: 'default'
      },
      typeID: type ? type : 1,
    };
  }

  //  создать пустую товарную накладную
  public static createIncoming(): Incoming {
    const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      invoice: randomString,
      comment: '',
      scan: this.createImage(),
      scanID: 0,
      checked: false,
      incomingItems: [],
    };
  }

  //  create random item
  public static createItem(): Item {
    //  variable
    let item: Item;
    //  contain
    item = {
      product: this.generateProduct(), //  продукт
      productID: 0, //  ид продукта
      deliveryPackageNumber: 1,  //  в каком из пакетов заказа этот айтем
      optionValues: [],  //  optionValues
      // optionsSets: [], //  optionsSet
      //  TODO: продумать статусы для разных складов
      status: null, //  status item
      placement: null,
      warehouseBoxNumber: Math.floor(Math.random() * 50) + 1, //  номер коробки на складе
      order: null, //  заказ
      comments: null, //  комментс`
      price: null,  //  цена продажи
      currency: null, //  валюта на момент покупки
      sale: null, //  скидка
      assembled: false,
      checked: false,
    };
    //  return result
    return item;
  }

  //  создать пустой производитель
  public static createManufacturer(): Manufacturer {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      name: '', //  название
      cent: 0, //  процент надбавки
      color: '',  //  цвет
      contacts: '', //  контактное лицо
      site: '', //  сайт производителя
      email: '',  //  почта
      vk: '', //  линк на ВК
      phone: '',  //  телефон
      bank: '', //  банковские реквизиты
      contract: '', //  данные контракта
      discount: '', //  скидка
      address: '',  //  адрес
      delivery: '', //  доставка
      comment: '',  //  комментарий
      brands: null
    };
  }

  //  создать пустую торговую марку
  public static createOption(): Option {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      localizedDescriptions:  GenerateService.createLocale(), //  локализованное описание
      priority: 5, //  приоритет
      type: GenerateService.createOptionType(), //  тип опции 1
      optionTypeID: 1,
      enabled: true, //  включено / выключено
      optionValues: [], //  варианты значений опции
    };
  }

  //  создать пустую торговую марку
  public static createOptionValue(): OptionValue {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      localizedDescriptions:  GenerateService.createLocale(), //  локализованное описание
      image: GenerateService.createImage(), // картинка
      imageID: 0, //  imageID
      us: '', //  US
      int: '',  //  INT
      ru: '', //  RU
      inch: '', //  INCH
      cent: '', //  CENT
      enabled: true, //  видимость
      priority: 5, //  приоритет
      option: GenerateService.createOption()
    };
  }

  //  создать пустой тип опции
  public static createOptionType(): OptionType {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      id: 1,
      name: 'color',
    };
  }

  //  создать пустой продукт
  public static createProduct(): Product {
    return {
      imageFront: GenerateService.generateImage(), // картинка
      imageFrontID: 1, //  ид картинки
      imageBack: GenerateService.generateImage(), // картинка
      imageBackID: 1, //  ид картинки
      productImages: [],
      items: [],  //  items
      localizedDescriptions: GenerateService.createLocale(), //  локализованное описание
      localizedFeatures: GenerateService.createLocaleText(), //  локализованые детали
      localizedMaterial: GenerateService.createLocaleText(), //  локализованые материалы
      localizedSEOs: GenerateService.createLocale(), //  сео расширенный
      localizedProductTypes: GenerateService.createLocale(), //  тип товара для таможни и не только
      seoLink: '',  //  сео линк
      brand: GenerateService.createBrand(), //  торговая марка
      brandID: null,  //  ID brand
      manufacturer: GenerateService.createManufacturer(), //  производитель
      manufacturerID: null, //  ID manufacturer
      price: {buy: 0, sell: 0}, //  цены
      categories: [], //  категории
  //  parameters
      restrictedDeliveries: [], //  запрещенные доставки
      weight: 0, // вес
      volumeWeightX: 0, //  объемный вес
      volumeWeightY: 0, //  объемный вес
      volumeWeightZ: 0, //  объемный вес
      fstek: '', // FSTEK
      warehousePosition: '',  //  позиция на складе
      article: '', //  артикул производителя
      priority: 1, // приоритет при отображении
      productAvailability: null, //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
      productAvailabilityID: 1, //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
      //  tslint:disable-next-line
      productVisibility: null, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
      productVisibilityID: 1, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
      productOptions: [], //  список доступных продукту опций
      optionsSets: [], //  список массивов опций
      sale: null, //  скидка
      type: null,
    };
  }

  //  создать пустого пользователя
  public static createUser(): User {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {

      username: GenerateService.generateRandomString('user', '_'),  //  юзернаме
      login: GenerateService.generateRandomString('login', '_'), //  логин
      description: GenerateService.generateRandomString('Описание', '_'), //  описание
      email: GenerateService.generateRandomString('Описание', '') + '@'
        + GenerateService.generateRandomString('mail', '-') + '.com', //  описание
      password: null,  //  пароль
      image: null,  //  картинка пользователя
      roles: [], // список ролей пользователя
    };
  }


  //  создать пустую zone
  public static createZone(): Zone {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      countries: [],  //  страны
      zoneCosts: []   //  цены
    };
  }

  //  ****  GENERATE  *****

  //  создать случайную Address
  public static generateAddress(): Address {
    //  variable
    let address: Address;
    //  contain
    address = {
      zip: null, //  название
      country: null, //  страна
      region: null, //  регион
      city: null, //  город
      description: null, //  описание
      firstName: null,  //  имя получателя
      lastName: null,  //  имя получателя
      phone: null,  //  имя получателя
      email: null,  //  имя получателя
      comments: null,  //  имя получателя
    };
    //  return result
    return address;
  }

  //  создать случайную сборку
  public static generateAssembly(id: number): Assembly {
    //  variable
    let assembly: Assembly;
    //  contain
    assembly = {
      id,
      order: GenerateService.generateOrder(),
      // order: null,
      deliveryColor: GenerateService.generateColorString(),
      weight:  + Math.floor(Math.random() * 10)
    };
    //  return result
    return assembly;
  }

  //  создать случайную brand
  public static generateBrand(): Brand {
    //  variable
    let brand: Brand;
    //  contain
    brand = null;
    //  return result
    return brand;
  }

  //  создать случайную Customer
  public static generateCustomer(): Customer {
    //  variable
    let customer: Customer;
    //  contain
    customer = null;
    //  return result
    return customer;
  }

  //  рандомный бит
  public static generateBit(): boolean {
    // console.log(Math.floor(Math.random() * 2) === 1);
    return Math.floor(Math.random() * 2) === 1;
  }

  //  генерация случайной шестнадцатиричной цифры
  public static generateColorLeter(): string {
    const randomDigit = Math.floor(Math.random() * 16);
    if (randomDigit < 10) {
      return String(randomDigit);
    } else {
      switch (randomDigit) {
        case 10:
          return 'a';
        case 11:
          return 'b';
        case 12:
          return 'c';
        case 13:
          return 'd';
        case 14:
          return 'e';
        default:
          return 'f';
      }
    }
  }

  //  categories
  public static generateCategory(categories?: Category[]): Category {
    return {
      id: Math.floor(Math.random() * 3 + 1),
      parentID: null, //  ID родительской категории
      localizedDescriptions: GenerateService.createLocale(), //  локализованное описание
      localizedSEOs: GenerateService.createLocale(), //  локализованное описание
      imageBackground: GenerateService.generateImage(), // фон
      imageIcon: GenerateService.generateImage(), // иконка
      // firstImage: GenerateService.generateImage(), // первая картинка
      // secondImage: GenerateService.generateImage(), // вторая картинка
      images: [GenerateService.generateImage(), GenerateService.generateImage()], // картинки
      categories: [],  //  дети
      priority: categories ? categories.length + 1 : 1,
      url: '',
      seoLink: '',
      isEnabled: false
    };
  }

  //  create random color
  public static generateColorString(): string {
    let randomColor: string = '#';
    for (let i = 0; i < 6; i++) {
      randomColor += this.generateColorLeter();
    }
    return randomColor;
  }

  //  deliveries
  public static generateDelivery(priority: number): Delivery {
    return {
      localizedDescriptions: GenerateService.createLocale(), //  локализованное описание
      image: GenerateService.generateImage(), // картинка
      weightType: GenerateService.generateBit(), //  Regular or Volume
      color: GenerateService.generateColorString(), //  color
      zones: [], //  гео зоны
      enabled: GenerateService.generateBit(), //  visilble
      priority
    };
  }

  //  currency
  public static generateCurrency(): Currency {
    // const randomDigit: string = String(Math.floor(Math.random() * 4) + 1);
    return {
      code: 'RU',  //  код
      title: 'Рубль',  //  выводимое название валюты
      multiplier: 1,  //  мнодитель
      symbol: '₽',  //  символ валюты
      before: false,  // положение символа валюты
    };
  }

  //  images
  public static generateImage(): Image {
    const randomDigit: string = String(Math.floor(Math.random() * 4) + 1);
    return {
      name: GenerateService.generateRandomString('Image'),  //  описание
      url: '/assets/img/user-' + randomDigit + '.png', //  url
      priority: 0, //  priority
      options: null,  //  options
      optionsID: null,
      type: null, //  type
      typeID: null,
    };
  }

  //  create random item
  public static generateItem(): Item {
    //  variable
    let item: Item;
    //  contain
    item = {
      product: this.generateProduct(), //  продукт
      productID: 0, //  продукт ID
      deliveryPackageNumber: 1,  //  в каком из пакетов заказа этот айтем
      optionValues: [],  //  optionValues
      // optionsSets: [], //  optionsSet
      //  TODO: продумать статусы для разных складов
      status: null, //  status item
      placement: null,
      warehouseBoxNumber: Math.floor(Math.random() * 50) + 1, //  номер коробки на складе
      order: null, //  заказ
      comments: null, //  комментс
      price: null,  //  цена продажи
      currency: null, //  валюта на момент покупки
      sale: null, //  скидка
      assembled: false,
      checked: false,
    };
    //  return result
    return item;
  }

  //  create random manufacturer
  public static generateManufacturer(): Manufacturer {
    //  variable
    let manufacturer: Manufacturer;
    //  contain
    manufacturer = null;
    //  return result
    return manufacturer;
  }

  //  admin-menu
  public static generateMenu(): Menu[] {
    return [
      {
        id: 1,
        name: 'brands',
        title: 'Brands',
        route: '/brands',
        children: []
      },
      {
        id: 19,
        name: 'images',
        title: 'Images',
        route: '/images',
        children: []
      },
      {
        id: 2,
        name: 'options',
        title: 'Options',
        route: '/options',
        children: []
      },
      {
        id: 2,
        name: 'categories',
        title: 'Menu',
        route: '/categories',
        children: []
      },
      {
        id: 3,
        name: 'deliveries',
        title: 'Delivery',
        route: '/deliveries',
        children: []
      },
      // {
      //   id: 4,
      //   name: 'money',
      //   title: 'Money',
      //   route: '/money',
      //   children: []
      // },
      // {
      //   id: 5,
      //   name: 'currency',
      //   title: 'Currency',
      //   route: '/currencies',
      //   children: []
      // },
      {
        id: 6,
        name: 'payments',
        title: 'Payments',
        route: '/payments',
        children: []
      },
      {
        id: 7,
        name: 'products',
        title: 'Products',
        route: '/products',
        children: []
      },
      {
        id: 8,
        name: 'incomings',
        title: 'Incomings',
        route: '/incomings',
        children: []
      },
      {
        id: 9,
        name: 'orders',
        title: 'Orders',
        route: '/orders',
        children: []
      },
      {
        id: 10,
        name: 'assemblies',
        title: 'Assemblies',
        route: '/assemblies',
        children: []
      },
      {
        id: 11,
        name: 'check-send',
        title: 'Check/Send',
        route: '/check_send',
        children: []
      },
      // {
      //   id: 12,
      //   name: 'menu',
      //   title: 'Admin_Menu',
      //   route: '/menu',
      //   children: []
      // },
      // {
      //   id: 13,
      //   name: 'orion-store',
      //   title: 'Orion Store',
      //   route: '/orion',
      //   icon: 'fa fa-biohazard',
      //   children: []
      // },
      {
        id: 14,
        name: 'logs',
        title: 'Logs',
        route: '/logs',
        children: []
      },
      // {
      //   id: 15,
      //   name: 'ebay',
      //   title: 'eBay',
      //   route: '/ebay',
      //   children: []
      // },
      // {
      //   id: 16,
      //   name: 'purchases',
      //   title: 'Purchases',
      //   route: '/purchases',
      //   children: []
      // },
      // {
      //   id: 17,
      //   name: 'users',
      //   title: 'Users',
      //   route: '/users',
      //   children: []
      // },
      // {
      //   id: 18,
      //   name: 'Returns',
      //   title: 'Returns',
      //   route: '/returns',
      //   children: []
      // },
      {
        id: 19,
        name: 'settings',
        title: 'Settings',
        route: '/settings',
        children: []
      },
    ];
  }

  //  создать случайную сборку
  public static generateOrder(): Order {
    // String(Math.floor(Math.random() * 1000000) + 1),
    // console.log('generateOrder');
    //  variable
    let order: Order;
    const cartItems: CartItem[] = [];
    const items: Item[] = [];
    //  contain Array of Items
    for (let i = 0; i < 10; i++) {
      items.push(GenerateService.generateItem());
    }

    // console.log('generateOrder - order');
    //  contain
    order = {
      orderID: String(Math.floor(Math.random() * 1000000) + 1),  //  Идентификатор для людей
      cartItems,
      items,  //  список айтемов
      status: null, // статус заказа
      statusID: 1, // ID - статус заказа
      customer: GenerateService.generateCustomer(), //  покупатель
      createdShippingAddress: GenerateService.generateAddress(), //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
      currentShippingAddress: GenerateService.generateAddress(),  //  измененный адрес доставки
      comments: '', //  комментарий
      delivery: null, //  доставка
      deliveryID: 0, //  доставка
      deliveryPackageNumber: 1, //  номер пакетов в заказе
      deliveryPackageCount: Math.floor(Math.random() * 10) + 1, //  количество пакетов в заказе
      deliveryPrice: GenerateService.generateRandomNumber(100), // доставка
      warehouseBoxNumber: 1, // номер коробки на складе
      weight: 0,  //  вес
      payment: null, //  оплата
      paymentID: 0, //  оплата
      paymentURL: '', //  оплата
      shop: GenerateService.generateShop(), //  shop
      deliveryCost: 0, //  итоговая цена
      productsCost: 0, //  итоговая цена
      totalCost: 0, //  итоговая цена
      currency: GenerateService.generateCurrency(), //  currency
      currencyID: 0,  // currencyID
      //  history
      trackingNumber: null, //  номер отслеживания
      priority: null, //  приоритет заказа
      priorityID: 2, //  ID - приоритет заказа
      sale: null, //  скидка
      //  shadow
      currencyShadow: null,
      shopShadow: null,
      itemsShadow: null, //  список покупок при создании
      shippingAddressShadow:  GenerateService.generateAddress(),  //  измененный адрес доставки
    };
    //  return result
    return order;
  }

  //  create random product
  public static generateProduct(): Product {
    //  variable
    let product: Product;
    //  contain
    product = {
      imageBack: GenerateService.generateImage(), // картинка
      imageBackID: 1,
      imageFront: GenerateService.generateImage(), // картинка
      imageFrontID: 1,
      productImages: [],
      items: [],  //  items
      localizedDescriptions: GenerateService.createLocale(), //  локализованное описание
      localizedFeatures: GenerateService.createLocaleText(), //  локализованые детали
      localizedMaterial: GenerateService.createLocaleText(), //  локализованые материалы
      localizedSEOs: GenerateService.createLocale(), //  сео расширенный
      seoLink: '',  //  сео линк
      brand: GenerateService.generateBrand(), //  торговая марка
      brandID: null, //  ID - торговая марка
      manufacturer: GenerateService.generateManufacturer(), //  производитель
      manufacturerID: null, //  ID - производитель
      price: null, //  цены
      categories: [GenerateService.generateCategory()], //  категории
  //  parameters
      restrictedDeliveries: null, //  запрещенные доставки
      weight: Math.floor(Math.random() * 100000) + 1, // вес
      volumeWeightX: Math.floor(Math.random() * 100000) + 1, //  объемный вес
      volumeWeightY: Math.floor(Math.random() * 100000) + 1, //  объемный вес
      volumeWeightZ: Math.floor(Math.random() * 100000) + 1, //  объемный вес
      fstek: '', // FSTEK
      warehousePosition: String(Math.floor(Math.random() * 100) + 1),  //  позиция на складе
      localizedProductTypes:  GenerateService.createLocale(), //  тип товара для таможни и не только
      article: 'string', //  артикул производителя
      priority: 1, // приоритет при отображении
      productAvailability: null, //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
      productAvailabilityID: 1, //  ID - доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
      //  tslint:disable-next-line
      productVisibility: null, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
      productVisibilityID: 1, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
      productOptions: null, //  список доуступных продукту опций
      optionsSets: null, //  список массивов опций
      sale: null, //  скидка
      type: null, //  скидка
    };
    //  return result
    return product;
  }

  //  генерация случайного числа
  public static generateRandomNumber(maxSize?: number): number {
    //  если текст передали
    if (maxSize && maxSize > 0) {
        //  если разделитель не передали
        return Math.floor(Math.random() * maxSize) + 1;
    } else {
      //  если текст не передали
      return Math.floor(Math.random() * 1000) + 1;
    }
  }

  //  генерация названия с случайными цифрами в конце
  public static generateRandomString(text?: string, splitter?: string): string {
    //  если текст передали
    if (text && text.length) {
      if (splitter && splitter.length) {
        //  если разделитель передали
        return text + splitter + String(Math.floor(Math.random() * 1000) + 1);
      } else {
        //  если разделитель не передали
        return text + ' ' + String(Math.floor(Math.random() * 1000) + 1);
      }
    } else {
      //  если текст не передали
      return 'text ' + String(Math.floor(Math.random() * 1000) + 1);
    }
  }

  //  shop
  public static generateShop(): Shop {
    return {
      name: 'grey-shop.ru',  //  описание
    };
  }
}

