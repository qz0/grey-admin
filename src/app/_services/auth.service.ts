import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';

import {ConvertService} from './convert.service';
import {Role, RoleServer, User, UserServer} from '../_models/users.types';
import {baseUrl, urlAuth, urlUser, urlRole, apiProto} from '../_models/urls';
import {HttpStatusTypes} from '../_models/http-status.types';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

//  сервис авторизации
export class AuthService {  //  public
  // расшифрованный токен
  public decodedToken: any;
  public currentUser: User;
  //  casts
  //  каст isLogin
  public castIsLogon: Observable<boolean>;
  //  каст роли
  public castRole: Observable<Role>;
  //  каст ролей
  public castRoles: Observable<Role[]>;
  //  каст пользователя
  public castUser: Observable<User>;
  //  каст пользователей
  public castUsers: Observable<User[]>;

  //  private
  // хелпер для проверки токена
  private jwtHelper = new JwtHelperService();
  //  bs
  //  bs для isLogin
  private isLogon: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  //  bs для роли
  private role: BehaviorSubject<Role> = new BehaviorSubject<Role>(null);
  //  bs для ролей
  private roles: BehaviorSubject<Role[]> = new BehaviorSubject<Role[]>(null);
  //  bs для пользователя
  private user: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  //  bs для пользователей
  private users: BehaviorSubject<User[]> = new BehaviorSubject<User[]>(null);


  //    конструктор
  constructor(private http: HttpClient, private convertService: ConvertService) {
    // привязки кастов и bs
    //  привязываем каст и БС для бита изменяемости карточки brand
    this.castIsLogon = this.isLogon.asObservable();
    //  привязываем каст и БС для бита изменяемости роли
    this.castRole = this.role.asObservable();
    //  привязываем каст и БС для бита изменяемости ролей
    this.castRoles = this.roles.asObservable();
    //  привязываем каст и БС для бита изменяемости пользователя
    this.castUser = this.user.asObservable();
    //  привязываем каст и БС для бита изменяемости пользователей
    this.castUsers = this.users.asObservable();

    this.testToken();

  }

  //  логин
  //  username - логин
  //  password - пароль
  public login(username: string, password: string) {
    // console.log('login: ', username, password);
    //
    // //  дергаем соответствующий сервис
    // this.http.post<{token: string}>(baseProto + baseUrl + urlAuth.signIn, {username, password}, {observe: 'response'})
    //   .subscribe((response) => {
    //     console.log('resp status: ', response.status);
    //     console.log('resp: ', response.body.token);
    //     if (response.status === 200 && response.body && response.body.token) {
    //       //  сохраняем в локалСторадж
    //       localStorage.setItem('token', response.body.token);
    //       this.isLogon.next(true);
    //     }
    //   }, error => {
    //     console.log(error, 'ERROR in setCurrencies service');
    //   });

    //  публикуем новое значение
    // this.currencies.next(newCurrencies);
  }

  //  логоут
  public logout(user: User) {
    // //  грохаем токен в локалСторадж
    // localStorage.removeItem('token');
    // this.isLogon.next(false);
    // console.log('logout');
    //
    // //  дергаем соответствующий сервис
    // this.http.post(baseProto + baseUrl + urlAuth.signOut, user)
    //   .subscribe((httpStatus: HttpStatusTypes) => {
    //     console.log(httpStatus);
    //   }, error => {
    //     console.log(error, 'ERROR in logout service');
    //   });
  }

  //  конвертация прилетевшего списка пользователей - вспомогательная функция
  // private convertUsers(usersServer: UserServer[]) {
  //   //  возвращаем  новый массив  со  сконвертированными элементами
  //   return usersServer.map(userServer => {
  //     //  возвращаем  сконвертированный элемент
  //     return this.convertService.userServerToClient(userServer);
  //   });
  // }

  //  create Role
  //  role - new Role
  public createRole(role: Role) {
    console.log('createUser: ', role);
  }

  //  create User
  //  создание карточки пользователя
  //  user - пользователь
  public createUser(user: User) {
    const serverUser: UserServer = this.convertService.userClientToServer(user);
    //  проверка, что передали непустоту
    if (user != null) {
      //  дергаем сервис
      this.http.put<UserServer[]>(apiProto + baseUrl + urlUser.create, serverUser)
        .pipe(
          //  конвертация массива
          map(usersServer =>
        usersServer.map(userServer => this.convertService.userServerToClient(userServer))),
          //  публикация  массива
          tap(users => this.publishUsers(users)),
          catchError(err => {
            console.log(err, 'ERROR in createUser service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  get Role by ID
  //  roleID - needed Role ID
  public getRoleByID(roleID: number) {
    console.log('getRoleByID: ', roleID);
  }

  //  get User by ID
  //  userID - needed Role ID
  public getUserByID(userID: number) {
    console.log('getUserByID: ', userID);
  }

  //  get All Roles
  //  получение значений ролей
  public getRoles() {
    //  дергаем соответствующий сервис
    this.http.get<RoleServer[]>(apiProto + baseUrl + urlRole.readAll)
      .subscribe((rolesServer: RoleServer[]) => {
        // const serverCurrencies: CurrencyServer = JSON.parse(resp);
        const newRoles: Role[] = [];
        rolesServer.forEach((roleServer: RoleServer) => {
          newRoles.push(this.convertService.roleServerToClient(roleServer));
        });
        //  публикуем новое значение
        this.roles.next(newRoles);
      }, error => {
        console.log(error, 'ERROR in getCurrencies service');
      });
  }

  //  get All Users
  public getUsers() {
    //  дергаем соответствующий сервис
    this.http.get<UserServer[]>(apiProto + baseUrl + urlUser.readAll)
      .pipe(
        //  конвертация массива
        map(usersServer =>
          usersServer.map(userServer => this.convertService.userServerToClient(userServer))),
        //  публикация  массива
        tap(users => this.publishUsers(users)),
        catchError(err => {
          console.log(err, 'ERROR in getUsers service');
          return of(null);
        })
      ).subscribe();
  }

  // //  get All Users
  // public getUsers() {
  //   //  дергаем соответствующий сервис
  //   this.http.get<UserServer[]>(apiProto + baseUrl + urlUser.readAll)
  //   // .pipe(
  //   //   map((userServer: UserServer) => this.convertService.userServerToClient(userServer)),
  //   //   tap((users: User[]) => {
  //   //     this.users.next(users);
  //   //   }),
  //   //   catchError(err => {
  //   //     console.log(err, 'ERROR in getCurrencies service');
  //   //     return of(null);
  //   //   })
  //   // )
  //   //     .cat
  //     .subscribe((usersServer: UserServer[]) => {
  //       // const serverCurrencies: CurrencyServer = JSON.parse(resp);
  //       const newUsers: User[] = [];
  //       console.log('get users: ', newUsers);
  //       usersServer.forEach((userServer: UserServer) => {
  //         newUsers.push(this.convertService.userServerToClient(userServer));
  //       });
  //       //  публикуем новое значение
  //       this.users.next(newUsers);
  //     }, error => {
  //       console.log('Error in getUsers: ', error);
  //       // this.putUsers(3);
  //     });
  // }


  //  delete Role
  //  roleID - deleted Role ID
  public deleteRole(roleID: number) {
    console.log('deleteRole: ', roleID);
  }

  //  удаление пользователя
  //  userID - id пользователя
  public deleteUser(userID: number) {
    //  дергаем соответствующий сервис
    //  дергаем сервис
    this.http.delete<UserServer[]>(apiProto + baseUrl + urlUser.delete, {
      params: new HttpParams().set(`id`, String(userID))
    })
      .pipe(
        //  конвертация массива
        map(usersServer =>
          usersServer.map(userServer => this.convertService.userServerToClient(userServer))),
        //  публикация  массива
        tap(users => {
          //  проверка, что передали непустоту
          if (userID != null) {
            //  публикация  users
            this.publishUsers(users);
          } else {
            //  публикация  null
            this.users.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in deleteUser service');
          return of(null);
        })
      ).subscribe();
  }

  // //  удаление пользователя
  // //  userID - id пользователя
  // public deleteUser(userID: number) {
  //   //  дергаем соответствующий сервис
  //   if (userID != null) {
  //     //  дергаем сервис
  //     this.http.delete<UserServer[]>(apiProto + baseUrl + urlUser.delete, {
  //       params: new HttpParams().set(`id`, String(userID))
  //     })
  //       .subscribe((usersServer: UserServer[]) => {
  //         const users: User[] = [];
  //         if (usersServer) {
  //           usersServer.forEach(userServer => {
  //             users.push(this.convertService.userServerToClient(userServer));
  //             //  публикуем полученные опции
  //             this.users.next(users);
  //           }, error => {
  //             console.log(error, 'ERROR in deleteUser service');
  //           });
  //         } else {
  //           this.users.next(null);
  //         }
  //       });
  //   }
  // }

  //  публикация прилетевшего списка  пользователей - вспомогательная функция
  private publishUsers(users: User[]) {
    //  проверка, что передали непустоту
    if (users != null) {
      //  публикуем новое значение
      this.users.next(users);
    } else {
      //  публикуем null
      this.users.next(null);
    }
  }


  //  update Role
  //  role - updated Role
  public updateRole(role: Role) {
    console.log('updateRole: ', role);
  }

  //  изменение карточки пользователя
  //  user - пользователь
  public updateUser(user: User) {
    const serverUser: UserServer = this.convertService.userClientToServer(user);
    //  дергаем соответствующий сервис
    if (serverUser != null) {
      //  дергаем сервис
      this.http.patch<UserServer[]>(apiProto + baseUrl + urlUser.update, serverUser)
        .pipe(
          map(usersServer =>
            usersServer.map(userServer => this.convertService.userServerToClient(userServer))),
          tap(users => this.publishUsers(users)),
          catchError(err => {
            console.log(err, 'ERROR in updateUser service');
            return of(null);
          })
        ).subscribe();
    }
  }


  //  TOKEN
  //  заполняем логин
  public testToken() {
    if (localStorage.getItem('token')) {
      this.isLogon.next(true);
    }
  }
}
