import {Injectable} from '@angular/core';
import {Currency, CurrencyServer, CurrencyShadow, CurrencyShadowServer} from '../_models/currencies.types';
import {Brand, BrandServer, Manufacturer, ManufacturerServer} from '../_models/brands.types';
import {
  Language,
  LanguageServer,
  LocalizedDescription,
  LocalizedDescriptionServer,
  LocalizedText,
  LocalizedTextServer,
  LocalizedKeywordServer,
  LocalizedKeyword
} from '../_models/localization';
import {Role, RoleServer, User, UserServer} from '../_models/users.types';
import {
  Option,
  OptionServer,
  OptionsSet,
  OptionsSetServer,
  OptionType,
  OptionTypeServer,
  OptionValue,
  OptionValueServer
} from '../_models/options.types';
import {Image, ImageOptions, ImageOptionsServer, ImageServer, ImageType, ImageTypeServer} from '../_models/image.types';
import {Delivery, DeliveryServer, Zone, ZoneCost, ZoneCostServer, ZoneServer} from '../_models/deliveries.types';
import {Category, CategoryServer} from '../_models/categories.types';
import {
  Order,
  OrderPriority, OrderPriorityServer,
  OrderServer,
  OrderStatus,
  OrderStatusServer,
} from '../_models/orders.types';
import {Payment, PaymentDiscount, PaymentDiscountServer, PaymentServer} from '../_models/payments.types';
import {Purchase, PurchaseServer} from '../_models/purchases.types';
import {
  Address,
  AddressServer,
  Country,
  CountryServer,
  CountryShort,
  CountryShortServer,
  Region,
  RegionServer
} from '../_models/address.types';
import {Item, ItemServer, ItemShadow, ItemShadowServer, ItemStatus, ItemStatusServer} from '../_models/items.types';
import {Customer, CustomerRank, CustomerRankServer, CustomerServer} from '../_models/customers.types';
import {Sale, SaleServer} from '../_models/sales.types';
import {
  Product, ProductAvailability, ProductAvailabilityServer, ProductImage, ProductImageServer, ProductOption, ProductOptionServer,
  ProductServer,
  ProductShadow,
  ProductShadowServer, ProductVisibility, ProductVisibilityServer,
} from '../_models/products.types';
import {Price, PriceServer} from '../_models/prices.types';
import {Assembly, AssemblyServer} from '../_models/assemblies.types';
import {Incoming, IncomingItem, IncomingItemServer, IncomingServer} from '../_models/incomings.types';
import {Shop, ShopServer, ShopShadowServer, ShopShadow} from '../_models/shops.types';
import {Cart, CartItem, CartItemServer, CartServer} from '../_models/cart.types';
import { Log, LogServer } from '../_models/logs.types';

@Injectable({
  providedIn: 'root'
})
export class ConvertService {

  constructor() {
  }

  //  ADDRESS
  //  из клиентской в серверную
  public addressClientToServer(clientAddress: Address): AddressServer {
    //  проверка на пустоту
    if (clientAddress) {
      //  создаем начальное значение
      let serverAddress: AddressServer;
      const extended: string[] = [];
      //  конвертация массивов
      if (clientAddress.extended) {
        clientAddress.extended.forEach(item => {
          extended.push(item);
        });
      }
      //  заполняем структуру
      serverAddress = {
        ID: clientAddress.id, //  ИД
        zip: clientAddress.zip, //  почтовый индекс
        country: clientAddress.country ? this.countryClientToServer(clientAddress.country) : null, //  страна
        region: clientAddress.region ? this.regionClientToServer(clientAddress.region) : null, //  регион
        city: clientAddress.city, //  город
        company: clientAddress.company, //  название компании
        description: clientAddress.description, //  описание
        firstName: clientAddress.firstName,  //  имя получателя
        lastName: clientAddress.lastName,  //  имя получателя
        phone: clientAddress.phone,  //  имя получателя
        email: clientAddress.email,  //  имя получателя
        comments: clientAddress.comments,  //  имя получателя
        extended, //
      };
      //  возвращаем сконвертированное значение
      return serverAddress;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public addressServerToClient(serverAddress: AddressServer): Address {
    //  проверка на пустоту
    if (serverAddress) {
      //  создаем начальное значение
      let clientAddress: Address;
      const extended: string[] = [];
      //  конвертация массивов
      if (serverAddress.extended) {
        serverAddress.extended.forEach(item => {
          extended.push(item);
        });
      }
      //  заполняем структуру
      clientAddress = {
        id: serverAddress.ID, //  ИД
        zip: serverAddress.zip, //  почтовый индекс
        country: serverAddress.country ? this.countryServerToClient(serverAddress.country) : null, //  страна
        region: serverAddress.region ? this.regionServerToClient(serverAddress.region) : null, //  регион
        city: serverAddress.city, //  город
        company: serverAddress.company, //  название компании
        description: serverAddress.description, //  описание
        firstName: serverAddress.firstName,  //  имя получателя
        lastName: serverAddress.lastName,  //  имя получателя
        phone: serverAddress.phone,  //  имя получателя
        email: serverAddress.email,  //  имя получателя
        comments: serverAddress.comments,  //  имя получателя
        extended, //
      };
      //  возврат полученного результата
      return clientAddress;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ASSEMBLY
  //  Server >> Client
  public assemblyServerToClient(serverAssembly: AssemblyServer): Assembly {
    //  проверка на пустоту
    if (serverAssembly) {
      //  переменные
      let clientAssembly: Assembly;
      //  заполняем клиента
      clientAssembly = {
        id: serverAssembly.ID,  //  ID
        order: this.orderServerToClient(serverAssembly.order), // номер заказа
        deliveryColor: serverAssembly.deliveryColor, //  цвет доставки
        weight: serverAssembly.weight, //  вес
      };
      //  return
      return clientAssembly;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public assemblyClientToServer(clientAssembly: Assembly): AssemblyServer {
    //  проверка на пустоту
    if (clientAssembly) {
      //  переменные
      let serverAssembly: AssemblyServer;
      //  заполняем клиента
      serverAssembly = {
        ID: clientAssembly.id,
        order: this.orderClientToServer(clientAssembly.order), // номер заказа
        deliveryColor: clientAssembly.deliveryColor, //  цвет доставки
        weight: clientAssembly.weight, //  вес
      };
      //  return
      return serverAssembly;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  BRAND
  //  из клиентской в серверную
  public brandClientToServer(clientBrand: Brand): BrandServer {
    //  проверка на пустоту
    if (clientBrand) {
      //  создаем начальное значение
      let serverBrand: BrandServer;
      const localizedDescriptionArray: LocalizedDescriptionServer[] = [];
      if (clientBrand.localizedDescriptions) {
        clientBrand.localizedDescriptions.forEach(serverLocalizedDescription => {
          const localizedDescription: LocalizedDescriptionServer = {
            ID: serverLocalizedDescription.id,
            language: serverLocalizedDescription.language,
            name: serverLocalizedDescription.name,
            title: serverLocalizedDescription.title,
            description: serverLocalizedDescription.description,
          };
          localizedDescriptionArray.push(this.localizedDescriptionServerToClient(localizedDescription));
        });
      }
      //  заполняем значения
      serverBrand = {
        ID: clientBrand.id,
        image: clientBrand.image ? this.imageClientToServer(clientBrand.image) : null, // url на лого
        priority: clientBrand.priority, //  приоритет
        manufacturer: clientBrand.manufacturer ? this.manufacturerClientToServer(clientBrand.manufacturer) : null, // производитель
        seoLinks: clientBrand.seoLinks, //  seo url
        localizedDescriptions: localizedDescriptionArray, //
        ownerID: clientBrand.ownerID,
        ownerType: clientBrand.ownerType,
      };
      //  возвращаем сконвертированное значение
      return serverBrand;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из серверной в клиентскую
  public brandServerToClient(serverBrand: BrandServer): Brand {
    //  проверка на пустоту
    if (serverBrand) {
      //  создаем начальное значение
      let clientBrand: Brand;
      const localizedDescriptions: LocalizedDescription[] = [];
      //  обход массивов
      if (serverBrand.localizedDescriptions) {
        serverBrand.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }

      //  формируем ответ
      clientBrand = {
        id: serverBrand.ID,
        image: serverBrand.image ? this.imageServerToClient(serverBrand.image) : null, // url на лого
        priority: serverBrand.priority, //  приоритет
        manufacturer: serverBrand.manufacturer ? this.manufacturerServerToClient(serverBrand.manufacturer) : null, // производитель
        seoLinks: serverBrand.seoLinks, //  seo url
        title: localizedDescriptions.find(locale => locale.language === 'EN') ?
          localizedDescriptions.find(locale => locale.language === 'EN').title : 'none',
        localizedDescriptions,
        ownerID: serverBrand.ownerID,
        ownerType: serverBrand.ownerType,
      };
      //  возвращаем сконвертированное значение
      return clientBrand;
    } else {
      return null;
    }
  }
  //  CART
  //  из клиентской в серверную
  public cartClientToServer(clientCart: Cart): CartServer {
    //  проверка на пустоту
    if (clientCart) {
      //  создаем начальное значение
      let serverCart: CartServer;
      const cartItems: CartItemServer[] = [];
      //  проход массивов
      if (clientCart.cartItems) {
        clientCart.cartItems.forEach((cartItem: CartItem) => {
          cartItems.push(this.cartItemClientToServer(cartItem));
        });
      }
      //  заполняем клиента
      serverCart = {
        ID: clientCart.id,  //  ID
        cartItems,  //  список товаров
        order: null,  //  заказ
        orderID: clientCart.orderID,  //  ид заказа
        delivery: null, //  доставка
        deliveryID: clientCart.deliveryID,  //  ид доставки
        payment: null,  //  оплата
        paymentID: clientCart.paymentID,  //  ид оплаты
        address: null,  //  адрес
        addressID: clientCart.addressID,  //  ид адреса
        customer: null, //  заказчик
        customerID: clientCart.customerID,  //  ид заказчика
        comment: clientCart.comment,  //   комментарий
      };
      //  return
      return serverCart;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public cartServerToClient(serverCart: CartServer): Cart {
    //  проверка на пустоту
    if (serverCart) {
      //  создаем начальное значение
      let clientCart: Cart;
      const cartItems: CartItem[] = [];
      //  проход массивов
      if (serverCart.cartItems) {
        serverCart.cartItems.forEach((cartItem: CartItemServer) => {
          cartItems.push(this.cartItemServerToClient(cartItem));
        });
      }
      clientCart = {
        id: serverCart.ID,
        cartItems,  //  список товаров
        order: null,  //  заказ
        orderID: serverCart.orderID,  //  ид заказа
        delivery: null, //  доставка
        deliveryID: serverCart.deliveryID,  //  ид доставки
        payment: null,  //  оплата
        paymentID: serverCart.paymentID,  //  ид оплаты
        address: null,  //  адрес
        addressID: serverCart.addressID,  //  ид адреса
        customer: null, //  заказчик
        customerID: serverCart.customerID,  //  ид заказчика
        comment: serverCart.comment,  //   комментарий
      };
      //  возврат полученного результата
      return clientCart;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  CART ITEM
  //  из клиентской в серверную
  public cartItemClientToServer(clientCartItem: CartItem): CartItemServer {
    //  проверка на пустоту
    if (clientCartItem) {
      //  создаем начальное значение
      let serverCartItem: CartItemServer;
      const optionValues: OptionValueServer[] = [];
      //  обход массивов
      if (clientCartItem.optionValues) {
        clientCartItem.optionValues.forEach(optionValue => {
          optionValues.push(this.optionValueClientToServer(optionValue));
        });
      }
      //  заполняем клиента
      serverCartItem = {
        ID: clientCartItem.id,  //  ID
        product: null,  //  продукт
        productID: clientCartItem.productID,  //  ID продукта
        optionValues, //  значения опций
        orderCount: clientCartItem.orderCount,  //  необходимое количество
        availableCount: clientCartItem.availableCount,  //  есть в наличии
        cartID: clientCartItem.cartID,  //   ID cart
      };
      //  return
      return serverCartItem;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public cartItemServerToClient(serverCartItem: CartItemServer): CartItem {
    //  проверка на пустоту
    if (serverCartItem) {
      //  создаем начальное значение
      let clientCartItem: CartItem;
      const optionValues: OptionValue[] = [];
      //  обход массивов
      if (serverCartItem.optionValues) {
        serverCartItem.optionValues.forEach(optionValue => {
          optionValues.push(this.optionValueServerToClient(optionValue));
        });
      }
      clientCartItem = {
        id: serverCartItem.ID,
        product: null,  //  продукт
        productID: serverCartItem.productID,  //  ID продукта
        optionValues, //  значения опций
        orderCount: serverCartItem.orderCount,  //  необходимое количество
        availableCount: serverCartItem.availableCount,  //  есть в наличии
        cartID: serverCartItem.cartID,  //   ID cart
      };
      //  возврат полученного результата
      return clientCartItem;
    } else {
      //  возвращаем null
      return null;
    }
  }


  //  CATEGORY
  //  client >> server
  public categoryClientToServer(clientCategory: Category): CategoryServer {
    //  проверка на пустоту
    if (clientCategory) {
      //  создаем начальное значение
      let serverCategory: CategoryServer;
      const localizedDescriptions: LocalizedDescription[] = [];
      const localizedSEOs: LocalizedDescription[] = [];
      const categories: CategoryServer[] = [];
      const images: ImageServer[] = [];
      //  обход массивов
      if (clientCategory.localizedDescriptions) {
        clientCategory.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientCategory.localizedSEOs) {
        clientCategory.localizedSEOs.forEach(localDescription => {
          localizedSEOs.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientCategory.categories) {
        clientCategory.categories.forEach((category: Category) => {
          categories.push(this.categoryClientToServer(category));
        });
      }
      if (clientCategory.images) {
        clientCategory.images.forEach((image: Image) => {
          images.push(this.imageClientToServer(image));
        });
      }
      //  фомируем категорию
      serverCategory = {
        ID: clientCategory.id,  // ID категории
        parentID: clientCategory.parentID, //  ID родительской категории
        localizedDescriptions,  //  описания
        localizedSEOs,  //  SEO
        imageBackground: clientCategory.imageBackground ? this.imageClientToServer(clientCategory.imageBackground) : null, //  фон
        imageIcon: clientCategory.imageIcon ? this.imageClientToServer(clientCategory.imageIcon) : null, //  иконка
        // firstImage: clientCategory.firstImage ? this.imageClientToServer(clientCategory.firstImage) : null, //  первая картинка
        // secondImage: clientCategory.secondImage ? this.imageClientToServer(clientCategory.secondImage) : null, //  вторая картинка
        images,  //  картинки
        priority: clientCategory.priority, //  приоритет в списке
        categories, //  дети
        url: clientCategory.url, // url
        seoLink: clientCategory.seoLink, // seoLink
        isEnabled: clientCategory.isEnabled, // видимость
        type: 'category', //  тип для брокера
      };
      //  возвращаем значение
      return serverCategory;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  server >> client
  public categoryServerToClient(serverCategory: CategoryServer): Category {
    //  проверка на пустоту
    if (serverCategory) {
      //  создаем начальное значение
      let clientCategory: Category;
      const localizedDescriptions: LocalizedDescription[] = [];
      const localizedSEOs: LocalizedDescription[] = [];
      const categories: Category[] = [];
      const images: Image[] = [];
      //  обход массивов
      serverCategory.localizedDescriptions.forEach(localDescription => {
        localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
      });
      if (serverCategory.localizedSEOs) {
        serverCategory.localizedSEOs.forEach(localizedSEO => {
          localizedSEOs.push(this.localizedDescriptionServerToClient(localizedSEO));
        });
      }
      //  категории
      if (serverCategory.categories) {
        serverCategory.categories.forEach((category: CategoryServer) => {
          categories.push(this.categoryServerToClient(category));
        });
      }
      //  картинки
      if (serverCategory.images) {
        serverCategory.images.forEach((image: ImageServer) => {
          images.push(this.imageServerToClient(image));
        });
      }

      //  фомируем категорию
      clientCategory = {
        id: serverCategory.ID,  // ID категории
        parentID: serverCategory.parentID, //  ID родительской категории
        localizedDescriptions,  //  описания
        localizedSEOs,  //  описания
        imageBackground: serverCategory.imageBackground ? this.imageServerToClient(serverCategory.imageBackground) : null, //  фон
        // firstImage: serverCategory.firstImage ? this.imageServerToClient(serverCategory.firstImage) : null, //  первая картинка
        // secondImage: serverCategory.secondImage ? this.imageServerToClient(serverCategory.secondImage) : null, //  вторая картинка
        imageIcon: serverCategory.imageIcon ? this.imageServerToClient(serverCategory.imageIcon) : null, //  иконка
        images,  //  картинки
        priority: serverCategory.priority, //  приоритет в списке
        categories, //  дети
        url: serverCategory.url,  //  url
        seoLink: serverCategory.seoLink,  //  seoLink
        isEnabled: serverCategory.isEnabled // видимость
      };
      //  возвращаем значение
      return clientCategory;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  СТРАНА
  //  из клиентской в серверную
  public countryClientToServer(clientCountry: Country): CountryServer {
    //  проверка на пустоту
    if (clientCountry) {
      //  создаем начальное значение
      let serverCountry: CountryServer;
      const regions: RegionServer[] = [];
      const localizedDescriptions: LocalizedDescription[] = [];
      //  конвертация массивов
      if (clientCountry.localizedDescriptions) {
        clientCountry.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientCountry.regions) {
        clientCountry.regions.forEach((clientRegion: Region) => {
          regions.push(this.regionClientToServer(clientRegion));
        });
      }
      serverCountry = {
        ID: clientCountry.id,
        countryID: clientCountry.countryID, //  country_id
        localizedDescriptions,
        name: clientCountry.name,
        nameRU: clientCountry.nameRu,
        regions,
        code2: clientCountry.code2, //  iso code2
        code3: clientCountry.code3, //  iso code3
        status: clientCountry.status,  //  какой-то статус
      };
      //  возвращаем сконвертированное значение
      return serverCountry;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public countryServerToClient(serverCountry: CountryServer): Country {
    //  проверка на пустоту
    if (serverCountry) {
      //  создаем начальное значение
      let clientCountry: Country;
      const regions: Region[] = [];
      const localizedDescriptions: LocalizedDescription[] = [];
      //  конвертация массивов
      if (serverCountry.localizedDescriptions) {
        serverCountry.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      // ковертация регионов
      if (serverCountry.regions) {
        serverCountry.regions.forEach((serverRegion: RegionServer) => {
          regions.push(this.regionServerToClient(serverRegion));
        });
      }
      clientCountry = {
        id: serverCountry.ID,
        countryID: serverCountry.countryID, //  country_id
        localizedDescriptions,
        name: serverCountry.name,
        nameRu: serverCountry.nameRU,
        regions,
        code2: serverCountry.code2, //  iso code2
        code3: serverCountry.code3, //  iso code3
        status: serverCountry.status,  //  какой-то статус
      };
      //  возврат полученного результата
      return clientCountry;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  CountryShort
  //  из клиентской в серверную
  public countryShortClientToServer(clientCountryShort: CountryShort): CountryShortServer {
    //  проверка на пустоту
    if (clientCountryShort) {
      //  создаем начальное значение
      let serverCountryShort: CountryShortServer;
      //  присваиваем
      serverCountryShort = {
        ID: clientCountryShort.id,
        name: clientCountryShort.localizedDescriptions.find(item => item.language === 'EN').name,
        nameRU: clientCountryShort.localizedDescriptions.find(item => item.language === 'RU').name,
      };
      //  возвращаем сконвертированное значение
      return serverCountryShort;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из серверной в клиентскую
  public countryShortServerToClient(serverCountry: CountryShortServer): CountryShort {
    //  проверка на пустоту
    if (serverCountry) {
      //  создаем начальное значение
      let clientCountry: CountryShort;
      const localizedDescriptions: LocalizedDescription[] = [];
      //  EN
      const localeEN: LocalizedDescription = {
        id: null,
        language: 'EN',
        name: serverCountry.name,
        title: serverCountry.name,
        description: '',
        keywords: '',
      };
      //  RU
      const localeRU: LocalizedDescription = {
        id: null,
        language: 'RU',
        name: serverCountry.nameRU,
        title: serverCountry.nameRU,
        description: '',
        keywords: '',
      };
      //  заполняем
      localizedDescriptions.push(localeEN, localeRU);
      clientCountry = {
        id: serverCountry.ID,
        localizedDescriptions,
        title: localeEN.title
      };
      //  возврат полученного результата
      return clientCountry;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ВАЛЮТА
  //  из клиентской в серверную
  public currencyClientToServer(clientCurrency: Currency): CurrencyServer {
    //  проверка на пустоту
    if (clientCurrency) {
      //  создаем начальное значение
      let serverCurrency: CurrencyServer;
      //  заполняем клиента
      serverCurrency = {
        ID: clientCurrency.id,  //  ID
        code: clientCurrency.code,  //  код
        multiplier: clientCurrency.multiplier,  //  множитель
        title: clientCurrency.title,  //  отображаемое название
        symbol: clientCurrency.symbol,  //  символ валюты
        before: clientCurrency.before, //  положение символа валюты
      };
      //  return
      return serverCurrency;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public currencyServerToClient(serverCurrency: CurrencyServer): Currency {
    //  проверка на пустоту
    if (serverCurrency) {
      //  создаем начальное значение
      let clientCurrency: Currency;
      clientCurrency = {
        id: serverCurrency.ID,  //  ID
        code: serverCurrency.code,  //  код
        multiplier: serverCurrency.multiplier,  //  множитель
        title: serverCurrency.title,  //  отображаемое название
        symbol: serverCurrency.symbol,  //  символ валюты
        before: serverCurrency.before, //  положение символа валюты
      };
      //  возврат полученного результата
      return clientCurrency;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public currencyShadowClientToServer(clientCurrencyShadow: CurrencyShadow): CurrencyShadowServer {
    //  проверка на пустоту
    if (clientCurrencyShadow) {
      //  создаем начальное значение
      let serverCurrencyShadow: CurrencyShadowServer;
      serverCurrencyShadow = {
        ID: clientCurrencyShadow.id, // ID в бд
        code: clientCurrencyShadow.code, // код валюты
        multiplier: clientCurrencyShadow.multiplier, // множитель валюты
        title: clientCurrencyShadow.title // титул валюты
      };
      //  возвращаем сконвертированное значение
      return serverCurrencyShadow;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public currencyShadowServerToClient(serverCurrencyShadow: CurrencyShadowServer): CurrencyShadow {
    //  проверка на пустоту
    if (serverCurrencyShadow) {
      //  создаем начальное значение
      let clientCurrencyShadow: CurrencyShadow;
      clientCurrencyShadow = {
        id: serverCurrencyShadow.ID,  // ID в бд
        code: serverCurrencyShadow.code, // код валюты
        multiplier: serverCurrencyShadow.multiplier, // множитель валюты
        title: serverCurrencyShadow.title // титул валюты
      };
      //  возврат полученного результата
      return clientCurrencyShadow;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  CUSTOMER
  //  Customer
  //  Server >> Client
  public customerServerToClient(serverCustomer: CustomerServer): Customer {
    //  проверка на пустоту
    if (serverCustomer) {
      //  переменные
      let clientCustomer: Customer;
      const addresses: Address[] = [];
      //  конвертация массивов
      if (serverCustomer.addresses) {
        serverCustomer.addresses.forEach(address => {
          addresses.push(this.addressServerToClient(address));
        });
      }
      //  заполняем клиента
      clientCustomer = {
        id: serverCustomer.ID, // ID в бд
        addresses, //  адресс
        username: serverCustomer.username,  //  логин
        title: serverCustomer.title, //  заголовок
        description: serverCustomer.description, //  описание
        password: serverCustomer.password,  //  пароль
        rank: serverCustomer.rank ? this.customerRankServerToClient(serverCustomer.rank) : null, // список ролей пользователя
      };
      //  return
      return clientCustomer;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public customerClientToServer(clientCustomer: Customer): CustomerServer {
    //  проверка на пустоту
    if (clientCustomer) {
      //  переменные
      let serverCustomer: CustomerServer;
      const addresses: AddressServer[] = [];
      //  конвертация массивов
      clientCustomer.addresses.forEach(address => {
        addresses.push(this.addressClientToServer(address));
      });
      //  заполняем клиента
      serverCustomer = {
        ID: clientCustomer.id,  // ID в бд
        addresses, //  адресс
        username: clientCustomer.username,  //  логин
        title: clientCustomer.title, //  заголовок
        description: clientCustomer.description, //  описание
        password: clientCustomer.password,  //  пароль
        rank: clientCustomer.rank ? this.customerRankClientToServer(clientCustomer.rank) : null, // список ролей пользователя
      };
      //  return
      return serverCustomer;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  customerRank
  //  из серверной в клиентскую
  public customerRankServerToClient(serverCustomerRank: CustomerRankServer): CustomerRank {
    //  проверка на пустоту
    if (serverCustomerRank) {
      //  создаем начальное значение
      let clientCustomerRank: CustomerRank;
      const localizedDescriptions: LocalizedDescription[] = [];
      //  конвертация массивов
      if (serverCustomerRank.localizedDescriptions) {
        serverCustomerRank.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      //  присваиваем значения
      clientCustomerRank = {
        id: serverCustomerRank.ID, //  ID
        localizedDescriptions,  //  локализованное описание
      };
      //  возвращаем сконвертированное значение
      return clientCustomerRank;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public customerRankClientToServer(clientCustomerRank: CustomerRank): CustomerRankServer {
    //  проверка на пустоту
    if (clientCustomerRank) {
      //  создаем начальное значение
      let serverCustomerRank: CustomerRankServer;
      const localizedDescriptions: LocalizedDescriptionServer[] = [];
      //  конвертация массивов
      if (clientCustomerRank.localizedDescriptions) {
        clientCustomerRank.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      //  присваиваем значения
      serverCustomerRank = {
        ID: clientCustomerRank.id, //  ID
        localizedDescriptions,  //  локализованное описание
      };
      //  возвращаем сконвертированное значение
      return serverCustomerRank;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  DELIVERY
  //  Server >> Client
  public deliveryServerToClient(serverDelivery: DeliveryServer): Delivery {
    //  проверка на пустоту
    if (serverDelivery) {
      let clientDelivery: Delivery;
      const image: Image = this.imageServerToClient(serverDelivery.image);
      const localizedDescriptions: LocalizedDescription[] = [];
      const zones: Zone[] = [];
      //  конвертация массивов
      if (serverDelivery.localizedDescriptions) {
        serverDelivery.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      if (serverDelivery.zones) {
        serverDelivery.zones.forEach(zone => {
          zones.push(this.zoneServerToClient(zone));
        });
      }
      clientDelivery = {
        id: serverDelivery.ID, // id в бд
        localizedDescriptions, //  локализованное описание
        image, // url на лого
        weightType: serverDelivery.weightType, //  Regular or Volume
        color: serverDelivery.color, //  color
        zones,  //  гео зоны
        enabled: serverDelivery.enabled,  //  enabled
        priority: serverDelivery.priority,  //  приоритет
      };
      return clientDelivery;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public deliveryClientToServer(clientDelivery: Delivery): DeliveryServer {
    //  проверка на пустоту
    if (clientDelivery) {
      let serverDelivery: DeliveryServer;
      const image: ImageServer = this.imageClientToServer(clientDelivery.image);
      const localizedDescriptions: LocalizedDescription[] = [];
      const zones: ZoneServer[] = [];
      //  конвертация массивов
      if (clientDelivery.localizedDescriptions) {
        clientDelivery.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientDelivery.zones) {
        clientDelivery.zones.forEach(zone => {
          zones.push(this.zoneClientToServer(zone));
        });
      }
      serverDelivery = {
        ID: clientDelivery.id, // id в бд
        localizedDescriptions, //  локализованное описание
        image, // url на лого
        weightType: clientDelivery.weightType, //  Regular or Volume
        color: clientDelivery.color, //  color
        zones,  //  гео зоны
        enabled: clientDelivery.enabled,  //  enabled
        priority: clientDelivery.priority,  //  приоритет
      };
      return serverDelivery;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  IMAGE
  //  из клиентской в серверную
  public imageClientToServer(clientImage: Image): ImageServer {
    //  проверка на пустоту
    if (clientImage) {
      //  создаем начальное значение
      let serverImage: ImageServer;
      serverImage = {
        ID: clientImage.id, // id в бд
        name: clientImage.name, // имя картинки
        url: clientImage.url, // serverImage.id,
        priority: clientImage.priority, // приоритет картинки
        options: this.imageOptionsClientToServer(clientImage.options),  //  options
        optionsID: clientImage.optionsID, //  ID опции
        type: this.imageTypeClientToServer(clientImage.type),  //  цена, //  ty[e
        typeID: clientImage.typeID, //  ID типа
      };
      //  возвращаем сконвертированное значение
      return serverImage;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public imageServerToClient(serverImage: ImageServer): Image {
    //  проверка на пустоту
    if (serverImage) {
      //  создаем начальное значение
      let clientImage: Image;
      clientImage = {
        id: serverImage.ID,    // id в бд
        name: serverImage.name, // имя картинки
        url: serverImage.url, // serverImage.id,
        priority: serverImage.priority, // приоритет картинки
        options: this.imageOptionsServerToClient(serverImage.options),  //  options
        optionsID: serverImage.optionsID, //  ID опции
        type: this.imageTypeServerToClient(serverImage.type),  //  цена, //  ty[e
        typeID: serverImage.typeID, //  ID типа
      };
      //  возврат полученного результата
      return clientImage;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  IMAGE OPTIONS
  //  из клиентской в серверную
  public imageOptionsClientToServer(clientImageOptions: ImageOptions): ImageOptionsServer {
    //  проверка на пустоту
    if (clientImageOptions) {
      //  создаем начальное значение
      let serverImageOptions: ImageOptionsServer;
      serverImageOptions = {
        ID: clientImageOptions.id, // ID опции
        name: clientImageOptions.name, // имя опции
      };
      //  возвращаем сконвертированное значение
      return serverImageOptions;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public imageOptionsServerToClient(serverImageOptions: ImageOptionsServer): ImageOptions {
    //  проверка на пустоту
    if (serverImageOptions) {
      //  создаем начальное значение
      let clientImageOptions: ImageOptions;
      clientImageOptions = {
        id: serverImageOptions.ID, // ID опции
        name: serverImageOptions.name,  // имя опции
      };
      //  возврат полученного результата
      return clientImageOptions;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  IMAGE OPTIONS
  //  из клиентской в серверную
  public imageTypeClientToServer(clientImageType: ImageType): ImageTypeServer {
    //  проверка на пустоту
    if (clientImageType) {
      //  создаем начальное значение
      let serverImageType: ImageTypeServer;
      serverImageType = {
        ID: clientImageType.id,     //  id в бд
        name: clientImageType.name, //  имя
        path: clientImageType.path, //  путь до картинки
      };
      //  возвращаем сконвертированное значение
      return serverImageType;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public imageTypeServerToClient(serverImageType: ImageTypeServer): ImageType {
    //  проверка на пустоту
    if (serverImageType) {
      //  создаем начальное значение
      let clientImageType: ImageType;
      clientImageType = {
        id: serverImageType.ID,  // id в бд
        name: serverImageType.name, //  имя
        path: serverImageType.path, //  путь до картинки
      };
      //  возврат полученного результата
      return clientImageType;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  INCOMING
  //  Server >> Client
  public incomingServerToClient(serverIncoming: IncomingServer): Incoming {
    console.log('incoming:', serverIncoming);
    //  проверка на пустоту
    if (serverIncoming) {
      //  переменные
      let clientIncoming: Incoming;
      const incomingItems: IncomingItem[] = [];
      //  конвертация массивов
      if (serverIncoming.incomingItems != null) {
        serverIncoming.incomingItems.forEach(incomingItem => {
          incomingItems.push(this.incomingItemServerToClient(incomingItem));
        });
      }
      //  заполняем клиента
      clientIncoming = {
        id: serverIncoming.ID, // ID инкоминга
        createdAt: new Date(serverIncoming.CreatedAt),
        invoice: serverIncoming.invoice,  //  номер инвойса
        comment: serverIncoming.comment,  //  comment
        scan: serverIncoming.scan ? this.imageServerToClient(serverIncoming.scan) : null, // скан накладной
        scanID: serverIncoming.scanID,  //  scan ID
        checked: serverIncoming.checked, //  статус првоерки списка
        incomingItems,  //  incoming items
      };
      //  возврат полученного результата
      return clientIncoming;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  CLIENT >> Server
  public incomingClientToServer(clientIncoming: Incoming): IncomingServer {
    //  проверка на пустоту
    if (clientIncoming) {
      //  переменные
      let serverIncoming: IncomingServer;
      const incomingItems: IncomingItemServer[] = [];
      //  заполняем массивы
      if (clientIncoming.incomingItems) {
        clientIncoming.incomingItems.forEach(incomingItem => {
          incomingItems.push(this.incomingItemClientToServer(incomingItem));
        });
      }
      //  заполняем клиента
      serverIncoming = {
        ID: clientIncoming.id, // ID инкоминга
        invoice: clientIncoming.invoice,  //  номер инвойса
        comment: clientIncoming.comment,  //  comment
        scan: clientIncoming.scan ? this.imageClientToServer(clientIncoming.scan) : null, // скан накладной
        scanID: clientIncoming.scanID,  //  scan ID
        checked: clientIncoming.checked, //  статус првоерки списка
        incomingItems,  //  incoming items
      };
      //  возврат полученного результата
      return serverIncoming;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  IncomingItem
  //  Server >> Client
  public incomingItemServerToClient(serverIncomingItem: IncomingItemServer): IncomingItem {
    //  проверка на пустоту
    if (serverIncomingItem) {
      //  переменные
      let clientIncomingItem: IncomingItem;
      const optionValues: OptionValue[] = [];
      //  конвертация массивов
      if (serverIncomingItem.optionValues != null) {
        serverIncomingItem.optionValues.forEach(item => {
          optionValues.push(this.optionValueServerToClient(item));
        });
      }
      //  заполняем клиента
      clientIncomingItem = {
        id: serverIncomingItem.ID,   // ID инкоминга
        incomingID: serverIncomingItem.incomingID,  //  ид накладной
        product: serverIncomingItem.product && serverIncomingItem.product.ID ?
          this.productServerToClient(serverIncomingItem.product) : null, //  продукт,
        productID: serverIncomingItem.productID,  //  ид продукта
        optionValues,  //  айтемы
        amount: serverIncomingItem.amount, //  количество товара
      };
      //  возврат полученного результата
      return clientIncomingItem;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  CLIENT >> Server
  public incomingItemClientToServer(clientIncoming: IncomingItem): IncomingItemServer {
    //  проверка на пустоту
    if (clientIncoming) {
      //  переменные
      let serverIncoming: IncomingItemServer;
      const optionValues: OptionValueServer[] = [];
      //  конвертация массивов
      if (clientIncoming.optionValues != null) {
        console.log('clientIncoming.optionValues: ', clientIncoming.optionValues);
        clientIncoming.optionValues.forEach(incomingItem => {
          optionValues.push(this.optionValueClientToServer(incomingItem));
        });
      }
      //  заполняем клиента
      serverIncoming = {
        ID: clientIncoming.id,
        incomingID: clientIncoming.incomingID,  //  ид накладной
        product: clientIncoming.product && clientIncoming.product.id ?
          this.productClientToServer(clientIncoming.product) : null, //  продукт,
        productID: clientIncoming.productID,  //  ид продукта
        optionValues,  //  айтемы
        amount: clientIncoming.amount, //  количество товара
      };
      //  возврат полученного результата
      return serverIncoming;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ITEM
  //  Item
  //  Server >> Client
  public itemServerToClient(serverItem: ItemServer): Item {
    //  проверка на пустоту
    if (serverItem) {
      //  переменные
      let clientItem: Item;
      const optionValues: OptionValue[] = [];
      // const optionsSets: OptionsSet[] = [];
      //  заполняем массивы
      if (serverItem.optionValues) {
        serverItem.optionValues.forEach(optionValue => {
          optionValues.push(this.optionValueServerToClient(optionValue));
        });
      }
      // if (serverItem.optionsSets) {
      //   serverItem.optionsSets.forEach(optionSet => {
      //     optionsSets.push(this.optionsSetServerToClient(optionSet));
      //   });
      // }
      console.log('2');
      //  заполняем клиента
      clientItem = {
        id: serverItem.ID,
        itemID: serverItem.itemID, // идентификатор для куаркода и склада
        //  TODO: понять как работать с продуктом и надо ли его конвертить
        product:  serverItem.product ? this.productServerToClient(serverItem.product) : null, //  заказ, //  продукт
        // product: serverItem.product && serverItem.product.ID ? this.productServerToClient(serverItem.product) : null, //  продукт
        productID: serverItem.productID, // идентификатор продукта
        deliveryPackageNumber: serverItem.deliveryPackageNumber,  //  в каком из пакетов заказа этот айтем
        optionValues, //  optionValues
        // optionsSets, //  optionsSet
        //  TODO: продумать статусы для разных складов
        status: this.itemStatusServerToClient(serverItem.status), //  status item
        placement: serverItem.placement,
        warehouseBoxNumber: serverItem.warehouseBoxNumber, //  номер коробки на складе
        order: serverItem.order && serverItem.order.ID ? this.orderServerToClient(serverItem.order) : null, //  заказ
        comments: serverItem.comments, //  комментс
        price: this.priceServerToClient(serverItem.price),  //  цена
        currency: this.currencyServerToClient(serverItem.currency), //  валюта на момент покупки
        sale: this.saleServerToClient(serverItem.sale), //  скидка
      };
      //  return
      return clientItem;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public itemClientToServer(clientItem: Item): ItemServer {
    //  проверка на пустоту
    if (clientItem) {
      //  переменные
      let serverItem: ItemServer;
      const optionValues: OptionValueServer[] = [];
      const optionsSets: OptionsSetServer[] = [];
      //  заполняем массивы
      if (clientItem.optionValues) {
        clientItem.optionValues.forEach(optionValue => {
          optionValues.push(this.optionValueClientToServer(optionValue));
        });
      }
      // if (clientItem.optionsSets) {
      //   clientItem.optionsSets.forEach(optionSet => {
      //     optionsSets.push(this.optionsSetClientToServer(optionSet));
      //   });
      // }
      //  заполняем клиента
      serverItem = {
        ID: clientItem.id,
        itemID: clientItem.itemID, // идентификатор для куаркода и склада
        product: clientItem.product && clientItem.product.id ? this.productClientToServer(clientItem.product) : null, //  продукт
        productID: clientItem.productID, // идентификатор продукта
        deliveryPackageNumber: clientItem.deliveryPackageNumber,  //  в каком из пакетов заказа этот айтем
        optionValues, //  optionValues
        // optionsSets, //  optionsSet
        //  TODO: продумать статусы для разных складов
        status: this.itemStatusClientToServer(clientItem.status), //  status item
        placement: clientItem.placement,
        warehouseBoxNumber: clientItem.warehouseBoxNumber, //  номер коробки на складе
        order: clientItem.order && clientItem.order.id ? this.orderClientToServer(clientItem.order) : null, //  заказ
        comments: clientItem.comments, //  комментс
        price: this.priceClientToServer(clientItem.price),  //  цена
        currency: this.currencyClientToServer(clientItem.currency), //  валюта на момент покупки
        sale: this.saleClientToServer(clientItem.sale), //  скидка
      };
      //  return
      return serverItem;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ItemShadow
  //  Server >> Client
  public itemShadowServerToClient(serverItemShadow: ItemShadowServer): ItemShadow {
    //  проверка на пустоту
    if (serverItemShadow) {
      //  переменные
      let clientItemShadow: ItemShadow;
      const options: Option[] = [];
      const optionsSets: OptionsSet[] = [];
      //  заполняем массивы
      serverItemShadow.options.forEach(option => {
        options.push(this.optionServerToClient(option));
      });
      serverItemShadow.optionsSets.forEach(optionSet => {
        optionsSets.push(this.optionsSetServerToClient(optionSet));
      });
      //  заполняем клиента
      clientItemShadow = {
        id: serverItemShadow.ID,
        itemID: serverItemShadow.itemID, // идентификатор для куаркода и склада
        product: this.productShadowServerToClient(serverItemShadow.product),  //  product
        oldProductName: serverItemShadow.oldProductName, //  продукт
        deliveryPackageNumber: serverItemShadow.deliveryPackageNumber,  //  в каком из пакетов заказа этот айтем
        options,  //  options
        optionsSets, //  optionsSet
        status: this.itemStatusServerToClient(serverItemShadow.status), //  status item
        placement: serverItemShadow.placement,
        warehouseBoxNumber: serverItemShadow.warehouseBoxNumber, //  номер коробки на складе
        order: this.orderServerToClient(serverItemShadow.order), //  заказ
        comments: serverItemShadow.comments, //  комментс
        currency: this.currencyShadowServerToClient(serverItemShadow.currency), //  валюта на момент покупки
        sale: this.saleServerToClient(serverItemShadow.sale), //  скидка
      };
      //  return
      return clientItemShadow;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public itemShadowClientToServer(clientItemShadow: ItemShadow): ItemShadowServer {
    //  проверка на пустоту
    if (clientItemShadow) {
      //  переменные
      let serverItemShadow: ItemShadowServer;
      const options: OptionServer[] = [];
      const optionsSets: OptionsSetServer[] = [];
      //  заполняем массивы
      clientItemShadow.options.forEach(option => {
        options.push(this.optionClientToServer(option));
      });
      clientItemShadow.optionsSets.forEach(optionSet => {
        optionsSets.push(this.optionsSetClientToServer(optionSet));
      });
      //  заполняем клиента
      serverItemShadow = {
        ID: clientItemShadow.id,
        itemID: clientItemShadow.itemID, // идентификатор для куаркода и склада
        product: this.productShadowClientToServer(clientItemShadow.product),  //  product
        oldProductName: clientItemShadow.oldProductName, //  продукт
        deliveryPackageNumber: clientItemShadow.deliveryPackageNumber,  //  в каком из пакетов заказа этот айтем
        options,  //  options
        optionsSets, //  optionsSet
        status: this.itemStatusClientToServer(clientItemShadow.status), //  status item
        placement: clientItemShadow.placement,
        warehouseBoxNumber: clientItemShadow.warehouseBoxNumber, //  номер коробки на складе
        order: this.orderClientToServer(clientItemShadow.order), //  заказ
        comments: clientItemShadow.comments, //  комментс
        currency: this.currencyShadowClientToServer(clientItemShadow.currency), //  валюта на момент покупки
        sale: this.saleClientToServer(clientItemShadow.sale), //  скидка
      };
      //  return
      return serverItemShadow;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ITEM STATUS
  //  Server >> Client
  public itemStatusServerToClient(serverItemStatus: ItemStatusServer): ItemStatus {
    //  проверка на пустоту
    if (serverItemStatus) {
      //  переменные
      let clientItemStatus: ItemStatus;
      //  заполняем клиента
      clientItemStatus = {
        id: serverItemStatus.ID,  // id в бд
        name: serverItemStatus.name,  // имя
      };
      //  return
      return clientItemStatus;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public itemStatusClientToServer(clientItemStatus: ItemStatus): ItemStatusServer {
    //  проверка на пустоту
    if (clientItemStatus) {
      //  переменные
      let serverItemStatus: ItemStatusServer;
      //  заполняем клиента
      serverItemStatus = {
        ID: clientItemStatus.id,       // id в бд
        name: clientItemStatus.name,   // имя
      };
      //  return
      return serverItemStatus;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  languages
  //  из серверной в клиентскую
  public languageServerToClient(serverLanguage: LanguageServer): Language {
    //  проверка на пустоту
    if (serverLanguage) {
      //  создаем начальное значение
      let language: Language;
      //  присваиваем значения
      language = {
        id: serverLanguage.ID,             // id в бд
        language: serverLanguage.language, // язык
        title: serverLanguage.title,       // название
      };
      //  возвращаем сконвертированное значение
      return language;
    } else {
      //  возвращаем null
      return null;
    }
  }


  //  из клиентской в серверную
  public languageClientToServer(clientLanguage: Language): LanguageServer {
    //  проверка на пустоту
    if (clientLanguage) {
      //  создаем начальное значение
      let serverLanguage: LanguageServer;
      //  присваиваем значения
      serverLanguage = {
        ID: clientLanguage.id,             // id в бд
        language: clientLanguage.language, // язык
        title: clientLanguage.title, // название
      };
      //  возвращаем сконвертированное значение
      return serverLanguage;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  logs
  //  из серверной в клиентскую
  public logServerToClient(serverLog: LogServer): Log {
    //  проверка на пустоту
    if (serverLog) {
      //  создаем начальное значение
      let log: Log;
      //  присваиваем значения
      log = {
        id: serverLog.ID, // id
        сreatedAt: serverLog.CreatedAt, // время создания лога
        type: serverLog.type,  //  тип изменяемой сущности
        action: serverLog.action,  //  действие
        objectId: serverLog.objectId,  // обьект изменяемой сущности
        funcPath: serverLog.funcPath,  // хлебные крошки функций вызвавших создание лога.
        json: serverLog.json,  //  что было до изменения
        jsonRequest: serverLog.jsonRequest, //  что стало после изменения
      };
      //  возвращаем сконвертированное значение
      return log;
    } else {
      //  возвращаем null
      return null;
    }
  }


  //  из клиентской в серверную
  public logClientToServer(clientLog: Log): LogServer {
    //  проверка на пустоту
    if (clientLog) {
      //  создаем начальное значение
      let serverLog: LogServer;
      //  присваиваем значения
      serverLog = {
        ID: clientLog.id,             // id в бд
        CreatedAt: clientLog.сreatedAt, // время создания лога
        type: clientLog.type,  //  тип изменяемой сущности
        action: clientLog.action, //  действие
        objectId: clientLog.objectId,  // обьект изменяемой сущности
        funcPath: clientLog.funcPath,  // хлебные крошки функций вызвавших создание лога.
        json: clientLog.json,  //  что было до изменения
        jsonRequest: clientLog.jsonRequest, //  что стало после изменения
      };
      //  возвращаем сконвертированное значение
      return serverLog;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public localizedKeywordClientToServer(clientLocalizedKeyword: LocalizedKeyword): LocalizedKeywordServer {
    //  проверка на пустоту
    if (clientLocalizedKeyword) {
      //  создаем начальное значение
      let serverLocalizedKeyword: LocalizedKeyword;
      //  присваиваем значения
      serverLocalizedKeyword = {
        ID: clientLocalizedKeyword.ID,           // id в бд
        keyword: clientLocalizedKeyword.keyword, // ключевое слово
        ru: clientLocalizedKeyword.ru,           // значение по ключевому слову на русском
        en: clientLocalizedKeyword.en,           // значение по ключевому слову на английском
        description: clientLocalizedKeyword.description, // описание ключевого слова. Где зачем и почему оно используется.
      };
      //  возвращаем сконвертированное значение
      return serverLocalizedKeyword;
    } else {
      //  возвращаем null
      return null;
    }
  }


  //  из клиентской в серверную
  public LocalizedKeywordServerToClient(serverLocalizedKeyword: LocalizedKeywordServer): LocalizedKeyword {
    //  проверка на пустоту
    if (serverLocalizedKeyword) {
      //  создаем начальное значение
      let localizedKeyword: LocalizedKeyword;
      //  присваиваем значения
      localizedKeyword = {
        ID: serverLocalizedKeyword.ID,           // id в бд
        keyword: serverLocalizedKeyword.keyword, // ключевое слово
        ru: serverLocalizedKeyword.ru, // значение по ключевому слову на русском
        en: serverLocalizedKeyword.en, // значение по ключевому слову на английском
        description: serverLocalizedKeyword.description, // описание ключевого слова. Где зачем и почему оно используется.
      };
      //  возвращаем сконвертированное значение
      return localizedKeyword;
    } else {
      //  возвращаем null
      return null;
    }
  }


  // localizedDescription
  //  из серверной в клиентскую
  public localizedDescriptionServerToClient(serverLocalizedDescription: LocalizedDescriptionServer): LocalizedDescription {
    //  проверка на пустоту
    if (serverLocalizedDescription) {
      //  создаем начальное значение
      let clientLocalizedDescription: LocalizedDescription;
      //  присваиваем значения
      clientLocalizedDescription = {
        id: serverLocalizedDescription.ID,
        language: serverLocalizedDescription.language, // язык
        name: serverLocalizedDescription.name,  //  имя
        title: serverLocalizedDescription.title, //  тайтл
        description: serverLocalizedDescription.description, // описание
        descriptionTwo: serverLocalizedDescription.descriptionTwo, // описание
        descriptionThree: serverLocalizedDescription.descriptionThree, // описание
        keywords: serverLocalizedDescription.keywords, // keywords
      };
      //  возвращаем сконвертированное значение
      return clientLocalizedDescription;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public localizedDescriptionClientToServer(clientLocalizedDescription: LocalizedDescription): LocalizedDescriptionServer {
    //  проверка на пустоту
    if (clientLocalizedDescription) {
      //  создаем начальное значение
      let serverLocalizedDescription: LocalizedDescriptionServer;
      //  присваиваем значения
      serverLocalizedDescription = {
        ID: clientLocalizedDescription.id,
        language: clientLocalizedDescription.language, // язык
        name: clientLocalizedDescription.name,  //  имя
        title: clientLocalizedDescription.title, //  тайтл
        description: clientLocalizedDescription.description, // описание
        descriptionTwo: clientLocalizedDescription.descriptionTwo, // описание
        descriptionThree: clientLocalizedDescription.descriptionThree, // описание
        keywords: clientLocalizedDescription.keywords, // keywords
      };
      //  возвращаем сконвертированное значение
      return serverLocalizedDescription;
    } else {
      //  возвращаем null
      return null;
    }
  }

  // localizedText
  //  из серверной в клиентскую
  public localizedTextServerToClient(serverLocalizedText: LocalizedTextServer): LocalizedText {
    //  проверка на пустоту
    if (serverLocalizedText) {
      //  создаем начальное значение
      let clientLocalizedText: LocalizedText;
      //  присваиваем значения
      clientLocalizedText = {
        id: serverLocalizedText.ID,
        language: serverLocalizedText.language, // язык
        text: serverLocalizedText.text,  //  текст
      };
      //  возвращаем сконвертированное значение
      return clientLocalizedText;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public localizedTextClientToServer(clientLocalizedText: LocalizedText): LocalizedTextServer {
    //  проверка на пустоту
    if (clientLocalizedText) {
      //  создаем начальное значение
      let serverLocalizedText: LocalizedTextServer;
      //  присваиваем значения
      serverLocalizedText = {
        ID: clientLocalizedText.id,
        language: clientLocalizedText.language, // язык
        text: clientLocalizedText.text,  //  текст
      };
      //  возвращаем сконвертированное значение
      return serverLocalizedText;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  MANUFACTURERS
  //  из клиентской в серверную
  public manufacturerClientToServer(clientManufacturer: Manufacturer): ManufacturerServer {
    //  проверка на пустоту
    if (clientManufacturer) {
      //  создаем переменные
      let serverManufacturer: ManufacturerServer;
      const brands: BrandServer[] = [];
      //  перебираем массив
      if (clientManufacturer.brands) {
        clientManufacturer.brands.forEach((clientBrand: Brand) => {
          brands.push(this.brandClientToServer(clientBrand));
        });
      }
      //  заполняем структуру
      serverManufacturer = {
        ID: clientManufacturer.id,
        name: clientManufacturer.name, //  название
        cent: clientManufacturer.cent, //  процент надбавки
        color: clientManufacturer.color,  //  цвет
        contacts: clientManufacturer.contacts, //  контактное лицо
        site: clientManufacturer.site, //  сайт производителя
        email: clientManufacturer.email,  //  почта
        vk: clientManufacturer.vk, //  линк на ВК
        phone: clientManufacturer.phone,  //  телефон
        bank: clientManufacturer.bank, //  банковские реквизиты
        contract: clientManufacturer.contract, //  данные контракта
        discount: clientManufacturer.discount, //  скидка
        address: clientManufacturer.address,  //  адрес
        delivery: clientManufacturer.delivery, //  доставка
        comment: clientManufacturer.comment,  //  комментарий
        brands
      };
      //  возвращаем сконвертированное значение
      return serverManufacturer;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из серверной в клиентскую
  public manufacturerServerToClient(serverManufacturer: ManufacturerServer): Manufacturer {
    //  проверка на пустоту
    if (serverManufacturer) {
      //  создаем переменные
      let clientManufacturer: Manufacturer;
      const brands: Brand[] = [];
      //  перебираем массив
      if (serverManufacturer.brands) {
        serverManufacturer.brands.forEach((serverBrand: BrandServer) => {
          brands.push(this.brandServerToClient(serverBrand));
        });
      }
      //  создаем начальные значения
      clientManufacturer = {
        id: serverManufacturer.ID,
        name: serverManufacturer.name, //  название
        cent: serverManufacturer.cent, //  процент надбавки
        color: serverManufacturer.color,  //  цвет
        contacts: serverManufacturer.contacts, //  контактное лицо
        site: serverManufacturer.site, //  сайт производителя
        email: serverManufacturer.email,  //  почта
        vk: serverManufacturer.vk, //  линк на ВК
        phone: serverManufacturer.phone,  //  телефон
        bank: serverManufacturer.bank, //  банковские реквизиты
        contract: serverManufacturer.contract, //  данные контракта
        discount: serverManufacturer.discount, //  скидка
        address: serverManufacturer.address,  //  адрес
        delivery: serverManufacturer.delivery, //  доставка
        comment: serverManufacturer.comment,  //  комментарий
        brands  //  брэндс
      };
      //  возвращаем сконвертированное значение
      return clientManufacturer;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  OPTIONS
  //  option
  //  из серверной в клиентскую
  public optionServerToClient(serverOption: OptionServer): Option {
    //  проверка на пустоту
    if (serverOption) {
      //  создаем начальное значение
      let clientOption: Option;
      const optionValues: OptionValue[] = [];
      const localizedDescriptions: LocalizedDescription[] = [];
      //  конвертация массивов
      if (serverOption.localizedDescriptions) {
        serverOption.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      if (serverOption.optionValues != null) {
        serverOption.optionValues.forEach(item => {
          optionValues.push(this.optionValueServerToClient(item));
        });
      }
      //  присваиваем значения
      clientOption = {
        id: serverOption.ID, // ID
        title: localizedDescriptions.find(locale => locale.language === 'EN') ?
          localizedDescriptions.find(locale => locale.language === 'EN').title : 'none',
        localizedDescriptions,
        priority: serverOption.priority, // приоритет
        enabled: serverOption.enabled, // приоритет
        type: serverOption.type ? this.optionTypeServerToClient(serverOption.type) : null, // тип
        optionTypeID: serverOption.optionTypeID,  //  id типа опции
        optionValues // значения опций
      };
      //  возвращаем сконвертированное значение
      return clientOption;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public optionClientToServer(clientOption: Option): OptionServer {
    //  проверка на пустоту
    if (clientOption) {
      //  создаем начальное значение
      let optionServer: OptionServer;
      const optionValues: OptionValueServer[] = [];
      const localizedDescriptions: LocalizedDescriptionServer[] = [];
      //  конвертация массивов
      if (clientOption.localizedDescriptions) {
        clientOption.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientOption.optionValues) {
        clientOption.optionValues.forEach(item => {
          optionValues.push(this.optionValueClientToServer(item));
        });
      }

      //  присваиваем значения
      optionServer = {
        ID: clientOption.id, // ID
        localizedDescriptions,
        priority: clientOption.priority, // приоритет
        enabled: clientOption.enabled,
        type: clientOption.type ? this.optionTypeClientToServer(clientOption.type) : null, // тип
        optionTypeID: clientOption.optionTypeID,  //  id типа опции
        optionValues // значения опций
      };
      //  возвращаем сконвертированное значение
      return optionServer;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  optionType
  //  из серверной в клиентскую
  public optionTypeServerToClient(optionTypeServer: OptionTypeServer): OptionType {
    //  проверка на пустоту
    if (optionTypeServer) {
      //  создаем начальное значение
      let optionType: OptionType;
      //  присваиваем значения
      optionType = {
        id: optionTypeServer.ID, //  ID
        name: optionTypeServer.name, // название
      };
      //  возвращаем сконвертированное значение
      return optionType;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public optionTypeClientToServer(optionType: OptionType): OptionTypeServer {
    //  проверка на пустоту
    if (optionType) {
      //  создаем начальное значение
      let optionTypeServer: OptionTypeServer;
      //  присваиваем значения
      optionTypeServer = {
        ID: optionType.id, //  ID
        name: optionType.name // название
      };
      //  возвращаем сконвертированное значение
      return optionTypeServer;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  optionValue
  //  из серверной в клиентскую
  public optionValueServerToClient(optionValueServer: OptionValueServer, optionID?: string): OptionValue {
    //  проверка на пустоту
    if (optionValueServer) {
      //  создаем начальное значение
      let optionValue: OptionValue;
      const localizedDescriptions: LocalizedDescription[] = [];
      //  конвертация массивов
      if (optionValueServer.localizedDescriptions) {
        optionValueServer.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      //  convert title
      let title: string = '';
      //  если это сайз - берем ру
      if (optionValueServer.option && optionValueServer.option.optionTypeID === 2) {
        title = optionValueServer.ru;
        //  если это цвет - берем тайтл из локали
      } else if (optionValueServer.option && optionValueServer.option.optionTypeID === 1) {
        title = localizedDescriptions.find(locale => locale.language === 'EN') ?
          localizedDescriptions.find(locale => locale.language === 'EN').name : 'none';
      } else {
        //  если все остальное - берем из локали
        title = optionValueServer.ru.length ? optionValueServer.ru :
          localizedDescriptions.find(locale => locale.language === 'EN') ?
          localizedDescriptions.find(locale => locale.language === 'EN').name : 'none';
      }
      //  присваиваем значения
      optionValue = {
        id: optionValueServer.ID, //  ID
        image: optionValueServer.image ? this.imageServerToClient(optionValueServer.image) : null, // картинка
        imageID: optionValueServer.imageID, //  imageID
        us: optionValueServer.us, //  US
        int: optionValueServer.int,  //  INT
        ru: optionValueServer.ru, //  RU
        inch: optionValueServer.inch, //  INCH
        cent: optionValueServer.cent, //  CENT
        title,
        localizedDescriptions, //  локализованное описание
        enabled: optionValueServer.enabled,  //  видимость
        priority: optionValueServer.priority, //  приоритет
        option: optionValueServer.option &&  optionValueServer.option.ID ?
          this.optionServerToClient(optionValueServer.option) : null, //  option
        optionID: optionID != null ? optionID : undefined,
        // option: optionServer ? this.optionServerToClient(optionServer) : null  //  option

      };
      //  возвращаем сконвертированное значение
      return optionValue;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public optionValueClientToServer(optionValue: OptionValue): OptionValueServer {
    //  проверка на пустоту
    if (optionValue) {
      //  создаем начальное значение
      let optionValueServer: OptionValueServer;
      const localizedDescriptions: LocalizedDescriptionServer[] = [];
      if (optionValue.localizedDescriptions) {
        optionValue.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      //  присваиваем значения
      optionValueServer = {
        ID: optionValue.id, //  ID
        image: optionValue.image ? this.imageClientToServer(optionValue.image) : null, // картинка
        imageID: optionValue.imageID, //  imageID
        us: optionValue.us, //  US
        int: optionValue.int,  //  INT
        ru: optionValue.ru, //  RU
        inch: optionValue.inch, //  INCH
        cent: optionValue.cent, //  CENT
        localizedDescriptions, //  локализованное описание
        enabled: optionValue.enabled,  //  видимость
        priority: optionValue.priority, //  приоритет
        option: optionValue.option &&  optionValue.option.id ? this.optionClientToServer(optionValue.option) : null //  option
        // option: option ? this.optionClientToServer(option) : null  //  option
      };
      //  возвращаем сконвертированное значение
      return optionValueServer;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  OPTIONS SET
  //  Server >> Client
  public optionsSetServerToClient(serverOptionSet: OptionsSetServer): OptionsSet {
    //  проверка на пустоту
    if (serverOptionSet) {
      //  переменные
      let clientOptionSet: OptionsSet;
      const options: Option[] = [];
      //  конвертация массивов
      if (serverOptionSet.options) {
        serverOptionSet.options.forEach(option => {
          options.push(this.optionServerToClient(option));
        });
      }
      //  заполняем значениями
      clientOptionSet = {
        id: serverOptionSet.ID, //  ID
        options,  //  набор опций
        weight: serverOptionSet.weight, //  вес
        volumeWeight: serverOptionSet.volumeWeight, //  объемный вес
        price: serverOptionSet.price,  //  цена
        article: serverOptionSet.article, // артикль производителя
        enabled: serverOptionSet.enabled,  //  видимость
        image: serverOptionSet.image ? this.imageClientToServer(serverOptionSet.image) : null,  //  картинка
      };
      //  return
      return clientOptionSet;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public optionsSetClientToServer(clientOptionSet: OptionsSet): OptionsSetServer {
    //  проверка на пустоту
    if (clientOptionSet) {
      //  переменные
      let serverOptionSet: OptionsSetServer;
      const options: OptionServer[] = [];
      //  конвертация массивов
      if (clientOptionSet.options) {
        clientOptionSet.options.forEach(option => {
          options.push(this.optionClientToServer(option));
        });
      }
      //  заполняем значениями
      serverOptionSet = {
        ID: clientOptionSet.id, //  ID
        options,  //  набор опций
        weight: clientOptionSet.weight, //  вес
        volumeWeight: clientOptionSet.volumeWeight, //  объемный вес
        price: clientOptionSet.price,  //  цена
        article: clientOptionSet.article, // артикль производителя
        enabled: clientOptionSet.enabled, // видимость
        image: clientOptionSet.image ? this.imageClientToServer(clientOptionSet.image) : null,  //  картинка
      };
      //  return
      return serverOptionSet;
    } else {

      //  возвращаем null
      return null;
    }
  }

  //  ORDER
  //  Server >> Client
  public orderServerToClient(serverOrder: OrderServer): Order {
    //  проверка на пустоту
    if (serverOrder) {
      //  переменные
      let clientOrder: Order;
      const items: Item[] = [];
      const cartItems: CartItem[] = [];
      const itemsShadow: ItemShadow[] = [];
      //  конвертация массивов
      if (serverOrder.cartItems) {
        serverOrder.cartItems.forEach(cartItem => {
          cartItems.push(this.cartItemServerToClient(cartItem));
        });
      }
      if (serverOrder.items) {
        serverOrder.items.forEach(currentItem => {
          items.push(this.itemServerToClient(currentItem));
        });
      }
      if (serverOrder.itemsShadow) {
        serverOrder.itemsShadow.forEach(currentItemShadow => {
          itemsShadow.push(this.itemShadowServerToClient(currentItemShadow));
        });
      }
      //  заполняем клиента
      clientOrder = {
        id: serverOrder.ID,
        createdAt: serverOrder.createdAt,  //  дата создания
        updatedStatusAt: serverOrder.updatedStatusAt,  //  дата создания
        orderID: serverOrder.orderID,  //  Идентификатор для людейво пакетов в заказе
        cartItems,  //  список предварительных айтемов из корзины
        items,  //  список айтемов
        status: this.orderStatusServerToClient(serverOrder.status), // статус заказа
        statusID: serverOrder.statusID, //  ID - статус заказа
        customer: this.customerServerToClient(serverOrder.customer), //  покупатель
        //  tslint:disable-next-line
        createdShippingAddress: this.addressServerToClient(serverOrder.createdShippingAddress), //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
        currentShippingAddress: this.addressServerToClient(serverOrder.currentShippingAddress),  //  измененный адрес доставки
        comments: serverOrder.comments, //  комментарий
        delivery: this.deliveryServerToClient(serverOrder.delivery), //  доставка
        deliveryID: serverOrder.deliveryID, //  id доставка
        deliveryPackageNumber: serverOrder.deliveryPackageNumber, //  номер пакетов в заказе
        deliveryPackageCount: serverOrder.deliveryPackageCount, //  количество пакетов в заказе
        deliveryPrice: serverOrder.deliveryPrice, //  цена доставки
        warehouseBoxNumber: serverOrder.warehouseBoxNumber, //  номер коробки на складе
        weight: serverOrder.weight, //  вес
        payment: this.paymentServerToClient(serverOrder.payment), //  оплата
        paymentID: serverOrder.paymentID, //  id оплата
        paymentURL: serverOrder.paymentURL, //  url страницы оплаты
        shop: serverOrder.shop ? this.shopServerToClient(serverOrder.shop) : null,  //  shop
        deliveryCost: serverOrder.deliveryCost, //  цена доставки
        productsCost: serverOrder.productsCost, //  цена товаров
        totalCost: serverOrder.totalCost, //  итоговая цена
        currency: serverOrder.currency ? this.currencyClientToServer(serverOrder.currency) : null,  //  currency
        currencyID: serverOrder.currencyID, //  currency id
        //  history
        trackingNumber: serverOrder.trackingNumber, //  номер отслеживания
        priority: this.orderPriorityServerToClient(serverOrder.priority), //  приоритет заказа
        priorityID: serverOrder.priorityID, //  ID - приоритет заказа
        sale: this.saleServerToClient(serverOrder.sale), //  скидка
        //  shadow
        itemsShadow, //  список покупок при создании
        shippingAddressShadow: this.addressServerToClient(serverOrder.shippingAddressShadow),  //  измененный адрес доставки
        shopShadow: serverOrder.shopShadow ? this.shopShadowServerToClient(serverOrder.shopShadow) : null,  //  shop shadow
        currencyShadow: serverOrder.currencyShadow ? this.currencyShadowServerToClient(serverOrder.currencyShadow) : null,  //  shop
      }
      ;
      //  return
      return clientOrder;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public orderClientToServer(clientOrder: Order): OrderServer {
    //  проверка на пустоту
    if (clientOrder) {
      //  переменные
      let serverOrder: OrderServer;
      const items: ItemServer[] = [];
      const cartItems: CartItemServer[] = [];
      const itemsShadow: ItemShadowServer[] = [];
      //  конвертация массивов
      if (clientOrder.cartItems) {
        clientOrder.cartItems.forEach(cartItem => {
          cartItems.push(this.cartItemClientToServer(cartItem));
        });
      }
      clientOrder.items.forEach(currentItem => {
        items.push(this.itemClientToServer(currentItem));
      });
      clientOrder.itemsShadow.forEach(currentItemShadow => {
        itemsShadow.push(this.itemShadowClientToServer(currentItemShadow));
      });
      //  заполняем клиента
      serverOrder = {
        ID: clientOrder.id,
        createdAt: clientOrder.createdAt,  //  дата создания
        updatedStatusAt: clientOrder.updatedStatusAt,  //  дата создания
        orderID: clientOrder.orderID,  //  Идентификатор для людейличество пакетов в заказе
        cartItems,  //  список предварительных айтемов из корзины
        items,  //  список айтемов
        status: this.orderStatusClientToServer(clientOrder.status), // статус заказа
        statusID: clientOrder.statusID, //  ID - статус заказа
        customer: this.customerClientToServer(clientOrder.customer), //  покупатель
        //  tslint:disable-next-line
        createdShippingAddress: this.addressClientToServer(clientOrder.createdShippingAddress), //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
        currentShippingAddress: this.addressClientToServer(clientOrder.currentShippingAddress),  //  измененный адрес доставки
        comments: clientOrder.comments, //  комментарий
        delivery: this.deliveryClientToServer(clientOrder.delivery), //  доставка
        deliveryID: clientOrder.deliveryID, //  id доставка
        deliveryPackageNumber: clientOrder.deliveryPackageNumber, //  номер пакетов в заказе
        deliveryPackageCount: clientOrder.deliveryPackageCount, //  количество пакетов в заказе
        deliveryPrice: clientOrder.deliveryPrice, //  цена доставки
        warehouseBoxNumber: clientOrder.warehouseBoxNumber, //  номер коробки на складе
        weight: clientOrder.weight, //  вес
        payment: this.paymentClientToServer(clientOrder.payment), //  оплата
        paymentID: clientOrder.paymentID, //  id оплата
        paymentURL: clientOrder.paymentURL, //  url страницы оплаты
        shop: clientOrder.shop ? this.shopClientToServer(clientOrder.shop) : null,  //  shop
        deliveryCost: clientOrder.deliveryCost, //  цена доставки
        productsCost: clientOrder.productsCost, //  цена товаров
        totalCost: clientOrder.totalCost, //  итоговая цена
        currency: clientOrder.currency ? this.currencyClientToServer(clientOrder.currency) : null,  //  currency
        currencyID: clientOrder.currencyID, //  currency id
        //  history
        trackingNumber: clientOrder.trackingNumber, //  номер отслеживания
        priority: this.orderPriorityClientToServer(clientOrder.priority), //  приоритет заказа
        priorityID: clientOrder.priorityID, //  ID - приоритет заказа
        sale: this.saleClientToServer(clientOrder.sale), //  скидка
        //  shadow
        itemsShadow, //  список покупок при создании
        shippingAddressShadow: this.addressClientToServer(clientOrder.shippingAddressShadow),  //  измененный адрес доставки
        shopShadow: clientOrder.shopShadow ? this.shopShadowClientToServer(clientOrder.shopShadow) : null,  //  shop shadow
        currencyShadow: clientOrder.currencyShadow ? this.currencyShadowClientToServer(clientOrder.currencyShadow) : null,  //  currency sh
      };
      //  return
      return serverOrder;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ORDER PRIORITY
  //  Server >> Client
  public orderPriorityServerToClient(serverOrderPriority: OrderPriorityServer): OrderPriority {
    //  проверка на пустоту
    if (serverOrderPriority) {
      //  переменные
      let clientOrderPriority: OrderStatus;
      //  заполняем клиента
      clientOrderPriority = {
        id: serverOrderPriority.ID,
        name: serverOrderPriority.name,
      };
      //  return
      return clientOrderPriority;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public orderPriorityClientToServer(clientOrderPriority: OrderPriority): OrderPriorityServer {
    //  проверка на пустоту
    if (clientOrderPriority) {
      //  переменные
      let serverOrderPriority: OrderPriorityServer;
      //  заполняем клиента
      serverOrderPriority = {
        ID: clientOrderPriority.id,
        name: clientOrderPriority.name,
      };
      //  return
      return serverOrderPriority;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ORDER STATUS
  //  Server >> Client
  public orderStatusServerToClient(serverOrderStatus: OrderStatusServer): OrderStatus {
    //  проверка на пустоту
    if (serverOrderStatus) {
      //  переменные
      let clientOrderStatus: OrderStatus;
      //  заполняем клиента
      clientOrderStatus = {
        id: serverOrderStatus.ID,
        name: serverOrderStatus.name,
      };
      //  return
      return clientOrderStatus;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public orderStatusClientToServer(clientOrderStatus: OrderStatus): OrderStatusServer {
    //  проверка на пустоту
    if (clientOrderStatus) {
      //  переменные
      let serverOrderStatus: OrderStatusServer;
      //  заполняем клиента
      serverOrderStatus = {
        ID: clientOrderStatus.id,
        name: clientOrderStatus.name,
      };
      //  return
      return serverOrderStatus;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  PAYMENT
  //  Payment
  //  Server >> Client
  public paymentServerToClient(serverPayment: PaymentServer): Payment {
    //  проверка на пустоту
    if (serverPayment) {
      //  переменные
      let clientPayment: Payment;
      const localizedDescriptions: LocalizedDescriptionServer[] = [];
      const discounts: PaymentDiscount[] = [];
      //  конвертация массивов
      if (serverPayment.localizedDescriptions) {
        serverPayment.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      if (serverPayment.discounts) {
        serverPayment.discounts.forEach(discount => {
          discounts.push(this.paymentDiscountServerToClient(discount));
        });
      }
      //  заполняем клиента
      clientPayment = {
        id: serverPayment.ID,
        localizedDescriptions, //  локализованное описание
        image: this.imageServerToClient(serverPayment.image), //  картинка
        discounts,  //  скидки,
        priority: serverPayment.priority, //  приоритеты
        enabled: false  //  видимость
      };
      //  return
      return clientPayment;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public paymentClientToServer(clientPayment: Payment): PaymentServer {
    //  проверка на пустоту
    if (clientPayment) {
      //  переменные
      let serverPayment: PaymentServer;
      const localizedDescriptions: LocalizedDescription[] = [];
      const discounts: PaymentDiscountServer[] = [];
      //  конвертация массивов
      if (clientPayment.localizedDescriptions) {
        clientPayment.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientPayment.discounts) {
        clientPayment.discounts.forEach(discount => {
          discounts.push(this.paymentDiscountClientToServer(discount));
        });
      }
      //  заполняем клиента
      serverPayment = {
        ID: clientPayment.id,
        localizedDescriptions, //  локализованное описание
        image: this.imageClientToServer(clientPayment.image), //  картинка
        discounts,  //  скидки
        priority: clientPayment.priority, //  приоритеты
        enabled: false  //  видимость
      };
      //  return
      return serverPayment;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  PaymentDiscount
  //  Server >> Client
  public paymentDiscountServerToClient(serverPaymentDiscount: PaymentDiscountServer): PaymentDiscount {
    //  проверка на пустоту
    if (serverPaymentDiscount) {
      //  переменные
      let clientPaymentDiscount: PaymentDiscount;
      //  заполняем клиента
      clientPaymentDiscount = {
        id: serverPaymentDiscount.ID,
        shop: serverPaymentDiscount.shop,
        discount: serverPaymentDiscount.discount, //  размер скидки
        enabled: serverPaymentDiscount.enabled, //  активно?
      };
      //  return
      return clientPaymentDiscount;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public paymentDiscountClientToServer(clientPaymentDiscount: PaymentDiscount): PaymentDiscountServer {
    //  проверка на пустоту
    if (clientPaymentDiscount) {
      //  переменные
      let serverPaymentDiscount: PaymentDiscountServer;
      //  заполняем клиента
      serverPaymentDiscount = {
        ID: clientPaymentDiscount.id,
        shop: clientPaymentDiscount.shop,
        discount: clientPaymentDiscount.discount, //  размер скидки
        enabled: clientPaymentDiscount.enabled, //  активно?
      };
      //  return
      return serverPaymentDiscount;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  PRICE
  //  Server >> Client
  public priceServerToClient(serverPrice: PriceServer): Price {
    //  проверка на пустоту
    if (serverPrice) {
      //  переменные
      let clientPrice: Price;
      //  заполняем клиента
      clientPrice = {
        id: serverPrice.ID,
        sell: serverPrice.sell,
        buy: serverPrice.buy,
      };
      //  return
      return clientPrice;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public priceClientToServer(clientPrice: Price): PriceServer {
    //  проверка на пустоту
    if (clientPrice) {
      //  переменные
      let serverPrice: PriceServer;
      //  заполняем клиента
      serverPrice = {
        ID: clientPrice.id,
        sell: clientPrice.sell,
        buy: clientPrice.buy,
      };
      //  return
      return serverPrice;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  PRODUCT
  //  Product
  //  Server >> Client
  public productServerToClient(serverProduct: ProductServer): Product {
    //  проверка на пустоту
    if (serverProduct) {
      //  переменные
      let clientProduct: Product;
      const productImages: ProductImage[] = [];
      const items: Item[] = [];
      const localizedDescriptions: LocalizedDescription[] = [];
      const localizedFeatures: LocalizedText[] = [];
      const localizedMaterial: LocalizedText[] = [];
      const localizedSEOs: LocalizedDescription[] = [];
      const localizedProductTypes: LocalizedDescription[] = [];
      const categories: Category[] = [];
      const restrictedDeliveries: Delivery[] = [];
      const productOptions: ProductOption[] = [];
      const optionsSets: OptionsSet[] = [];
      //  конвертация массивов
      if (serverProduct.productImages) {
        serverProduct.productImages.forEach(productImage => {
          productImages.push(this.productImageServerToClient(productImage));
        });
      }
      if (serverProduct.items) {
        serverProduct.items.forEach(item => {
          items.push(this.itemServerToClient(item));
        });
      }
      if (serverProduct.localizedDescriptions) {
        serverProduct.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      if (serverProduct.localizedFeatures) {
        serverProduct.localizedFeatures.forEach(localText => {
          localizedFeatures.push(this.localizedTextServerToClient(localText));
        });
      }
      if (serverProduct.localizedMaterial) {
        serverProduct.localizedMaterial.forEach(localText => {
          localizedMaterial.push(this.localizedTextServerToClient(localText));
        });
      }
      if (serverProduct.localizedSEOs) {
        serverProduct.localizedSEOs.forEach(localDescription => {
          localizedSEOs.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      if (serverProduct.localizedProductTypes) {
        serverProduct.localizedProductTypes.forEach(localDescription => {
          localizedProductTypes.push(this.localizedDescriptionServerToClient(localDescription));
        });
      }
      if (serverProduct.categories) {
        serverProduct.categories.forEach(category => {
          categories.push(this.categoryServerToClient(category));
        });
      }

      if (serverProduct.restrictedDeliveries) {
        serverProduct.restrictedDeliveries.forEach(delivery => {
          restrictedDeliveries.push(this.deliveryServerToClient(delivery));
        });
      }
      if (serverProduct.productOptions) {
        serverProduct.productOptions.forEach(productOption => {
          productOptions.push(this.productOptionServerToClient(productOption));
        });
      }
      if (serverProduct.optionsSets) {
        serverProduct.optionsSets.forEach(optionSet => {
          optionsSets.push(this.optionsSetServerToClient(optionSet));
        });
      }
      //  заполняем клиента
      clientProduct = {
        id: serverProduct.ID,
        imageFront: serverProduct.imageFront ? this.imageServerToClient(serverProduct.imageFront) : null,  //  картинка
        imageFrontID: serverProduct.imageFrontID,  //  ид картинки
        imageBack: serverProduct.imageBack ? this.imageServerToClient(serverProduct.imageBack) : null,  //  картинка
        imageBackID: serverProduct.imageBackID,  //  ид картинки
        productImages,  //  картинки продукта
        items,
        title: localizedDescriptions.find(locale => locale.language === 'EN') ?
          localizedDescriptions.find(locale => locale.language === 'EN').title : 'none',
        localizedDescriptions, //  описание
        localizedFeatures, //  локализованые детали
        localizedMaterial, // локализованные материалы
        localizedSEOs, //  сео расширенный
        seoLink: serverProduct.seoLink,  //  сео линк
        brand: serverProduct.brand ? this.brandServerToClient(serverProduct.brand) : null, //  торговая марка
        brandID: serverProduct.brandID, //  ID brand
        manufacturer: serverProduct.manufacturer ? this.manufacturerServerToClient(serverProduct.manufacturer) : null, //  производитель
        manufacturerID: serverProduct.manufacturerID, //  ID manufacturer
        price: serverProduct.price ? this.priceServerToClient(serverProduct.price) : null, //  цена
        categories, //  категории
        //  parameters
        restrictedDeliveries, //  запрещенные доставки
        weight: serverProduct.weight, // вес
        volumeWeightX: serverProduct.volumeWeightX, //  объемный вес X
        volumeWeightY: serverProduct.volumeWeightY, //  объемный вес Y
        volumeWeightZ: serverProduct.volumeWeightZ, //  объемный вес Z
        fstek: serverProduct.fstek, // fstek
        warehousePosition: serverProduct.warehousePosition,  //  позиция на складе
        localizedProductTypes, //  тип товара для таможни и не только
        article: serverProduct.article, //  артикул производителя
        priority: serverProduct.priority, // приоритет при отображении
        //  tslint:disable-next-line
        productAvailability: serverProduct.productAvailability ? this.productAvailabilityServerToClient(serverProduct.productAvailability) : null, //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
        productAvailabilityID: serverProduct.productAvailabilityID, // id
        //  tslint:disable-next-line
        productVisibility: serverProduct.productVisibility ? this.productVisibilityServerToClient(serverProduct.productVisibility) : null, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
        productVisibilityID: serverProduct.productVisibilityID, // id
        productOptions, //  список доступных продукту опци
        optionsSets, //  список массивов опций
        sale: serverProduct.sale ? this.saleServerToClient(serverProduct.sale) : null, //  скидка
        type: serverProduct.type, //  скидка
      };
      //  return
      return clientProduct;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public productClientToServer(clientProduct: Product): ProductServer {
    //  проверка на пустоту
    if (clientProduct) {
      //  переменные
      let serverProduct: ProductServer;
      const items: ItemServer[] = [];
      //  TODO SERVER TYPE!!!!
      const productImages: ProductImageServer[] = [];
      const localizedDescriptions: LocalizedDescription[] = [];
      const localizedMaterial: LocalizedText[] = [];
      const localizedFeatures: LocalizedText[] = [];
      const localizedSEOs: LocalizedDescription[] = [];
      const localizedProductTypes: LocalizedDescription[] = [];
      const categories: CategoryServer[] = [];
      const restrictedDeliveries: DeliveryServer[] = [];
      const productOptions: ProductOptionServer[] = [];
      const optionsSets: OptionsSetServer[] = [];
      //  конвертация массивов
      if (clientProduct.productImages) {
        clientProduct.productImages.forEach(productImage => {
          productImages.push(this.productImageClientToServer(productImage));
        });
      }
      if (clientProduct.items) {
        clientProduct.items.forEach(item => {
          items.push(this.itemClientToServer(item));
        });
      }
      if (clientProduct.localizedDescriptions) {
        clientProduct.localizedDescriptions.forEach(localDescription => {
          localizedDescriptions.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientProduct.localizedFeatures) {
        clientProduct.localizedFeatures.forEach(localText => {
          localizedFeatures.push(this.localizedTextClientToServer(localText));
        });
      }
      if (clientProduct.localizedMaterial) {
        clientProduct.localizedMaterial.forEach(localText => {
          localizedMaterial.push(this.localizedTextClientToServer(localText));
        });
      }
      if (clientProduct.localizedSEOs) {
        clientProduct.localizedSEOs.forEach(localDescription => {
          localizedSEOs.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientProduct.localizedProductTypes) {
        clientProduct.localizedProductTypes.forEach(localDescription => {
          localizedProductTypes.push(this.localizedDescriptionClientToServer(localDescription));
        });
      }
      if (clientProduct.categories) {
        clientProduct.categories.forEach(category => {
          categories.push(this.categoryClientToServer(category));
        });
      }
      if (clientProduct.restrictedDeliveries) {
        clientProduct.restrictedDeliveries.forEach(delivery => {
          restrictedDeliveries.push(this.deliveryClientToServer(delivery));
        });
      }
      if (clientProduct.productOptions) {
        clientProduct.productOptions.forEach(productOption => {
          productOptions.push(this.productOptionClientToServer(productOption));
        });
      }
      if (clientProduct.optionsSets) {
        clientProduct.optionsSets.forEach(optionSet => {
          optionsSets.push(this.optionsSetClientToServer(optionSet));
        });
      }
      //  заполняем клиента
      serverProduct = {
        ID: clientProduct.id,
        imageFront: clientProduct.imageFront ? this.imageClientToServer(clientProduct.imageFront) : null,  //  картинка
        imageFrontID: clientProduct.imageFrontID,  //  ид картинки
        imageBack: clientProduct.imageBack ? this.imageClientToServer(clientProduct.imageBack) : null,  //  картинка
        imageBackID: clientProduct.imageBackID,  //  ид картинки
        productImages,  //  картинки продукта
        items, // items
        localizedDescriptions, //  описание
        localizedMaterial, //  локализованые материалы
        localizedFeatures, //  локализованые детали
        localizedSEOs, //  сео расширенный
        seoLink: clientProduct.seoLink,  //  сео линк
        // brand: clientProduct.brand ? this.brandClientToServer(clientProduct.brand) : null, //  торговая марка
        brand: null, //  торговая марка
        brandID: clientProduct.brandID, //  ID brand
        // manufacturer: clientProduct.manufacturer ? this.manufacturerClientToServer(clientProduct.manufacturer) : null, //  производитель
        manufacturer: null, //  производитель
        manufacturerID: clientProduct.manufacturerID, // ID manufacturer
        price: clientProduct.price ? this.priceClientToServer(clientProduct.price) : null, //  цена
        categories, //  категории
        //  parameters
        restrictedDeliveries, //  запрещенные доставки
        weight: clientProduct.weight, // вес
        volumeWeightX: clientProduct.volumeWeightX, //  объемный вес X
        volumeWeightY: clientProduct.volumeWeightY, //  объемный вес Y
        volumeWeightZ: clientProduct.volumeWeightZ, //  объемный вес Z
        fstek: clientProduct.fstek, // fstek
        warehousePosition: clientProduct.warehousePosition,  //  позиция на складе
        localizedProductTypes, //  тип товара для таможни и не только
        article: clientProduct.article, //  артикул производителя
        priority: clientProduct.priority, // приоритет при отображении
        //  tslint:disable-next-line
        productAvailability: clientProduct.productAvailability ? this.productAvailabilityClientToServer(clientProduct.productAvailability) : null, //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
        productAvailabilityID: clientProduct.productAvailabilityID,
        //  tslint:disable-next-line
        productVisibility: clientProduct.productVisibility ? this.productVisibilityClientToServer(clientProduct.productVisibility) : null, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
        productVisibilityID: clientProduct.productVisibilityID,
        // options, // список опций
        productOptions, // список доступных продукту опций
        optionsSets, //  список массивов опций
        sale: clientProduct.sale ? this.saleClientToServer(clientProduct.sale) : null, //  скидка
        type: 'product', //  тип для брокера
      };
      return serverProduct;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Product image
  //  из серверной в клиентскую
  public productImageServerToClient(serverProductImage: ProductImageServer): ProductImage {
    //  проверка на пустоту
    if (serverProductImage) {
      //  создаем начальное значение
      let clientProductImage: ProductImage;
      const optionValues: OptionValue[] = [];
      // const option: LocalizedDescription[] = [];
      //  конвертация массивов
      if (serverProductImage.optionValues) {
        serverProductImage.optionValues.forEach(item => {
          optionValues.push(this.optionValueServerToClient(item));
        });
      }
      //  присваиваем значения
      clientProductImage = {
        id: serverProductImage.ID, // ID
        productID: serverProductImage.productID, // option id
        product: serverProductImage.product ? this.productServerToClient(serverProductImage.product) : null, // option
        imageID: serverProductImage.imageID, // приоритет
        image: serverProductImage.image ? this.imageServerToClient(serverProductImage.image) : null, // image
        optionValues // значения опций
      };
      //  возвращаем сконвертированное значение
      return clientProductImage;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public productImageClientToServer(clientProductImage: ProductImage): ProductImageServer {
    //  проверка на пустоту
    if (clientProductImage) {
      //  создаем начальное значение
      let serverProductImage: ProductImageServer;
      const optionValues: OptionValueServer[] = [];

      //  конвертация массивов
      if (clientProductImage.optionValues) {
        clientProductImage.optionValues.forEach(item => {
          optionValues.push(this.optionValueClientToServer(item));
        });
      }
      //  присваиваем значения
      serverProductImage = {
        ID: clientProductImage.id, // ID
        productID: clientProductImage.productID, // option id
        product: clientProductImage.product ? this.productClientToServer(clientProductImage.product) : null, // option
        imageID: clientProductImage.imageID, // option id
        image: clientProductImage.image ? this.imageClientToServer(clientProductImage.image) : null, // image
        optionValues // значения опций
      };
      //  возвращаем сконвертированное значение
      return serverProductImage;
    } else {
      //  возвращаем null
      return null;
    }
  }


  //  Product option
  //  из серверной в клиентскую
  public productOptionServerToClient(serverProductOption: ProductOptionServer): ProductOption {
    //  проверка на пустоту
    if (serverProductOption) {
      //  создаем начальное значение
      let clientProductOption: ProductOption;
      const optionValues: OptionValue[] = [];
      // const option: LocalizedDescription[] = [];
      //  конвертация массивов
      if (serverProductOption.optionValues) {
        serverProductOption.optionValues.forEach(item => {
          optionValues.push(this.optionValueServerToClient(item));
        });
      }
      //  присваиваем значения
      clientProductOption = {
        id: serverProductOption.ID, // ID
        productID: serverProductOption.productID, // option id
        product: serverProductOption.product ? this.productServerToClient(serverProductOption.product) : null, // option
        optionID: serverProductOption.optionID, // приоритет
        option: serverProductOption.option ? this.optionServerToClient(serverProductOption.option) : null, // тип
        optionValues // значения опций
      };
      //  возвращаем сконвертированное значение
      return clientProductOption;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public productOptionClientToServer(clientProductOption: ProductOption): ProductOptionServer {
    //  проверка на пустоту
    if (clientProductOption) {
      //  создаем начальное значение
      let serverProductOption: ProductOptionServer;
      const optionValues: OptionValueServer[] = [];

      //  конвертация массивов
      if (clientProductOption.optionValues) {
        clientProductOption.optionValues.forEach(item => {
          optionValues.push(this.optionValueClientToServer(item));
        });
      }
      //  присваиваем значения
      serverProductOption = {
        ID: clientProductOption.id, // ID
        productID: clientProductOption.productID, // option id
        // product: clientProductOption.product ? this.productClientToServer(clientProductOption.product) : null, // option
        product: null, // option
        optionID: clientProductOption.optionID, // option id
        // option: clientProductOption.option ? this.optionClientToServer(clientProductOption.option) : null, // option
        option: null, // option
        optionValues // значения опций
      };
      //  возвращаем сконвертированное значение
      return serverProductOption;
    } else {
      //  возвращаем null
      return null;
    }
  }


  //  ProductVisibilityStatus
  //  Server >> Client
  public productAvailabilityServerToClient(serverProductAvailability: ProductAvailabilityServer): ProductAvailability {
    //  проверка на пустоту
    if (serverProductAvailability) {
      //  переменные
      let clientProductAvailability: ProductAvailability;
      //  заполняем клиента
      clientProductAvailability = {
        id: serverProductAvailability.ID,
        name: serverProductAvailability.name,
      };
      //  return
      return clientProductAvailability;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public productAvailabilityClientToServer(clientProductAvailability: ProductAvailability): ProductAvailabilityServer {
    //  проверка на пустоту
    if (clientProductAvailability) {
      //  переменные
      let serverProductAvailability: ProductAvailabilityServer;
      //  заполняем клиента
      serverProductAvailability = {
        ID: clientProductAvailability.id,
        name: clientProductAvailability.name,
      };
      //  return
      return serverProductAvailability;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Product Visibility Status
  //  Server >> Client
  public productVisibilityServerToClient(serverProductVisibility: ProductVisibilityServer): ProductVisibility {
    //  проверка на пустоту
    if (serverProductVisibility) {
      //  переменные
      let clientProductVisibility: ProductVisibility;
      //  заполняем клиента
      clientProductVisibility = {
        id: serverProductVisibility.ID,
        name: serverProductVisibility.name,
      };
      //  return
      return clientProductVisibility;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public productVisibilityClientToServer(clientProductVisibility: ProductVisibility): ProductVisibilityServer {
    //  проверка на пустоту
    if (clientProductVisibility) {
      //  переменные
      let serverProductVisibility: ProductVisibilityServer;
      //  заполняем клиента
      serverProductVisibility = {
        ID: clientProductVisibility.id,
        name: clientProductVisibility.name,
      };
      //  return
      return serverProductVisibility;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ProductShadow
  //  Server >> Client
  public productShadowServerToClient(serverProductShadow: ProductShadowServer): ProductShadow {
    //  проверка на пустоту
    if (serverProductShadow) {
      //  переменные
      let clientProductShadow: ProductShadow;
      const categories: string[] = [];
      const options: Option[] = [];
      const optionsSets: OptionsSet[] = [];
      //  конвертация массивов
      serverProductShadow.categories.forEach(category => {
        categories.push(category);
      });
      serverProductShadow.options.forEach(option => {
        options.push(this.optionServerToClient(option));
      });
      serverProductShadow.optionsSets.forEach(optionSet => {
        optionsSets.push(this.optionsSetServerToClient(optionSet));
      });
      //  заполняем клиента
      clientProductShadow = {
        id: serverProductShadow.ID,
        image: this.imageServerToClient(serverProductShadow.image), //  image
        seoLink: serverProductShadow.seoLink,  //  сео линк
        brand: this.brandServerToClient(serverProductShadow.brand), //  торговая марка
        manufacturer: this.manufacturerServerToClient(serverProductShadow.manufacturer), //  производитель
        price: this.priceServerToClient(serverProductShadow.price), //  цена
        categories, //  категории
        //  parameters
        weight: serverProductShadow.weight, // вес
        volumeWeight: serverProductShadow.volumeWeight, //  объемный вес
        article: serverProductShadow.article, //  артикул производителя
        options, // список опций
        optionsSets, //  список массивов опций
        sale: this.saleServerToClient(serverProductShadow.sale), //  скидка
      };
      //  return
      return clientProductShadow;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public productShadowClientToServer(clientProductShadow: ProductShadow): ProductShadowServer {
    //  проверка на пустоту
    if (clientProductShadow) {
      //  переменные
      let serverProductShadow: ProductShadowServer;
      const categories: string[] = [];
      const options: OptionServer[] = [];
      const optionsSets: OptionsSetServer[] = [];
      //  конвертация массивов
      clientProductShadow.categories.forEach(category => {
        categories.push(category);
      });
      clientProductShadow.options.forEach(option => {
        options.push(this.optionClientToServer(option));
      });
      clientProductShadow.optionsSets.forEach(optionSet => {
        optionsSets.push(this.optionsSetClientToServer(optionSet));
      });
      //  заполняем клиента
      serverProductShadow = {
        ID: clientProductShadow.id,
        image: this.imageClientToServer(clientProductShadow.image),  //  картинка
        seoLink: clientProductShadow.seoLink,  //  сео линк
        brand: this.brandClientToServer(clientProductShadow.brand), //  торговая марка
        manufacturer: this.manufacturerClientToServer(clientProductShadow.manufacturer), //  производитель
        price: this.priceClientToServer(clientProductShadow.price), //  цена
        categories, //  категории
        //  parameters
        weight: clientProductShadow.weight, // вес
        volumeWeight: clientProductShadow.volumeWeight, //  объемный вес
        article: clientProductShadow.article, //  артикул производителя
        options, // список опций
        optionsSets, //  список массивов опций
        sale: this.saleClientToServer(clientProductShadow.sale), //  скидка
      };
      //  return
      return serverProductShadow;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  PURCHASE
  //  Server >> Client
  public purchaseServerToClient(serverPurchase: PurchaseServer): Purchase {
    //  проверка на пустоту
    if (serverPurchase) {
      //  переменные
      let clientPurchase: Purchase;
      //  заполняем клиента
      clientPurchase = {
        id: serverPurchase.ID,
      };
      //  return
      return clientPurchase;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public purchaseClientToServer(clientPurchase: Purchase): PurchaseServer {
    //  проверка на пустоту
    if (clientPurchase) {
      //  переменные
      let serverPurchase: PurchaseServer;
      //  заполняем клиента
      serverPurchase = {
        ID: clientPurchase.id,
      };
      //  return
      return serverPurchase;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  REGION
  //  из клиентской в серверную
  public regionClientToServer(clientRegion: Region): RegionServer {
    //  проверка на пустоту
    if (clientRegion) {
      //  создаем начальное значение
      let serverRegion: RegionServer;
      serverRegion = {
        ID: clientRegion.id,
        country: clientRegion.country ? this.countryClientToServer(clientRegion.country) : null, //  countries
        name: clientRegion.name,
        code: clientRegion.code
      };
      //  возвращаем сконвертированное значение
      return serverRegion;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  из клиентской в серверную
  public regionServerToClient(serverRegion: RegionServer): Region {
    //  проверка на пустоту
    if (serverRegion) {
      //  создаем начальное значение
      let clientRegion: Region;
      clientRegion = {
        id: serverRegion.ID,
        country: serverRegion.country ? this.countryServerToClient(serverRegion.country) : null, //  country_id
        name: serverRegion.name, //  iso code2
        code: serverRegion.code
      };
      //  возврат полученного результата
      return clientRegion;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  ROLE
  //  из серверной в клиентскую
  public roleServerToClient(serverRole: RoleServer): Role {
    //  проверка на пустоту
    if (serverRole) {
      //  создаем начальное значение
      let clientRole: Role;
      //  присваиваем значения
      clientRole = {
        id: serverRole.ID,
        description: serverRole.description, // описание
        name: serverRole.name, // название
        title: serverRole.title, // заголовок
      };
      //  возвращаем сконвертированное значение
      return clientRole;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  client >> server
  public roleClientToServer(clientRole: Role): RoleServer {
    //  проверка на пустоту
    if (clientRole) {
      //  создаем начальное значение
      let serverRole: RoleServer;
      //  присваиваем значения
      serverRole = {
        ID: clientRole.id,  //  ID
        description: clientRole.description, // описание
        name: clientRole.name, // название
        title: clientRole.title, // заголовок
      };
      //  возвращаем сконвертированное значение
      return serverRole;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  SALE
  //  Server >> Client
  public saleServerToClient(serverSale: SaleServer): Sale {
    //  проверка на пустоту
    if (serverSale) {
      //  переменные
      let clientSale: Sale;
      //  заполняем клиента
      clientSale = {
        id: serverSale.ID,
        discount: serverSale.discount,
      };
      //  return
      return clientSale;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public saleClientToServer(clientSale: Sale): SaleServer {
    //  проверка на пустоту
    if (clientSale) {
      //  переменные
      let serverSale: SaleServer;
      //  заполняем клиента
      serverSale = {
        ID: clientSale.id,
        discount: clientSale.discount,
      };
      //  return
      return serverSale;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  SHOP
  //  Server >> Client
  public shopServerToClient(serverShop: ShopServer): Shop {
    //  проверка на пустоту
    if (serverShop) {
      //  переменные
      let clientShop: Shop;
      //  заполняем клиента
      clientShop = {
        id: serverShop.ID,
        name: serverShop.name,
      };
      //  return
      return clientShop;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public shopClientToServer(clientShop: Shop): ShopServer {
    //  проверка на пустоту
    if (clientShop) {
      //  переменные
      let serverShop: ShopServer;
      //  заполняем клиента
      serverShop = {
        ID: clientShop.id,
        name: clientShop.name,
      };
      //  return
      return serverShop;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  SHOP SHADOW
  //  Server >> Client
  public shopShadowServerToClient(serverShopShadow: ShopShadowServer): ShopShadow {
    //  проверка на пустоту
    if (serverShopShadow) {
      //  переменные
      let clientShop: ShopShadow;
      //  заполняем клиента
      clientShop = {
        id: serverShopShadow.ID,
        name: serverShopShadow.name,
      };
      //  return
      return clientShop;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public shopShadowClientToServer(clientShopShadow: ShopShadow): ShopShadowServer {
    //  проверка на пустоту
    if (clientShopShadow) {
      //  переменные
      let serverShopShadow: ShopShadowServer;
      //  заполняем клиента
      serverShopShadow = {
        ID: clientShopShadow.id,
        name: clientShopShadow.name,
      };
      //  return
      return serverShopShadow;
    } else {
      //  возвращаем null
      return null;
    }
  }

  // USERS
  //  Server >> Client
  public userServerToClient(serverUser: UserServer): User {
    //  проверка на пустоту
    if (serverUser) {
      //  переменные
      let clientUser: User;
      const roles: Role[] = [];
      //  конвертация массивов
      if (serverUser.roles) {
        serverUser.roles.forEach(role => {
          roles.push(role);
        });
      }
      clientUser = {
        id: serverUser.ID,
        username: serverUser.username,
        login: serverUser.login,
        password: serverUser.password,
        email: serverUser.email,
        description: serverUser.description,
        image: serverUser.image ? this.imageServerToClient(serverUser.image) : null,
        roles
      };
      return clientUser;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public userClientToServer(clientUser: User): UserServer {
    //  проверка на пустоту
    if (clientUser) {
      //  переменные
      let serverUser: UserServer;
      const roles: RoleServer[] = [];
      //  конвертация массивов
      if (clientUser.roles) {
        clientUser.roles.forEach(role => {
          roles.push(role);
        });
      }
      //  наполняем
      serverUser = {
        ID: clientUser.id,
        username: clientUser.username,
        login: clientUser.login,
        password: clientUser.password,
        email: clientUser.email,
        description: clientUser.description,
        image: clientUser.image ? this.imageClientToServer(clientUser.image) : null,
        roles
      };
      return serverUser;
    } else {
      //  возвращаем null
      return null;
    }
  }

  // zones
  //  Server >> Client
  public zoneServerToClient(serverZone: ZoneServer): Zone {
    //  проверка на пустоту
    if (serverZone) {
      //  переменные
      let clientZone: Zone;
      const countries: Country[] = [];
      const zoneCosts: ZoneCost[] = [];
      //  конвертация массивов
      if (serverZone.countries) {
        serverZone.countries.forEach(country => {
          countries.push(this.countryServerToClient(country));
        });
      }
      if (serverZone.zoneCosts) {
        serverZone.zoneCosts.forEach(zoneCost => {
          zoneCosts.push(this.ZoneCostServerToClient(zoneCost));
        });
      }
      //  наполняем
      clientZone = {
        id: serverZone.ID,
        countries,
        zoneCosts,
      };
      return clientZone;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public zoneClientToServer(clientZone: Zone): ZoneServer {
    //  проверка на пустоту
    if (clientZone) {
      //  переменные
      let serverZone: ZoneServer;
      const countries: CountryServer[] = [];
      const zoneCosts: ZoneCostServer[] = [];
      //  конвертация массивов
      if (clientZone.countries) {
        clientZone.countries.forEach(country => {
          countries.push(this.countryClientToServer(country));
        });
      }
      if (clientZone.zoneCosts) {
        clientZone.zoneCosts.forEach(zoneCost => {
          zoneCosts.push(this.ZoneCostClientToServer(zoneCost));
        });
      }
      serverZone = {
        ID: clientZone.id,
        countries,
        zoneCosts,
      };
      return serverZone;
    } else {
      //  возвращаем null
      return null;
    }
  }


  //  ZONE COST
  //  Server >> Client
  public ZoneCostServerToClient(serverZoneCost: ZoneCostServer): ZoneCost {
    //  проверка на пустоту
    if (serverZoneCost) {
      //  переменные
      let clientZoneCost: ZoneCost;
      //  заполняем клиента
      clientZoneCost = {
        id: serverZoneCost.ID,
        weight: serverZoneCost.weight,
        cost: serverZoneCost.cost,
      };
      //  return
      return clientZoneCost;
    } else {
      //  возвращаем null
      return null;
    }
  }

  //  Client >> Server
  public ZoneCostClientToServer(clientZoneCost: ZoneCost): ZoneCostServer {
    //  проверка на пустоту
    if (clientZoneCost) {
      //  переменные
      let serverZoneCost: ZoneCostServer;
      //  заполняем клиента
      serverZoneCost = {
        ID: clientZoneCost.id,
        weight: Number(clientZoneCost.weight),
        cost: Number(clientZoneCost.cost),

      };
      //  return
      return serverZoneCost;
    } else {
      //  возвращаем null
      return null;
    }
  }
}
