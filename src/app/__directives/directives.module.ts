import { NgModule } from '@angular/core';
import { AngularFittextDirective } from './fittext';

@NgModule({
    imports: [],
    declarations: [AngularFittextDirective],
    exports: [AngularFittextDirective]
})
export class DirectivesModule { }