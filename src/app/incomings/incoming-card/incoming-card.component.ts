import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Incoming, IncomingItem} from '../../_models/incomings.types';
import {ActivatedRoute, Router} from '@angular/router';
import {IncomingService} from '../incoming.service';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';
import {imgProto, imgURL} from '../../_models/urls';
import {Animations} from '../../Animations';

@Component({
  selector: 'app-incoming-card',
  templateUrl: './incoming-card.component.html',
  styleUrls: ['./incoming-card.component.scss'],
  animations: [Animations.fadeInOut]
})
export class IncomingCardComponent implements OnInit {
  //  public
  public imagePath: string = imgProto + imgURL + 'incomings/';
  public incomingItems: boolean = false; // incoming options visible
  @Input() public incoming: Incoming = null;

  //  private
  private incomingSub: Subscription = null;

  constructor(private activatedRoute: ActivatedRoute,
              private incomingService: IncomingService,
              private dataService: DataService,
              private router: Router) {}

  //  init
  ngOnInit() {
    console.log('Incoming: ', this.incoming);
  }

  //  открыть лист сверки
  public checkIncoming(incoming: Incoming) {
    this.router.navigate(['incomings/items/' + incoming.id]);
  }

  //  удалить накладную
  public delIncoming(incoming: Incoming) {
    //  дергаем сервис
    this.incomingService.deleteIncoming(incoming);
  }

  // open incoming details block
  public openIncomingItems() {
    this.incomingItems = !this.incomingItems;
  }

  //  create / update incoming
  public updateIncoming(imcoming?: Incoming) {
    console.log('updateIncoming: ', imcoming);
    //  проверяем, что передали
    if (!imcoming) {
      //  создаем нового производителя
      this.router.navigate(['/incomings/items']);
    } else {
      //  правим существующего производителя
      //  TODO: router.navigate(['brand', {id: brandId}])
      this.router.navigate(['/incomings/items', imcoming.id]);
    }
  }

  //  получить список производителей
  public getManufacturers(incoming: Incoming): string {
    //  если не пустой
    if (incoming && incoming.incomingItems) {
      //  возвращаем список производителей без дублей в строке
      return Array.from(new Set(incoming.incomingItems
        .map((incomingItem: IncomingItem) => incomingItem.product.manufacturer.name)))
        .join(', ');
    }
    return '';
  }


}
