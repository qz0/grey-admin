import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../../../_models/items.types';
import {IncomingItem} from '../../../_models/incomings.types';
import {OptionValue} from '../../../_models/options.types';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {
  //  public
  @Input() incomingItem: IncomingItem = null; //  item для отображения

  constructor() { }

  ngOnInit() {
  }

  //  конвертим массив значений опций для отображения
  public convertOptionValueToLabel(optionValues: OptionValue[]): string {
    console.log('STRING: ', optionValues, optionValues.map((optionValue: OptionValue) => optionValue.title).join(', '));
    return optionValues.map((optionValue: OptionValue) => optionValue.title).join();
  }
}
