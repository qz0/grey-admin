import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Manufacturer} from '../../_models/brands.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import {Product} from '../../_models/products.types';
import {OptionType, OptionValue} from '../../_models/options.types';
import {NgForm} from '@angular/forms';
import {IncomingService} from '../incoming.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GenerateService} from '../../_services/generate.service';
import {Incoming, IncomingItem} from '../../_models/incomings.types';
import {Image} from '../../_models/image.types';
import {ProductsService} from '../../products/products.service';
import {BrandsService} from '../../brands/brands.service';

@Component({
  selector: 'app-incoming.detail',
  templateUrl: './incoming-detail.component.html',
  styleUrls: ['./incoming-detail.component.scss']
})
export class IncomingDetailComponent implements OnInit, OnDestroy {
  @ViewChild('f', { static: false }) incForm: NgForm;

  //  public
  public incoming: Incoming = null;  //  товарная накладная
  public manufacturer: Manufacturer = null;  //  manufacturer
  public manufacturers: Manufacturer[] = null;  //  список  manufacturers
  public product: Product = null;  //  продукт
  public products: Product[] = null;  //  список продуктов
  public disabledSave: boolean = true;  //  бит неактивности в сохранении
  public enabledAddItems: boolean = false;  //  бит неактивности в сохранении
  public optionValues: OptionValue[] = null;  //  список значений опций
  public amount: number = 1;  //  количество товара

  public optionTypes: OptionType[] = null;  //  список типов опций
  public showOptions: boolean = false;  //  показать/скрыть опции
  public filterProducts = [];  //  отфильтрованные  продукты

  //  for test
  public itemName: string = null;
  public testNumber: number = 1;
  public manufacturerName: string = null;
  public comment: string = null;
  public invoice: string = null;

  //  private
  private incomingSub: Subscription;
  private manufacturersSub: Subscription;
  private productsSub: Subscription;
  private optionTypesSub: Subscription;

  //  constructor
  constructor(
    private activatedRoute: ActivatedRoute,   //  активированный роут
    private brandsService: BrandsService,         //  сервис доступа к данным
    private dataService: DataService,         //  сервис доступа к данным
    private incomingService: IncomingService, //  сервис инкаминга
    private productsService: ProductsService,         //  сервис доступа к данным
    private router: Router, //  router
  ) {
    //  подписка  incoming
    this.incomingSub = this.dataService.castIncoming
      .subscribe((incoming: Incoming) => {
        this.incoming = incoming;
        console.log('Incoming: ', incoming);
      });
    //  подписка  manufacturers
    this.manufacturersSub = this.dataService.castManufacturers
      .subscribe((manufacturers: Manufacturer[]) => this.manufacturers = manufacturers);
    //  подписка  products
    this.productsSub = this.dataService.castProducts
      .subscribe((products: Product[]) => {
        this.products = products;
        console.log('all products', this.products);
      });
    //  подписка  optionTypes
    this.optionTypesSub = this.dataService.castOptionTypes
      .subscribe((optionTypes: OptionType[]) => this.optionTypes = optionTypes);
  }

  //  destroy
  ngOnDestroy() {
    //  отписка incomingSub
    this.incomingSub.unsubscribe();
    //  отписка manufacturersSub
    this.manufacturersSub.unsubscribe();
    //  отписка productsSub
    this.productsSub.unsubscribe();
    //  отписка optionTypesSub
    this.optionTypesSub.unsubscribe();
  }

  //  init
  ngOnInit() {
    //  получаем incomingID
    const incomingID = this.activatedRoute.snapshot.paramMap.get('id');

    console.log('incomingID', incomingID);

    //  дергаем сервис
    this.incomingService.getIncomingByID(incomingID);
    //  получение всех  manufacturers
    this.brandsService.getAllManufacturers();
    //  получение всех  products
    this.productsService.getProducts();
    //  получение всех  optionTypes
    this.dataService.getOptionTypes();
  }

  //  add incoming
  public addItem(form: NgForm) {
    //  создаем новый инкомин
    const newIncomingItem: IncomingItem = {
      incomingID: this.incoming.id ? this.incoming.id : null,  // ид накладной
      product: this.product,  //  продукт
      productID: this.product.id,  // ид продукта
      optionValues: [...this.optionValues],  //  опции товара
      amount: this.amount //  количество товара
    };
    //  добавляем к списку
    this.incoming.incomingItems.push(newIncomingItem);
    //  обнуляем выбор
    // this.product = null;
    // this.optionValues = [];
    // this.amount = 1;
    // this.enabledAddItems = false;
    this.disabledSave = false;
  }

  //  изменить картинку
  public changeImage(eventImage: Image) {
    this.incoming.scan = eventImage;
  }

  //  выбрали продукт
  public changeManufacturerSelect() {
    //  дергаем сервис получения нового списка продуктов
    if (this.manufacturer) {
      this.productsService.getProductsByManufacturer(this.manufacturer);
    }
  }

  //  выбрали продукт
  public changeOptionSelect() {
    //  создаем массив опций необходимой длины
    if (!this.optionValues.some((optionValue: OptionValue) => optionValue == null)) {
      this.enabledAddItems = true;
    }
  }

  //  выбрали продукт
  public changeProductSelect() {
    //  создаем массив опций необходимой длины
    this.optionValues = Array<OptionValue>(this.product.productOptions.length).fill(null);
    //  если производитель не заполнен - заполняем
    if (!this.manufacturer && this.product.manufacturer) {
      this.manufacturer = this.product.manufacturer;
    }
    //  если у продукта нет опций открываем добавление товара
    this.enabledAddItems = true;
  }

  //  конвертим массив значений опций для отображения
  public convertOptionValueToLabel(optionValues: OptionValue[]): string {
    return optionValues.map((optionValue: OptionValue) => optionValue.title).join();
  }

  //  разрешить сохранение
  public enableSave() {
    this.disabledSave = false;
  }

  //  удаление incoming item
  public deleteIncoming(index: number) {
    this.incoming.incomingItems.splice(index, 1);
  }

  //  сохранение накладной
  public saveIncoming() {
    //  дерагем сервис
    this.incomingService.updateIncoming(this.incoming);
    //  возвращаемся на страницу инкамингов
    this.router.navigate(['/incomings']);
  }

}
