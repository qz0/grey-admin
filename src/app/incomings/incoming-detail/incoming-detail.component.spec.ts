import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingDetailComponent } from './incoming-detail.component';

describe('Incoming.DetailComponent', () => {
  let component: IncomingDetailComponent;
  let fixture: ComponentFixture<IncomingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
