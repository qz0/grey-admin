import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingItemsComponent } from './incoming-items.component';

describe('IncomingItemsComponent', () => {
  let component: IncomingItemsComponent;
  let fixture: ComponentFixture<IncomingItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomingItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
