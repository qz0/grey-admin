import {Component, OnDestroy, OnInit} from '@angular/core';
import {Item} from '../../_models/items.types';
import {IncomingService} from '../incoming.service';
import {DataService} from '../../_services/data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GenerateService} from '../../_services/generate.service';
import {Subscription} from 'rxjs';
import {Incoming} from '../../_models/incomings.types';
import {imgProto, imgURL} from '../../_models/urls';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-incoming-items',
  templateUrl: './incoming-items.component.html',
  styleUrls: ['./incoming-items.component.scss']
})
export class IncomingItemsComponent implements OnInit, OnDestroy {
  // public
  public imagePath: string = imgProto + imgURL + 'products/';
  public incoming: Incoming = null;  //  товарная накладная
  public item: Item;

  // private
  private incomingSub: Subscription;

  constructor(
    private incomingService: IncomingService, //  сервис инкаминга
    private dataService: DataService,         //  сервис доступа к данным
    private activatedRoute: ActivatedRoute,   //  активированный роут
    private generateService: GenerateService  //  сервис генерации новых сущностей
  ) {
    //  подписка  incoming
    this.incomingSub = this.dataService.castIncoming
      .subscribe((incoming: Incoming) => this.incoming = incoming);
  }

  ngOnDestroy() {

    //  отписка incomingSub
    this.incomingSub.unsubscribe();
  }

  ngOnInit() {
    //  получаем incomingID
    const incomingID = this.activatedRoute.snapshot.paramMap.get('id');

    console.log('incomingID', incomingID);

    //  если передали id
    if (incomingID) {
      this.incomingService.getIncomingByID(incomingID);
    } else {
      this.incomingService.getIncomingByID(null);
    }
  }

  //  запуск создания айтемов
  public checkedIncoming() {
    //  проверяем что все прочекали
    if (this.incoming && !this.incoming.incomingItems.some(incomingItem => !incomingItem.checked)) {
      this.incomingService.createItemsFromIncoming(this.incoming.id);
    }
  }

}
