import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IncomingListsComponent} from './incoming-lists/incoming-lists.component';
import {IncomingDetailComponent} from './incoming-detail/incoming-detail.component';
import {IncomingItemsComponent} from './incoming-items/incoming-items.component';

//  Incoming
export const incomingRoutes: Routes = [
  //  конкретный список
  { path: 'incoming/:id', component: IncomingDetailComponent},
  //  общая
  { path: '', component: IncomingListsComponent},
  //  отдельный incoming
  { path: 'incoming', component: IncomingDetailComponent},
  //  отдельный incoming
  { path: 'items', component: IncomingItemsComponent},
  //  отдельный incoming
  { path: 'items/:id', component: IncomingItemsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(incomingRoutes)],
  exports: [RouterModule]
})
export class IncomingsRoutingModule { }
