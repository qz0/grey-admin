import { Injectable } from '@angular/core';
import {Brand, Manufacturer} from '../_models/brands.types';
import {DataService} from '../_services/data.service';
import {Incoming} from '../_models/incomings.types';

@Injectable({
  providedIn: 'root'
})
export class IncomingService {

  constructor(private dataService: DataService) { }

  //  создание товарная накладная
  //  incoming - товарная накладная
  public createIncoming(incoming: Incoming) {
    //  если не пустой
    if (incoming) {
      this.dataService.createIncoming(incoming);
    }
    console.log('createIncoming: ', incoming);
  }

  //  создание айтемов на основе инкаминга и проставления бита чек
  public createItemsFromIncoming(incomingID) {
    this.dataService.createItemsFromIncoming(incomingID);
  }

  //  изменение товарная накладная
  //  incoming - товарная накладная
  public deleteIncoming(incoming: Incoming) {
    //  если есть что удалять
    if (incoming && String(incoming.id).length) {
      this.dataService.deleteIncoming(String(incoming.id));
    }
    console.log('deleteIncoming: ', incoming);
  }

  //  получение списка всех брэндов
  public getAllIncomings() {
    //  дергаем сервис
    this.dataService.getIncomings();
  }

  //  получение товарной накладной по ID
  //  incomingID - ID товарной накладной
  public getIncomingByID(incomingID: string) {
    //  дергаем сервис
    this.dataService.getIncomingByID(incomingID);
  }

  //  изменение товарной накладной
  //  incoming - товарная накладная
  public updateIncoming(incoming: Incoming) {
    //  если не пустой
    if (incoming) {
      //  новый или существующий?
      if (incoming.id) {
        this.dataService.updateIncoming(incoming);
      } else {
        this.dataService.createIncoming(incoming);
      }
    }
  }
}
