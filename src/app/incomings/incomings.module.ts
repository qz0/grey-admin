import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IncomingsRoutingModule } from './incomings-routing.module';
import { IncomingListsComponent } from './incoming-lists/incoming-lists.component';
import { IncomingDetailComponent } from './incoming-detail/incoming-detail.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';
import { IncomingItemsComponent } from './incoming-items/incoming-items.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {ImagesModule} from '../images/images.module';
import { ItemCardComponent } from './incoming-card/item-card/item-card.component';
import {IncomingCardComponent} from './incoming-card/incoming-card.component'

@NgModule({
  declarations: [
    IncomingListsComponent,
    IncomingDetailComponent,
    IncomingItemsComponent,
    IncomingCardComponent,
    ItemCardComponent
  ],
  imports: [
    CommonModule,
    IncomingsRoutingModule,
    FontAwesomeModule,
    FormsModule,
    NgSelectModule,
    ImagesModule
  ]
})
export class IncomingsModule {
}
