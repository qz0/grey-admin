import { TestBed } from '@angular/core/testing';

import { IncomingService } from './incoming.service';

describe('IncomingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncomingService = TestBed.get(IncomingService);
    expect(service).toBeTruthy();
  });
});
