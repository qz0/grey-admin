import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingListsComponent } from './incoming-lists.component';

describe('IncomingListsComponent', () => {
  let component: IncomingListsComponent;
  let fixture: ComponentFixture<IncomingListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomingListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
