import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IncomingService} from '../incoming.service';
import {DataService} from '../../_services/data.service';
import {BrandsService} from '../../brands/brands.service';
import {ProductsService} from '../../products/products.service';
import {Subscription} from 'rxjs';
import {Incoming, IncomingItem} from '../../_models/incomings.types';
import {Animations} from '../../Animations';

@Component({
  selector: 'app-incoming.lists',
  templateUrl: './incoming-lists.component.html',
  styleUrls: ['./incoming-lists.component.scss'],
  animations: [Animations.fadeInOut]
})
export class IncomingListsComponent implements OnInit {
  //  public
  public incomings: Incoming[] = null; // список накладных

  //  private
  private incomingsSub: Subscription;

  constructor(
    private router: Router,
    private incomingService: IncomingService, //  сервис инкаминга
    private dataService: DataService,         //  сервис доступа к данным
  ) {
    //  подписка  incoming
    this.incomingsSub = this.dataService.castIncomings
      .subscribe((incomings: Incoming[]) => {
        this.incomings = incomings;
        console.log('Incomings: ', incomings);
      });
  }

  //  destroy
  ngOnDestroy() {
    //  отписка incomingSub
    this.incomingsSub.unsubscribe();
  }

  ngOnInit() {
    this.dataService.getIncomings();
  }

  //  создать новую накладную
  public createIncoming() {
    this.router.navigate(['incomings/incoming']);
  }
}
