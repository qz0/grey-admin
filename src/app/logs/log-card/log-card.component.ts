import { Component, OnInit, Input } from '@angular/core';
import { Log } from 'src/app/_models/logs.types';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { LogService } from '../logs.service';
import { DataService } from 'src/app/_services/data.service';
import {Animations} from '../../Animations';
@Component({
  selector: 'app-log-card',
  templateUrl: './log-card.component.html',
  styleUrls: ['./log-card.component.scss'],
  animations: [Animations.fadeInOut]
})
export class LogCardComponent implements OnInit {

 //  public
 public logJsons: boolean = false; // product options visible
 @Input() public log: Log = null;
//  продукт до изменений
 public dataBeforeChanges = null;
//  продукт после изменений
 public dataAfterChanges = null;

 //  private
 private logSub: Subscription = null;

 constructor(private activatedRoute: ActivatedRoute,
             private logService: LogService,
             private dataService: DataService,
             private router: Router) {}

 //  init
  ngOnInit() {
    console.log('Log: ', this.log);
    // конвертация json в разбитую построчно строку
    this.dataBeforeChanges = JSON.stringify(JSON.parse(this.log.json), null, 4);
    this.dataAfterChanges = JSON.stringify(JSON.parse(this.log.jsonRequest), null, 4);
  }

  // open product details block
  public openLogJsons() {
    this.logJsons = !this.logJsons;
  }
}
