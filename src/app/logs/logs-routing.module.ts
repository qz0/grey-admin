import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogListComponent } from './log-list/log-list.component';
// import {IncomingItemsComponent} from './incoming-items/incoming-items.component';

//  Incoming
export const logRoutes: Routes = [
  //  общая
  { path: '', component: LogListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(logRoutes)],
  exports: [RouterModule]
})
export class IncomingsRoutingModule { }
