import { Injectable } from '@angular/core';
import { DataService } from '../_services/data.service';
import { Log } from '../_models/logs.types';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private dataService: DataService) { }
  //  Log - логи создания изменения и удаления сущностей
  //  получение списка всех логов
  public getAllLogs() {
    //  дергаем сервис
    this.dataService.getLogs();
  }
}
