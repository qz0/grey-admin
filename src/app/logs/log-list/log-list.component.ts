import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Log } from 'src/app/_models/logs.types';
import { Router } from '@angular/router';
import { LogService } from '../logs.service';
import { DataService } from 'src/app/_services/data.service';

@Component({
  selector: 'app-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['./log-list.component.scss']
})
export class LogListComponent implements OnInit {

  //  public
  public logs: Log[] = null; // список логов

  //  private
  private logsSub: Subscription;

  constructor(
    private router: Router,
    private logService: LogService, //  сервис логов
    private dataService: DataService,         //  сервис доступа к данным
  ) {
    //  подписка  log
    this.logsSub = this.dataService.castLogs
      .subscribe((logs: Log[]) => {
        this.logs = logs;
        console.log('Logs: ', logs);
      });
  }

  //  destroy
  ngOnDestroy() {
    //  отписка logSub
    this.logsSub.unsubscribe();
  }

  ngOnInit() {
    this.dataService.getLogs();
  }


}
