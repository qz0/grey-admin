import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//  экспортируем маршруты
export const itemsRoutes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(itemsRoutes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }
