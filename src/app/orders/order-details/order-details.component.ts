import { Component, OnInit } from '@angular/core';
import {Order} from '../../_models/orders.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import {ActivatedRoute} from '@angular/router';
import {OrdersService} from '../orders.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  //  public
  public menuLink: string = 'data';
  // public orders: Order[] = null;
  public order: Order = null;

  // private
  // private ordersSub: Subscription;
  private orderSub: Subscription;

  constructor(private dataService: DataService, private ordersService: OrdersService, private activatedRoute: ActivatedRoute) {
    //  подписываемся на асинхроны
    // this.ordersSub = this.dataService.castOrders
    //   .subscribe((orders: Order[]) => this.orders = orders);
    this.orderSub = this.dataService.castOrder
      .subscribe((order: Order) => {
        this.order = order;
        console.log(this.order);
      });
  }

  ngOnInit() {
    // this.dataService.getOrders();

    const orderID = this.activatedRoute.snapshot.paramMap.get('id');


    console.log('orderID: ', orderID);
    //  дергаем сервис получения ордера
    if (orderID) {
      this.ordersService.getOrderByID(orderID);
    }

    // if (this.orders && orderID) {
    //   this.order = this.orders.find(order => order.id === Number(orderID));
    // }

    console.log(this.order);
  }

  public selectMenu(menu: string) {
    this.menuLink = menu;
  }
}
