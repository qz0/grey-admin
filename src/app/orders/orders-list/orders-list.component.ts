import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrderCardConfig} from '../../_models/orders.types';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit {
  //  public
  public menuLink: string = 'in work';
  public orderConfigs: OrderCardConfig[] = null;

  //  private

  constructor() {}

  ngOnInit() {
    this.initOrderCardConfig();
  }

  //  очистка полей dateStart, dateEnd  and select
  public clearOrdersFilter() {
    console.log('clear filter');
  }

  //  init config
  private initOrderCardConfig() {
    this.orderConfigs = [];
    //  add in Work
    const orderInWork: OrderCardConfig = {
      name: 'in work',  //  name of slice
      statuses: [
        {
          id: 2, name: 'payment check'
        },
        {
          id: 3, name: 'comment check'
        },
        {
          id: 4, name: 'waiting'
        },
      ], //  statuses of slice
    };
    //  add Failed
    const orderFailed: OrderCardConfig = {
      name: 'failed',  //  name of slice
      statuses: [
        {
          id: 1, name: 'failed'
        },
        {
          id: 8, name: 'canceled'
        },
      ], //  statuses of slice
    };
    //  add Completed
    const orderCompleted: OrderCardConfig = {
      name: 'completed',  //  name of slice
      statuses: [
        {
          id: 5, name: 'assembly'
        },
        {
          id: 6, name: 'in box'
        },
        {
          id: 7, name: 'shipped'
        },
      ], //  statuses of slice
    };
    // add all slice into config
    this.orderConfigs.push(orderInWork);
    this.orderConfigs.push(orderFailed);
    this.orderConfigs.push(orderCompleted);
  }

  //  выбор menuLink
  public selectMenu(menu: string) {
    this.menuLink = menu;
  }

  //  Установить  сегодняшнюю дату
  public setTodayDate() {
    console.log(new Date());
  }
}
