import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { OrdersService } from './orders.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { OrderPageComponent} from './order-page/order-page.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { OrderCardComponent } from './order-card/order-card.component';
import { OrderStatusCardComponent } from './order-status-card/order-status-card.component';
import { OrderDetailsDataComponent } from './order-details-data/order-details-data.component';
import { OrderDetailsHistoryComponent } from './order-details-history/order-details-history.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    OrdersListComponent,
    OrderDetailsComponent,
    OrderPageComponent,
    OrderCardComponent,
    OrderStatusCardComponent,
    OrderDetailsDataComponent,
    OrderDetailsHistoryComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    FontAwesomeModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    NgSelectModule
  ],
  providers: [
    OrdersService
  ]
})
export class OrdersModule {
}
