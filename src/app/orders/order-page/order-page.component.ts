import {Component, Input, OnInit} from '@angular/core';
import {OrderCardConfig, OrderStatus} from '../../_models/orders.types';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.scss']
})
export class OrderPageComponent implements OnInit {
  //  public
  @Input() orderStatuses: OrderStatus[] = null;

  constructor() { }

  ngOnInit() {
  }

}
