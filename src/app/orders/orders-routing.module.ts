import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OrdersListComponent} from './orders-list/orders-list.component';
import {OrderDetailsComponent} from './order-details/order-details.component';

export const ordersRoutes: Routes = [
  {
    //  все orders
    path: '', component: OrdersListComponent
  },
  {
    //  отдельный order
    path: 'order/:id', component: OrderDetailsComponent
  },
  {
    //  отдельный order
    path: 'order', component: OrderDetailsComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(ordersRoutes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
