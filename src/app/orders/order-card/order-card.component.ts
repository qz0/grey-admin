import {Component, Input, OnInit} from '@angular/core';
import {Order} from '../../_models/orders.types';
import {Router} from '@angular/router';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.scss']
})
export class OrderCardComponent implements OnInit {
  @Input() public order: Order = null; // заказ

  constructor(private router: Router) { }

  ngOnInit() {
    console.log(this.order);
  }

  // тень для карточки
  public getShadowColor(hex: string) {
    const r = parseInt(hex.slice(0, 2), 16);
    const g = parseInt(hex.slice(2, 4), 16);
    const b = parseInt(hex.slice(4, 6), 16);

    return `0 .15rem .4rem rgba(${r},${g},${b},0.3)`;
  }

  //  перейти к отдельному order
  public orderDetail(order: Order) {
    //  если order не пустой
    if (order) {
      if (order.id) {
        this.router.navigate(['orders/order', order.id]);
      } else {
        this.router.navigate(['orders/order']);
      }
    } else {
      this.router.navigate(['orders/order']);
    }
  }

}
