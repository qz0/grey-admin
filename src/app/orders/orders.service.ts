import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Order, OrderStatus} from '../_models/orders.types';
import {Observable, of, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private dataService: DataService) { }

  public getOrders() {
    this.dataService.getOrders();
  }

  //  получение orders по статусу
  public getOrdersByStatus(orderStatus: OrderStatus): Observable<Order[]> {
    //  проверка, что передали непустоту
    if (orderStatus) {
      return this.dataService.getOrdersByStatus(orderStatus, 0, 50)
        .pipe(
          tap(orders => {
            return orders;
          })
        );
    } else {
      return of(null);
    }
  }

  //  получение orders по статусу
  public getOrdersForAssembly() {
    const status: OrderStatus = {
      id: 5,
      name: 'assembly'
    };
    this.dataService.getOrdersByStatus(status, 0, 50).subscribe((orders: Order[]) => {
      if (orders) {
        this.dataService.publishOrders(orders);
      }
    });
  }

  //  получение orders по статусу
  public getOrdersForCheckSend() {
    const status: OrderStatus = {
      id: 6,
      name: 'in box'
    };
    this.dataService.getOrdersByStatus(status, 0, 50).subscribe((orders: Order[]) => {
      if (orders) {
        this.dataService.publishOrders(orders);
      }
    });
  }

  //  проставление ордеру чека
  public orderHasAssemblied(orderID: number, boxNumber: string) {
    //  дергаем функцию
    this.dataService.changeOrderStatus(orderID, 6, '1');
  }

  //  проставление ордеру повторного чека
  public orderHasCheckSended(orderID: number) {
    //  дергаем функцию
    this.dataService.changeOrderStatus(orderID, 7, null);
  }

  //  получение order по ID
  public getOrderByID(orderID: string) {
    //  проверка, что передали непустоту
    if (orderID) {
      this.dataService.getOrderByID(orderID);
    }
  }
}
