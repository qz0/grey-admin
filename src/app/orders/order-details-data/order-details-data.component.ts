import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Order} from '../../_models/orders.types';
import {imgProto, imgURL} from '../../_models/urls';
import {Country, CountryShort, Region} from '../../_models/address.types';
import {DataService} from '../../_services/data.service';
import {Delivery} from '../../_models/deliveries.types';
import {DeliveriesService} from '../../deliveries/deliveries.service';

@Component({
  selector: 'app-order-details-data',
  templateUrl: './order-details-data.component.html',
  styleUrls: ['./order-details-data.component.scss']
})
export class OrderDetailsDataComponent implements OnInit {
  // public
  @Input() order: Order;
  public currentShippingAddress: boolean = false;
  public imagePath: string = imgProto + imgURL;
  public countryList: CountryShort[] = [];  //  список стран в сокращенном виде
  public regionList: Region[] = null; //  регионы
  public deliveries: Delivery[] = null; //  доставки

  // private
  private countryListSub: Subscription = null;
  private regionsListSub: Subscription = null;
  private deliveriesSub: Subscription = null;

  constructor(
    private dataService: DataService,
    private deliveriesService: DeliveriesService) {
    //  подписка на список стран
    this.countryListSub = this.dataService.castCountriesShort.subscribe((countryList: CountryShort[]) => {this.countryList = countryList;});
    //  подписываемся на доставки
    this.deliveriesSub = this.dataService.castDeliveries.subscribe((deliveries: Delivery[]) => this.deliveries = deliveries);
  }

  ngOnInit() {
    this.deliveriesService.getDeliveries();
  }

  public showCurrentShippingAddress() {
    this.currentShippingAddress = !this.currentShippingAddress;
  }

  //  выбрали страну
  public chooseCountry() {
  }

  public chooseRegion() {
  }

  public chooseDelivery() {

  }

}
