import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {of, Subscription} from 'rxjs';

import {Order, OrderStatus} from '../../_models/orders.types';
import {OrdersService} from '../orders.service';
import {DataService} from '../../_services/data.service';
import {catchError} from 'rxjs/operators';

@Component({
  selector: 'app-order-status-card',
  templateUrl: './order-status-card.component.html',
  styleUrls: ['./order-status-card.component.scss']
})
export class OrderStatusCardComponent implements OnInit, OnDestroy {
  //  public
  @Input() orderStatus: OrderStatus = null;  // название статуса
  public ordersByStatus: Order[] = null;  //  список заказов по статусу
  // public orders: Order[] = null;  //  список заказов по статусу

  //  private
  private ordersByStatusSub: Subscription;
  // private ordersSub: Subscription;

  constructor(private dataService: DataService, private ordersService: OrdersService) {
    // this.ordersSub = this.dataService.castOrders
    //   .subscribe((orders: Order[]) => {
    //     this.orders = orders;
    //     orders.forEach(order => console.log('order:  _ ', order.statusID, this.orderStatus.id));
    //     this.ordersByStatus = orders.filter(order => order.statusID === this.orderStatus.id);
    //   });
  }

  // destroy
  ngOnDestroy() {
    // отписка  ordersByStatusSub
    this.ordersByStatusSub.unsubscribe();
    // this.ordersSub.unsubscribe();
  }

  //  init
  ngOnInit() {
    // console.log('order-status-card - OnInit');
    //  подписка ordersByStatus
    this.ordersByStatusSub = this.ordersService.getOrdersByStatus(this.orderStatus)
      .subscribe((orders: Order[]) => {
        //  проверка, что передали непустоту
        if (orders) {
          //  список orders по статусу
          this.ordersByStatus = orders;
          // console.log('Orders:', orders);
          // return orders;
        } else {
          this.ordersByStatus = null;
          // console.log('err');
          // return of(null);
        }
      }, catchError(_ => {
        return of(null);
      }) );
    // console.log('orders by status', this.ordersByStatus);
    // console.log('order statuses', this.orderStatus);
  }

}
