import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router, Params} from '@angular/router';
import {Manufacturer} from '../../_models/brands.types';
import {BrandsService} from '../brands.service';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';

@Component({
  selector: 'app-manufacturer',
  templateUrl: './manufacturer.component.html',
  styleUrls: ['./manufacturer.component.scss']
})
export class ManufacturerComponent implements OnInit, OnDestroy {
  //  id  Manufacturer
  public id: string;

  //  объявление формы
  public manufacturerForm: FormGroup;

  // дапнные мануфактуры
  //
  public currentManufacturer: Manufacturer = null;

  //  private
  private manufacturerSub: Subscription;

  constructor(private fb: FormBuilder,  //  импорт формбилдера
              private activatedRoute: ActivatedRoute, //  импорт состояния роутера
              private router: Router, // навигация и URL манипуляции
              private brandsService: BrandsService, //  сервис brands
              private dataService: DataService, //  сервис доступа к данным
  ) {
    this.manufacturerSub = this.dataService.castManufacturer.subscribe((manufacturer: Manufacturer) => {
      this.currentManufacturer = manufacturer;
      console.log('currentManufacturer', this.currentManufacturer);
    });
  }

  //  функция удаления производителя
  //  manufacturer - экземпляр производителя
  // public deleteManufacturer(manufacturer: Manufacturer) {
  //   console.log('deleted manufacturer with id:' + this.manufacturer.id);
  //   // this.onSaveComplete();
  // }

  //  инитор
  public ngOnInit() {
    //  получаем brand
    const manufacturerId = this.activatedRoute.snapshot.paramMap.get('id');

    console.log('manufacturerId', manufacturerId);

    //  если передали id
    if (manufacturerId) {
      this.brandsService.getManufacturerByID(manufacturerId);
    } else {
      this.brandsService.getManufacturerByID(null);
    }
  }

  ngOnDestroy() {
    this.manufacturerSub.unsubscribe();
  }

  //  Удаление  Manufacturer
  deleteManufacturer() {
    this.brandsService.deleteManufacturer(this.currentManufacturer);
    this.router.navigate(['/brands']);
  }

  //  Сохранение или изменение Manufacturer
  updateManufacturer() {
    if (this.currentManufacturer.id) {
      console.log('update', this.currentManufacturer);
      this.brandsService.updateManufacturer(this.currentManufacturer);
    } else {
      console.log('save', this.currentManufacturer);
      this.brandsService.createManufacturer(this.currentManufacturer);
      this.router.navigate(['/brands']);
    }
  }

  //  обновляем компонент
  public refreshManufacturer() {
    //  дергаем инитор
    this.ngOnInit();
  }

  //  функция внесения изменений в производителя
  //  manufacturer - экземпляр производителя
  // public updateManufacturer(manufacturer: Manufacturer) {
  //   console.log({ ...this.manufacturer, ...this.manufacturerForm });
  //   // this.onSaveComplete();
  // }
}
