import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrandsRoutingModule } from './brands-routing.module';
import { ManufacturerComponent } from './manufacturer/manufacturer.component';
import { BrandComponent } from './brand/brand.component';
import { BrandsComponent } from './brands/brands.component';
import { ManufacturersComponent } from './manufacturers/manufacturers.component';
import { BrandsGeneralComponent } from './brands.general/brands.general.component';
import { PipesModule } from '../pipes/pipes.module';
import { BrandsService } from './brands.service';
import { DndModule } from 'ngx-drag-drop';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { BrandCardComponent } from './brand-card/brand-card.component';
import { ManufacturerCardComponent } from './manufacturer-card/manufacturer-card.component';
import {ImagesModule} from '../images/images.module';

@NgModule({
  declarations: [
    BrandComponent,
    BrandsComponent,
    BrandsGeneralComponent,
    ManufacturerComponent,
    ManufacturersComponent,
    BrandCardComponent,
    ManufacturerCardComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrandsRoutingModule,
    PipesModule,
    DndModule,
    FontAwesomeModule,
    ImagesModule,
  ],
  providers: [
    BrandsService
  ]
})
export class BrandsModule {
}
