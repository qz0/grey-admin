import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, NgForm} from '@angular/forms';

import { Brand } from '../../_models/brands.types';
import { BrandsService } from '../brands.service';
import { DataService } from '../../_services/data.service';
import { imgProto, imgURL } from '../../_models/urls';
import { GenerateService } from '../../_services/generate.service';
import { Subscription } from 'rxjs';
import {Image} from '../../_models/image.types';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
//  Брэнд
export class BrandComponent implements OnInit {
  // @ViewChild('f', {static: false}) brandForm
  //  экземпляр формы
  @ViewChild('f', {static: true}) brandForm: NgForm;
  //  public
  // объявление формы
  // public brandForm: FormGroup;
  //  текущий брэнд
  public currentBrand: Brand = null;
  public currentLangTab = 'EN';
  public imagePath: string = imgProto + imgURL + '/brands/';

  //  private
  private brandSub: Subscription;

  //  конструктор
  constructor(private formBuilder: FormBuilder, //  формбилдер
              private activatedRoute: ActivatedRoute, //  активированный роут
              private router: Router, //  роутер
              private generateService: GenerateService, //  сервис генерации новых сущностей
              private brandsService: BrandsService, //  сервис брендов
              private dataService: DataService, //  сервис брендов
  ) {
    this.brandSub = this.dataService.castBrand.subscribe((brand: Brand) => {
      this.currentBrand = brand;
      console.log('brand: ', brand);
      // this.brandForm.setValue({
      //   priority: '123',
      // });
    });
  }

  public changeImage(eventImage: Image) {
    console.log('EVENT', event);
    console.log('BEFORE BRAND', this.currentBrand);
    this.currentBrand.image = eventImage;
    console.log('AFTER BRAND', this.currentBrand);
  }

  //  меняем приоритет у брэнда
  public changePriority(priority: number) {
    //  если передали корректный параметр - меняем значение
    if (priority && priority > 0 && priority <= 10) {
      this.currentBrand.priority = priority;
    }
    console.log(priority);
  }

  //  удаление брэнда
  //  brand - экземпляр брэнда
  public deleteBrand() {
    // //  получаем текущие значения формы
    // const value = form.value;
    // //  проверяем новый или уже есть в базе
    // if (this.currentBrand.id) {
    //   this.brandsService.updateBrand(this.currentBrand);
    // } else {
    //   this.brandsService.createBrand(this.currentBrand);
    // }
    // console.log(value, this.currentBrand);
  }

  //  удаляем картинку из системы
  public deleteImage(brand: Brand) {
  //
  }

  //  инитор
  public ngOnInit() {
    //  получаем brand
    const brandId = this.activatedRoute.snapshot.paramMap.get('id');

    console.log('brandId', brandId);

    //  если передали id
    if (brandId) {
      this.brandsService.getBrandByID(brandId);
    } else {
      this.brandsService.getBrandByID(null);
    }
  }

  //  обновляем компонент
  public refreshBrand() {
    //  дергаем инитор
    this.ngOnInit();
  }

  //  сохранение изменений
  //  form - экземпляр формы
  public updateBrand(form: NgForm) {
    //  получаем текущие значения формы
    const value = form.value;
    //  проверяем новый или уже есть в базе
    if (this.currentBrand.id) {
      this.brandsService.updateBrand(this.currentBrand);
    } else {
      this.brandsService.createBrand(this.currentBrand);
    }
    console.log(value, '123', this.currentBrand);
  }

  //  переключение табов языка
  public selectLangTab(lang: string) {
    this.currentLangTab = lang;
  }
}
