import {Component, Input, OnInit} from '@angular/core';
import {Brand} from '../../_models/brands.types';
import {Router} from '@angular/router';
import { imgProto, imgURL} from '../../_models/urls';

@Component({
  selector: 'app-brand-card',
  templateUrl: './brand-card.component.html',
  styleUrls: ['./brand-card.component.scss']
})
export class BrandCardComponent implements OnInit {
  //  public
  @Input() public brand: Brand = null;  //  brand
  public picturePath: string = imgProto + imgURL;

  //  constructor
  constructor(private router: Router) { }

  //  initor
  public ngOnInit() {
    if (this.brand) {
      console.log('brand: ', this.brand);
    }
  }

  //  создание / изменение брэнда
  public updateBrand(brand?: Brand) {
    console.log('updateBrand: ', brand);
    //  проверяем, что передали
    if (!brand) {
      //  создаем нового производителя
      this.router.navigate(['/brands/brand']);
    } else {
      //  правим существующего производителя
      //  TODO: router.navigate(['brand', {id: brandId}])
      this.router.navigate(['/brands/brand', {id: brand.id}]);
    }
  }

}
