import {Injectable} from '@angular/core';
import {DataService} from '../_services/data.service';
import {Brand, Manufacturer} from '../_models/brands.types';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
//  сервис взаимодействия с brands
export class BrandsService {
  //  public
  //  casts
  //  каст brand
  public castBrands: Observable<Brand[]>;
  //  каст manufacturer
  public castManufacturers: Observable<Manufacturer[]>;

  //  private
  //  bs
  //  bs для brands
  private brands: BehaviorSubject<Brand[]>  = new BehaviorSubject<Brand[]>(null);
  //  bs для manufacturers
  private manufacturers: BehaviorSubject<Manufacturer[]>  = new BehaviorSubject<Manufacturer[]>(null);
  //  конструктор
  constructor(private dataService: DataService) {
    // привязки кастов и bs
    //  привязываем каст и БС для бита изменяемости карточки brand
    this.castBrands = this.brands.asObservable();
    //  привязываем каст и БС для бита изменяемости карточки brand
    this.castManufacturers = this.manufacturers.asObservable();
  }

  //  создание торговой марки
  //  brand - торговая марка
  public createBrand(brand: Brand) {
    //  если не пустой
    if (brand) {
      this.dataService.createBrand(brand);
    }
    console.log('createBrand: ', brand);
  }

  //  создание производителя
  //  manufacturer - производитель
  public createManufacturer(manufacturer: Manufacturer) {
    //  если не пустой
    if (manufacturer) {
      this.dataService.createManufacturer(manufacturer);
    }
    console.log('createBrand: ', manufacturer);
  }

  //  изменение торговой марки
  //  brand - торговая марка
  public deleteBrand(brand: Brand) {
    if (brand && String(brand.id).length) {
      this.dataService.deleteBrand(String(brand.id));
    }
    console.log('updateBrand: ', brand);
  }

  //  изменение производителя
  //  manufacturer - производитель
  public deleteManufacturer(manufacturer: Manufacturer) {
    //  TODO: проверка, что нет брэндов с этим производителем
    if (manufacturer && String(manufacturer.id).length) {
      this.dataService.deleteManufacturer(String(manufacturer.id));
    }
    console.log('updateManufacturer: ', manufacturer);
  }

  //  получение списка всех брэндов
  public getAllBrands() {
    //  дергаем сервис
    this.dataService.getBrands();
  }

  //  получение списка всех производителей
  public getAllManufacturers() {
    //  дергаем сервис
    this.dataService.getManufacturers();
    //  создание переменной
    // const manufacturers: Manufacturer[] = [];
    // const manufacturer1: Manufacturer = {
    //   id: 1,
    //   name: 'Производитель 1',
    //   cent: 50,
    //   color: '',
    //   contacts: '',
    //   site: '',
    //   email: '',  //  почта
    //   vk: '',
    //   phone: '',
    //   bank: '',
    //   contract: '',
    //   discount: '',
    //   address: '',
    //   delivery: '',
    //   comment: '',
    //   brands: []
    // };
    // const manufacturer2 = {
    //   id: 2,
    //   name: 'Производитель 2',
    //   cent: 50,
    //   color: '',
    //   contacts: '',
    //   site: '',
    //   email: '',
    //   vk: '',
    //   phone: '',
    //   bank: '',
    //   contract: '',
    //   discount: '',
    //   address: '',
    //   delivery: '',
    //   comment: '',
    //   brands: [{
    //     id: 3,
    //     localizedDescriptions: [
    //       {
    //         id: 1,
    //         name: 'Заголовок 3',
    //         title: 'Заголовок 3',
    //         description: 'Описание',
    //         language: 'RU'
    //       },
    //       {
    //         id: 2,
    //         name: 'Title 3',
    //         title: 'Title 3',
    //         description: 'Description',
    //         language: 'EN'
    //       }
    //     ],
    //     image: '',
    //     priority: 2,
    //     manufacturer: null,
    //     seoLinks: ''
    //   }]
    // };
    //
    // //  заполнение брэндов
    // manufacturers.push(manufacturer1);
    // // manufacturers.push(manufacturer2);
    //
    // //  возврат списка всех производителей
    // return manufacturers;
  }

  //  получение брэнда по ID
  //  brandID - идентификатор брэнда
  public getBrandByID(brandID: string) {
    console.log('...', brandID);
    //  дергаем сервис
    this.dataService.getBrandByID(brandID);
  }

  //  получение брэнда по ID
  //  manufacturerID - идентификатор производителя
  public getManufacturerByID(manufacturerID: string) {
    //  дергаем сервис
    this.dataService.getManufacturerByID(manufacturerID);
  }

  //  изменение торговой марки
  //  brand - торговая марка
  public updateBrand(brand: Brand) {
    if (brand) {
      this.dataService.updateBrand(brand);
    }
    // console.log('updateBrand: ', brand);
  }

  //  изменение производителя
  //  manufacturer - производитель
  public updateManufacturer(manufacturer: Manufacturer) {
    if (manufacturer) {
      this.dataService.updateManufacturer(manufacturer);
    }
    console.log('updateManufacturer: ', manufacturer);
  }

  //  создание/изменение производителя
  //  brand - производитель
  public moveBrand(brand: Brand, manufacturer: Manufacturer) {
    //  присваеваем значение производителя для брэнда
    brand.manufacturer = manufacturer;
    //  дергаем внесение изменений в брэнд
    this.updateBrand(brand);
    //  дергаем обновленную версию брендов
    // this.getAllBrands();
    console.log('moveBrand: ', brand, manufacturer);
  }
}
