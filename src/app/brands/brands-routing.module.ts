import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManufacturerComponent } from './manufacturer/manufacturer.component';
import { BrandComponent } from './brand/brand.component';
import { BrandsGeneralComponent } from './brands.general/brands.general.component';

export const brandsRoutes: Routes = [
  //  общая
  { path: '', component: BrandsGeneralComponent },
  // //  блок бренды
  // { path: 'brands', component: BrandsGeneralComponent },
  //  блок отдельный бренд
  { path: 'brand', component: BrandComponent },
  { path: 'brand/:id', component: BrandComponent },
  // //  блок производители
  // { path: '', component: ListUsersComponent },
  //  блок отдельный производитель
  { path: 'manufacturer', component: ManufacturerComponent },
  { path: 'manufacturer/:id', component: ManufacturerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(brandsRoutes)],
  exports: [RouterModule]
})
export class BrandsRoutingModule {
}
