import {Component, Input, OnInit} from '@angular/core';
import {Brand, Manufacturer} from '../../_models/brands.types';
import {Router} from '@angular/router';
import {BrandsService} from '../brands.service';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';

@Component({
  selector: 'app-manufacturer-card',
  templateUrl: './manufacturer-card.component.html',
  styleUrls: ['./manufacturer-card.component.scss']
})
export class ManufacturerCardComponent implements OnInit {
  //  public
  @Input() public manufacturer: Manufacturer = null; //  производитель

  public brands: Brand[] = [];

  //  private
  private brandsSub: Subscription;

  constructor(private router: Router, private brandService: BrandsService, private dataService: DataService) {
    // this.brandsSub = this.dataService.castBrands.subscribe((brands: Brand[]) => this.brands = brands);
  }

  ngOnInit() {
  }

  //  изменение существующего производителя
  public updateManufacturer(manufacturer: Manufacturer) {
    if (manufacturer && manufacturer.id) {
      //  правим существующего производителя
      this.router.navigate(['/brands/manufacturer', manufacturer.id]);
    }
  }

  //  перетаскивание начато
  public dragStart(event: any) {
  }

  //  перетаскивание завершено
  public dragEnd(event: any) {
  }

  //  перемещение
  public dragover(event: any) {
  }

  //  брэнд сбросили
  public drop(event: any) {
    //  производителем
    const brand: Brand = event.data;
    //  заполняем бренд необходимыми данными
    brand.ownerID = this.manufacturer.id;
    brand.ownerType = 'public.manufacturers';
    brand.manufacturer = this.manufacturer;
    // console.log('Попытка перемещения бренда', brand, this.manufacturer)
    this.brandService.updateBrand(brand);
    // console.log('brand event data', event.data);
    // console.log('manufacturer', this.manufacturer);

    // const addBrandToManufactures = (brandItems: Brand[], BrandItemToAdd: Brand) => {
    //   const existingBrandItem = brandItems.find(
    //     brandItem => brandItem.id === BrandItemToAdd.id
    //   );
    //
    //   if (existingBrandItem) {
    //     return brandItems.map(brandItem => brandItem);
    //   }
    //
    //   return [...brandItems, { ...BrandItemToAdd}];

    // this.manufacturer.brands = addBrandToManufactures(this.brands, event.data);
    // console.log('updateManufacturerBrands', this.manufacturer);
    // this.dataService.updateManufacturer(this.manufacturer);
    // this.brandService.moveBrand(event.data, this.manufacturer);
  }
}
