import {Component, OnDestroy, OnInit} from '@angular/core';
import { Brand } from '../../_models/brands.types';
import { BrandsService } from '../brands.service';
import { Router } from '@angular/router';
import { imgProto, imgURL} from '../../_models/urls';
import {Currency} from '../../_models/currencies.types';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss']
})

//  отображение брэндов списком
export class BrandsComponent implements OnInit, OnDestroy {
  // public
  public imageFileName: string = null;
  public imagePath: string = imgProto + imgURL;
  //  брэнды
  public brands: Brand[] = null;

  //  private
  private brandsSub: Subscription;

  //  конструктор
  constructor(private dataService: DataService, private brandService: BrandsService,
              private router: Router) {
    //  подписка brands
    this.brandsSub = this.dataService.castBrands.subscribe((brands: Brand[]) => this.brands = brands);
  }

  public dragover(event) {
    console.log(event);
  }

  public drop(event) {
    console.log('Дроп', event);
  }

  public dragStart(event) {
    console.log('Дроп начали', event);
  }

  public dragEnd(event) {
    console.log('Дроп закончили', event);
  }

  //  destroy
  public ngOnDestroy() {
    //  отписываемся от подписок
    this.brandsSub.unsubscribe();
  }

  //  инитор
  public ngOnInit() {
    this.brandService.getAllBrands();
  }

  //  создание / изменение брэнда
  public updateBrand(brand?: Brand) {
    console.log('updateBrand: ', brand);
    //  проверяем, что передали
    if (!brand) {
      //  создаем нового производителя
      this.router.navigate(['/brands/brand']);
    } else {
      //  правим существующего производителя
      //  TODO: router.navigate(['brand', {id: brandId}])
      this.router.navigate(['/brands/brand', {id: brand.id}]);
    }
  }

}
