import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandsGeneralComponent } from './brands.general.component';

describe('Brands.GeneralComponent', () => {
  let component: BrandsGeneralComponent;
  let fixture: ComponentFixture<BrandsGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandsGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandsGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
