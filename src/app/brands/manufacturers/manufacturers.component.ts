import {Component, OnDestroy, OnInit} from '@angular/core';
import { Brand, Manufacturer } from '../../_models/brands.types';
import { BrandsService } from '../brands.service';
import { Router } from '@angular/router';
import {imgProto, imgURL} from '../../_models/urls';
import {LocalizedDescription} from '../../_models/localization';
import {DndDropEvent} from 'ngx-drag-drop';
import {Category} from '../../_models/categories.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';

@Component({
  selector: 'app-manufacturers',
  templateUrl: './manufacturers.component.html',
  styleUrls: ['./manufacturers.component.scss']
})
export class ManufacturersComponent implements OnInit, OnDestroy {
  // public
  public imageFileName: string = null;
  public imagePath: string = imgProto + imgURL;
  //  производители
  public manufacturers: Manufacturer[] = null;

  // private
  private manufacturersSub: Subscription; //  подписка manufacturers

  //  конструктор
  constructor(private brandService: BrandsService,
              private router: Router,
              private dataService: DataService) {
    //  подписка manufacturers
    this.manufacturersSub = this.dataService.castManufacturers
      .subscribe((manufacturers: Manufacturer[]) => {
        this.manufacturers = manufacturers;
        console.log('Manufacturers', this.manufacturers);
      });
  }

  // Destroy
  public ngOnDestroy() {
    //  отписка от manufacturers
    this.manufacturersSub.unsubscribe();
  }

  //  Инитор
  public ngOnInit() {
    this.brandService.getAllManufacturers();
    console.log(this.manufacturers);
  }

  public onDragover(event) {
    console.log('onDragover: ', event);
  }

  //  Сброшен
  onDropBrand(event: DndDropEvent, i: number, parentCategoriesArray: Category[]) {

    console.log('Added: ', event.data, 'event: ', event.index, ' to: ', parentCategoriesArray);
    // if (this.containerId != null) {
    //   this.bigCategories[this.containerId] = this.bigCategories[this.containerId].filter(category => category.id !== event.data.id);
    //   // this.bigCategories[this.containerId].splice(this.bigCategories.indexOf(event.data), 1);
    // }
    // this.bigCategories[i].splice(event.index, 0, event.data);
    console.log('dropped', JSON.stringify(event, null, 2));

    if (event && event.data && parentCategoriesArray && parentCategoriesArray.length >= event.index) {
      //  добавляем локально
      parentCategoriesArray.splice(event.index, 0, event.data);
    }
  }

//  создание изменение брэнда
  public updateBrand(brand?: Brand) {
    //  проверяем, что передали
    if (!brand) {
      //  создаем нового производителя
      this.router.navigate(['/brands/brand']);
    } else {
      //  правим существующего производителя
      //  TODO: router.navigate(['brand', {id: brandId}])
      this.router.navigate(['/brands/brand', brand.id]);
    }
  }

  //  создание изменение производителя
  public updateManufacturer(manufacturer?: Manufacturer) {
    //  проверяем, что передали
    if (!manufacturer) {
      //  создаем нового производителя
      this.router.navigate(['/brands/manufacturer']);
    } else {
      //  правим существующего производителя
      this.router.navigate(['/brands/manufacturer', manufacturer.id]);
    }
  }
}
