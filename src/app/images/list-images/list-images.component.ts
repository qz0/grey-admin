import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';

import { ImagesService } from '../images.service';
import { imgProto, imgURL} from '../../_models/urls';
import { BehaviorSubject, Observable } from 'rxjs';
import {Image} from '../../_models/image.types';
import {Brand} from '../../_models/brands.types';

@Component({
  selector: 'app-list-images',
  templateUrl: './list-images.component.html',
  styleUrls: ['./list-images.component.scss']
})
export class ListImagesComponent implements OnInit {
  //  public
  //  filename
  public imageFileName: string = null;
  public imagePath: string = imgProto + imgURL;
  public images: Image[] = null;

  //  cast
  //  каст imageName
  // public castImageName: Observable<string>;
  // public castImages: Observable<Image[]>;

  //  private
  //  bs
  //  bs для imageName
  // private imageName: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private imagesService: ImagesService, private dataService: DataService) {
    //  привязываем каст и БС для бита изменяемости карточки image
    // this.castImageName = this.imageName.asObservable();
    // this.castImages = this.images.asObservable();
  }

  ngOnInit() {
    //  подписка isChangeable
    this.imagesService.castImageName.subscribe((imageName: string) => this.imageFileName = imageName);
    this.dataService.castImages.subscribe((images: Image[]) => this.images = images);
    // this.imagesService.castImages.subscribe((images: Image[]) => this.images = images);
    // this.imagesService.castImageыName.subscribe((imageName: ) => this.imageFileName = imageName);
    this.dataService.getImages();
  }
  //  сохранение картинки в базу
  public imageSave(title: HTMLInputElement, pririty: HTMLInputElement) {
    console.log('title, priority: ', title.value, pririty.value);
    //  проверки?
    const newImage: Image = {
      name: title.value,
      url: this.imageFileName,
      priority: Number(pririty.value),
      options: null,  //  options
      optionsID: null,
      type: null, //  type
      typeID: null,
    };

    console.log('imagesService.createImage');
    //  дергаем сервис
    this.imagesService.createImage(newImage);
    //  обнуляем выбранное
    if (this.imageFileName == null) {
      title.value = '';
      pririty.value = '';
    }

    console.log(this.images);
  }
}
