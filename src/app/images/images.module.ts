import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImagesRoutingModule } from './images-routing.module';
import { ListImagesComponent } from './list-images/list-images.component';
import { ImageDetailsComponent } from './image-details/image-details.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ImageCardComponent } from './image-card/image-card.component';

@NgModule({
  declarations: [ListImagesComponent, ImageDetailsComponent, ImageCardComponent],
  exports: [
    ImageCardComponent
  ],
  imports: [
    CommonModule,
    ImagesRoutingModule,
    FontAwesomeModule
  ]
})
export class ImagesModule {
}
