import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {Option} from '../_models/options.types';
import {Image} from '../_models/image.types';
import {DataService} from '../_services/data.service';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  //  public
  //  casts
  //  каст images
  public castImages: Observable<Image[]>;
  //  каст imageName
  public castImageName: Observable<string>;

  //  private
  //  bs
  //  bs для images
  private images: BehaviorSubject<Image[]> = new BehaviorSubject<Image[]>(null);
  //  bs для imageName
  private imageName: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private dataService: DataService) {
    // привязки кастов и bs
    //  привязываем каст и БС для бита изменяемости карточки images
    this.castImages = this.images.asObservable();
    //  привязываем каст и БС для бита изменяемости карточки image
    this.castImageName = this.imageName.asObservable();
  }

  //  создание картинки
  //  option - опция для удаления
  public createImage(image: Image) {
    console.log(image);
    //  создаем новую опцию
    this.dataService.createImage(image);
    this.imageName.next(null);
    this.getAllImages(null, null);
  }

  //  обнуляем сохраненное имя
  public emptyImageName() {
    this.imageName.next(null);
  }

  //    получение списка всех картинок
  public getAllImages(count: number, offset: number) {
    //  дергаем свежую копию images
    this.dataService.getImages();
  }

  //  загрузка файла картинки
  //  files - файллист для прицепления
  public uploadImage(files: FileList, imageType: number): Observable<Image>  {
    // дергаем сервис аплоада
    return this.dataService.uploadImage(files, imageType);
  }
}
