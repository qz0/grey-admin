import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ListImagesComponent} from './list-images/list-images.component';
import {ImageDetailsComponent} from './image-details/image-details.component';

export const imagesRoutes: Routes = [
  {
    path: '',
    component: ListImagesComponent,
  },
  {
    path: 'image/:id',
    component: ImageDetailsComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(imagesRoutes)],
  exports: [RouterModule]
})
export class ImagesRoutingModule { }
