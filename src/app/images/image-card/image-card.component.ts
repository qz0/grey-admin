import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, DoCheck,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {Image, ImageType} from '../../_models/image.types';
import {imgProto, imgURL} from '../../_models/urls';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import {ImagesService} from '../images.service';

@Component({
  selector: 'app-image-card',
  templateUrl: './image-card.component.html',
  styleUrls: ['./image-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageCardComponent implements OnInit, OnDestroy, OnChanges {
  @Input() image: Image = null;  //  картинка для отображения
  @Input() imageTypeID: number = null;  //  идентификатор типа картинки
  // public current: Brand = null;
  // public currentLangTab = 'EN';
  public imageType: ImageType = null;
  public imagePath: string = null;
  // public imagePath: string = imgProto + imgURL + '/brands/';
  public imageNone: string = imgProto + imgURL + 'default/none.jpg';

  //  private
  private imageTypesSub: Subscription;
  // ивент для переданного для поиска тега
  @Output() public changeImageEvent: EventEmitter<Image> = new EventEmitter();

  //  constructor
  constructor(private imagesService: ImagesService, private dataService: DataService, private changeDetectorRef: ChangeDetectorRef) {
  }

  //  change detector
  ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line:forin
    if (changes.image && changes.image.currentValue) {
      // console.log('changes.image: ', changes.image.currentValue);
      // console.log('changes.image.previousValue: ', changes.image.previousValue);
      // console.log('this.imageType: ', this.imageType);
      // this.changeDetectorRef.detectChanges();
      this.getPath(this.image);
    }
  }
  //
  // ngDoCheck() {
  // }

  //  destroy
  ngOnDestroy() {
    //  отписываемся
  this.imageTypesSub.unsubscribe();
  // this.imagePathSub.unsubscribe();
  }

  //  initor
  ngOnInit() {
    // console.log('image:', this.image);
    //  subs
    this.imageTypesSub = this.dataService.castImageTypes.subscribe((imageTypes: ImageType[]) => {
      if (imageTypes) {
        this.imageType = imageTypes.find(imgType => imgType.id === this.imageTypeID);
        // console.log('this.image: ', this.image);
        this.getPath(this.image);
      }
    });
  }

  //  возвращаем путь по типу
  public getPath(changedPath?: Image) {
    //  проверяем, что передали
    // console.log('getPath: ', this.image );
    // console.log('changedPath: ', changedPath);
    //  если есть что показывать
    if (this.imageType && this.imageType.path) {
      //  проверяем, передали ли путь, если да - подставляем его, нет берем тот, что прилетел сверху
      if (changedPath) {
        this.imagePath = imgProto + imgURL + changedPath.type.path + '/' + changedPath.url + '.jpg';
      } else {
        //  тест на а есть ли вообще картинка
        // if (this.image && this.image.url) {
        //   this.imagePath = imgProto + imgURL + this.imageType.path + '/' + this.image.url + '.jpg';
        // }
        this.imagePath = this.imageNone;
      }
    } else {
        this.imagePath = this.imageNone;
    }
  }



  //  браузер подгрузил файл
  //  target -  указанного инпута
  public filePreload(target: any, imageType: number): void {
    //  проверяем не передали ли пустой файл (при отмене после диалогового окна)
    if (target && target.files && target.files.length > 0) {
      //  помечаем файл в запись
      // this.selectedTree.isLoadingFile = true;
      //  сохраняем карточку перед изменениями
      // this.updateNode();
      // console.log('filePreload: ', imageType);
      //  дергаем сервис аплоада
      this.imagesService.uploadImage(target.files, imageType)
        .subscribe((image: Image) => {
          // эмитим полученное изображение
          this.changeImageEvent.emit(image);
          // console.log('return in image card component');
          this.getPath(image);
          // this.imageName.next(image.name);
        }, err => {
          console.log('Error in preload', err);
        });
    }
    //  изменяем таргет (чтобы отрабатывал повторный выбор файла)
    target.value  = '';
  }
}
