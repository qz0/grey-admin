import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OptionsService} from './options.service';
import {ListOptionsComponent} from './list-options/list-options.component';
import {OptionDetailsComponent} from './option-details/option-details.component';

//  роутинг
export const optionsRoutes: Routes = [
  //  блок отдельная опция
  { path: 'option', component: OptionDetailsComponent },
  //  блок отдельная опция
  { path: 'option/:id', component: OptionDetailsComponent },
  //  общая
  { path: '', component: ListOptionsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(optionsRoutes)],
  exports: [RouterModule],
  providers: [
    // OptionsService
  ]
})
export class OptionsRoutingModule { }
