import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OptionsRoutingModule } from './options-routing.module';
import { ListOptionsComponent } from './list-options/list-options.component';
import { OptionDetailsComponent } from './option-details/option-details.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {PipesModule} from '../pipes/pipes.module';
import { OptionValueCardComponent } from './option-value-card/option-value-card.component';
import {FormsModule} from '@angular/forms';
import {NgxBootstrapSliderModule} from 'ngx-bootstrap-slider';
import {ImagesModule} from '../images/images.module';

@NgModule({
  declarations: [
    ListOptionsComponent,
    OptionDetailsComponent,
    OptionValueCardComponent
  ],
  imports: [
    CommonModule,
    OptionsRoutingModule,
    FontAwesomeModule,
    PipesModule,
    FormsModule,
    NgxBootstrapSliderModule,
    ImagesModule
  ]
})
export class OptionsModule {
}
