import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionValueCardComponent } from './option-value-card.component';

describe('OptionValueCardComponent', () => {
  let component: OptionValueCardComponent;
  let fixture: ComponentFixture<OptionValueCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionValueCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionValueCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
