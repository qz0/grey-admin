import {Component, Input, OnInit} from '@angular/core';
import {Option, OptionType, OptionValue} from '../../_models/options.types';
import {imgProto, imgURL} from '../../_models/urls';
import {Image} from '../../_models/image.types';

@Component({
  selector: 'app-option-value-card',
  templateUrl: './option-value-card.component.html',
  styleUrls: ['./option-value-card.component.scss']
})
export class OptionValueCardComponent implements OnInit {
  //  public
  public currentLangTab = 'EN'; //  текущий язык
  public imagePath: string = imgProto + imgURL + '/';
  @Input() public optionValue: OptionValue = null;  //  текущая опция
  @Input() public optionType: OptionType = null;  //  текущий тип опции

  //  private


  constructor() { }

  ngOnInit() {
    console.log('optionValue:', this.optionValue);
  }

  //  изменить картинку
  public changeImage(eventImage: Image) {
    this.optionValue.image = eventImage;
  }

  //  создаем новое значение опции
  public createOptionValue() {

  }

  //  удаляем значение опции
  public deleteOptionValue() {

  }

  //  инвертирование видимости
  public invertEnabledOptionValue() {
    //  инвертируем видимость
    this.optionValue.enabled = !this.optionValue.enabled;
  }

  //  изменяем существующее значение опции
  public updateOptionValue() {

  }

  //  переключение табов языка
  public selectLangTab(lang: string) {
    this.currentLangTab = lang;
  }

  //  изменяем видимость
  public enableOptionValue() {}

  //  меняем приоритет у опции
  public changePriority(priority: string) {
    const numPriority = Number(priority);
    //  если передали корректный параметр - меняем значение
    if (numPriority > 0 && numPriority <= 10) {
      this.optionValue.priority = numPriority;
    }
    console.log(priority);
  }

}
