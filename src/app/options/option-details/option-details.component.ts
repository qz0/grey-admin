import {Component, OnDestroy, OnInit} from '@angular/core';

import {OptionsService} from '../options.service';
import {DataService} from '../../_services/data.service';
import {Option, OptionType, OptionValue} from '../../_models/options.types';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {GenerateService} from '../../_services/generate.service';

@Component({
  selector: 'app-option-details',
  templateUrl: './option-details.component.html',
  styleUrls: ['./option-details.component.scss']
})

//  карточка подробностей
export class OptionDetailsComponent implements OnInit, OnDestroy {
  //  public
  public options: Option[] = null; //  опции
  public optionTypes: OptionType[] = null; //  опции
  public option: Option = null; //  опция
  public currentLangTab = 'EN'; //  текущий язык

  //  private
  //  subscription
  private optionsSubscription: Subscription;
  private optionSubscription: Subscription;
  private optionTypesSubscription: Subscription;

  //  конструктор
  constructor(private optionsService: OptionsService, private dataService: DataService, private activatedRoute: ActivatedRoute) {
    //  подписка на опции
    this. optionSubscription = this.dataService.castOption.subscribe((option: Option) => {
      console.log(option);
      this.option = option;
    });
    this. optionsSubscription = this.dataService.castOptions.subscribe((options: Option[]) => this.options = options);
    this. optionTypesSubscription = this.dataService.castOptionTypes
      .subscribe((optionTypes: OptionType[]) => this.optionTypes = optionTypes);
  }

  //  дестройер
  public ngOnDestroy() {
    //  отписываемся от подписок
    this. optionSubscription.unsubscribe();
    this. optionsSubscription.unsubscribe();
    this. optionTypesSubscription.unsubscribe();
  }
  //  инитор
  public ngOnInit() {
    //  получаем brand
    const optionID = this.activatedRoute.snapshot.paramMap.get('id');
    //  получаем типы опций
    this.optionsService.getAllOptionTypes();
    //  получаем опции
    this.optionsService.getAllOptions();
    //  если передали id
    if (optionID) {
      this.optionsService.getOptionByID(optionID);
    } else {
      this.optionsService.getOptionByID(null);
    }
  }

  //  меняем приоритет у опции
  public changePriority(priority: string) {
    const numPriority = Number(priority);
    //  если передали корректный параметр - меняем значение
    if (numPriority > 0 && numPriority <= 10) {
      this.option.priority = numPriority;
    }
    console.log(priority);
  }

  //  поменяли тип
  public changeType(type: number) {
    console.log(type);
    this.option.type = this.optionTypes.find(optionType => optionType.id === type);
    console.log(this.optionTypes, this.option);
  }

  //  добавить новое значение опции
  public createOptionValue() {
    //  если опция существует
    if (this.option) {
      //  если массив вэлью существует - пушим в него
      if (this.option.optionValues) {
        this.option.optionValues.push(GenerateService.createOptionValue());
      } else {
        //  если нет - создаем с нуля
        this.option.optionValues = [GenerateService.createOptionValue()];
      }
    }

  }

  //  переключение табов языка
  public selectLangTab(lang: string) {
    this.currentLangTab = lang;
  }

  //  изменить опцию
  public updateOption() {
    //  дергаем соответствующий сервис
    this.optionsService.updateOption(this.option);
  }
}
