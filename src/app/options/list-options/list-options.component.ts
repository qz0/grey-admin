import { Component, OnDestroy, OnInit } from '@angular/core';

import { OptionsService } from '../options.service';
import { Option } from '../../_models/options.types';
import {DataService} from '../../_services/data.service';
import {Subscription} from 'rxjs';
import {Delivery} from '../../_models/deliveries.types';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-options',
  templateUrl: './list-options.component.html',
  styleUrls: ['./list-options.component.scss']
})

//  список опций
export class ListOptionsComponent implements OnInit, OnDestroy {
  //  public
  public options: Option[] = null; //  опции

  //  private
  //  subscription
  private optionsSub: Subscription;

  //  конструктор
  constructor(private optionsService: OptionsService, private dataService: DataService, private router: Router) {
    //  подписка на опции
    this. optionsSub = this.dataService.castOptions.subscribe((options: Option[]) => {
      this.options = options;
      console.log('OPTIONS: ', options);
    });
  }

  //  дестройер
  public ngOnDestroy() {
    //  отписываемся от подписок
    this. optionsSub.unsubscribe();
  }
  //  инитор
  public ngOnInit() {
  //  получаем список опций
    this.dataService.getOptions();
    //  получаем список типов опций
    this.dataService.getOptionTypes();
  }

  //  change Option
  public changeOption(option: Option) {
    console.log(option);
    //  если доставка не пустая
    if (option && option.id) {
      this.router.navigate(['options/option', option.id]);
    } else {
      this.router.navigate(['options/option']);
    }
  }
}
