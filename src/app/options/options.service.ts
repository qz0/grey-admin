import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

import { DataService } from '../_services/data.service';
import { Option } from '../_models/options.types';

@Injectable({
  providedIn: 'root'
})

//  сервис работы с опциями
export class OptionsService {
  //  public
  //  casts
  //  каст brand
  public castOptions: Observable<Option[]>;

  //  private
  //  bs
  //  bs для brands
  private options: BehaviorSubject<Option[]>  = new BehaviorSubject<Option[]>(null);

  //  конструктор
  constructor(private dataService: DataService) {
    // привязки кастов и bs
    //  привязываем каст и БС для бита изменяемости карточки brand
    this.castOptions = this.options.asObservable();
  }
  //  option - значение опции для создания
  public createOption(option: Option) {
    //  создаем новую опцию
    this.dataService.updateOption(option);
  }

  //  изменение опции
  //  option - опция для удаления
  public updateOption(option: Option) {
    console.log('updateOption - service: ', option);
    //  новая или уже существующая?
    if (option && option.id) {
      //  дергаем апдейт
      this.dataService.updateOption(option);
    } else {
      //  создаем новую опцию
      this.dataService.createOption(option);
    }
  }

  //  удаление опции
  //  option - опция для удаления
  public deleteOption(option: Option) {
    //  дергаем свежую копию options
    this.dataService.deleteOption(option);
  }

  //    получение списка всех опций
  public getAllOptions() {
    //  дергаем свежую копию options
    this.dataService.getOptions();
  }

  //    получение списка всех типов опций
  public getAllOptionTypes() {
    //  дергаем свежую копию options
    this.dataService.getOptionTypes();
  }

  //    получение списка всех опций
  //  optionID - идентификатор опции
  public getOptionByID(optionID: string) {
    //  дергаем свежую копию options
    this.dataService.getOptionByID(optionID);
  }
}
