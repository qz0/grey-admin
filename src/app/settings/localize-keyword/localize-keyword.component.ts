import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocalizedKeyword } from '../../_models/localization';
import { DataService } from '../../_services/data.service';
import { SettingsService } from '../settings.service';
import { Subscription } from 'rxjs';
import { kStringMaxLength } from 'buffer';

@Component({
  selector: 'app-localize-keyword',
  templateUrl: './localize-keyword.component.html',
  styleUrls: ['./localize-keyword.component.scss']
})
export class LocalizeKeywordComponent implements OnInit {
  //  public
  public localizedKeywords: LocalizedKeyword[] = []; //  список валюты
  public createdMode: boolean = false;  // режим  создания localizedKeywords
  public currentEdit: number = null;  //  номер редактируемой ячейки

  // private
  private localizedKeywordsSub: Subscription;

  constructor(private dataService: DataService, private settingsService: SettingsService) { }

  //  Destroy
  public ngOnDestroy() {
    //  отписка от localizedKeywords подписок
    this.localizedKeywordsSub.unsubscribe();
  }

  // Инитор
  public ngOnInit() {
    //  подписка localizedKeywords
    this.localizedKeywordsSub = this.dataService.castLocalizedKeywords.subscribe(
      (localizedKeywords: LocalizedKeyword[]) => (this.localizedKeywords = localizedKeywords)
    );

    // получение localizedKeywords
    this.settingsService.getLocalizedKeywords();
  }

  //  добавление  нового поля  ввода  для LocalizedKeyword
  public addLocalizedKeywordInput() {
    this.createdMode = true;
    this.localizedKeywords.push({
      ID: null,            //id в бд
      keyword: null,       //ключевое слово для замены значением на сайте
      ru: null,            //русская версия для замены
      en: null,            //английская версия для замены
      description: null,   //описание. Где используется ключевое слово итд
    });
    this.currentEdit = this.localizedKeywords.length - 1;
  }

  // отмена изменений LocalizedKeyword
  public cancelLocalizedKeyword() {
    this.createdMode = false;
    this.currentEdit = null;
    this.settingsService.getLocalizedKeywords();
  }

  //  создание LocalizedKeyword
  public createLocalizedKeyword(localizedKeyword: LocalizedKeyword) {
    // Тестовая валидация
    if (
      localizedKeyword.keyword == null ||
      localizedKeyword.ru == null ||
      localizedKeyword.en == null ||
      localizedKeyword.description == null
    ) {
      return this.cancelLocalizedKeyword();
    }
    this.createdMode = false;
    this.currentEdit = null;
    this.settingsService.createLocalizedKeyword(localizedKeyword);
  }

  // удаление LocalizedKeyword
  public deleteLocalizedKeyword(localizedKeyword: LocalizedKeyword) {
    if (this.localizedKeywords.length > 0) {
      this.settingsService.deleteLocalizedKeyword(localizedKeyword);
      this.createdMode = false;
      this.currentEdit = null;
    }
  }

  // изменение LocalizedKeyword
  public editLocalizedKeyword(index: number) {
    this.createdMode = false;
    this.currentEdit = index;

  }

  // сохранение LocalizedKeyword
  public updateLocalizedKeyword(localizedKeyword) {
    this.settingsService.updateLocalizedKeyword(localizedKeyword);
    this.currentEdit = null;
  }
}
