import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from '../../_services/data.service';
import {SettingsService} from '../settings.service';
import {Subscription} from 'rxjs';
import {Country, Region} from '../../_models/address.types';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit, OnDestroy {
  // public
  public countries: Country[] = null; //  список  стран
  public regions: Region[] = null; //  список  регионов
  public createdMode: boolean = false;  // режим  создания
  public currentCountryEdit: number = null;  //  номер редактируемой ячейки - страна
  public currentRegionEdit: number = null;  //  номер редактируемой ячейки - регион
  public selectedTable: string = null;  //  редактируемая таблица - страна/регион

  // private
  private countriesSub: Subscription;
  private regionsSub: Subscription;

  constructor(private dataService: DataService, private settingsService: SettingsService) {
    //  подписка countries
    this.countriesSub = this.dataService.castCountries
      .subscribe((countries: Country[]) => this.countries = countries);
    //  подписка regions
    this.regionsSub = this.dataService.castRegions
      .subscribe((regions: Region[]) => this.regions = regions);
  }

  // destroy
  ngOnDestroy() {
    //  отписка countriesSub
    this.countriesSub.unsubscribe();
    //  отписка regionsSub
    this.regionsSub.unsubscribe();
  }

  //  init
  ngOnInit() {
    //  get all countries
    this.settingsService.getCountries();
    //  get all regions
    this.settingsService.getRegions();
  }

  //  добавить пустую строчку для ввода
  public addInput(selectedTable: string) {
    this.selectedTable = selectedTable;
    if (selectedTable === 'country') {
      this.createdMode = true;
      this.countries.unshift({
        countryID: null,
        id: null,
        name: null,
        nameRu: null,
        code2: null,
        code3: null,
        regions: null,
        localizedDescriptions: null,
        status: null
      });
      this.currentCountryEdit = 0;
    } else {
      this.createdMode = true;
      this.regions.unshift({
        id: null,
        name: null,
        country: null,
        code: null
      });
      this.currentRegionEdit = 0;
    }
  }

  //  delete country or region
  public delete() {
    if (this.selectedTable === 'country') {
      console.log('delete country');
    } else {
      console.log('delete region');
    }
  }

  //  отменить редактирование country or region
  public cancel() {
    if (this.selectedTable === 'country') {
      this.createdMode = false;
      this.currentCountryEdit = null;
      this.settingsService.getCountries();
    } else {
      this.createdMode = false;
      this.currentRegionEdit = null;
      this.settingsService.getRegions();
    }
  }

  // create country or region
  public create(index: number) {
    if (this.selectedTable === 'country') {
      this.settingsService.createCountry(this.countries[index]);
      this.createdMode = false;
      this.currentCountryEdit = null;
    } else {
      this.settingsService.createRegion(this.regions[index]);
      this.createdMode = false;
      this.currentRegionEdit = null;
    }
  }

  // редактирование страны или региона
  public edit(index: number, selectedTable: string) {
    this.selectedTable = selectedTable;
    if (this.selectedTable === 'country') {
      this.currentRegionEdit = null;
      this.currentCountryEdit = index;
    } else {
      this.currentCountryEdit = null;
      this.currentRegionEdit = index;
    }
    this.createdMode = false;
    console.log('index: ', index, ' currentCountryEdit: ', this.currentCountryEdit);
  }

  // изменение страны или региона
  public update(index: number) {
    if (this.selectedTable === 'country') {
      this.settingsService.updateCountry(this.countries[index]);
      this.currentCountryEdit = null;
    } else {
      console.log(this.selectedTable);
      this.dataService.updateRegion(this.regions[index]);
      this.currentRegionEdit = null;
    }
  }

}
