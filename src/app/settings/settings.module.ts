import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CountriesComponent} from './countries/countries.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';
import {SettingsComponent} from './settings/settings.component';
import {ProductsModule} from '../products/products.module';
import {ListUsersComponent} from '../users/list-users/list-users.component';
import {CurrenciesComponent} from './currencies/currencies.component';
import { LocalizedComponent } from './localized/localized.component';
import {LocalizeKeywordComponent} from './localize-keyword/localize-keyword.component'


@NgModule({
  declarations: [CountriesComponent, SettingsComponent, ListUsersComponent, CurrenciesComponent, LocalizedComponent,LocalizeKeywordComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ProductsModule,
  ]
})
export class SettingsModule {
}
