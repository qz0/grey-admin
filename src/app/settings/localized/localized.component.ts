import { Component, OnInit, OnDestroy } from '@angular/core';
import { Language } from '../../_models/localization';
import { DataService } from '../../_services/data.service';
import { SettingsService } from '../settings.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-localized',
  templateUrl: './localized.component.html',
  styleUrls: ['./localized.component.scss']
})
export class LocalizedComponent implements OnInit {
  //  public
  public languages: Language[] = []; //  список валюты
  public createdMode: boolean = false;  // режим  создания languages
  public currentEdit: number = null;  //  номер редактируемой ячейки

  // private
  private languagesSub: Subscription;

  constructor(private dataService: DataService, private settingsService: SettingsService) {}

  //  Destroy
  public ngOnDestroy() {
    //  отписка language
    this.languagesSub.unsubscribe();
  }

  // Инитор
  public ngOnInit() {
    //  подписка languages
    this.languagesSub = this.dataService.castLanguages.subscribe(
      (languages: Language[]) => (this.languages = languages)
    );

    // получение languages
    this.settingsService.getLanguages();
  }

  //  добавление  нового поля  ввода  для languages
  public addLanguageInput() {
    this.createdMode = true;
    this.languages.push({
      id: null,    //id в бд
      language: null,  // язык код
      title: null,  //полное название языка
    });
    this.currentEdit = this.languages.length - 1;
  }

  // отмена изменений Language
  public cancelLanguage() {
    this.createdMode = false;
    this.currentEdit = null;
    this.settingsService.getLanguages();
  }

  //  создание Language
  public createLanguage(language: Language) {
    // Тестовая валидация
    if (
      language.language == null ||
      language.title == null
    ) {
      return this.cancelLanguage();
    }
    this.createdMode = false;
    this.currentEdit = null;
    this.settingsService.createLanguage(language);
  }

  // удаление Language
  public deleteLanguage(language: Language) {
    // if (this.languages.length > 3) {
      if (language.language!="EN" && language.language!="RU"){
      this.settingsService.deleteLanguage(language);
     console.log("i`m try")
      this.createdMode = false;
      this.currentEdit = null;
    }
    // }
  }

  // изменение Language
  public editLanguage(index: number) {
    this.createdMode = false;
    this.currentEdit = index;
    console.log('index: ', index, ' currentEdit: ', this.currentEdit);
  }

  // сохранение Language
  public updateLanguage(language: Language) {
    console.log('Language is ',language);
    this.settingsService.updateLanguage(language);
    this.currentEdit = null;
  }
}
