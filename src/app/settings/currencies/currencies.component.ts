import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Currency } from '../../_models/currencies.types';
import { DataService } from '../../_services/data.service';
import {SettingsService} from '../settings.service';

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.scss']
})
export class CurrenciesComponent implements OnInit, OnDestroy {
  //  public
  public currencies: Currency[] = []; //  список валюты
  public createdMode: boolean = false;  // режим  создания currency
  public currentEdit: number = null;  //  номер редактируемой ячейки

  // private
  private currenciesSub: Subscription;

  constructor(private dataService: DataService, private settingsService: SettingsService) {}

  //  Destroy
  public ngOnDestroy() {
    //  отписка currencies
    this.currenciesSub.unsubscribe();
  }

  // Инитор
  public ngOnInit() {
    //  подписка currencies
    this.currenciesSub = this.dataService.castCurrencies.subscribe(
      (currencies: Currency[]) => (this.currencies = currencies)
    );
    // получение currencies
    this.settingsService.getCurrencies();
  }

  //  добавление  нового поля  ввода  для currency
  public addCurrencyInput() {
    this.createdMode = true;
    this.currencies.push({
      id: null,
      code: null,
      title: null,
      multiplier: null,
      symbol: null,
      before: null
    });
    this.currentEdit = this.currencies.length - 1;
  }

  // отмена изменений currency
  public cancelCurrency() {
    this.createdMode = false;
    this.currentEdit = null;
    this.settingsService.getCurrencies();
  }

  //  создание currency
  public createCurrency(currency: Currency) {
    // Тестовая валидация
    if (
      currency.code == null ||
      currency.title == null ||
      currency.multiplier == null ||
      currency.symbol == null ||
      currency.before == null
    ) {
      return this.cancelCurrency();
    }
    this.createdMode = false;
    this.currentEdit = null;
    this.settingsService.createCurrency(currency);
  }

  // удаление currency
  public deleteCurrency(currency: Currency) {
    this.settingsService.deleteCurrency(currency);
    this.createdMode = false;
    this.currentEdit = null;
  }

  // изменение currency
  public editCurrency(index: number) {
    this.createdMode = false;
    this.currentEdit = index;
    console.log('index: ', index, ' currentEdit: ', this.currentEdit);
  }

  // сохранение currency
  public updateCurrency(currency: Currency) {
    console.log('currency is ', currency);
    this.settingsService.updateCurrency(currency);
    this.currentEdit = null;
  }
}
