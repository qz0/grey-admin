import { Injectable } from '@angular/core';

import { DataService } from '../_services/data.service';
import { Currency } from '../_models/currencies.types';
import { Language, LocalizedKeyword } from '../_models/localization';
import { Country, Region } from '../_models/address.types';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private dataService: DataService) { }


  /******* LANGUAGE *******/
  // получить все языки
  public getLanguages() {
    this.dataService.getLanguages();
  }


  public createLanguage(newLanguage: Language) {
    //  проверка, что передали непустоту
    if (newLanguage) {
      this.dataService.createLanguage(newLanguage);
    }
  }

  //  удаление Language
  public deleteLanguage(killedLanguage: Language) {
    //  проверка, что передали непустоту
    if (killedLanguage) {
      this.dataService.deleteLanguage(killedLanguage);
    }
  }


  //  изменение Language
  public updateLanguage(changedLanguage: Language) {
    //  проверка, что передали непустоту
    if (changedLanguage) {
      this.dataService.updateLanguage(changedLanguage);
    }
  }


    /******* LocalizedKeywords *******/
  // получить все языки
  public getLocalizedKeywords() {
    this.dataService.getLocalizedKeywords();
  }


  public createLocalizedKeyword(newLocalizedKeyword: LocalizedKeyword) {
    //  проверка, что передали непустоту
    if (newLocalizedKeyword) {
      this.dataService.createLocalizedKeyword(newLocalizedKeyword);
    }
  }

  //  удаление LocalizedKeywords
  public deleteLocalizedKeyword(killedLocalizedKeyword: LocalizedKeyword) {
    //  проверка, что передали непустоту
    if (killedLocalizedKeyword) {
      this.dataService.deleteLocalizedKeyword(killedLocalizedKeyword);
    }
  }


  //  изменение LocalizedKeyword
  public updateLocalizedKeyword(changedLocalizedKeyword: LocalizedKeyword) {
    //  проверка, что передали непустоту
    if (changedLocalizedKeyword) {
      this.dataService.updateLocalizedKeyword(changedLocalizedKeyword);
    }
  }



  /******* COUNTRIES *******/
  //  получение всех стран
  public getCountries() {
    this.dataService.getCountries();
  }

  //  изменение страны
  public updateCountry(country: Country) {
    //  проверка, что передали непустоту
    if (country) {
      this.dataService.updateCountry(country);
    }
  }

  //  создать новую страну
  public createCountry(country: Country) {
    //  проверка, что передали непустоту
    if (country) {
      this.dataService.createCountry(country);
    }
  }

  /******* CURRENCIES *******/
  //  создание currency
  public createCurrency(newCurrency: Currency) {
    //  проверка, что передали непустоту
    if (newCurrency) {
      this.dataService.createCurrency(newCurrency);
    }
  }

  //  удаление currency
  public deleteCurrency(killedCurrency: Currency) {
    //  проверка, что передали непустоту
    if (killedCurrency) {
      this.dataService.deleteCurrency(killedCurrency);
    }
  }

  //  получение всех currencies
  public getCurrencies() {
    this.dataService.getCurrencies();
  }

  //  изменение currency
  public updateCurrency(changedCurrency: Currency) {
    //  проверка, что передали непустоту
    if (changedCurrency) {
      this.dataService.updateCurrency(changedCurrency);
    }
  }

  /******* Regions *******/
  //  создать новый регион
  public createRegion(region: Region) {
    //  проверка, что передали непустоту
    if (region) {
      this.dataService.createRegion(region);
    }
  }

  //  получение всех регионов
  public getRegions() {
    this.dataService.getRegions();
  }

  //  изменение региона
  public updateRegion(region: Region) {
    //  проверка, что передали непустоту
    if (region) {
      this.dataService.updateRegion(region);
    }
  }

  /******* USERS *******/

}
