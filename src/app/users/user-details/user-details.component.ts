import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { Role, User } from '../../_models/users.types';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertifyService } from '../../_services/alertify.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
//  вывод карточки конкретного пользователя
export class UserDetailsComponent implements OnInit {
  // //  входящие данные пользователя
  // @Input() userDetails: User;
  // //  public
  // //  форма пользователя и формконтрол для ролей
  // public userForm: FormGroup;
  // public selected = new FormControl();
  // //  хардкод данных
  // //  возможные роли пользователей
  // public userRoles: Role[] = [
  //   {
  //     id: 21,
  //     name: 'Role1',
  //     title: 'role1',
  //     description: 'role1 description'
  //   },
  //   {
  //     id: 22,
  //     name: 'Role2',
  //     title: 'role2',
  //     description: 'role2 description'
  //   },
  //   {
  //     id: 23,
  //     name: 'Role3',
  //     title: 'role3',
  //     description: 'role3 description'
  //   }
  // ];
  //
  // //  роли пользователя отдельно пока
  // public selectedRoles: Role[];

  //  constructor
  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private alertify: AlertifyService) {
  }
  //
  // //  создание массива ролей пользователя в форме
  // public buildUserRolesArray() {
  //   this.selectedRoles = this.selected.value.map(id => {
  //     return this.userRoles.find(role => role.id === id);
  //   });
  // }
  //
  // //  удаление пользователя
  // //  id - Ид пользователя
  // public deleteUser(id: number) {
  //   //  дергаем сервис
  //   this.authService.deleteUser(id);
  // }
  //
  public ngOnInit() {
    // this.userForm = this.formBuilder.group({
    //   username: ['', Validators.required],
    //   title: '',
    //   description: '',
    //   password: ['', Validators.required],
    // });
    // //  патчив юзера отдельно, роли отдельно
    // this.selectedRoles = this.userDetails.roles.map( role => role);
    // this.userForm.patchValue(this.userDetails);
    // this.selected.patchValue(this.userDetails.roles.map(it => it.id));
  }
  //
  // //  изменение пользователя
  // //  id - Ид пользователя
  // //  username - логин
  // //  title - приветствие (ФИО?)
  // //  description - описание
  // //  password - пароль
  // public updateUser(id: number, username: string, title: string, description: string, password: string, roles: Role[]) {
  //   //  создаем экземпляр пользователя
  //   const user: User = {
  //     id,  //  id
  //     username,  //  логин
  //     login, //  заголовок
  //     description, //  описание
  //     password,  //  пароль
  //     roles, // список ролей пользователя
  //   };
  //   this.authService.createUser(user);
  //   this.alertify.success('user saved');
  // }
}
