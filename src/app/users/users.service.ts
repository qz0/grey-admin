import { Injectable } from '@angular/core';
import {User} from '../_models/users.types';
import {AuthService} from '../_services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private authService: AuthService) {
  }

  //  добавление нового пользователя
  //  user - экземпляр пользователя
  public createUser(user: User) {
    if (user) {
      this.authService.createUser(user);
    }
  }

  //  удаление пользователя
  //  userID - идентификатор пользователя
  public deleteUser(userID: number) {
    if (userID) {
      //  дергаем сервис
      this.authService.deleteUser(userID);
    }
  }

  //  получение списка пользователей
  public getUsers() {
    this.authService.getUsers();
    // this.authService.putUsers(3);
  }

  //  не трогать
  // public sendAlertify(message: string) {
  //   console.log('sendAlertify');
  //   this.alertify.success(message);
  // }

  //  изменение пользователя
  //  user - идентификатор пользователя
  public updateUser(updateUser: User) {
    //  навигация на детализированную карточку
    if (updateUser) {
      //  новый?
      if (updateUser.id) {
        this.authService.updateUser(updateUser);
      } else {
        this.authService.createUser(updateUser);
      }
    }
  }
}
