import { Component } from '@angular/core';
import {AuthService} from '../../_services/auth.service';
import {Role, User} from '../../_models/users.types';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
//  вывод карточки конкретного пользователя
export class RegisterUserComponent {
  //  public
  public user: User;  //

  //  конструктор
  constructor(private authService: AuthService) {

  }

  //  регистрация нового пользователя
  //  username - логин
  //  title - приветствие (ФИО?)
  //  description - описание
  //  password - пароль
  public register(username: string, title: string, description: string, password: string) {
    //  создаем экземпляр пользователя
    // const user: User = {
    //   username,  //  логин
    //   title, //  заголовок
    //   description, //  описание
    //   password,  //  пароль
    //   roles: [], // список ролей пользователя
    // };
    // this.authService.createUser(user);
  }
}
