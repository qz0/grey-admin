import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListUsersComponent} from './list-users/list-users.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { UserDetailsComponent } from './user-details/user-details.component';

//  работа с пользователями
export const usersRoutes: Routes = [
  //  домашняя
  // { path: '', component: ListUsersComponent },
  //  домашняя
  { path: 'register', component: RegisterUserComponent },
  //  детали пользователя
  { path: 'user_details', component: UserDetailsComponent },
  // { path: 'user_details/:id', component: UserDetailsComponent },
  // //  окно правки пользователя
  // { path: 'update_user/:id', component: ListUsersComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(usersRoutes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
