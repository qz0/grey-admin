import {Component, OnDestroy, OnInit} from '@angular/core';
import { AlertifyService } from '../../_services/alertify.service';
import { User, UserServer } from '../../_models/users.types';
import { AuthService } from '../../_services/auth.service';
import {Subscription} from 'rxjs';
import {GenerateService} from '../../_services/generate.service';
import {UsersService} from '../users.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss'],
})
//  вывод списка всех пользователей
export class ListUsersComponent implements OnInit, OnDestroy {
  //  public
  public users: User[] = null;  //  пользователи

  public user: User = null; //  текущий пользователь

  public createMode: boolean = false; //  режим создания  user

  public userServer: UserServer = null;

  public updatingUserIndex: number = null;  //  строка которая в режиме запи


  //  private
  private usersSub: Subscription;

  //   конструктор
  constructor(private alertify: AlertifyService,
              private usersService: UsersService,
              private authService: AuthService
  ) {}

  //  destroy
  public ngOnDestroy() {
    //  отписка usersSub
    this.usersSub.unsubscribe();
  }

  //  инитор
  public ngOnInit() {
    //  подписка  users
    this.usersSub = this.authService.castUsers.subscribe((users: User[]) => this.users = users);

    //  получение users
    this.usersService.getUsers();
  }

  //  добавление  нового поля  ввода  для  user
  public addUserInput() {
    this.createMode = true;
    this.users.push({
      username: null,
      login: null,
      password: null,
      email: null,
      description: null,
      image: null,
      roles: null
    });
    // this.users.push(GenerateService.createUser());
    this.updatingUserIndex = this.users.length - 1;
  }

  //  отмена  добавления или изменения user
  public cancelUser() {
    this.refresh();
    this.usersService.getUsers();
  }

  //  созданеи  user
  public createUser(index: number) {
    if (index != null) {
      this.refresh();
      this.usersService.createUser(this.users[index]);
    }
  }

  //  удаление  пользователя
  public deleteUser(index: number) {
    if (index != null) {
      //  дергаем сервис
      this.usersService.deleteUser(this.users[index].id);
      this.updatingUserIndex = null;
    }
  }

  //  изменение user
  public updateUser(index: number) {
    if (index) {
      console.log('user is ', this.users[index]);
      this.usersService.updateUser(this.users[index]);
      this.updatingUserIndex = null;
    }
  }

  //  обновление  updateUserIndex
  public updateUserIndex(updatingUserIndex: number) {
    if (updatingUserIndex != null) {
      console.log(this.users[updatingUserIndex]);
      this.updatingUserIndex = updatingUserIndex;
    }
  }

  //  сброс createMode  и updatingUserIndex
  private refresh() {
    this.createMode = false;
    this.updatingUserIndex = null;
  }

  //  не трогать
  // public sendAlertify(message: string) {
  //   console.log('sendAlertify');
  //   this.alertify.success(message);
  // }
}
